<?php

class MiIterador implements Iterator
{
    private $var = array();
	private $pos_temp, $spd_temp;
    
    public function __construct($array)
    {
        if (is_array($array)) {
            $this->var = $array;
			$this->rewind();
        }
    }

    public function rewind()
    {
        //echo "rebobinando\n";
        reset($this->var);
    }

    public function current()
    {
        $var = current($this->var);
        return $var;
    }

    public function key()
    {
        $var = key($this->var);
        return $var;
    }
		
    public function prev()
    {
        $var = prev($this->var);
        return $var;
    }
	
	public function update_row($numrows, $option, $id)
    {
		$key = $this->key();
		$pos = NULL;
		$i = 0;
		
		if( $numrows > 0 )
		{
			while($pos != $key){
				if( $i++ < $numrows )
				{
					$this->prev();
					$this->update_prev_array($option, $id);
				}else{
					$this->next();				
				}
				$pos = $this->key();
			}
		}
    }
	
    public function next()
    {
        $var = next($this->var);
        return $var;
    }

    public function valid()
    {
        $clave = key($this->var);
        $var = ($clave !== NULL && $clave !== FALSE);
        //echo "válido: $var\n";
        return $var;
    }
	
    public function size()
    {
        $var = count($this->var);
        //echo "size: $var\n";
        return $var;
    }
    
	public function update_self_array($current)
    {
		$clave = key($this->var);
		$this->var[$clave] = $current;
        return true;
  	}
	
	public function update_prev_array($key, $val)
    {
		$clave = key($this->var);
		$this->var[$clave][$key] = $val;
		if($key = "move"){
			unset($this->var[$clave]["stop"]);
		}
		return true;
  	}
		
	public function get_array()
    {
        return $this->var;
   }
   
	public function current_size()
    {
        $var = count(current($this->var));
        //echo "current_size: $var\n";
        return $var;
    }
	
	public function reset_all_vars( &$var,  $pos = false ) {
	
		$sp = array( 'i_speed', 'time_speed');
		$st = array( 'i_stop' , 'time_stop' );
		$all = array( 'id_stop', 'id_move', 'id_speed' );
		//eval("print_r(\$$pos);");		
		
		switch ($pos) {
			case 'sp1':
				$vars = array();
				$this->spd_temp = TRUE;
				break;
			case 'sp':
				$vars = $sp;
				if( $this->spd_temp && $this->spd_temp != NULL ) {
					$var["id_speed"]++;
					$this->spd_temp = FALSE;
				}
				break;
			case 'mv':
				$vars = $st;
				if( $this->pos_temp != $pos && $this->pos_temp != NULL ) {
					//echo "$this->pos_temp != $pos <br>";
					$var["id_stop"]++;
				}
				break;
			case 'st':
				$vars = $sp;
				if( $this->pos_temp != $pos && $this->pos_temp != NULL ) {
					//echo "$this->pos_temp != $pos <br>";
					$var["id_move"]++;
				}
				break;
			default:
				$vars = array_merge($sp, $st, $all);
				break;
		}
		
		if( !in_array($pos,array("sp","sp1") ) ) {
			$this->pos_temp = $pos;
		}
		
		foreach($vars as $name) {
			$var[$name] = in_array($name,$all) ? 1 : 0;  //modificado a 1 en stop y move.
			//$var[$name] = 0;  //modificado a 1 en stop y move.
		}		
	}
}

?>
