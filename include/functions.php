<?php

function logout_session()
{
	global $mysqli, $_COOKIE ;
	if(isset($_COOKIE['sys_serco_id'])){
		$session_id = $_COOKIE['sys_serco_id'];
		setcookie('sys_serco_id', '', (times()+3600), '/', '', 0);
		//$sql = "DELETE FROM sessions WHERE session_id = '$session_id' LIMIT 1" ;
		$sql = "UPDATE `sessions` SET `session_st`= NULL WHERE (`session_id`='$session_id')";
		$mysqli->query($sql);
	}
}
	
function charge_session()
{
	global $mysqli, $_COOKIE ;
	//$client_ip = ( !empty($_SERVER['REMOTE_ADDR']) ) ? $_SERVER['REMOTE_ADDR'] : ( ( !empty($_ENV['REMOTE_ADDR']) ) ? $_ENV['REMOTE_ADDR'] : getenv('REMOTE_ADDR') );
	$client_ip = get_client_ip();
	$user_ip = encode_ip($client_ip);
	$session_id = @$_COOKIE['sys_serco_id'];
	$current_time = times();
	
	if ( !isset($session_id)) {
		return $session = 'login';
	}
	
	$sql = "SELECT 	
			`SE`.`session_time`,
			`US`.`usua_empr_id`,
			`US`.`usua_id`,
			`US`.`usua_nombre`,
			`US`.`usua_apellido`,
			`US`.`usua_tius_id` acU,
			`EP`.`empr_nombre`
				FROM sessions SE
				INNER JOIN usuario US ON `session_id` = '$session_id' AND `session_st`= TRUE # AND `session_ip` = '$user_ip'
				AND SE.session_user_id = US.usua_id 
				INNER JOIN empresa EP ON EP.empr_id = US.usua_empr_id";
			
	$result = $mysqli->query($sql);

	if($result->num_rows == 0){
		//$sql = "UPDATE `sessions` SET `session_st`= NULL WHERE session_id = '$session_id'";
		//$mysqli->query($sql);
		$session['session_time'] = 0;
	}

	$session = $result->fetch_assoc();
	$result->free();
	
	if( ( $current_time - $session['session_time'] ) > 60 )
	{
		$sql = "UPDATE `sessions` SET `session_st` = NULL 
			WHERE session_time < " . ( $current_time - 360 ) . " 
			#AND session_id <> '$session_id'
			AND `session_st` = 1";
		$mysqli->query($sql);

		if(!empty($user_ip))
		{
			$sql = "UPDATE sessions
					SET session_time = $current_time
					WHERE session_id = '$session_id' 
					AND `session_st` = 1
					AND session_ip = '$user_ip'";
			$mysqli->query($sql);
		}
	}
	//print_r($session);
	//die();
	return $session;
}	
	
function encode_ip($dotquad_ip)
{
	$ip_exp = explode('.', $dotquad_ip);
	return sprintf('%02x%02x%02x%02x', @$ip_exp[0], @$ip_exp[1], @$ip_exp[2], @$ip_exp[3]);
}

function decode_ip($int_ip)
{
    $hexipbang = explode('.', chunk_split($int_ip, 2, '.'));
    return hexdec($hexipbang[0]). '.' . hexdec($hexipbang[1]) . '.' . hexdec($hexipbang[2]) . '.' . hexdec($hexipbang[3]);
}

function times()
{
	return ( time() + 0 ); //(3600*4)
}

function sessionid()
{
	list($usec, $sec) = explode(' ', microtime());
	mt_srand((float) $sec + ((float) $usec * 100000));
	$session_id = md5(uniqid(mt_rand(), true));
	return $session_id;
}

function get_client_ip() {
	if(!empty($_SERVER['HTTP_CLIENT_IP'])){
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}else{
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return ( $ip == "::1" ) ? "127.0.0.1" : $ip ;
}

?>