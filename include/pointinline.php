<?php
include_once("db.php");
//$pointLocation = new PointLocation();

class PointLocation {

	private $geoc_rutas = array();	
    public $variable_km =  111.19457696;
	private $range_max;

	function __construct() {
		global $mysqli;
		global $range_max;
		$this->range_max = $range_max; // 0.030;
/*		$sql ="SELECT
				geoc_id, AsText(geoc_poligono) AS poliline
				FROM `geocerca`
				WHERE geoc_tipo = 8 AND geoc_visible = 1";*/
				
		$sql = "SELECT 
				GE.geoc_id,	AsText(if(RU.ruta_line IS NULL, GE.geoc_poligono, RU.ruta_line )) AS poliline
			 FROM `geocerca` AS GE 
				LEFT JOIN `ruta` AS RU ON GE.geoc_id = RU.ruta_geoc_id
				WHERE GE.geoc_tipo IN(8,9) AND GE.geoc_visible = 1";		
				
		
		if ($resultado = $mysqli->query($sql)) {
			while ($fila = $resultado->fetch_assoc())
			{
				$this->geoc_rutas[$fila["geoc_id"]] = $this->clear($fila["poliline"]);			
			}
		}
	}
	
	function Glength_all_point($id_gc = null, $point, $dist = true) {
		$points_ruta = $this->geoc_rutas[$id_gc];
		return $this->min_length_point_gc( $points_ruta, $point, $dist);	
	}

	function min_length_point_gc($points_ruta, $point, $dist) {
		$distance = NULL;
		for ($i=1; $i < count($points_ruta); $i++){
			$vertex1 = $this->pointStringToCoordinates($points_ruta[$i-1]);
			$vertex2 = $this->pointStringToCoordinates($points_ruta[$i]);
			$pointc = $this->pointStringToCoordinates($point);
			
			$point1_lat = $pointc['x'];
			$point1_long = $pointc['y'];
			
			list($point2_lat,$point2_long) = $this->puntorecta($vertex1,$vertex2,$pointc);
	
			if($point2_lat != 0 and $point2_long != 0){				

				$temp_dist = $this->Glength($point1_lat, $point1_long, $point2_lat, $point2_long);

				if($distance > $temp_dist || $distance === NULL )
				{
					$distance = $temp_dist;
					$temp_pos = array($vertex1['y'] ." ". $vertex1['x'], $vertex2['y'] ." ". $vertex2['x'],"$point2_long $point2_lat");
					
				}elseif($temp_dist < $this->range_max){  // MODIFICAR DISTANCIA DEL ALLSEARCH
					//break;	
				}
			}
		}
		return ($dist) ?
		( ( $distance === NULL ) ? NULL : $distance ) :
		$temp_pos;
	}

	function Gall_point($id_gc = null, $point) {	
		$points_ruta = $this->geoc_rutas[$id_gc];
		return $this->points_gc( $points_ruta, $point);	
	}

	function points_gc($points_ruta, $point) {

		$distance = NULL;
		$pointc = $this->pointStringToCoordinates($point);
		$point1_lat = $pointc['x'];
		$point1_long = $pointc['y'];
		$points_dist = array();
		$num_points_ruta = (count($points_ruta)) - 1;
		for ($i=0; $i <= $num_points_ruta; $i++){
			//$vertex1 = $this->pointStringToCoordinates($points_ruta[$i-1]);
			$vertex1 = $this->pointStringToCoordinates($points_ruta[$i]);
			$temp_dist = $this->Glength(  $point1_lat, $point1_long, $vertex1['x'], $vertex1['y']);
			
			//echo "i $i ) $point1_long, $point1_lat,". $vertex1['x'].",". $vertex1['y']. "   => $temp_dist \n";
		
			if( $distance === NULL || $temp_dist < $distance ){
				$distance = $temp_dist;
				$num_i_point = $i;
			}
			$points_dist[$i] = $temp_dist;				
		}

		$prev = ($num_i_point > 1)  ? $points_dist[$num_i_point-1] : NULL;

		/*if( $distance == 0.003322232323 ){
			print_r($points_ruta);
			echo "$num_i_point < $i --  $num_points_ruta";
			print_r($points_dist);
		}*/

		$next = ($num_i_point < $num_points_ruta ) ? $points_dist[$num_i_point+1] : NULL;

		if( ($prev < $next && $prev != NULL )|| $next === NULL ){
			$vertex1 = $this->pointStringToCoordinates( $points_ruta[$num_i_point-1]);
			$vertex2 = $this->pointStringToCoordinates($points_ruta[$num_i_point]);
		}else{
			$vertex1 = $this->pointStringToCoordinates($points_ruta[$num_i_point]);
			$vertex2 = $this->pointStringToCoordinates($points_ruta[$num_i_point+1]);
		}

		list($point2_lat,$point2_long) = $this->puntorecta($vertex1,$vertex2,$pointc);

		if(!empty($point2_lat) && !empty($point2_long)){
			$points_azumit = "$point2_long $point2_lat " . $vertex2["y"] . " " . $vertex2["x"];
			$azumit = $this->azumit($points_azumit);
		}else{
			$points_azumit = $vertex1["y"] . " " . $vertex1["x"] ." " . $vertex2["y"] . " " . $vertex2["x"];
//			$azumit = $points_azumit;
			$azumit = $this->azumit($points_azumit);
		}
		return round($azumit);
	}

	function Glength($lat1, $long1, $lat2, $long2) {
		//$degrees = rad2deg(acos((sin(deg2rad($lat1))*sin(deg2rad($lat2))) + (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lon1-$long2)))));

		$dvalue = (sin(deg2rad($lat1))*sin(deg2rad($lat2))) + (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($long1-$long2)));
		$dvalue = ( ($dvalue + 1) == 2 ) ? 1 : $dvalue;
		$degrees = rad2deg(acos($dvalue));
			
		/*
		$degtorad = 0.01745329;
		$radtodeg = 57.29577951; 
		$dlong = ($long1 - $long2);
		$dvalue = (sin($lat1 * $degtorad) * sin($lat2 * $degtorad)) + (cos($lat1 * $degtorad) * cos($lat2 * $degtorad) * cos($dlong * $degtorad));	
		$degrees = acos($dvalue) * $radtodeg;*/

		$distance = $degrees * $this->variable_km;
		return round( $distance, 3 );
    }
	
	function azumit($points){
		list($xa,$ya,$xb,$yb)= explode(" ",$points);
		$y = rad2deg(atan(($xb-$xa)/($yb-$ya)));
		
		switch (true) {
			case (($xb-$xa)>0 && ($yb-$ya)<0) || (($xb-$xa)<0 && ($yb-$ya)<0):
				return $y+180;
				break;
			case ($xb-$xa) < 0 && ($yb-$ya) > 0:
				return $y+360;
				break;
			default:
				return $y;
			break;
		}
	}
	
	function puntorecta($a,$b,$c){
		$div = ( pow(($b['x'] - $a['x']),2)  + pow(($b['y']-$a['y']),2) );
		if ($div != 0)
		{
			$u =  ( ($c['x']-$a['x']) * ($b['x'] - $a['x'] ) + ( $c['y'] - $a['y']) * ($b['y'] - $a['y'] )) /  $div;
			if(0<$u && $u < 1.0)
			{
				$x = $a['x'] + $u * ($b['x'] - $a['x'] );
				$y = $a['y'] + $u * ($b['y'] - $a['y'] );
				return array($x, $y);
			}else{
				return NULL;
			}		 
		}
	}

	function pointStringToCoordinates($pointString) {
		$coordinates = explode(" ", $pointString);
		return array("x" => $coordinates[1], "y" => $coordinates[0]);
	}
	
	function clear($geometry) {
		$charts = array("POLYGON", "LINESTRING", "(", ")");
		$points = str_replace($charts, "", $geometry);	
		return explode(",", $points);
	}
}
?>