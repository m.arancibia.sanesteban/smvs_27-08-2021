<?php

	include("../include/db.php");
	include("../include/pointinline.php");
	include("../include/read.php");

	class gps_serco{
		public $jsondata = array(), $db, $part = array(), $date_point, $time_start, $time_end; //private
		public $rec_pat, $date_end, $date_start, $sql, $pat_divisiones, $date_travels, $gravedad;
	 
		public function __construct() 
		{
			global $mysqli;
			$this->db = $mysqli;
			$this->date_point = strtotime("2018-12-22 11:00:00");
			$this->time_start = "00:00:00";
			$this->time_end   = "23:59:59";
			$this->gravedad();
		}

		public function format_vars($rec_pat, $start_date, $end_date) 
		{
			$this->rec_pat = $rec_pat;
			$this->date_start = $start_date;
			$this->date_end = $end_date;
			$bool_date_point = ($this->date_point > $this->date_start);
			$this->sql_all($bool_date_point,false);
			return true;
		}

		public function format_vars_class($veh)
		{
			$all_pat = ($veh==1)? true : false;
			$date = date( "Y-m-d", strtotime( " - 1 days"  ) );
			$this->date_start = strtotime("$date $this->time_start");
			$this->date_end = strtotime("$date $this->time_end");
			$this->allday_patdiv();
			if($veh==1){
				$all_pat = true;
				$this->rec_pat = $this->allday_viajes();
			} else {
				$all_pat = false;
				$this->rec_pat = $this->allday_camionetas();
				//$this->allday_viajes();
			}
			//die($this->rec_pat);
			$bool_date_point = ($this->date_point > $this->date_start);
			$this->sql_all( $bool_date_point, $all_pat);
			return true;
		}
		
		private function gravedad()
		{
			$mysqli = $this->db;
			$sql = "SELECT * FROM `gravedad`";
			if($resultado = $mysqli->query($sql)) {
				while ($fila = $resultado->fetch_assoc()) {
					$this->gravedad[$fila["grv_nombre"]]["str"] = $fila["grv_str"];
					$this->gravedad[$fila["grv_nombre"]]["end"] = $fila["grv_end"];
				}
				$resultado->free();	
			}
		}
		
		private function allday_patdiv()
		{					
			$sql ="SELECT PU.regi_vehi_id,
					LEFT(geoc_codigo,3) AS COD
					FROM
					(
					SELECT
								RE.regi_vehi_id,
								GE.geoc_visible,
								GE.geoc_tipo,
								GE.geoc_codigo
								FROM (
										SELECT
												regi_id,
												regi_vehi_id
										FROM registro
										WHERE regi_fecha_posicion BETWEEN $this->date_start AND $this->date_end
								) AS RE
									LEFT JOIN `gc_id` AS GI ON RE.regi_id = GI.gc_regi_id
									LEFT JOIN `geocerca` AS GE ON GE.geoc_id = GI.gc_geoc_id AND GE.geoc_visible = 1
					HAVING  ( GE.geoc_visible = 1 AND (GE.geoc_tipo = 1 OR (GE.geoc_tipo = 2 AND GE.geoc_codigo like '%OE%' )) )
					) AS PU
					GROUP BY regi_vehi_id, COD
					ORDER BY 1 ASC";		
			//die($sql);
			$mysqli = $this->db;

			if ($resultado = $mysqli->query($sql))
			{
				while ($fila = $resultado->fetch_assoc())
				{
					//$id_patentes[$fila["regi_vehi_id"]] = $fila["regi_vehi_id"];
					$id_patentes[$fila["regi_vehi_id"]][] =  $fila["COD"];;
				}
				$resultado->free();
				$this->pat_divisiones = $id_patentes;
			}
			
		}

		private function allday_camionetas()
		{
			$sql = "SELECT
					vehi_id
					FROM `vehiculo`
					where vehi_tive_id = 2";
			$mysqli = $this->db;
			
			if ($resultado = $mysqli->query($sql))
			{
				while ($fila = $resultado->fetch_assoc())
				{
					$camionetas[] =  $fila["vehi_id"];;
				}
				$resultado->free();
				return implode(",",$camionetas);
			}
			
		}
		
		private function allday_viajes()
		{
			$sql = "SELECT 
					VI.vi_vehi_id,
					VI.vi_regi_id_ori,
					VI.vi_regi_id_dest,
					FROM_UNIXTIME(VI.vi_finsert) AS FI,
					FROM_UNIXTIME(VI.vi_fsalida) AS FS
						FROM `viajes` AS VI
						INNER JOIN `registro` AS RE ON VI.vi_regi_id_dest = RE.regi_id 
						WHERE VI.vi_st = 0 AND ( RE.regi_fecha_posicion  BETWEEN $this->date_start AND $this->date_end ) 
						ORDER BY 1 ASC";
			$mysqli = $this->db;
	
			$pat_viajes = array();
			$date_travels = array();
			if ($resultado = $mysqli->query($sql))
			{
				while ($fila = $resultado->fetch_assoc())
				{
					$vi_vehi_id = $fila["vi_vehi_id"];
					$vi_regi_id_ori = $fila["vi_regi_id_ori"];
					$vi_regi_id_dest = $fila["vi_regi_id_dest"];		
					$pat_viajes[] =  "( regi_vehi_id = $vi_vehi_id AND ( regi_id BETWEEN $vi_regi_id_ori AND $vi_regi_id_dest) )" ;
					
					$this->date_travels[] = array("vi_vehi_id" => $vi_vehi_id,
										"fi" => $fila["FI"],
										"fs" => $fila["FS"]);
				}
				$resultado->free();
			}
			return implode(" ||\n",$pat_viajes);
		}

		private function allday_in_process()
		{
			$sql = "SELECT 
					VI.vi_vehi_id,
					VI.vi_regi_id_ori,
					VI.vi_regi_id_dest,
					FROM_UNIXTIME(VI.vi_finsert) AS FI,
					FROM_UNIXTIME(VI.vi_fsalida) AS FS,
					FROM_UNIXTIME(RE.regi_fecha_posicion),
					$this->date_end - RE.regi_fecha_posicion AS DATE_DIF,
					if(VI.vi_empr_id = 4, VI.vi_fsalida, VI.vi_finsert) AS FECHA
					FROM `viajes` AS VI
					LEFT JOIN `registro` AS RE ON VI.vi_regi_id_ori = RE.regi_id 
					WHERE VI.vi_st > 0
					HAVING FECHA < $this->date_end
					ORDER BY 1 ASC";
			$mysqli = $this->db;

			$pat_viajes = array();
			if ($resultado = $mysqli->query($sql))
			{
				while ($fila = $resultado->fetch_assoc())
				{
					$vi_vehi_id = $fila["vi_vehi_id"];
					$vi_regi_id_ori = $fila["vi_regi_id_ori"];
					$vi_regi_id_dest = $fila["vi_regi_id_dest"];
					$vi_date_diff = $fila["DATE_DIF"];
					$FI = $fila["FI"];
					$FS = $fila["FS"];
							
					if( $vi_regi_id_dest > 0 && $vi_regi_id_ori > 0 ){
						$date_diff = round($vi_date_diff/60);
					} else if( $vi_regi_id_ori > 0 || (  $vi_regi_id_dest > 0 && $vi_regi_id_ori == NULL ) ){ 
						$date_diff = "0";
					} else {
						$date_diff = "S/I";
					}
					$pat_viajes[] = array("vi_vehi_id" => $vi_vehi_id,
										"date_diff" => $date_diff,
										"fi" => $FI,
										"fs" => $FS );
					
				}
				$resultado->free();
			}
			return $pat_viajes;
		}
				
		public function exec_sql($op = true)
		{
			$mysqli = $this->db;
			//print_r($this->sql); die();
			//echo "$this->sql" . empty($this->sql);
			//die();
			if(empty($this->sql)){
				$in_process = $this->allday_in_process();
				return array( 0, 0, $this->pat_divisiones, $in_process, 0);
			
			}else if ($resultado = $mysqli->query($this->sql))
			{
				$this->part = $this->normalize_result($resultado);
				$array_rows = $this->analyze_rows($this->part);
				//print_r($array_rows); die();		
				return ($op == false ) ? $this->trace_rows_one($array_rows) : $this->trace_rows($array_rows);
			}else{
				$this->jsondata['msj_err'] = "ERROR: Datos ingresados con inconsistencia";
				return false;
			}
		}
		private function trace_rows( $array_rows ) 
		{	
			$moving = array();
			$speeding = array();
			$key_dual = 0;
			foreach( $array_rows as $key => $var){
				$date_self = $var;
				
				$num_rows = count($var);
				if( $num_rows > 1){
					$date_next = $var;
					$date_self["geoc_id"] = $date_next["geoc_id"];
					$date_self["geoc_nombre"] = $date_next["geoc_nombre"];
					$date_self["geoc_tipo"] = $date_next["geoc_tipo"];
				}
				
				$id_moving = ((isset($date_self["move"]))? "m" . $date_self["move"] : "s" . $date_self["stop"]) . "_" . $date_self["geoc_id"] 
				. $date_self["vehi_patente"];
		
				if( ($id_moving . $key_dual) != @$key_moving)
				{
					$key_dual++;
				}
				$id_moving .= $key_dual;
				$key_moving = $id_moving;
						
				if(!isset($moving[$id_moving]["ppu"])){
					
					$moving[$id_moving]["ppu"] = $date_self["vehi_patente"];
					$moving[$id_moving]["id_vh"] = $date_self["regi_vehi_id"];
					$moving[$id_moving]["geoc_id"] = $date_self["geoc_id"];
					$moving[$id_moving]["geoc_nombre"] = $date_self["geoc_nombre"];
					$moving[$id_moving]["geoc_tipo"] = $date_self["geoc_tipo"];
					$moving[$id_moving]["finicio"] = $date_self["fecha"];
					$moving[$id_moving]["op_azumit_vel"] = @$date_self["op_azumit_vel"];
					$moving[$id_moving]["time"] = 0;
					$moving[$id_moving]["lenght"] = 0;
					$moving[$id_moving]["promVel"] = 0;
					$moving[$id_moving]["num_rows"] = 0;
					$moving[$id_moving]["max_vel"] = 0;
				}
		
				$moving[$id_moving]["time"] += @$date_self["time_dif"];
				$moving[$id_moving]["lenght"] += @$date_self["dist_points"];
				$moving[$id_moving]["promVel"] += $date_self["regi_velocidad"];
				$moving[$id_moving]["point"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"] ;
				$moving[$id_moving]["num_rows"]++;
				
				if( $moving[$id_moving]["max_vel"] < $date_self["regi_velocidad"] || $moving[$id_moving]["max_vel"] == 0 ) {
					$moving[$id_moving]["max_vel"] = $date_self["regi_velocidad"];
				}
				
				$moving[$id_moving]["ffin"] = $date_self["fecha"];
				$moving[$id_moving]["move"] = @$date_self["move"];
				$moving[$id_moving]["stop"] = @$date_self["stop"];	
		
				if(isset($date_self["speeding"])) {
					$id_speeding = $date_self["speeding"] . $date_self["vehi_patente"];
			
					if(!isset($speeding[$id_speeding]["ppu"])){
						$speeding[$id_speeding]["ppu"] = $date_self["vehi_patente"];
						$speeding[$id_speeding]["id_vh"] = $date_self["regi_vehi_id"];
						$speeding[$id_speeding]["speeding"] = $date_self["speeding"];
						$speeding[$id_speeding]["finicio"] = $date_self["fecha"];
						$speeding[$id_speeding]["point_start"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"];
						$speeding[$id_speeding]["gc_start"] = $date_self["geoc_id"];
						
						$speeding[$id_speeding]["vel_start"] = $date_self["geoc_max_speed"];
						$speeding[$id_speeding]["time"] = 0;
						$speeding[$id_speeding]["lenght"] = 0;
						$speeding[$id_speeding]["promVel"] = 0;
						$speeding[$id_speeding]["num_rows"] = 0;
					}
					
					$speeding[$id_speeding]["emp"] = $date_self["empr_nombre"];
					$speeding[$id_speeding]["ffin"] = $date_self["fecha"];
					$speeding[$id_speeding]["time"] += @$date_self["time_dif"];
					$speeding[$id_speeding]["lenght"] += @$date_self["dist_points"];
					$speeding[$id_speeding]["point_end"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"];
					$speeding[$id_speeding]["vel_end"] = $date_self["geoc_max_speed"];
					$speeding[$id_speeding]["promVel"] += $date_self["regi_velocidad"];
					$speeding[$id_speeding]["num_rows"]++;
					$speeding[$id_speeding]["gc_end"] = $date_self["geoc_id"];
					
					if( !isset($speeding[$id_speeding]["speed_dif"]) || @$speeding[$id_speeding]["speed_dif"] < $date_self["speed_dif"] ) {
						$speeding[$id_speeding]["speed_dif"] = $date_self["speed_dif"];
						$speeding[$id_speeding]["dif_vel"] = $date_self["regi_velocidad"];
						$speeding[$id_speeding]["dif_geoc_id"] = $date_self["geoc_id"];
						$speeding[$id_speeding]["dif_point"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"];
						$speeding[$id_speeding]["dif_max_speed"] = $date_self["geoc_max_speed"];
						
						$calgrav = round((($date_self["speed_dif"]*100) / $date_self["geoc_max_speed"]),2);					
						foreach ( $this->gravedad as $key => $valor){
							if($valor["str"] <= $calgrav && $valor["end"] >= $date_self["speed_dif"] ){
								$speeding[$id_speeding]["gravedad"] = $key;
							}
						}
					}
		
					if( !isset($speeding[$id_speeding]["speed"]) || @$speeding[$id_speeding]["speed"] < $date_self["regi_velocidad"] ) {
						$speeding[$id_speeding]["speed"] = $date_self["regi_velocidad"];
						$speeding[$id_speeding]["speed_speed_dif"] = $date_self["speed_dif"];
						$speeding[$id_speeding]["speed_geoc_id"] = $date_self["geoc_id"];
						$speeding[$id_speeding]["speed_point"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"];
						$speeding[$id_speeding]["speed_max_speed"] = $date_self["geoc_max_speed"];
					}			
				}		
			}
			$in_process = $this->allday_in_process();

			return array( $moving, $speeding, $this->pat_divisiones, $in_process, $this->date_travels);	
		}
		
		private function trace_rows_one( $array_rows ) 
		{
			$trace = array();
			$key_dual = 0;
			$id_trace_data = -1;
			$trace_data = "";
			foreach($array_rows as $key => $date_self){
			
				$id_trace = ((isset($date_self["move"]))? "m" . $date_self["move"] : "s" . $date_self["stop"]);
		
				if(isset($date_self["speeding"]) ) {
					
					$id_trace = "x" . $date_self["speeding"];
					$key_dual = $date_self["speeding"];
					$color = "#FF0000";
					$st = "exceso";
					
				}elseif (isset($date_self["move"]) ) {
					
					$id_trace = $key_dual. "m" . $date_self["move"];
					$color = "#008000"; //#00FF00
					$st = NULL;
								
				}elseif (isset($date_self["stop"]) ) {
		
					$id_trace = "s" . $date_self["stop"];
					$color = "#FFFF00";
					$st = "stop";
				}
				
				if($id_trace != $trace_data) {
					$trace_data = $id_trace;
					$id_trace_data++;
				}
				
				$geoc_title = ($date_self["geoc_tipo"] == 9) ? $date_self["geoc_codigo"]: $date_self["geoc_nombre"];
		
				if(!isset($trace[$id_trace_data]["date_in"])){
					
					$trace[$id_trace_data]["dist_rec"] = 0;	
					$trace[$id_trace_data]["time_rec"] = 0;
					$trace[$id_trace_data]["speed_dif"] = 0;
					$trace[$id_trace_data]["rows"] = 0;
					$trace[$id_trace_data]["geoc_max_speed"] = 0;
					$trace[$id_trace_data]["max_speed"] = $date_self["regi_velocidad"];
					$trace[$id_trace_data]["color"] = $color;
				
					$trace[$id_trace_data]["date_in"] = $date_self["fecha"];
					$trace[$id_trace_data]["geoc_codigo1"] = utf8_encode($geoc_title);
					
					if(!empty($st)){
						$trace[$id_trace_data]["st"] = $st;
					}
					if(isset($point)){
						$trace[$id_trace_data]["points"][] = $point;
					}
				}
				if(isset($date_self["replay"])){
					@$trace[$id_trace_data]["replay"] += $date_self["replay"];
				}
	
				$point =  $date_self["regi_longitud"] . " " .$date_self["regi_latitud"];
				$trace[$id_trace_data]["geoc_codigo2"] = utf8_encode($geoc_title);
				$trace[$id_trace_data]["date_out"] = $date_self["fecha"];
				$trace[$id_trace_data]["dist_rec"] += $date_self["dist_points"];
				$trace[$id_trace_data]["time_rec"] += $date_self["time_dif"];
				$trace[$id_trace_data]["speed_dif"] += $date_self["regi_velocidad"];
				$trace[$id_trace_data]["geoc_max_speed"] += $date_self["geoc_max_speed"];
				$trace[$id_trace_data]["rows"]++;
	
				if( $trace[$id_trace_data]["max_speed"] < $date_self["regi_velocidad"]) {
					$trace[$id_trace_data]["max_speed"] = $date_self["regi_velocidad"];	
				}
				$trace[$id_trace_data]["points"][] = $point;
			}
			return $trace;
		}
		
		private function analyze_rows($part) 
		{			
			$it = new MiIterador($part);
			$pointLocation = new PointLocation();
			
			while($it->valid())
			{
				$current = $it->current();
			
				$pat = $current["vehi_patente"];
				$bool_self_pat = ($pat == @$prev["vehi_patente"]);
		
				$lat_self = $current["regi_latitud"];
				$lon_self = $current["regi_longitud"];
				
				if( isset($prev) && $bool_self_pat )
				{
					$lat_prev = $prev["regi_latitud"];
					$lon_prev = $prev["regi_longitud"];
				$current["dist_points"]=($lat_prev!=$lat_self || $lon_prev!=$lon_self)? $pointLocation->Glength($lat_prev,$lon_prev,$lat_self,$lon_self):0;				
					$time_prev =  $prev["regi_fecha_posicion"];
					$time_self =  $current["regi_fecha_posicion"];
					$current["time_dif"] =  $time_self - $time_prev;
					if($current["time_dif"]>0) {
						$current["speetcalc"] = round(($current["dist_points"]*60) / ($current["time_dif"] / 60) );
					}
				} else {					
					$current["dist_points"] = 0;
					$current["time_dif"] = 0;	
				}
				
				################ AZUMIT
				
				$geoc_id = $current["geoc_id"];
				$geoc_tipo = $current["geoc_tipo"];
				$geoc_m_b_speed = $current["geoc_m_b_speed"];
				if( $geoc_id > 0 && in_array($geoc_tipo,array(8,9)) && !empty($geoc_m_b_speed) ) {
					$azumit = $pointLocation->Gall_point($geoc_id,"$lon_self $lat_self");
					$dif_azumit = abs($azumit-$current["regi_azimut"]);
					
					$bool1 = (   0<=$dif_azumit && $dif_azumit<= 90 );
					$bool2 = ( 270<=$dif_azumit && $dif_azumit<=360 );
					$current["op_azumit_vel"] = ( $bool1 || $bool2 ) ? "a": "b";
				
					if($current["op_azumit_vel"] == "b"){
						$current["geoc_max_speed"] = $geoc_m_b_speed;
					}
					//$dif_azumit = ($dif_azumit < 0)? ($dif_azumit * -1) : $dif_azumit;
					//$current["azumit_ruta"] = "$azumit / $dif_azumit";
					//echo  "id $geoc_id; " . $current["regi_azimut"] . "// $azumit DIF".( $current["regi_azimut"] - $azumit )." \n";
					//echo "$geoc_id -- $point // $lon_self $lat_self \n";
				}
				//################ END AZUMIT 

				$geoc_max_speed = $this->max_speed( $current["geoc_max_speed"], $current["vehi_tive_id"] );
				$current["geoc_max_speed"] = $geoc_max_speed;
		
				$regi_velocidad = $current["regi_velocidad"];
				$current["speed_dif"] = (!empty($geoc_max_speed)) ? $regi_velocidad - ( $geoc_max_speed + 5 ) : 0;		
		
				//################  START   S P E E D I N G  / M O V I N G   / S T O P
								
				if(!$bool_self_pat){ 
					$it->reset_all_vars($pat_vars[$pat]);
				}
					
				if( $current["regi_velocidad"] > 0 || $current["dist_points"] > 0.050 ) {
		
					if( $current["speed_dif"] > 0 )
					{
						$pat_vars[$pat]["time_speed"] += $current["time_dif"];
						
						if( $pat_vars[$pat]["i_speed"] >= 1 && $pat_vars[$pat]["time_speed"] >= 120 ) {
											
							$current["speeding"] = $pat_vars[$pat]["id_speed"];
							$it->update_row( ($pat_vars[$pat]["i_speed"]), "speeding", $pat_vars[$pat]["id_speed"] );	
							$it->reset_all_vars($pat_vars[$pat], 'sp1');	
						}
						$pat_vars[$pat]["i_speed"]++;
		
					} else {
						$it->reset_all_vars($pat_vars[$pat], 'sp');				
					}
					
					if( $pat_vars[$pat]["time_stop"] <= 180 && $pat_vars[$pat]["time_stop"] > 0 )
					{
						--$pat_vars[$pat]["id_stop"];
						if( $pat_vars[$pat]["id_move"] > 0) {
							--$pat_vars[$pat]["id_move"];	
						}			
						$it->update_row( ($pat_vars[$pat]["i_stop"]) ,"move", $pat_vars[$pat]["id_move"]);
					}				
					
					$current["move"] = $pat_vars[$pat]["id_move"];
					//$current["vars"] = $pat_vars[$pat]["i_stop"] . " _ " . $pat_vars[$pat]["time_stop"];					
					$it->reset_all_vars($pat_vars[$pat], 'mv');
			
				}else{
		
					$current["stop"] = $pat_vars[$pat]["id_stop"];
					$pat_vars[$pat]["i_stop"]++;
					$pat_vars[$pat]["time_stop"] += $current["time_dif"];
					
					$it->reset_all_vars($pat_vars[$pat], 'st');
				}
				
				//################   END   M O V I N G	
				
				$it->update_self_array($current);
				$prev = $current;
				$it->next();
			}
			//print_r($it->get_array());
			return $it->get_array();	
		}
		
		private function max_speed( $gms, $vt )
		{
			$geoc_max_speed = !empty($gms) ? $gms : 100;
			return ( $vt == 1 && $geoc_max_speed > 90 ) ? 90 : $geoc_max_speed;
		}
		
		private function sql_all($bool_date_point, $all_pat = true) 
		{
			$temp_sql1 = "	LEFT JOIN `geocerca` AS GE ON GE.geoc_visible = 1
							AND
							(
								(ST_CONTAINS(
								geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
								) AND GE.geoc_tipo IN(1,2,3,4,5,6,7,9))
							)";

			$temp_sql2 = "	LEFT JOIN `gc_id` AS GI ON RE.regi_id = GI.gc_regi_id
				LEFT JOIN `geocerca` AS GE ON GE.geoc_id = GI.gc_geoc_id AND GE.geoc_visible = 1";

			$temp_sql = ($bool_date_point)? $temp_sql1 : $temp_sql2 ;
			
			// AND VE.vehi_patente IN($this->rec_pat)
			/*$this->sql = "SELECT
					RE.regi_id,
					VE.vehi_patente,
					VE.vehi_tive_id,
					from_unixtime(RE.regi_fecha_posicion) AS fecha,
					RE.regi_fecha_posicion,
					RE.regi_latitud,
					RE.regi_longitud,
					RE.regi_velocidad,
					RE.regi_azimut,
				GE.geoc_id,
				GE.geoc_nombre,
				GE.geoc_codigo,
				GE.geoc_tipo,
				GE.geoc_max_speed,
				GE.geoc_m_b_speed
					FROM `vehiculo` AS VE
					INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id
						AND RE.regi_vehi_id IN($this->rec_pat)
						AND RE.regi_fecha_posicion BETWEEN $this->date_start AND $this->date_end		
						AND RE.regi_latitud != 0 AND RE.regi_longitud != 0
					$temp_sql";	*/

		$this->sql = "SELECT
					RE.regi_id,
					VE.vehi_patente,
					VE.vehi_tive_id,
					RE.regi_vehi_id,
					from_unixtime(RE.regi_fecha_posicion) AS fecha,
					RE.regi_fecha_posicion,
					RE.regi_latitud,
					RE.regi_longitud,
					RE.regi_velocidad,
					RE.regi_azimut,
				GE.geoc_id,
				GE.geoc_nombre,
				GE.geoc_codigo,
				GE.geoc_tipo,
				GE.geoc_max_speed,
				GE.geoc_m_b_speed
					FROM `registro` AS RE
					INNER JOIN `vehiculo` AS VE ON VE.vehi_id = RE.regi_vehi_id #LEFT
					$temp_sql
				WHERE RE.regi_fecha_posicion BETWEEN $this->date_start AND $this->date_end AND RE.regi_latitud != 0 AND RE.regi_longitud != 0
				AND regi_vehi_id IN($this->rec_pat) #HAVING
				ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";

		if($all_pat){
			
			if(!empty($this->rec_pat))
			{
				$this->sql = "SELECT 
					VE.vehi_patente,
					VE.vehi_tive_id,
					EP.empr_nombre,
					RE.*,
					GE.geoc_id,
					GE.geoc_nombre,
					GE.geoc_codigo,
					GE.geoc_tipo,
					GE.geoc_max_speed,
					GE.geoc_m_b_speed
					FROM (
							SELECT
									regi_id,
									regi_vehi_id,
									from_unixtime(regi_fecha_posicion) AS fecha,
									regi_fecha_posicion,
									regi_latitud,
									regi_longitud,
									regi_velocidad,
									regi_azimut
							FROM registro
							WHERE #regi_fecha_posicion BETWEEN  $this->date_start AND $this->date_end
							#HAVING 
								#regi_vehi_id IN
								( $this->rec_pat )
								AND regi_latitud != 0 AND regi_longitud != 0
					) AS RE
					INNER JOIN vehiculo AS VE ON RE.regi_vehi_id = VE.vehi_id
					LEFT  JOIN empresa AS EP ON EP.empr_id = VE.vehi_empr_id
						$temp_sql
						ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";
				}else{
					$this->sql = NULL;	
				}
			}else{	
		
			$this->sql = "SELECT 
			VE.vehi_patente,
			VE.vehi_tive_id,
			EP.empr_nombre,
			RE.*,
			GE.geoc_id,
			GE.geoc_nombre,
			GE.geoc_codigo,
			GE.geoc_tipo,
			GE.geoc_max_speed,
			GE.geoc_m_b_speed
			FROM (
					SELECT
							regi_id,
							regi_vehi_id,
							from_unixtime(regi_fecha_posicion) AS fecha,
							regi_fecha_posicion,
							regi_latitud,
							regi_longitud,
							regi_velocidad,
							regi_azimut
					FROM registro
					WHERE regi_fecha_posicion BETWEEN  $this->date_start AND $this->date_end
					HAVING 
						regi_vehi_id IN ( $this->rec_pat )
						AND regi_latitud != 0 AND regi_longitud != 0
			) AS RE
			INNER JOIN vehiculo AS VE ON RE.regi_vehi_id = VE.vehi_id
			LEFT  JOIN empresa AS EP ON EP.empr_id = VE.vehi_empr_id
				$temp_sql
				ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";
			}
			//die($this->sql);
		}

		private function normalize_result($response)
		{
			if($response->num_rows > 5){
						
				while ($fila = $response->fetch_assoc())
				{
					$regi_id = $fila['regi_id'];
		
					if( isset($part[$regi_id])) {
						$temp_row = $part[$regi_id];

						switch ($temp_row["geoc_tipo"]) {
							case 1:
								if( $fila["geoc_tipo"] == 2 ) {
									$geoc_max_speed = $part[$regi_id]["geoc_max_speed"];
									$geoc_m_b_speed = $part[$regi_id]["geoc_m_b_speed"];
									$part[$regi_id] = $fila;
									$part[$regi_id]["geoc_max_speed"] = $geoc_max_speed;
									$part[$regi_id]["geoc_m_b_speed"] = $geoc_m_b_speed;
								}
								break;
							case 3:
								if($fila["geoc_tipo"] == 9){
									$part[$regi_id] = $fila;
								}
								break;
							case NULL:
									$part[$regi_id] = $fila;
								break;
						}				
					}else
					{
						$bool1 = (@$temp_fila["regi_latitud"] == $fila["regi_latitud"]);
						$bool2 = (@$temp_fila["regi_longitud"] == $fila["regi_longitud"]);
						$bool3 = (@$temp_fila["regi_fecha_posicion"] == $fila["regi_fecha_posicion"]);
						$bool4 = (@$temp_fila["regi_velocidad"] == $fila["regi_velocidad"]);
						if($bool1 && $bool2 && $bool3 && $bool4){
							$part[$temp_fila["regi_id"]]["replay"] = @++$num_replay;
							continue;
						}else{
							$num_replay = 0; 	
						}
						$temp_fila = $fila;
						$part[$regi_id] = $fila;			
					}
					
				}
				$response->free();			

				if(count($part) <= 5) {
					$this->jsondata['msj_err'] = "ERROR: No se han encontrado datos de informaci&oacute;n seleccionada";
					return false;	
				}else{
					//print_r($part);
					//die();
					return $part;
				}
			} else {
				$this->jsondata['msj_err'] = "ERROR: No se han encontrado datos de informaci&oacute;n seleccionada";
				return false;
			}				
		}
		
	}
?>