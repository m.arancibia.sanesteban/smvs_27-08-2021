<?php
/* Smarty version 3.1.33, created on 2019-03-15 17:03:03
  from '/home/sercoing/public_html/smvs.codelco/pagina/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c8c04f7e00a87_66757953',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ba5b64702ec88115cc1b3fa0582d9091234c0453' => 
    array (
      0 => '/home/sercoing/public_html/smvs.codelco/pagina/templates/index.tpl',
      1 => 1552680180,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c8c04f7e00a87_66757953 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="es">
<head>
<title>.:: SMVS | SERCOING ::.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/x-icon" href="https://sercoing.cl/firma/serco.ico" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"><?php echo '</script'; ?>
>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"><?php echo '</script'; ?>
>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<?php echo '<script'; ?>
 src="./js/jquery-ui-timepicker-addon.js"><?php echo '</script'; ?>
>
<link href="./css/jquery-ui-timepicker-addon.css" rel="stylesheet" media="screen">


<link rel="stylesheet" href="./css/overflow-table.css">

<link rel="stylesheet" href="./css/accordion-panel.css">
<link rel="stylesheet" href="./css/accordion-list.css">
<link rel="stylesheet" href="./css/style.css">

<style>
div.left_menu div.ui-accordion-content {
	height: calc( 100% - <?php echo $_smarty_tpl->tpl_vars['hacpan']->value;?>
px );
}
form#geoc div.list_ul ul{
	height:100%;
  	height:calc(100% - <?php echo $_smarty_tpl->tpl_vars['haclist']->value;?>
px);
}
</style>


<?php echo '<script'; ?>
 src="./js/ES_es.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="./js/functions.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="./js/script_init.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="./js/searchs.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="./js/markerclusterer.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="./js/parsePoly.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="./js/gps.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/JavaScript">
$(function(){
<?php if (in_array("mov",$_smarty_tpl->tpl_vars['seccion_priv']->value)) {?>
	form_select("patent");
<?php }
if (in_array("geoc",$_smarty_tpl->tpl_vars['seccion_priv']->value)) {?>	
	form_select("geoc");
<?php }
if (in_array("rut",$_smarty_tpl->tpl_vars['seccion_priv']->value)) {?>
	form_select("routes");
<?php }
if (in_array("rec",$_smarty_tpl->tpl_vars['seccion_priv']->value)) {?>
	searchtr();
<?php }?>
});
<?php echo '</script'; ?>
>

</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> 
      <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <div class="navbar-brand">
  		<img src="http://sercoing.cl/firma/SercoingLogo_100x150.png" alt="SMVS">
      	<a class="btn" href="#">SMVS-SERCOING LTDA</a>
      </div>      
<!--      <a class="navbar-brand" href="#">
      		<img src="http://sercoing.cl/firma/SercoingLogo_100x150.png" height="30px" width="auto" alt="SMVS">
            <h4 for="SMVS - SERCOING LTDA." class="title-site">&nbsp;SMVS - SERCOING LTDA.</h4>
      </a> -->
    </div>
    <div id="myNavbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-question-circle"></i> About</a></li>
        <li><a href="#"><i class="fa fa-info-circle"></i> Info</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><i class="fa fa-sign-out"></i> Log-out</a></li>
      </ul>
    </div>
  </div>
</nav>

<section>
  <div class="container-fluid text-center">
    <div class="row content">
      <div class="icono--div">
        <div class="icono"><a href="#"><span class="raya"></span><span class="raya"></span><span class="raya"></span></a></div>
      </div>
      <div id="mySidenav" class="sidenav">
        <div class="left_menu accordion">
<?php if (in_array("mov",$_smarty_tpl->tpl_vars['seccion_priv']->value)) {?>
          <h3> <i class="fa fa-car"></i> Moviles</h3>
          <div id="opcion1" class="panel">         
           	<div class="div-group">        
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                    <input name="pt" id="search_p" type="text" class="form-control" placeholder="Buscar patente">
                </div>
                <div id="btn_group">
	                <button type="button" class="btn btn-info btn-xs"><i class="fa fa-object-group" style="font-size:20px"></i></button>
                </div>
           	</div>  
            <form id="patent">       
              <div id="something" class="accordion list list_ul">
                <h3>CAMIONETAS</h3>
                <ul name="0">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['moviles']->value[2], 'camioneta', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['camioneta']->value) {
?>
					<li>
                        <input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_patent" />
                        <label for="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_patent" title=""><?php echo $_smarty_tpl->tpl_vars['camioneta']->value;?>
</label>
					</li>                                 
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>
                <h3>CAMIONES</h3>
                <ul name="1">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['moviles']->value[1], 'camion', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['camion']->value) {
?>
					<li>
                        <input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_patent" />
                        <label for="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_patent" title=""><?php echo $_smarty_tpl->tpl_vars['camion']->value;?>
</label>
					</li>                                       
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>
              </div>
              <div id="not-these">
                <div class="acction">
                  <p>seleccionado</p>
                </div>
                <div class="accordion list list_ul">
                  <h3>CAMIONETAS</h3>
                  <ul>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['moviles']->value[2], 'camioneta', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['camioneta']->value) {
?>
					<li>
                        <input type="checkbox" id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_sl" />
                        <label id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['camioneta']->value;?>
</label>
					</li>                    
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                  </ul>
                  <h3>CAMIONES</h3>
                  <ul>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['moviles']->value[1], 'camion', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['camion']->value) {
?>
					<li>
                        <input type="checkbox" id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_sl" />
                        <label id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['camion']->value;?>
</label>
					</li>                    
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                  </ul>
                </div>
              </div>
            </form>
          </div>
<?php }
if (in_array("geoc",$_smarty_tpl->tpl_vars['seccion_priv']->value)) {?>
          <h3> <i class="fa fa-map-marker"></i> Geocercas</h3>
          <div id="opcion2" class="panel">         
           	<div class="div-group">        
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                    <input name="gc" id="search_g" type="text" class="form-control" placeholder="Buscar Geocerca">
                </div>
           	</div>  
            <form id="geoc">       
              <div id="something" class="accordion list list_ul">
<?php $_smarty_tpl->_assignInScope('id_ac', 0);
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['geoc']->value, 'all_geoc', false, 'div');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['div']->value => $_smarty_tpl->tpl_vars['all_geoc']->value) {
?>                
                <h3><?php echo $_smarty_tpl->tpl_vars['div']->value;?>
</h3>
                <ul name="<?php echo $_smarty_tpl->tpl_vars['id_ac']->value;?>
">    
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['all_geoc']->value, 'dch', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['dch']->value) {
?>                
					<li>
						<input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_geoc" />
						<label for="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_geoc" title="<?php echo $_smarty_tpl->tpl_vars['dch']->value['nombre'];?>
"><?php echo $_smarty_tpl->tpl_vars['dch']->value['codigo'];?>
</label>
			        </li>                
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
$_smarty_tpl->_assignInScope('id_ac', $_smarty_tpl->tpl_vars['id_ac']->value+1);?>   
                </ul>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              </div>
              <div id="not-these">
                <div class="acction">
                  <p>seleccionado</p>
                </div>
                <div class="accordion list list_ul">
<?php if (isset($_smarty_tpl->tpl_vars['geoc']->value['DCH'])) {?>                  
                  <h3>DCH</h3>
                  <ul>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['geoc']->value['DCH'], 'dch', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['dch']->value) {
?>                  
					<li>
						<input type="checkbox" id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_sl" />
						<label id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['dch']->value['nombre'];?>
"><?php echo $_smarty_tpl->tpl_vars['dch']->value['codigo'];?>
</label>
                    </li>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>                                  
                  </ul>
<?php }
if (isset($_smarty_tpl->tpl_vars['geoc']->value['DMH'])) {?>
                  <h3>DMH</h3>
                  <ul>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['geoc']->value['DMH'], 'dmh', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['dmh']->value) {
?>                  
					<li>
						<input type="checkbox" id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_sl" />
						<label id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['dmh']->value['nombre'];?>
"><?php echo $_smarty_tpl->tpl_vars['dmh']->value['codigo'];?>
</label>
                    </li>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                  </ul>
<?php }
if (isset($_smarty_tpl->tpl_vars['geoc']->value['DSAL'])) {?>       
                  <h3>DSAL</h3>
                  <ul>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['geoc']->value['DSAL'], 'dsal', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['dsal']->value) {
?>                  
					<li>
						<input type="checkbox" id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_sl" />
						<label id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['dsal']->value['nombre'];?>
"><?php echo $_smarty_tpl->tpl_vars['dsal']->value['codigo'];?>
</label>
                    </li>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>                  
<?php }?>
                  </ul>
                  <h3>VARIAS</h3>
                  <ul>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['geoc']->value['VARIAS'], 'varias', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['varias']->value) {
?>                   
					<li>
						<input type="checkbox" id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
_sl" />
						<label id="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['varias']->value['nombre'];?>
"><?php echo $_smarty_tpl->tpl_vars['varias']->value['codigo'];?>
</label>
                    </li>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                  </ul>
                </div>
              </div>
            </form>
          </div>
<?php }
if (in_array("rut",$_smarty_tpl->tpl_vars['seccion_priv']->value)) {?>      
          <h3> <i class="fa fa-road"></i> Rutas</h3>
          <div id="opcion3" class="panel">
          <div class="div-group">        
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                    <input name="rt" id="search_r" type="text" class="form-control" placeholder="Buscar ruta">
                </div>
           	</div>
            <form id="routes">
              <div id="something" class="list list_li">
                <ul class="ui-accordion-content ui-widget-content">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['routes']->value, 'route');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['route']->value) {
?>
					<li>
                        <input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['route']->value;?>
" id="<?php echo $_smarty_tpl->tpl_vars['route']->value;?>
_routes" />
                        <label for="<?php echo $_smarty_tpl->tpl_vars['route']->value;?>
_routes" title=""><?php echo $_smarty_tpl->tpl_vars['route']->value;?>
</label>
      				</li>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>
              </div>
              <div id="not-these">
                <div class="acction">
                  <p>seleccionado</p>
                </div>
                <div class="list list_li">
                  <ul class="ui-accordion-content ui-widget-content">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['routes']->value, 'route');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['route']->value) {
?>
                    <li>
                            <input type="checkbox" id="<?php echo $_smarty_tpl->tpl_vars['route']->value;?>
_sl" />
                            <label id="<?php echo $_smarty_tpl->tpl_vars['route']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['route']->value;?>
</label>
                    </li>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                  </ul>
                </div>
              </div>
            </form>
          </div>
<?php }
if (in_array("rec",$_smarty_tpl->tpl_vars['seccion_priv']->value)) {?>    
          <h3> <i class="fa fa-map-o"></i> Recorridos</h3>
          <div id="opcion4" class="panel">
 	          <form id="recorrido" class="list" action="#" method="post">
 	            <label for="rec_hours"><b>Tiempo de seguimiento</b></label>
                <div>      
                    <select name="rec_hours" id="rec_hours">
                        <option value="1">1 hora</option>
                        <option value="2">2 horas</option>
                        <option value="3">3 horas</option>
                        <option value="4">4 horas</option>
                        <option value="6">6 horas</option>
                        <option value="8">8 horas</option>
                        <option value="12">12 horas</option>
                        <option value="24">24 horas</option>
                        <option value="48">48 horas</option>
                    </select>
                </div>
                <label for="datepicker"><b>Fecha termino</b></label>
                <div>
		<input type="text" id="datepicker" name="date_end" placeholder="&#xF073;&nbsp;Seleccione fecha">
                </div>
                <label for="sel_reg"><b>Patentes</b></label>
                <div id="trace">
					<div><input name="tp" id="search_tp" placeholder="&#xf1b9;&nbsp;Busque vehiculo" type="text"></div>
                    <select name="sel_reg" id="sel_reg" size="11" style="min-height:100px">
                    	<optgroup label="Camionetas">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['moviles']->value[2], 'camioneta', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['camioneta']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['camioneta']->value;?>
</option>                   
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </optgroup>
                    	<optgroup label="Camiones">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['moviles']->value[1], 'camion', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['camion']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['camion']->value;?>
</option>                    
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </optgroup>
                    </select>
                </div>
                <div><input type="button" id="maps_rec_clean" value="Limpiar"></div>
			</form>
          </div>
<?php }
if (in_array("not",$_smarty_tpl->tpl_vars['seccion_priv']->value)) {?>        
          <h3> <i class="fa fa-warning"></i> Notificaciones</h3>
          <div id="notialert" class="panel table-responsive ">  
       		<div id="constrainer">
              <table class="table table-hover table-dark fixed table-condensed table-bordered"> 
                <thead>
                  <tr>
                    <th id="ah">Movil</th>
                    <th>Geocerca</th>
                    <th>Hora</th>
                  </tr>
                </thead>
                <tbody id="alert_geoc">
                
                </tbody>
              </table>
            </div>
          </div>
<?php }?>
        </div>
      </div>
      <div id="main" class="text-left">
        <div id="map-canvas">&nbsp;</div>
      </div>
    </div>
  </div>
</section>
<footer class="container-fluid text-center">
  <p>SERCOING LTDA. - SMVS © 2019</p>
</footer>
<?php echo '<script'; ?>
 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPSWiYwgsb-AHamk_0G6oAcTANacBVQzc&callback=myMap"><?php echo '</script'; ?>
>
</body>
</html>
<?php }
}
