<?php
	include("../include/db.php");
	include("distance.php");
	
//	$patentes = array("DPTR81");
//	print_r(record( $patentes, "2018-09-11"));
	
function record($patentes, $fecha, $div = false )
{
	global $mysqli;
	
	$PATENTES = "'" . implode("', '",$patentes) . "'";
	
	if($div){
		$temp_in = "INNER";
		$temp_gc = "1";		
	
	}else{
		$temp_in = "LEFT";
		$temp_gc = "1,2,3,4,5,6,7";		
	}
	
	$sql = "SELECT
	RE.regi_id,
	RE.vehi_patente,
	from_unixtime(RE.regi_fecha_posicion) AS fecha,
	RE.point,
	RE.regi_fecha_posicion AS unix,
	RE.regi_velocidad,
	GE.geoc_nombre,
	GE.geoc_id,
	GE.geoc_tipo,
	GE.geoc_codigo
  FROM 
	(SELECT
		RE.regi_id,
		VE.vehi_patente,
		RE.regi_fecha_posicion,
		RE.regi_velocidad,
		CONCAT( RE.regi_longitud, ' ', RE.regi_latitud ) AS point
		FROM `vehiculo` AS VE 
		INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id 
		AND from_unixtime(RE.regi_fecha_posicion) BETWEEN '$fecha 00:00:00' AND '$fecha 23:59:59'
		AND VE.vehi_patente IN(
		$PATENTES
)
	ORDER BY 1 DESC
		) AS RE 
$temp_in JOIN `geocerca` AS GE ON 
			GE.geoc_tipo IN($temp_gc) AND GE.geoc_visible = 1
			AND	ST_CONTAINS(
			geoc_poligono, PointFromText(CONCAT( 'Point(', point, ')')))
			ORDER BY vehi_patente ASC, fecha ASC, regi_id ASC, geoc_tipo ASC";
		
		
	$sql = "SELECT
		RE.regi_id,
		VE.vehi_patente,
		from_unixtime(RE.regi_fecha_posicion) AS fecha,
		RE.regi_fecha_posicion,
		RE.regi_velocidad,
		RE.regi_fecha_posicion AS unix,
		CONCAT( RE.regi_longitud, ' ', RE.regi_latitud ) AS point,
	GE.geoc_nombre,
	GE.geoc_id,
	GE.geoc_tipo,
	GE.geoc_codigo
		FROM `vehiculo` AS VE 
		INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id 
			AND VE.vehi_patente IN($PATENTES)
			AND RE.regi_fecha_posicion BETWEEN UNIX_TIMESTAMP('$fecha 00:00:00') AND UNIX_TIMESTAMP('$fecha 23:59:59')
		$temp_in JOIN `geocerca` AS GE ON 
			GE.geoc_tipo IN($temp_gc) AND GE.geoc_visible = 1
			AND	ST_CONTAINS(
			geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')')))
			ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";
		
		
		
	$sql = "SELECT
		RE.regi_id,
		VE.vehi_patente,
		from_unixtime(RE.regi_fecha_posicion) AS fecha,
		RE.regi_fecha_posicion,
		RE.regi_velocidad,
		RE.regi_fecha_posicion AS unix,
		CONCAT( RE.regi_longitud, ' ', RE.regi_latitud ) AS point,
	GE.geoc_nombre,
	GE.geoc_id,
	GE.geoc_tipo,
	GE.geoc_codigo
		FROM `vehiculo` AS VE 
		INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id 
			AND VE.vehi_patente IN($PATENTES)
			AND RE.regi_fecha_posicion BETWEEN UNIX_TIMESTAMP('$fecha 00:00:00') AND UNIX_TIMESTAMP('$fecha 23:59:59')
			$temp_in JOIN `gc_id` AS GI ON GI.gc_regi_id =  RE.regi_id
			$temp_in JOIN `geocerca` AS GE ON GE.geoc_id = GI.gc_geoc_id
				AND GE.geoc_visible = 1	AND GE.geoc_tipo  IN($temp_gc)
			ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";	
		
	$path = 0;
	$geoc_temp = NULL;
	$discharge = false;
	$first = true;
	$polygons = array();
	$geoc_div = 1;
	
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc())
		{
			$geoc_tipo = ( $fila['geoc_tipo'] == NULL )? 99 : $fila['geoc_tipo'];
			$geoc_id = ( $fila['geoc_id'] == NULL )? 99 : $fila['geoc_id'];
			
			if($geoc_tipo != $geoc_div && isset($geoc_temp[1]) ){
				if( $geoc_temp[$geoc_div]['regi_id'] != $fila['regi_id'] ){			
					$path_div = $id_geoc[$geoc_temp[$geoc_div]['geoc_id']]['path'];
					$polygons[$path_div]['END'] = $geoc_temp[$geoc_div];
					// echo $geoc_temp[1]['regi_id'] ."  " . $fila['regi_id']. " /\n";
					unset($geoc_temp[$geoc_div]);
					$discharge = true;
				}
			}
					
			$bool1 = in_array($geoc_tipo, array(2,4,5,6)) && $geoc_id != @$geoc_temp[$geoc_tipo]['geoc_id'];  // E L I M I N A R
			
			$bool2 = $geoc_tipo == 1 && $geoc_id != @$geoc_temp[$geoc_tipo]['geoc_id'] || $discharge ;
			$bool4 = @$geoc_temp[$geoc_tipo]['regi_velocidad'] == 0 && $fila['regi_velocidad']  > 0;
			$bool5 = @$geoc_temp[$geoc_tipo]['regi_velocidad'] > 0 && $fila['regi_velocidad']  == 0;
			$bool3 = $geoc_tipo == 99 && ( @$geoc_temp[$geoc_tipo]['geoc_id'] != NULL || $bool4 || $bool5 || $discharge );
			
			
			$bool6 = (@$geoc_temp[$geoc_tipo]['vehi_patente'] !=  $fila['vehi_patente']);
			$bool7 = (( $path - @$id_geoc[$geoc_id]['path']) > 1 ) && $geoc_tipo > 1;
			
			if($bool3 || ( empty($fila['geoc_nombre']) && empty($fila['geoc_codigo']) )  ){
				$fila['geoc_codigo'] = "IN_ROUTE";
				$fila['geoc_nombre'] = ( $fila['regi_velocidad']  == 0)? "EXT-STOP" : "EXT-MOVE";
			}
	
			
			if( $first || $bool1 || $bool2 || $bool3 || $bool6 || $bool7 ){
				$id_geoc[$geoc_id]['path'] = $path++;
				$id_geoc[$geoc_id]['op'] = 'STAR';
				$first = false;
				if( $bool2 ){
					$discharge = false; // activar en geocerca de descarga
				}
			}else{
				$id_geoc[$geoc_id]['op'] = 'END';
			}
			$record = $id_geoc[$geoc_id]['path'];
			$op = $id_geoc[$geoc_id]['op'];
			
			$polygons[$record][$op] = $fila;
			$polygons[$record]["points"][] =  $fila["point"];
			if(!isset($polygons[$record]["velocidad"])){
				$polygons[$record]["velocidad"]["add"] = 0;
				$polygons[$record]["velocidad"]["nro"] = 0;
				$polygons[$record]["velocidad"]["max"] = 0;
				$polygons[$record]["velocidad"]["point"] = ""; //$fila["point"];
				$polygons[$record]["velocidad"]["hora"] = "";				
			}else if($fila["regi_velocidad"]>0){
				
				$polygons[$record]["velocidad"]["add"] +=  $fila["regi_velocidad"];
				$polygons[$record]["velocidad"]["nro"] ++;
				if( $fila["regi_velocidad"] > $polygons[$record]["velocidad"]["max"] )
				{
					$polygons[$record]["velocidad"]["max"] =  $fila["regi_velocidad"];
					list($lng,$lat) = explode(" ",$fila["point"]);
					$polygons[$record]["velocidad"]["point"] = "$lat $lng";
					$polygons[$record]["velocidad"]["hora"] =  date( "H:i:s", $fila["unix"]);//strtotime $fila["fecha"];
					
				}	
			}
			$geoc_temp["prev"] = $fila;
			$geoc_temp[$geoc_tipo] = $fila;
		}
	}
	//print_r($id_geoc);
	//print_r($polygons);

	//----------------------------------------------------------------------------

	$distance = new distance_geom();	

	if(count($polygons)>0){		
		foreach($polygons as $key => $valor) {
		
			$value = $valor["STAR"];				
			
			if( count($valor["points"]) > 1 && $value["geoc_nombre"] != "SxTOP"){ //&& $value["geoc_id"] != NULL 
				$points_dist = $valor["points"];
				$dist_rec = $distance->Sum_array($points_dist);
			}else{
				$dist_rec = "0";
			}
			$velocidad = $valor["velocidad"];
			if(!isset($valor["END"])){
				$valor["END"] = $valor["STAR"];	
			}

			$data[$key]["patente"] = $value["vehi_patente"];
			$data[$key]["geoc_id"] = $value["geoc_id"];
			$data[$key]["geoc_codigo"] = $value["geoc_codigo"];
			$data[$key]["geoc_nombre"] = $value["geoc_nombre"];
			$data[$key]["geoc_tipo"] = $value["geoc_tipo"];
			$data[$key]["date_in"] = $value["fecha"];
			$data[$key]["date_out"] = $valor["END"]["fecha"];
			$data[$key]["date_dif"] =  round(($valor["END"]["unix"]-$value["unix"])/60);
			$data[$key]["dist_rec"] = $dist_rec;
			if($velocidad["nro"]>0)  $data[$key]["promediovel"] = round($velocidad["add"]/$velocidad["nro"],3);
			else $data[$key]["promediovel"] = 0;
			$data[$key]["max"] = number_format($velocidad["max"]);
			$data[$key]["point"] = $velocidad["point"];
			$data[$key]["hora"] = $velocidad["hora"];
		}
	}
	//print_r($data);
	return $data;
			
	$z = 0;
  	$time_end = 0;
	if(count($data)>0){
		foreach($data as $key => $valor) {
			
			if(empty($times[$z])){
				$times[$z]["driv"] = 0;
				$times[$z]["distance"] = 0;
				$times[$z]["add"] = "";
			}

			if($valor['geoc_tipo'] == 1){
				//print_r($valor);
				$time_end = $valor['date_out'];
				$z++;
			}else if( $valor['date_in'] > $time_end || $time_end == 0) {
				$times[$z]["driv"] += $valor['date_dif'];
				$times[$z]["distance"] += $valor['dist_rec'];
				$times[$z]["add"] .=  " + " . $valor['date_dif'];			
				$times[$z]["pos"] =  $valor;
			}		
		}
		print_r($times);
		return;
		
	}
	
}
	
?>