<?php

	include("../include/db.php");
	include("../include/pointinline.php");
	include("distance.php");
	include("read.php");
		
	$gps = new gps_serco();	
	$gps->format_vars_class("DPYD78","2019-1-5");
	$gps->exec_sql();

	class gps_serco{
		public $jsondata = array(), $db, $part = array(), $date_point; //private
		public $rec_pat, $date_end, $date_start, $sql;
	 
		public function __construct() 
		{
			global $mysqli;
			$this->db = $mysqli;
			$this->date_point = strtotime("2018-12-22 11:00:00");
		}

		public function format_vars() 
		{
			$this->rec_pat = @$_POST["sel_reg"];
			$date_end = @$_POST["date_end"];
			$rec_hours = (@$_POST["rec_hours"] * 3600);
			$temp_date = str_replace("/", "-", $date_end);
			$this->date_end = strtotime($temp_date);
			$this->date_start = $date_end - $rec_hours;
			$bool_date_point = ($this->date_point > $this->date_start);
			$this->sql_all($bool_date_point);
			return true;
		}

		public function format_vars_class( $pat, $date ) 
		{
			$this->rec_pat = "'$pat'";
			$this->date_start = strtotime("$date 00:00:00");
			$this->date_end   = strtotime("$date 23:59:59");
			$bool_date_point = ($this->date_point > $this->date_start);
			$this->sql_all($bool_date_point);
			return true;
		}	
		
		public function exec_sql()
		{
			$mysqli = $this->db;
			if ($resultado = $mysqli->query($this->sql))
			{
				$this->part = $this->normalize_result($resultado);
				$array_rows = $this->analyze_rows($this->part);
				$array_rows = $this->trace_rows($array_rows);
				print_r($array_rows);
			}else{
				$this->jsondata['msj_err'] = "ERROR: Datos ingresados con inconsistencia";
				return false;
			}
		}

		private function trace_rows( $array_rows ) 
		{
			$trace = array();
			$key_dual = 0;
			$id_trace_data = -1;
			$trace_data = "";
			foreach($array_rows as $key => $date_self){
			
				$id_trace = ((isset($date_self["move"]))? "m" . $date_self["move"] : "s" . $date_self["stop"]);
		
				if(isset($date_self["speeding"]) ) {
					
					$id_trace = "x" . $date_self["speeding"];
					$key_dual = $date_self["speeding"];
					$color = "#FF0000";
					$st = "exceso";
					
				}elseif (isset($date_self["move"]) ) {
					
					$id_trace = $key_dual. "m" . $date_self["move"];
					$color = "#008000"; //#00FF00
					$st = NULL;
								
				}elseif (isset($date_self["stop"]) ) {
		
					$id_trace = "s" . $date_self["stop"];
					$color = "#FFFF00";
					$st = "stop";
				}
				
				if($id_trace != $trace_data){
					$trace_data = $id_trace;
					$id_trace_data++;
				}
		
				if(!isset($trace[$id_trace_data]["date_in"])){
					
					$trace[$id_trace_data]["dist_rec"] = 0;	
					$trace[$id_trace_data]["time_rec"] = 0;
					$trace[$id_trace_data]["speed_dif"] = 0;
					$trace[$id_trace_data]["rows"] = 0;
					$trace[$id_trace_data]["geoc_max_speed"] = 0;
					$trace[$id_trace_data]["max_speed"] = $date_self["regi_velocidad"];
					$trace[$id_trace_data]["color"] = $color;
				
					$trace[$id_trace_data]["date_in"] = $date_self["fecha"];
					$trace[$id_trace_data]["geoc_codigo1"] = utf8_encode($date_self["geoc_codigo"]);
					
					if(!empty($st)){
						$trace[$id_trace_data]["st"] = $st;
					}
					if(isset($point)){
						$trace[$id_trace_data]["points"][] = $point;
					}
				}
				if(isset($date_self["replay"])){
					@$trace[$id_trace_data]["replay"] += $date_self["replay"];
				}
	
				$point =  $date_self["regi_longitud"] . " " .$date_self["regi_latitud"];
				$trace[$id_trace_data]["geoc_codigo2"] = utf8_encode($date_self["geoc_codigo"]);
				$trace[$id_trace_data]["date_out"] = $date_self["fecha"];
				$trace[$id_trace_data]["dist_rec"] += $date_self["dist_points"];
				$trace[$id_trace_data]["time_rec"] += $date_self["time_dif"];
				$trace[$id_trace_data]["speed_dif"] += $date_self["regi_velocidad"];
				$trace[$id_trace_data]["geoc_max_speed"] += $date_self["geoc_max_speed"];
				$trace[$id_trace_data]["rows"]++;
	
				if( $trace[$id_trace_data]["max_speed"] < $date_self["regi_velocidad"]) {
					$trace[$id_trace_data]["max_speed"] = $date_self["regi_velocidad"];	
				}
				$trace[$id_trace_data]["points"][] = $point;
			}
			return $trace;
		}
		
		private function analyze_rows($part) 
		{			
			$it = new MiIterador($part);
			$pointLocation = new PointLocation();
			
			while($it->valid())
			{
				$current = $it->current();
			
				$pat = $current["vehi_patente"];
				$bool_self_pat = ($pat == @$prev["vehi_patente"]);
		
				$lat_self = $current["regi_latitud"];
				$lon_self = $current["regi_longitud"];
				
				if( isset($prev) && $bool_self_pat )
				{
					$lat_prev = $prev["regi_latitud"];
					$lon_prev = $prev["regi_longitud"];
				$current["dist_points"]=($lat_prev!=$lat_self || $lon_prev!=$lon_self)? $pointLocation->Glength($lat_prev,$lon_prev,$lat_self,$lon_self):0;				
					$time_prev =  $prev["regi_fecha_posicion"];
					$time_self =  $current["regi_fecha_posicion"];
					$current["time_dif"] =  $time_self - $time_prev;
					if($current["time_dif"]>0) {
						$current["speetcalc"] = round(($current["dist_points"]*60) / ($current["time_dif"] / 60) );
					}
				} else {					
					$current["dist_points"] = 0;
					$current["time_dif"] = 0;	
					if( !$bool_self_pat )
					{
						$it->reset_speeding();
						$pat_vars[$pat]["speeding_id"] = NULL;
						$pat_vars[$pat]["moving_id"] = $it->reset_move();
						$pat_vars[$pat]["temp_i_move"] = 0;
						$pat_vars[$pat]["temp_time_stop"] = 0;
						$pat_vars[$pat]["stop_id"] = 0;
					}
				}
				
				//################ AZUMIT
				
				$geoc_id = $current["geoc_id"];
				$geoc_tipo = $current["geoc_tipo"];
				$geoc_m_b_speed = $current["geoc_m_b_speed"];
				if( $geoc_id > 0 && in_array($geoc_tipo,array(8,9)) && !empty($geoc_m_b_speed) ) {
					$azumit = $pointLocation->Gall_point($geoc_id,"$lon_self $lat_self");
					$dif_azumit = abs($azumit-$current["regi_azimut"]);
					
					$bool1 = (   0<=$dif_azumit && $dif_azumit<= 90 );
					$bool2 = ( 270<=$dif_azumit && $dif_azumit<=360 );
					$current["op_azumit_vel"] = ( $bool1 || $bool2 ) ? "a": "b";
				
					if($current["op_azumit_vel"] == "b"){
						$current["geoc_max_speed"] = $geoc_m_b_speed;
					}
					//$dif_azumit = ($dif_azumit < 0)? ($dif_azumit * -1) : $dif_azumit;
					//$current["azumit_ruta"] = "$azumit / $dif_azumit";
					//echo  "id $geoc_id; " . $current["regi_azimut"] . "// $azumit DIF".( $current["regi_azimut"] - $azumit )." \n";
					//echo "$geoc_id -- $point // $lon_self $lat_self \n";
				}
				//################ AZUMIT END

				$geoc_max_speed = $this->max_speed( $current["geoc_max_speed"], $current["vehi_tive_id"] );
				$current["geoc_max_speed"] = $geoc_max_speed;
		
				$regi_velocidad = $current["regi_velocidad"];
				$current["speed_dif"] = (!empty($geoc_max_speed)) ? $regi_velocidad - ( $geoc_max_speed + 5 ) : 0;		
		
				//################ SPEEDING   reset_speeding
				
				if($current["speed_dif"] > 0)
				{
					@$pat_vars[$pat]["temp_i_speeding"]++;
					@$pat_vars[$pat]["temp_time_speeding"] += $current["time_dif"];		
					if( $pat_vars[$pat]["temp_i_speeding"] >= 2 && $pat_vars[$pat]["temp_time_speeding"] >= 120 ) {
						if($pat_vars[$pat]["speeding_id"] === NULL){
							//$current["entro"] = $pat_vars[$pat]["temp_i_speeding"];
							$pat_vars[$pat]["speeding_id"] = $it->update_row( ($pat_vars[$pat]["temp_i_speeding"] - 1),"speeding");	
						}
						$current["speeding"] = $pat_vars[$pat]["speeding_id"];
					}
				}else if( !isset($pat_vars[$pat]["speeding_id"]) || !empty($pat_vars[$pat]["speeding_id"]) ) {  
					$pat_vars[$pat]["temp_i_speeding"] = 0;
					$pat_vars[$pat]["temp_time_speeding"] = 0;
					$pat_vars[$pat]["speeding_id"] = NULL;
				}
				//################ SPEEDING END
					
				//################ MOVING

				if( $regi_velocidad > 0 || $current["dist_points"] > 0.050 )
				{
					if( $pat_vars[$pat]["temp_i_move"] > 0 && $pat_vars[$pat]["temp_time_stop"] > 180 ) {
						$pat_vars[$pat]["moving_id"] = $it->update_row( 0 ,"move");
						//echo $pat_vars[$pat]["temp_i_move"] . " -- " . $pat_vars[$pat]["moving_id"] . "  \n ";
					}
					if( $pat_vars[$pat]["temp_time_stop"] <= 180 && $pat_vars[$pat]["temp_time_stop"] > 0 && $pat_vars[$pat]["moving_id"] > 0 ) {

						$pat_vars[$pat]["moving_id"] = $it->update_row( ($pat_vars[$pat]["temp_i_move"]) ,"move");	
					}
					//$current["entro"] = $pat_vars[$pat]["temp_time_stop"] . " -- " . $pat_vars[$pat]["temp_i_move"];
					$current["move"] = $pat_vars[$pat]["moving_id"];
					$pat_vars[$pat]["temp_time_stop"] = 0;
					$pat_vars[$pat]["temp_i_move"] = 0;

				} else {
					if( ($pat_vars[$pat]["moving_id"]) > $pat_vars[$pat]["stop_id"] ) {

						$pat_vars[$pat]["stop_id"] = $pat_vars[$pat]["moving_id"];
					}
					$current["stop"] = $pat_vars[$pat]["stop_id"];
					$pat_vars[$pat]["temp_i_move"]++;
					$pat_vars[$pat]["temp_time_stop"] += $current["time_dif"];	
				}
				
				//################ MOVING END	
				
				$it->update_self_array($current);
				$prev = $current;
				$it->next();
			}
			//print_r($it->get_array());
			return $it->get_array();	
		}
		
		private function max_speed( $gms, $vt )
		{
			$geoc_max_speed = !empty($gms) ? $gms : 100;
			return ( $vt == 1 && $geoc_max_speed > 90 ) ? 90 : $geoc_max_speed;
		}
		
		private function sql_all($bool_date_point) 
		{
			$temp_sql1 = "	LEFT JOIN `geocerca` AS GE ON
							GE.geoc_visible = 1
							AND
							(
								(ST_CONTAINS(
								geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
								) AND GE.geoc_tipo IN(1,2,3,4,5,6,7,9))
							)
							ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";

			$temp_sql2 = "	LEFT JOIN `gc_id` AS GI ON RE.regi_id = GI.gc_regi_id
							LEFT JOIN `geocerca` AS GE ON
							GE.geoc_visible = 1 AND GE.geoc_id = GI.gc_geoc_id
							ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";

			$temp_sql = ($bool_date_point)? $temp_sql1 : $temp_sql2 ;
			
			$this->sql = "SELECT
					RE.regi_id,
					VE.vehi_patente,
					VE.vehi_tive_id,
					from_unixtime(RE.regi_fecha_posicion) AS fecha,
					RE.regi_fecha_posicion,
					RE.regi_latitud,
					RE.regi_longitud,
					RE.regi_velocidad,
					RE.regi_azimut,
				GE.geoc_id,
				GE.geoc_nombre,
				GE.geoc_codigo,
				GE.geoc_tipo,
				GE.geoc_max_speed,
				GE.geoc_m_b_speed
					FROM `vehiculo` AS VE
					INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id
						AND VE.vehi_patente IN($this->rec_pat)
						AND RE.regi_fecha_posicion BETWEEN $this->date_start AND $this->date_end		
						AND RE.regi_latitud != 0 AND RE.regi_longitud != 0
					$temp_sql";
		}
		
		private function normalize_result($response)
		{
			if($response->num_rows > 5){
						
				while ($fila = $response->fetch_assoc())
				{
					$regi_id = $fila['regi_id'];
		
					if( isset($part[$regi_id])) {
						$temp_row = $part[$regi_id];
							
						switch ($temp_row["geoc_tipo"]) {
							case 1:
								if( $fila["geoc_tipo"] == 2 ) {
									$geoc_max_speed = $part[$regi_id]["geoc_max_speed"];
									$geoc_m_b_speed = $part[$regi_id]["geoc_m_b_speed"];
									$part[$regi_id] = $fila;
									$part[$regi_id]["geoc_max_speed"] = $geoc_max_speed;
									$part[$regi_id]["geoc_m_b_speed"] = $geoc_m_b_speed;
								}
								break;
							case 3:
								if($fila["geoc_tipo"] == 9){
									$part[$regi_id] = $fila;
								}
								break;
							case NULL:
									$part[$regi_id] = $fila;
								break;
						}				
					}else
					{
						$bool1 = (@$temp_fila["regi_latitud"] == $fila["regi_latitud"]);
						$bool2 = (@$temp_fila["regi_longitud"] == $fila["regi_longitud"]);
						$bool3 = (@$temp_fila["regi_fecha_posicion"] == $fila["regi_fecha_posicion"]);
						$bool4 = (@$temp_fila["regi_velocidad"] == $fila["regi_velocidad"]);
						if($bool1 && $bool2 && $bool3 && $bool4){
							$part[$temp_fila["regi_id"]]["replay"] = @++$num_replay;
							continue;
						}else{
							$num_replay = 0; 	
						}
						$temp_fila = $fila;
						$part[$regi_id] = $fila;			
					}
				}				

				if(count($part) <= 5) {
					$this->jsondata['msj_err'] = "ERROR: No se han encontrado datos de informaci&oacute;n seleccionada";
					return false;	
				}else{
					return $part;	
				}
				
			} else {
				$this->jsondata['msj_err'] = "ERROR: No se han encontrado datos de informaci&oacute;n seleccionada";
				return false;
			}				
		}
	}
?>