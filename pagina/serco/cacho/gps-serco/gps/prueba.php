<?php

	// GEOCERCAS
	include("../include/db.php");
	require("../include/in_geoc.php");

	$sql = "SELECT
			geoc_codigo,
			AsText(geoc_poligono) AS poligono
			FROM `geocerca`";

    $converted_points = array();
	$polygons = array();	
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_object()) {
			$polygons = explode("((", $fila->poligono);
			$number = count($polygons)-1;
			for ( $n = 1; $n < count($polygons); $n++ )
			{
				$polygon = str_replace("))","",$polygons[1]);
				$points_str = explode(",",$polygon);
				foreach ($points_str as &$valor) {	
					$points = explode(" ",$valor);
					$lon  = $points[0];
					$lat  = $points[1];   
					$converted_points[$fila->geoc_codigo][] = "$lat,$lon";				
				}
			}
		}
		$resultado->close();
	}

	// FIN GEOCERCAS
	$pointLocation = new pointLocation();

	$geoc_pos = "-22.3582001, -68.9151993";
	
	
	$points = array("-22.3533001,-68.8904037",
"-22.3495998,-68.8864975",
"-22.3495998,-68.8864975",
"-22.3495998,-68.8864975",
"-22.3488007,-68.8862",
"-22.3488007,-68.8862",
"-22.3488007,-68.8862",
"-22.3488007,-68.8862",
"-22.3488007,-68.8862",
"-22.3488007,-68.8862",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002",
"-22.4391003,-68.8983002");



// Las últimas coordenadas tienen que ser las mismas que las primeras, para "cerrar el círculo"
	$alert_geoc = "";
	foreach($points as $key => $point) {
	
	

	foreach ($converted_points as $clave => $valor) {
		$info = $pointLocation->pointInPolygon( $point, $valor);
		if($info == "inside")
		{
			$alert_geoc .= "<li>  98 - p $point: $clave</li>";
		}
	}
}

	echo $alert_geoc;

?>