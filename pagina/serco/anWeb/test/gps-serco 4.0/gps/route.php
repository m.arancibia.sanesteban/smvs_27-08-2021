<?php
if( count($_POST) > -1 ) {
	include("../include/db.php");

	$jsondata = array();

	/*
	foreach($_POST as $key => $valor){
		if($valor == "on"){
			$geoc_codigo = $key;
		}
	};*/
	$idrt = $_POST['idrt'];
	
	if(!empty($idrt)){		
		$sqlX = "SELECT 
				geoc_nombre,
				geoc_color,
				geoc_max_speed,
				AsText(geoc_poligono) AS poligono,
				geoc_max_speed
			 FROM `geocerca` 
				WHERE geoc_tipo = 8 AND geoc_visible = 1 AND geoc_codigo = '$idrt'";
				
				
		$sql = "SELECT 
				GE.geoc_nombre,
				GE.geoc_color,
				GE.geoc_max_speed,
				GE.geoc_max_speed,
				AsText(if(RU.ruta_line IS NULL, GE.geoc_poligono, GE.geoc_poligono )) AS poligono
			 FROM `geocerca` AS GE 
				LEFT JOIN `ruta` AS RU ON GE.geoc_id = RU.ruta_geoc_id
				WHERE GE.geoc_tipo IN(8,9) AND GE.geoc_visible = 1 AND GE.geoc_codigo = '$idrt'";	
				
				
		$temp = array();
		$i = 0;
		if ($resultado = $mysqli->query($sql)) {
			while ($fila = $resultado->fetch_object()) {
				$temp[$i]['points'] =  $fila->poligono;
				$temp[$i]['color'] = $fila->geoc_color;
				$temp[$i]['max'] = $fila->geoc_max_speed;
				$temp[$i]['geoc_codigo'] = $idrt;
				$temp[$i++]['nombre'] =  $fila->geoc_nombre;
			}
			$jsondata['route'] = $temp;
			$resultado->close();
		}else{
			$jsondata['msj_err'] = "No se han encontrado datos";
		}
		$mysqli->close();		
	}else{
		$jsondata['msj_err'] = "Falta el ingreso de informacion";	
	}
	//$jsondata['route'] = $temp;
	$jsondata['post'] = serialize($_POST);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();
}