<?php 
	include("../include/db.php");
	include("../include/pointinline.php");
	include("distance.php");
	include("read.php");	
	//$patentes = "'CXSC74','DPLL84','DPRS16','DRHZ69'";
		$patentes = '"KLRV36",
"KLKB56",
"FWDB84",
"KVWP75",
"HKFT64",
"KGLR65",
"HKFT59",
"FGXG30",
"HLFT66",
"KKWJ15",
"HLVW35",
"JZJR20",
"KKWJ13",
"HKFT65",
"JPZK55",
"HRBX37",
"KCFJ98",
"GHVK75",
"KHSX71",
"GRCK50",
"KHSX67",
"KHSX69",
"KHSX72",
"KPKJ12",
"FTTR74",
"HKSW28",
"KPKJ57"
';

	//	$patentes = '"KHSX69"';
//	$patentes = "'HCKL25','FGPK13'"; 
//	$patentes = '"CDLV64","BGJL29"';


	$date_str = "2018-11-18";
	$date_end = "2018-11-18";
	
	$sql ="SELECT
				RE.regi_id,
				VE.vehi_patente,
				from_unixtime(RE.regi_fecha_posicion) AS fecha,
				RE.regi_fecha_posicion,
				RE.regi_latitud,
				RE.regi_longitud,
				RE.regi_velocidad,
				RE.regi_azimut,
			GE.geoc_id,
			GE.geoc_nombre,
			GE.geoc_codigo,
			GE.geoc_tipo,
			GE.geoc_max_speed,
			GE.geoc_m_b_speed
				FROM `vehiculo` AS VE 
				INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id 
					AND VE.vehi_patente IN($patentes)
					AND RE.regi_fecha_posicion BETWEEN UNIX_TIMESTAMP('$date_str 00:00:00') AND UNIX_TIMESTAMP('$date_end 23:59:59')
					AND RE.regi_latitud != 0 AND RE.regi_longitud != 0
				LEFT JOIN `geocerca` AS GE ON 
					GE.geoc_visible = 1
					AND
					(
						(ST_CONTAINS(
						geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
						) AND GE.geoc_tipo IN(1,2,3,4,5,6,7,9))
					)
					ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";

	$pointLocation = new PointLocation();
	$part1 = array();
	$range_max =  0.030;
	$temp_end_gc = NULL;
	$temp_prev = array();
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc())
		{		
			$regi_id = $fila['regi_id'];

			if( isset($part1[$regi_id][0])){
				$temp_row = $part1[$regi_id][0];
					
				switch ($temp_row["geoc_tipo"]) 
				{
					case 1:
						if( $fila["geoc_tipo"] == 2  ){
							$part1[$regi_id][] = $fila;
						}
						break;
					case 3:
						if($fila["geoc_tipo"] == 9){
							$part1[$regi_id][0] = $fila;
						}
						break;
					case NULL:
							$part1[$regi_id][0] = $fila;
						break;
				}				
			}else
			{
				$part1[$regi_id][0] = $fila;			
			}
		}
	}

	//print_r($part1);
	//die();

	//########################################################################################################################
	
	$it = new MiIterador($part1);
	while($it->valid())
	{
		$current = $it->current();
		$current = $current[0];
	
		$pat = $current["vehi_patente"];
		$bool_self_pat = ($pat == @$prev["vehi_patente"]);

		$lat_self = $current["regi_latitud"];
		$lon_self = $current["regi_longitud"];
		
		if( isset($prev) && $bool_self_pat )
		{
			
			$lat_prev = $prev["regi_latitud"];
			$lon_prev = $prev["regi_longitud"];
			$current["dist_points"] = ($lat_prev!=$lat_self || $lon_prev!=$lon_self)?$pointLocation->Glength($lat_prev,$lon_prev,$lat_self,$lon_self):0;
			
			$time_prev =  $prev["regi_fecha_posicion"];
			$time_self =  $current["regi_fecha_posicion"];
			$current["time_dif"] =  $time_self - $time_prev; 
		} else {
			
			$current["dist_points"] = 0;
			$current["time_dif"] = 0;
								
			if( !$bool_self_pat )
			{
				$it->reset_speeding();
				
				$pat_vars[$pat]["moving_id"] = $it->reset_move();
				$pat_vars[$pat]["temp_i_move"] = 0;
				$pat_vars[$pat]["temp_time_stop"] = 0;
				$pat_vars[$pat]["stop_id"] = 0;
			}
		}
		
		//################ AZUMIT
		$geoc_id = $current["geoc_id"];
		$geoc_tipo = $current["geoc_tipo"];
		$geoc_m_b_speed = $current["geoc_m_b_speed"];
		if( $geoc_id > 0 && in_array($geoc_tipo,array(8,9)) && !empty($geoc_m_b_speed) ) { // 
			$azumit = $pointLocation->Gall_point($geoc_id,"$lon_self $lat_self");
			$dif_azumit = (  $azumit - $current["regi_azimut"] );
			$dif_azumit = ($dif_azumit < 0)? ($dif_azumit * -1): $dif_azumit;
			
			//$current["op_azumit_vel"] = ($dif_azumit >= 176) ? "b": "a";

			$bool1 = ( 0<=$dif_azumit && $dif_azumit<=90 );
			$bool2 = ( 270<=$dif_azumit && $dif_azumit<=360 );
			$current["op_azumit_vel"] = ( $bool1 || $bool2 ) ? "a": "b";
			
			
			$current["azumit_ruta"] = "$azumit / $dif_azumit";
			
			if($current["op_azumit_vel"] == "b"){
				$current["geoc_max_speed"] = $geoc_m_b_speed;
			}
			//echo  "id $geoc_id; " . $current["regi_azimut"] . "// $azumit DIF".( $current["regi_azimut"] - $azumit )." \n";
			//echo "$geoc_id -- $point // $lon_self $lat_self \n";
			//die();
		}
		//################ AZUMIT END
		
		$geoc_max_speed = $current["geoc_max_speed"];

		$regi_velocidad = $current["regi_velocidad"];
		$current["speed_dif"] = (!empty($geoc_max_speed)) ? $regi_velocidad - ( $geoc_max_speed + 5 ) : 0;		

		//################ SPEEDING   reset_speeding
		if($current["speed_dif"] > 0)
		{
			@$pat_vars[$pat]["temp_i_speeding"]++;
			@$pat_vars[$pat]["temp_time_speeding"] += $current["time_dif"];		
			if( $pat_vars[$pat]["temp_i_speeding"] >= 2 && $pat_vars[$pat]["temp_time_speeding"] >= 120 ) {
				if($pat_vars[$pat]["speeding_id"] === NULL){
					//$current["entro"] = $pat_vars[$pat]["temp_i_speeding"];
					$pat_vars[$pat]["speeding_id"] = $it->update_row( ($pat_vars[$pat]["temp_i_speeding"] - 1),"speeding");	
				}
				$current["speeding"] = $pat_vars[$pat]["speeding_id"];
			}
		}else if( !isset($pat_vars[$pat]["speeding_id"]) || !empty($pat_vars[$pat]["speeding_id"]) ) {  
			$pat_vars[$pat]["temp_i_speeding"] = 0;
			$pat_vars[$pat]["temp_time_speeding"] = 0;
			$pat_vars[$pat]["speeding_id"] = NULL;
		}
		//################ SPEEDING END
			
		//################ MOVING 
		
		if( $regi_velocidad > 0 || $current["dist_points"] > 0.050 )
		{
			if( $pat_vars[$pat]["temp_i_move"] > 0 && $pat_vars[$pat]["temp_time_stop"] > 180 ) {
				$pat_vars[$pat]["moving_id"] = $it->update_row( 0 ,"move");
				//echo $pat_vars[$pat]["temp_i_move"] . " -- " . $pat_vars[$pat]["moving_id"] . "  \n ";
			}
			if( $pat_vars[$pat]["temp_time_stop"] <= 180 && $pat_vars[$pat]["temp_time_stop"] > 0  ) {
				if($pat_vars[$pat]["moving_id"] > 0){
					$pat_vars[$pat]["moving_id"] = $it->update_row( ($pat_vars[$pat]["temp_i_move"]) ,"move");	
				}
			}
			//$current["entro"] = $pat_vars[$pat]["temp_time_stop"] ."  -- " . $pat_vars[$pat]["temp_i_move"] ;
			$current["move"] = $pat_vars[$pat]["moving_id"];
			$pat_vars[$pat]["temp_time_stop"] = 0;
			$pat_vars[$pat]["temp_i_move"] = 0;

		}else{  
			if( ($pat_vars[$pat]["moving_id"]) > $pat_vars[$pat]["stop_id"] ) {
				$pat_vars[$pat]["stop_id"] = $pat_vars[$pat]["moving_id"];
			}
			$current["stop"] = $pat_vars[$pat]["stop_id"];
			$pat_vars[$pat]["temp_i_move"]++;
			$pat_vars[$pat]["temp_time_stop"] += $current["time_dif"];	
		}
		
		//################ MOVING END	
		
		$it->update_self_array($current);
		$prev = $current;
		$it->next();
	}
	
	//print_r($it->get_array());
	//die();
	
	//########################################################################################################################

	$moving = array();
	$speeding = array();
	$key_dual = 0;
	foreach($it->get_array() as $key => $var){
		$date_self = $var[0];
		$num_rows = count($var);
		if( $num_rows > 1){
			$date_next = $var[1];
			$date_self["geoc_id"] = $date_next["geoc_id"];
			$date_self["geoc_nombre"] = $date_next["geoc_nombre"];
			$date_self["geoc_tipo"] = $date_next["geoc_tipo"];
		}
		
		$id_moving = ((isset($date_self["move"]))? "m" . $date_self["move"] : "s" . $date_self["stop"]) . "_" . $date_self["geoc_id"] 
		. $date_self["vehi_patente"];

		if( ($id_moving . $key_dual) != @$key_moving)
		{
			$key_dual++;
		}
		$id_moving .= $key_dual;
		$key_moving = $id_moving;
				
		if(!isset($moving[$id_moving]["ppu"])){
			
			$moving[$id_moving]["ppu"] = $date_self["vehi_patente"];
			$moving[$id_moving]["geoc_id"] = $date_self["geoc_id"];
			$moving[$id_moving]["geoc_nombre"] = $date_self["geoc_nombre"];
			$moving[$id_moving]["geoc_tipo"] = $date_self["geoc_tipo"];
			$moving[$id_moving]["finicio"] = $date_self["fecha"];
			$moving[$id_moving]["op_azumit_vel"] = @$date_self["op_azumit_vel"];
			$moving[$id_moving]["time"] = 0;
			$moving[$id_moving]["lenght"] = 0;
			$moving[$id_moving]["promVel"] = 0;
			$moving[$id_moving]["num_rows"] = 0;
		}

		$moving[$id_moving]["time"] += @$date_self["time_dif"];
		$moving[$id_moving]["lenght"] += @$date_self["dist_points"];
		$moving[$id_moving]["promVel"] += $date_self["regi_velocidad"];
		$moving[$id_moving]["point"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"] ;
		$moving[$id_moving]["num_rows"]++;
		
		$moving[$id_moving]["ffin"] = $date_self["fecha"];
		$moving[$id_moving]["move"] = @$date_self["move"];
		$moving[$id_moving]["stop"] = @$date_self["stop"];


		if(isset($date_self["speeding"])){
			$id_speeding = $date_self["speeding"] . $date_self["vehi_patente"];
	
			if(!isset($speeding[$id_speeding]["ppu"])){
				$speeding[$id_speeding]["ppu"] = $date_self["vehi_patente"];
				$speeding[$id_speeding]["speeding"] = $date_self["speeding"];
				$speeding[$id_speeding]["finicio"] = $date_self["fecha"];
				$speeding[$id_speeding]["point_start"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"];
				$speeding[$id_speeding]["gc_start"] = $date_self["geoc_id"];
				
				$speeding[$id_speeding]["vel_start"] = $date_self["geoc_max_speed"];
				$speeding[$id_speeding]["time"] = 0;
				$speeding[$id_speeding]["lenght"] = 0;
				$speeding[$id_speeding]["promVel"] = 0;
				$speeding[$id_speeding]["num_rows"] = 0;
			}
			
			$speeding[$id_speeding]["ffin"] = $date_self["fecha"];
			$speeding[$id_speeding]["time"] += @$date_self["time_dif"];
			$speeding[$id_speeding]["lenght"] += @$date_self["dist_points"];
			$speeding[$id_speeding]["point_end"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"];
			$speeding[$id_speeding]["vel_end"] = $date_self["geoc_max_speed"];
			$speeding[$id_speeding]["promVel"] += $date_self["regi_velocidad"];
			$speeding[$id_speeding]["num_rows"]++;
			$speeding[$id_speeding]["gc_end"] = $date_self["geoc_id"];
			
			if( !isset($speeding[$id_speeding]["speed_dif"]) || @$speeding[$id_speeding]["speed_dif"] < $date_self["speed_dif"] ){
				$speeding[$id_speeding]["speed_dif"] = $date_self["speed_dif"];
				$speeding[$id_speeding]["dif_vel"] = $date_self["regi_velocidad"];
				$speeding[$id_speeding]["dif_geoc_id"] = $date_self["geoc_id"];
				$speeding[$id_speeding]["dif_point"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"];
				$speeding[$id_speeding]["dif_max_speed"] = $date_self["geoc_max_speed"];
			}

			if( !isset($speeding[$id_speeding]["speed"]) || @$speeding[$id_speeding]["speed"] < $date_self["regi_velocidad"] ){
				$speeding[$id_speeding]["speed"] = $date_self["regi_velocidad"];
				$speeding[$id_speeding]["speed_speed_dif"] = $date_self["speed_dif"];
				$speeding[$id_speeding]["speed_geoc_id"] = $date_self["geoc_id"];
				$speeding[$id_speeding]["speed_point"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"];
				$speeding[$id_speeding]["speed_max_speed"] = $date_self["geoc_max_speed"];
			}			
		}

		//print_r($date_self);

	}

	//print_r($speeding);
	//die();

	/*
	$it->current_size();
	$it->prev();
	*/
?>
<!DOCTYPE html>
<html>
<head>
<style>
 thead {
    color:green;
  }
  tbody {
    color:black;
  }
  tfoot {
    color:red;
  }
  table{
    width: auto;
    margin: 0 auto; 
  }
  table, th, td {
      border: 1px solid black;
      font-family: Calibri,sans-serif; 
      font-size: .9em;
      border-spacing: 0;
  }
  th{
    background-color: #8D0404;
    color: white;
  }
</style>
</head>

<body>
<table>
  <thead>
    <tr>
      <th>PPU</th>
      <th>IDev</th>
      <th>INICIOEv</th>
      <th>FINEv</th>
      <th>DURACIONEv [MIN]</th>
      <th>DISTEv [KM]</th>
      <th>VELX [KM/H]</th>
      <th>POINT_INI</th>     
      <th>POINT_FIN</th>    
      <th>IDGC_INI</th>     
      <th>IDGC_FIN</th>        
      <th>VELPERM_INI [KM/H]</th>     
      <th>VELPERM_FIN [KM/H]</th> 
		<!--INICIO DATOS FALTA-->
      <th>DIFMAX_FALTA [KM/H]</th>     
      <th>VELMAX_FALTA [KM/H]</th>
      <th>IDGC_FALTA</th>
      <th>POINT_FALTA</th>
      <th>VELPERM_FALTA [KM/H]</th>
		<!--FIN DATOS FALTA-->
		<!--INICIO VEL MAX REGISTRADA-->
      <th>VELMAX_TRAMO [KM/H]</th>     
      <th>DIFMAX_VELMAX_TRAMO [KM/H]</th>
      <th>IDGC_VELMAX</th>
      <th>POINT_VELMAX</th>
      <th>VELPERM_VELMAX [KM/H]</th> 
      	<!--FIN VEL MAX REGISTRADA-->     
	</tr>
  </thead>
  <tbody>
<?php

	foreach($speeding as $var){
		echo "<tr>";
		echo "<td>" . $var["ppu"] . "</td>";
		echo "<td>" . $var["speeding"] . "</td>";
		echo "<td>" . $var["finicio"] . "</td>";
		echo "<td>" . $var["ffin"] . "</td>";
		echo "<td>" . number_format(($var["time"] / 60), 2, ',', '.') . "</td>";
		echo "<td>" . number_format($var["lenght"], 2, ',', '.') . "</td>";
		echo "<td>" . round($var["promVel"]/ $var["num_rows"]) . "</td>";
		echo "<td>" . $var["point_start"] . "</td>";
		echo "<td>" . $var["point_end"] . "</td>";
		echo "<td>" . $var["gc_start"] . "</td>";
		echo "<td>" . $var["gc_end"] . "</td>";		
		echo "<td>" . $var["vel_start"] . "</td>";
		echo "<td>" . $var["vel_end"] . "</td>";

		echo "<td>" . $var["speed_dif"] . "</td>";
		echo "<td>" . $var["dif_vel"] . "</td>";
		echo "<td>" . $var["dif_geoc_id"] . "</td>";
		echo "<td>" . $var["dif_point"] . "</td>";
		echo "<td>" . $var["dif_max_speed"] . "</td>";

		echo "<td>" . $var["speed"] . "</td>";
		echo "<td>" . $var["speed_speed_dif"] . "</td>";
		echo "<td>" . $var["speed_geoc_id"] . "</td>";
		echo "<td>" . $var["speed_point"] . "</td>";
		echo "<td>" . $var["speed_max_speed"] . "</td>";
		echo "</tr>";
	}
?>  </tbody>
</table>



<table>
  <thead>
    <tr>
      <th>PPU</th>
      <th>ID_GC</th>
      <th>NOM_GC</th>
      <th>INIEv</th>
      <th>FINEv</th>
      <th>DURACION [MIN]</th>
      <th>DISTANCIA [KM/H]</th>
      <th>VELPROM</th>
      <th>IDMOV</th>
      <th>IDSTOP</th>
      <th>LASTPOINT</th>
      <th>TIPOGC</th>
   </tr>
  </thead>
  <tbody>
<?php

	foreach($moving as $var){
		echo "<tr>";

		echo "<td>" . $var["ppu"] . "</td>";
		echo "<td>" . $var["geoc_id"] . "</td>";
		echo "<td>" . utf8_encode($var["geoc_nombre"]) . "</td>";
		echo "<td>" . $var["finicio"] . "</td>";
		echo "<td>" . $var["ffin"] . "</td>";
		echo "<td>" . number_format(($var["time"]/60), 2, ',', '.'). "</td>";
		echo "<td>" . number_format(($var["lenght"]), 2, ',', '.') . "</td>";
		echo "<td>" . round($var["promVel"]/ $var["num_rows"]) . "</td>";
		echo "<td>" . $var["move"] . "</td>";
		echo "<td>" . $var["stop"] . "</td>";
		echo "<td>" . $var["point"] . "</td>";
		echo "<td>" . $var["geoc_tipo"] . "</td>";
		echo "<td>" . $var["op_azumit_vel"] . "</td>";
		
		
		
		


		echo "</tr>";
	}

?>  </tbody>
</table>
</body>
</html>
