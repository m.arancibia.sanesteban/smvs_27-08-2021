<?php 

$vel_max = 0;

if( count($_POST) > -1 ) {
	include("../include/db.php");
	include("../include/pointinline.php");
	include("distance.php");
	include("read.php");	
	//$patentes = "'CXSC74','DPLL84','DPRS16','DRHZ69'";
	//$patentes = '"KHSX69"';
	//	$patentes = "'HCKL25','FGPK13'"; 
	//	$patentes = '"CDLV64","BGJL29"';

	$date_str = "2018-11-18";
	$date_end = "2018-11-18";
	
	$jsondata = array();
	
	$rec_pat = @$_POST["sel_reg"];
	$date_end = @$_POST["date_end"];
	$rec_hours = (@$_POST["rec_hours"] * 3600);
	
	$temp_date = str_replace("/", "-", $date_end);
	$end_date = strtotime($temp_date);
	$start_date = $end_date - $rec_hours;
	
/*	$rec_pat = 1700566;
	$start_date = 1537498740;
	$end_date = 1537585140;

	$rec_pat = 135;
	$start_date =1544580000;
	$end_date = 1544583600;
*/		
	if($rec_pat > 0 && $start_date > 0 && $end_date > 0) {

	$sql ="SELECT
				RE.regi_id,
				VE.vehi_patente,
				from_unixtime(RE.regi_fecha_posicion) AS fecha,
				RE.regi_fecha_posicion,
				RE.regi_latitud,
				RE.regi_longitud,
				RE.regi_velocidad,
				RE.regi_azimut,
			GE.geoc_id,
			GE.geoc_nombre,
			GE.geoc_codigo,
			GE.geoc_tipo,
			GE.geoc_max_speed,
			GE.geoc_m_b_speed
				FROM `vehiculo` AS VE
				INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id
					AND RE.regi_vehi_id = $rec_pat
					AND RE.regi_fecha_posicion BETWEEN $start_date AND $end_date		
					AND RE.regi_latitud != 0 AND RE.regi_longitud != 0
				LEFT JOIN `geocerca` AS GE ON
					GE.geoc_visible = 1
					AND
					(
						(ST_CONTAINS(
						geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
						) AND GE.geoc_tipo IN(1,2,3,4,5,6,7,9))
					)
					ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";

	$pointLocation = new PointLocation();
	$part1 = array();
	$temp_end_gc = NULL;
	$temp_prev = array();
	
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc())
		{		
			$regi_id = $fila['regi_id'];

			if( isset($part1[$regi_id][0])){
				$temp_row = $part1[$regi_id][0];
					
				switch ($temp_row["geoc_tipo"]) 
				{
					case 1:
						if( $fila["geoc_tipo"] == 2  ){
							$part1[$regi_id][] = $fila;
						}
						break;
					case 3:
						if($fila["geoc_tipo"] == 9){
							$part1[$regi_id][0] = $fila;
						}
						break;
					case NULL:
							$part1[$regi_id][0] = $fila;
						break;
				}				
			}else
			{
				$bool1 = (@$temp_fila["regi_latitud"] == $fila["regi_latitud"]);
				$bool2 = (@$temp_fila["regi_longitud"] == $fila["regi_longitud"]);
				$bool3 = (@$temp_fila["regi_fecha_posicion"] == $fila["regi_fecha_posicion"]);
				$bool4 = (@$temp_fila["regi_velocidad"] == $fila["regi_velocidad"]);
				if($bool1 && $bool2 && $bool3 && $bool4) continue;
				$temp_fila = $fila;
				
				$part1[$regi_id][0] = $fila;			
			}
		}
		if(count($part1) <= 5) {
			$jsondata['msj_err'] = "No se han encontrado datos de unidad seleccionada" ;		
		}
		//print_r($part1);
		//die();
		
	
		//########################################################################################################################
		
		$it = new MiIterador($part1);
		while($it->valid())
		{
			$current = $it->current();
			$current = $current[0];
		
			$pat = $current["vehi_patente"];
			$bool_self_pat = ($pat == @$prev["vehi_patente"]);
	
			$lat_self = $current["regi_latitud"];
			$lon_self = $current["regi_longitud"];
			
			if( isset($prev) && $bool_self_pat )
			{
				
				$lat_prev = $prev["regi_latitud"];
				$lon_prev = $prev["regi_longitud"];
				$current["dist_points"] = ($lat_prev!=$lat_self || $lon_prev!=$lon_self)?$pointLocation->Glength($lat_prev,$lon_prev,$lat_self,$lon_self):0;				
				$time_prev =  $prev["regi_fecha_posicion"];
				$time_self =  $current["regi_fecha_posicion"];
				$current["time_dif"] =  $time_self - $time_prev; 
				if($current["time_dif"]>0) {
					$current["speetcalc"] = round(($current["dist_points"]*60)/($current["time_dif"]/60));
				}
			} else {
				
				$current["dist_points"] = 0;
				$current["time_dif"] = 0;
									
				if( !$bool_self_pat )
				{
					$it->reset_speeding();
					$pat_vars[$pat]["speeding_id"] = NULL;
					$pat_vars[$pat]["moving_id"] = $it->reset_move();
					$pat_vars[$pat]["temp_i_move"] = 0;
					$pat_vars[$pat]["temp_time_stop"] = 0;
					$pat_vars[$pat]["stop_id"] = 0;
				}
			}
			
			//################ AZUMIT
			$geoc_id = $current["geoc_id"];
			$geoc_tipo = $current["geoc_tipo"];
			$geoc_m_b_speed = $current["geoc_m_b_speed"];
			if( $geoc_id > 0 && in_array($geoc_tipo,array(8,9)) && !empty($geoc_m_b_speed) ) { // 
				$azumit = $pointLocation->Gall_point($geoc_id,"$lon_self $lat_self");
				$dif_azumit = (  $azumit - $current["regi_azimut"] );
				$dif_azumit = ($dif_azumit < 0)? ($dif_azumit * -1): $dif_azumit;
				
				//$current["op_azumit_vel"] = ($dif_azumit >= 176) ? "b": "a";
	
				$bool1 = ( 0<=$dif_azumit && $dif_azumit<=90 );
				$bool2 = ( 270<=$dif_azumit && $dif_azumit<=360 );
				$current["op_azumit_vel"] = ( $bool1 || $bool2 ) ? "a": "b";
				
				
				$current["azumit_ruta"] = "$azumit / $dif_azumit";
				
				if($current["op_azumit_vel"] == "b"){
					$current["geoc_max_speed"] = $geoc_m_b_speed;
				}
				//echo  "id $geoc_id; " . $current["regi_azimut"] . "// $azumit DIF".( $current["regi_azimut"] - $azumit )." \n";
				//echo "$geoc_id -- $point // $lon_self $lat_self \n";
				//die();
			}
			//################ AZUMIT END
			
			$geoc_max_speed = $current["geoc_max_speed"];
	
			$regi_velocidad = $current["regi_velocidad"];
			$current["speed_dif"] = (!empty($geoc_max_speed)) ? $regi_velocidad - ( $geoc_max_speed + 5 ) : 0;		
	
			//################ SPEEDING   reset_speeding
			if($current["speed_dif"] > 0)
			{
				@$pat_vars[$pat]["temp_i_speeding"]++;
				@$pat_vars[$pat]["temp_time_speeding"] += $current["time_dif"];		
				if( $pat_vars[$pat]["temp_i_speeding"] >= 2 && $pat_vars[$pat]["temp_time_speeding"] >= 120 ) {
					if($pat_vars[$pat]["speeding_id"] === NULL){
						//$current["entro"] = $pat_vars[$pat]["temp_i_speeding"];
						$pat_vars[$pat]["speeding_id"] = $it->update_row( ($pat_vars[$pat]["temp_i_speeding"] - 1),"speeding");	
					}
					$current["speeding"] = $pat_vars[$pat]["speeding_id"];
				}
			}else if( !isset($pat_vars[$pat]["speeding_id"]) || !empty($pat_vars[$pat]["speeding_id"]) ) {  
				$pat_vars[$pat]["temp_i_speeding"] = 0;
				$pat_vars[$pat]["temp_time_speeding"] = 0;
				$pat_vars[$pat]["speeding_id"] = NULL;
			}
			//################ SPEEDING END
				
			//################ MOVING 
			
			if( $regi_velocidad > 0 || $current["dist_points"] > 0.050 )
			{
				if( $pat_vars[$pat]["temp_i_move"] > 0 && $pat_vars[$pat]["temp_time_stop"] > 180 ) {
					$pat_vars[$pat]["moving_id"] = $it->update_row( 0 ,"move");
					//echo $pat_vars[$pat]["temp_i_move"] . " -- " . $pat_vars[$pat]["moving_id"] . "  \n ";
				}
				if( $pat_vars[$pat]["temp_time_stop"] <= 180 && $pat_vars[$pat]["temp_time_stop"] > 0  ) {
					if($pat_vars[$pat]["moving_id"] > 0){
						$pat_vars[$pat]["moving_id"] = $it->update_row( ($pat_vars[$pat]["temp_i_move"]) ,"move");	
					}
				}
				//$current["entro"] = $pat_vars[$pat]["temp_time_stop"] . " -- " . $pat_vars[$pat]["temp_i_move"] ;
				$current["move"] = $pat_vars[$pat]["moving_id"];
				$pat_vars[$pat]["temp_time_stop"] = 0;
				$pat_vars[$pat]["temp_i_move"] = 0;
	
			}else{  
				if( ($pat_vars[$pat]["moving_id"]) > $pat_vars[$pat]["stop_id"] ) {
					$pat_vars[$pat]["stop_id"] = $pat_vars[$pat]["moving_id"];
				}
				$current["stop"] = $pat_vars[$pat]["stop_id"];
				$pat_vars[$pat]["temp_i_move"]++;
				$pat_vars[$pat]["temp_time_stop"] += $current["time_dif"];	
			}
			
			//################ MOVING END	
			
			$it->update_self_array($current);
			$prev = $current;
			$it->next();
		}
		
		//print_r($it->get_array());
		//die();
		
		//########################################################################################################################
	
		$trace = array();
		$key_dual = 0;
		$id_trace_data = -1;
		$trace_data = "";
		foreach($it->get_array() as $key => $var){
			$date_self = $var[0];
			$num_rows = count($var);
			
			if( $num_rows > 1){
				$date_next = $var[1];
				$date_self["geoc_id"] = $date_next["geoc_id"];
				$date_self["geoc_nombre"] = $date_next["geoc_nombre"];
				$date_self["geoc_tipo"] = $date_next["geoc_tipo"];
				$date_self["geoc_codigo"] = $date_next["geoc_codigo"];
			}
			
			$id_trace = ((isset($date_self["move"]))? "m" . $date_self["move"] : "s" . $date_self["stop"]);
	
			if(isset($date_self["speeding"]) ) {
				
				$id_trace = "x" . $date_self["speeding"];
				$key_dual = $date_self["speeding"];
				$color = "#FF0000";
				$st = "exceso";
				
			}elseif (isset($date_self["move"]) ) {
				
				$id_trace = $key_dual. "m" . $date_self["move"];
				$color = "#008000"; //#00FF00
				$st = NULL;
							
			}elseif (isset($date_self["stop"]) ) {
	
				$id_trace = "s" . $date_self["stop"];
				$color = "#FFFF00";
				$st = "stop";
			}
			
			if($id_trace != $trace_data){
				$trace_data = $id_trace;
				$id_trace_data++;
			}
	
			if(!isset($trace[$id_trace_data]["date_in"])){
				
				$trace[$id_trace_data]["dist_rec"] = 0;	
				$trace[$id_trace_data]["time_rec"] = 0;
				$trace[$id_trace_data]["speed_dif"] = 0;
				$trace[$id_trace_data]["rows"] = 0;
				$trace[$id_trace_data]["geoc_max_speed"] = 0;
				$trace[$id_trace_data]["max_speed"] = $date_self["regi_velocidad"];
				$trace[$id_trace_data]["color"] = $color;				
				$trace[$id_trace_data]["date_in"] = $date_self["fecha"];
				$trace[$id_trace_data]["geoc_codigo1"] = $date_self["geoc_codigo"];
				
				if(!empty($st)){
					$trace[$id_trace_data]["st"] = $st;
				}
				if(isset($point)){
					$trace[$id_trace_data]["points"][] = $point;
				}
			}
			
			$point =  $date_self["regi_longitud"] . " " .$date_self["regi_latitud"];
			$trace[$id_trace_data]["geoc_codigo2"] = $date_self["geoc_codigo"];
			$trace[$id_trace_data]["date_out"] = $date_self["fecha"];
			$trace[$id_trace_data]["dist_rec"] += $date_self["dist_points"];
			$trace[$id_trace_data]["time_rec"] += $date_self["time_dif"];
			$trace[$id_trace_data]["speed_dif"] += $date_self["regi_velocidad"];
			$trace[$id_trace_data]["geoc_max_speed"] += $date_self["geoc_max_speed"];
			$trace[$id_trace_data]["rows"]++;
			
			
			if( $trace[$id_trace_data]["max_speed"] < $date_self["regi_velocidad"]) {
				$trace[$id_trace_data]["max_speed"] = $date_self["regi_velocidad"];	
			}
			
			$trace[$id_trace_data]["points"][] = $point;
		}
			//print_r($trace);
			//die();
			$jsondata['pos'] = $trace;
	
		$tr = "";
		$id_tr = -1;
		foreach($trace as $var){
			
			$id_tr++;
			
			if(!isset($var['st'])) continue;
			
			$icon = ( @$var['st'] == "exceso") ? "glyphicon-hdd speeding" : "glyphicon-minus-sign stop";
			
			$tr .= "<tr id=\"$id_tr\">
				<th scope=\"row\"><span class=\"glyphicon $icon \"></span></th>
				<td>" . $var['date_in'] . "</td>
				<td>" . $var['geoc_codigo1'] . "</td>
				<td>" . $var['date_out'] . "</td>
				<td>" . $var['geoc_codigo2'] . "</td>			
				<td>" . $var['dist_rec'] . "&nbsp;KM</td>
				<td>" . round($var['time_rec']/60) . "&nbsp;min</td>			
				<td>" . round($var['speed_dif']/$var['rows']) . "</td>	
				<td>" . $var['max_speed'] . "</td>				
				<td>" . round($var['geoc_max_speed']/$var['rows']) . "</td>	
				</tr>\n\t\t\t";
		}
	
		$jsondata['popup'] = "<div id=\"constrainer\">
		<table>
	  	<thead>
			<tr>
			  <th>Evento</th>		
			  <th>Inicio Evento</th>
			  <th>Geoc Inicio</th>
			  <th>Fin Evento</th>
			  <th>Geoc Fin</th>
			  <th>Distancia</th>
			  <th>Duracion</th>
			  <th>Velocidad %</th>
			  <th>Velocidad MAX</th>		  
			  <th>Velocidad Perm %</th>
	   		</tr>
	  	</thead>
		<tbody>
				$tr
		</tbody>
		</table>
		</div>";
	}else{
			$jsondata['msj_err'] = "No se han encontrado datos de unidad seleccionada" ;		
	}
	}else{
			$jsondata['msj_err'] = "Falta el ingreso de informacion";	
	}
	//$jsondata['msj_err'] = $sql;	
	header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();		
}
?>