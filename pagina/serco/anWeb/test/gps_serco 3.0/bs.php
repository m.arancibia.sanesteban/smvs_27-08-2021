<!DOCTYPE html>
<html lang="en">
<head>
<title>GPS</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

 
	<script src="//code.jquery.com/jquery-1.12.4.js"></script>
  	 
	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
 
 	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   
	<script src="js/menu-desp.js"></script>
 	<script src="js/searchs.js"></script>
   	<script src="js/parsePoly.js"></script>
   	<script src="js/gps.js"></script>
   	<script src="js/markerclusterer.js"></script>
	<script src="data.json"></script>
 	<link href="css/style.css" rel="stylesheet" media="screen">

	<script src="./js/jquery-ui-timepicker-addon.js"></script>
	<link href="./css/jquery-ui-timepicker-addon.css" rel="stylesheet" media="screen">
    
    <script>
	//$(document).ready(function() {
	var progressTimer, progressbar, dialog, closeDownload;
	  
	$(function() {
		
		google.maps.event.addDomListener(map, 'zoom_changed', function() {
			zoom = map.getZoom();
			//alert(zoom);
		});

		progressbar = $( "#progressbar" ),
		dialog = $( "#dialog_loading" ).dialog({
			autoOpen: false,
			closeOnEscape: false,
			resizable: false,
			open: function() {
				progressTimer = setTimeout( progress, 500 );
			}
		});
	 
    	progressbar.progressbar({
			value: false,
			complete: function() {
				closeDownload();
			}
		});

		function progress() {
			var val = progressbar.progressbar( "value" ) || 0;
			if ( val > 95 ) {
				 val = 0;
			}
			progressbar.progressbar( "value", val + Math.floor( Math.random() * 3 ) );
			progressTimer = setTimeout( progress, 60 );
		}

		closeDownload = function closeDownload() {
			clearTimeout( progressTimer );
			dialog.dialog( "close" );
			progressbar.progressbar( "value", false );
		}

		$( "#dialog" ).dialog({
			autoOpen: false,
			show: {
				effect: "blind",
				duration: 200
		  	},
		  	hide: {
				effect: "explode",
				duration: 200
		  	}
		});

	   	$( "#datepicker" ).datetimepicker({
			dateFormat: "dd-mm-yy",
			timeFormat: "HH:mm",
			currentText: "Nueva",
			closeText: "Hecho",
			timeText: "Horario",
			hourText: "Hora",
			minuteText: "Minutos"			
		});

		searchp();
		searchg();
		searchtp();

		$("ul li div input[type*='checkbox']").prop('checked', false);

		$("form div" ).on( "click", "input#maps_rec_clean", function(){
			if(recorrido != null){
				recorrido.setMap(null);
				recorrido = null;
			}
		});
		
		$("div#opcion1 input#group[type=checkbox]" ).prop("checked",true);		
		$("div#opcion1" ).on( "click", "input#group[type=checkbox]", function(){
			
			//mkcluster.getMarkers()
			mkcluster.clearMarkers();
			if(this.checked){
				$.each(Object.keys(markers), function(key, value){
					if(markers[value].getMap()!= null){
						markers[value].setMap(null);
						mkcluster.addMarker(markers[value]);
					}
				})
			}else{
				$.each(Object.keys(markers), function(key, value){
					markers[value].setMap(map);
				})
			}
		});
		
		$("form#patent ul#not-these" ).on( "click", "label", function(){
			id_patente = $(this).attr("id");
			myposition = markers[id_patente].getPosition();
			map.panTo(myposition);	
		});

		$("form#geocercas ul#not-these" ).on( "click", "label", function(){
			id_geoc = $(this).attr("id");
			myposition = geoc[id_geoc]["center"];
			map.panTo(myposition);
		});

		
		$("form#recorrido" ).on( "click", "select#sel_reg", function(){
			myRec();	
		});
		
		form_select("routes");

		
		$("form#geocercas" ).on( "click", "input[type=checkbox]", function(){
			var id_ul = $(this).parents("ul").attr("id");	
			if ( id_ul == 'something' )
			{
				$(this).parent().attr("class","hidden");
				var id = "form#geocercas input[id="+ $(this).attr("name") +"_gch]";
				$(id).prop('checked',true);
				$(id).parent().attr("class","show");
				myGeocerca();
			}else if(id_ul == 'not-these'){
				$(this).parent().attr("class","hidden");
				var id_gc = $(this).attr("id").replace("_gch", "");
				var id = "form#geocercas input[id="+ id_gc +"_gc]"
				$(id).parent().attr("class","show");
				$(id).prop('checked',false);
				geoc[id_gc]['polygon'].setMap(null);
			}
		});
		
		form_select("patent");
		
		$("form#patentxxxx" ).on( "click", "input[type=checkbox]", function(){				
			var id_ul = $(this).parents("ul").attr("id");	
			if ( id_ul == 'something' )
			{
				$(this).parent().attr("class","hidden");
				var id = "form#patent input[id="+ $(this).attr("name") +"_id]";
				$(id).prop('checked',true);
				$(id).parent().attr("class","show");

			}else if(id_ul == 'not-these'){
				$(this).parent().attr("class","hidden");
				var patente = $(this).attr("id").replace("_id", "");
				var id = "form#patent input[id="+ patente +"]";
				$(id).parent().attr("class","show");
				$(id).prop('checked',false);
				markers[patente].setMap(null);
				markers[patente] = null;
			}	
		});
	});
	
	function form_select(pref){
		var form = "form#" + pref;
		$(form).on( "click", "input[type=checkbox]", function(){
			var id_ul = $(this).parents("ul").attr("id"); //	console.log(id_ul);
			if ( id_ul == 'something' )
			{
				$(this).parent().attr("class","hidden");
				var id = form + " input[id=" + $(this).attr("name") + "]";
				$(id).prop('checked',true);
				$(id).parent().attr("class","show");
				switch(pref) {
					case "routes":
						route( $(this).attr("name") );
						break;
					case "patent":
						myGps( $(this).attr("name") );
						break;				
				} 
			}else if(id_ul == 'not-these'){
				$(this).parent().attr("class","hidden");
				var id_input = $(this).attr("id"); //.replace("", "");
				var id = form + " input[id="+ id_input + "_" + pref + "]"
				$(id).parent().attr("class","show");
				$(id).prop('checked',false);
				
				switch(pref) {
					case "routes":
						routes[id_input].forEach(function(elemento,i) {
							elemento.setMap(null);
						});
						//routes.splice(0,routes.length);
						delete routes[id_input];
						break;
					case "patent":
						if( $("div#opcion1 input#group[type=checkbox]").is(':checked') ){
							//alert("entro");
							mkcluster.removeMarker(markers[id_input]);
							//markers[id_input] = null;
						}else{
							markers[id_input].setMap(null);
						}
						delete markers[id_input];	
						break;				
				}
			}
		});
	}
</script>

</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="col-md-9 col-sm-7"> <a class="navbar-brand" href="#">Sercoing</a> </div>
    <div class="col-md-3 col-sm-5" id="myNavbar"> 
      <!-- navbar-header  // collapse navbar-collapse
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Projects</a></li>
        <li><a href="#">Contact</a></li>
      </ul>-->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Log-out</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid content">
  <div class="row">

	<div class="container-fluid contenedor--div">
        <div class="icono--div">
            <div class="icono"><a href="#"><span class="raya"></span><span class="raya"></span><span class="raya"></span></a></div>
        </div>

        <div class="contenedor--menu">
        <div id="accordion" class="sidenav">

          <h3>Patentes</h3>  
          <div id="opcion1" class="opciones text-left">
          	<div class="group"><input type="checkbox" name="group" id="group" checked /><label for="group">Agrupar</label></div>  
            <div><input name="d" placeholder="Buscar Patente" id="search_p" type="text"></div>
            <form id="patent" class="list" action="#" method="post">
    <?php
        include("./include/db.php");
        $sql = 'SELECT
                max(vehi_id) as id,
                vehi_patente as patente
                FROM vehiculo
				WHERE LEFT(vehi_patente, 1) != "@"
                GROUP BY patente
                ORDER BY 2 ASC';
        $li1 = null; 
        $li2 = null;
		$op_sel = null;
        if ($resultado = $mysqli->query($sql)) {
            while ($fila = $resultado->fetch_object()) {
				$op_sel .= '<option value="' . $fila->id . '">' . $fila->patente . '</option>\n';
                $li1 .= '    <li>
            <div class="show">
               	<input type="checkbox" name="' . $fila->id . '" id="' . $fila->id . '_patent" />
                <label for="' . $fila->id . '_patent">' . $fila->patente . '</label>
            </div>
        </li>';
                $li2 .= '    <li>
            <div class="hidden">
                <input type="checkbox" id="' . $fila->id . '" />
                <label id="' . $fila->id . '">' . $fila->patente . '</label>
            </div>
        </li>';
            }
            $resultado->close();
        }
        $mysqli->close();
    ?>
              <ul id="something">
                <?php echo $li1; ?>
              </ul>
              <ul id="not-these">
                <?php echo $li2; ?>
              </ul>
            </form>
          </div>
          
          <h3>Geocercas</h3>   
          <div id="opcion2" class="opciones text-left">
            <div><input name="g" id="search_g" placeholder="Buscar Geocerca" type="text"></div>
            <form id="geocercas" class="list" action="#" method="post">
    <?php
        include("./include/db.php");
            $sql = "SELECT
                GE.geoc_id AS id,
                GE.geoc_nombre,
                GE.geoc_codigo,
                GE.geoc_empr_id,
                EM.empr_nombre
                FROM `geocerca` AS GE
	                LEFT JOIN empresa AS EM ON GE.geoc_empr_id = EM.empr_id 
					WHERE geoc_tipo IN(1,2,3,4,5,6,7)
					ORDER BY 4 DESC, 3 ASC";
        $li1 = null; 
        $li2 = null;
        $temp = null;
        if ($resultado = $mysqli->query($sql)) {
            while ($fila = $resultado->fetch_object()) {
                if($temp == null || $temp != $fila->geoc_empr_id ){
                $li1 .= '    <li id="title"><h5><strong>'.$fila->empr_nombre.'</strong></h5></li>';
                $temp = $fila->geoc_empr_id;
                }
                $li1 .= '    <li>
            <div class="show">
                <input type="checkbox" name="'.$fila->id.'" id="'.$fila->id.'_gc" />
                <label for="'.$fila->id.'_gc" title="'.utf8_encode($fila->geoc_nombre).'">'.$fila->geoc_codigo.'</label>
            </div>
        </li>';
                $li2 .= '    <li>
            <div class="hidden">
                <input type="checkbox" id="'.$fila->id.'_gch" />
                <label id="'.$fila->id.'" title="'.$fila->geoc_nombre.'">'.$fila->geoc_codigo.'</label>
            </div>
        </li>';
            }
            $resultado->close();
        }
        $mysqli->close();
    ?>
              <ul id="something">
                <?php echo $li1; ?>
              </ul>
              <ul id="not-these">
                <?php echo $li2; ?>
              </ul>
            </form>
          </div>
    
          <h3>Rutas</h3>
          <div id="opcion3" class="text-left">
            <div><input name="r" id="search_r" placeholder="Buscar ruta" type="text"></div>
            <form id="routes" class="list" action="#" method="post">
    <?php
        include("./include/db.php");
            $sql = "SELECT
						rout_id AS id,
						rout_nombre,
						rout_codigo
						FROM `route`";
						
						
			$sql = 'SELECT
					GE.geoc_codigo
					FROM `geocerca` AS GE
						WHERE geoc_tipo IN(8)
						GROUP BY geoc_codigo
						ORDER BY 1 ASC';
        $li1 = null; 
        $li2 = null;
        if ($resultado = $mysqli->query($sql)) {
            while ($fila = $resultado->fetch_object()) {
                $li1 .= '    <li>
            <div class="show">
                <input type="checkbox" name="' . $fila->geoc_codigo . '" id="' . $fila->geoc_codigo . '_routes" />
                <label for="' . $fila->geoc_codigo . '_routes">' . $fila->geoc_codigo . '</label>
            </div>
        </li>';
                $li2 .= '    <li>
            <div class="hidden">
                <input type="checkbox" id="' . $fila->geoc_codigo . '" />
                <label id="' . $fila->geoc_codigo . '" >' . $fila->geoc_codigo . '</label>
            </div>
        </li>';
            }
            $resultado->close();
        }
        $mysqli->close();
    ?>
              <ul id="something">
                <?php echo $li1; ?>
              </ul>
              <ul id="not-these">
                <?php echo $li2; ?>
              </ul>
            </form>
          </div>
          
          <h3>Recorridos</h3>
          
          <div id="opcion4" class="text-left">
 	          <form id="recorrido" class="list" action="#" method="post">
 	            <label for="rec_hours"><b>Tiempo de seguimiento</b></label>
                <div>      
                    <select name="rec_hours" id="rec_hours">
                        <option value="1">1 hora</option>
                        <option value="2">2 horas</option>
                        <option value="4">4 horas</option>
                        <option value="6">6 horas</option>
                        <option value="8">8 horas</option>
                        <option value="12">12 horas</option>
                        <option value="24">24 horas</option>
                        <option value="48">48 horas</option>
                    </select>
                </div>
                <label for="datepicker"><b>Fecha termino</b></label>
                <div><input type="text" id="datepicker" name="date_end"></div>
                <label for="sel_reg"><b>Patentes</b></label>
                <div>
					<div><input name="tp" id="search_tp" placeholder="Buscar Patente" type="text"></div>
                    <select name="sel_reg" size="12"  id="sel_reg">
                        <?php echo $op_sel; ?>
                    </select>	
                </div>
                <div><input type="button" id="maps_rec_clean" value="Limpiar"></div>
			</form>
          </div>          
          <h3>Alertas</h3>
          <div id="opcion5" class="text-left">
       		<ul id="alert_geoc1" title="Nuevas"></ul>
       		<ul id="alert_geoc2" title="Antiguas"></ul>
          </div>
        </div>
    
        </div>
    </div>
   
    <div id="container-map" class="col-sm-12">
      <div id="map-canvas">maps</div>
    </div>
    
  </div>
</div>
<div id="dialog" title="Mensaje">
  <p>Se han cargado alertas</p>
</div>
<div id="dialog_loading" title="Cargando Recorrido">
  <div id="progressbar"></div>
</div>
<script>
	$( "#accordion" ).accordion({
		heightStyle: "fill",
		collapsible: true
	});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPSWiYwgsb-AHamk_0G6oAcTANacBVQzc&callback=myMap"></script>
</body>
</html>
