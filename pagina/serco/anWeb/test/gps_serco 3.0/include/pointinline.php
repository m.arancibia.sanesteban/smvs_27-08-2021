<?php
include_once("db.php");
$pointLocation = new pointLocation();
//echo $pointLocation->distanceCalculation(278,"-69.8586715 -26.314617");

class pointLocation {
	private $geoc_rutas = array();	

	function __construct() {
		global $mysqli;
			
		$sql ="SELECT
				geoc_id, AsText(geoc_poligono) AS poliline
				FROM `geocerca`
				WHERE geoc_tipo = 8 AND geoc_visible = 1";
		
		if ($resultado = $mysqli->query($sql)) {
			while ($fila = $resultado->fetch_assoc())
			{
				$this->geoc_rutas[$fila["geoc_id"]] = $this->clear($fila["poliline"]);			
			}
		}
	}
	function distanceCalculation($id_gc = null, $point){
	
		$points_ruta = $this->geoc_rutas[$id_gc];
		$distance = $this->calculation( $points_ruta, $point);
		return $distance;
		
		if($distance  === NULL){
			
			//$points_ruta = $this->geoc_rutas[$id_gc];
			//$distance = $this->calculation( $points_ruta, $point);	
		}		
	}

	function calculation($points_ruta, $point){
		$distance = NULL;
		for ($i=1; $i < count($points_ruta); $i++){
			$vertex1 = $this->pointStringToCoordinates($points_ruta[$i-1]);
			$vertex2 = $this->pointStringToCoordinates($points_ruta[$i]);
			$pointc = $this->pointStringToCoordinates($point);
			
			$point1_lat = $pointc['x'];
			$point1_long = $pointc['y'];
			
			list($point2_lat,$point2_long) = $this->puntorecta($vertex1,$vertex2,$pointc);
	
			if($point2_lat != 0 and $point2_long != 0){
				$degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
				$temp_dist = $degrees * 111.13384;

				if($distance > $temp_dist || $distance == NULL )
				{
					$distance = $temp_dist;
					$temp_pos  = " $point2_long,$point2_lat";
				}elseif($temp_dist < 0.030){  // MODIFICAR DISTANCIA DEL ALLSEARCH
					break;	
				}
			}
		}
		return ( ( $distance === NULL ) ? NULL : round($distance, 3) );
	}
	
	function puntorecta($a,$b,$c){
			$div = ( pow(($b['x'] - $a['x']),2)  + pow(($b['y']-$a['y']),2) );
			
			if ($div != 0){
				$u =  ( ($c['x']-$a['x']) * ($b['x'] - $a['x'] ) + ( $c['y'] - $a['y']) * ($b['y'] - $a['y'] )) 
				/  $div;
				if(0<$u && $u < 1.50) // dist max
				{
					$x = $a['x'] + $u * ($b['x'] - $a['x'] );
					$y = $a['y'] + $u * ($b['y'] - $a['y'] );
					return array($x, $y);
				}else{
					return NULL;
				}		 
			}
	}

	function pointStringToCoordinates($pointString) {
		$coordinates = explode(" ", $pointString);
		return array("x" => $coordinates[1], "y" => $coordinates[0]);
	}
	
	function clear($geometry){
		$charts = array("POLYGON", "LINESTRING", "(", ")");
		$points = str_replace($charts, "", $geometry);	
		return explode(",", $points);
	}
}
?>