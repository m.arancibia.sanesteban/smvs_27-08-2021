1232 / 
<?php 
	include("../include/db.php");
	include("../include/pointinline.php");
	include("distance.php");

	//$patentes = "'CXSC74','DPLL84','DPRS16','DRHZ69'";
	$patentes = "'CXSC74'";
	
	$date_str = "2018-10-12";
	$date_end = "2018-10-12";

	$sql ="SELECT
				RE.regi_id,
				VE.vehi_patente,
				from_unixtime(RE.regi_fecha_posicion) AS fecha,
				RE.regi_fecha_posicion,
				RE.regi_velocidad,
			GE.geoc_max_speed,
				RE.regi_fecha_posicion AS unix,
				CONCAT( RE.regi_longitud, ' ', RE.regi_latitud ) AS point,
			GE.geoc_nombre,
			GE.geoc_id,
			GE.geoc_tipo,
			GE.geoc_codigo
				FROM `vehiculo` AS VE 
				INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id 
					AND VE.vehi_patente IN($patentes)
					AND RE.regi_fecha_posicion BETWEEN UNIX_TIMESTAMP('$date_str 00:00:00') AND UNIX_TIMESTAMP('$date_end 23:59:59')
				LEFT JOIN `geocerca` AS GE ON 
					GE.geoc_visible = 1
					AND
					(
						(ST_CONTAINS(
						geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
						) AND GE.geoc_tipo IN(1,2,3,4,5,6,7,9))
						OR
						(CONTAINS(
						geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
						) AND GE.geoc_tipo IN(8))
					)
					ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";

	$pointLocation = new pointLocation();
	$part1 = array();
	$range_max =  0.030;
	$temp_end_gc = NULL;
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc())
		{
			$regi_id = $fila['regi_id'];
			

			if( $fila['geoc_tipo'] == NULL and $temp_end_gc != NULL )
			{
				$fila['geoc_id'] = $part1[$temp_end_gc][0]["geoc_id"];
				$fila['geoc_nombre'] = $part1[$temp_end_gc][0]["geoc_nombre"];
				$fila['geoc_tipo'] = $part1[$temp_end_gc][0]["geoc_tipo"];
				$fila['geoc_codigo'] = $part1[$temp_end_gc][0]["geoc_codigo"];
				$fila['geoc_max_speed'] = $part1[$temp_end_gc][0]["geoc_max_speed"];
			}


			if( $fila['geoc_tipo'] == 8 ){
				
				$temp_dist = $pointLocation->distanceCalculation( $fila['geoc_id'], $fila['point'] );
						
				if($temp_dist > $range_max || $temp_dist === NULL ){
					$fila['geoc_id'] = NULL;
					$fila['geoc_nombre'] = NULL;
					$fila['geoc_tipo'] = NULL;
					$fila['geoc_codigo'] = NULL;
					$fila['geoc_max_speed'] = NULL;
				}else{
					$fila["dist"] = $temp_dist;
				}
					$fila["dist"] = $temp_dist;
					
				if($fila['geoc_codigo'] != NULL ){
					
					$temp_end_gc = $regi_id;
					//$fila["ID_POS"] = $temp_end_gc;			
				}elseif ( $temp_dist > 3 )
				{
					$temp_end_gc = NULL;
				}
				
			}
						
			if( isset($part1[$regi_id][0])){
				$temp_row = $part1[$regi_id][0];
					
				switch ($temp_row["geoc_tipo"]) {
					case 1:
						if( $fila["geoc_tipo"] == 2  ){
							$part1[$regi_id][] = $fila;
						}
						break;
					case 3:
						if($fila["geoc_tipo"] == 8 and $temp_dist <= $range_max){
							$part1[$regi_id][0] = $fila;
						}
						break;
					case 8:
						if($fila["geoc_tipo"] == 8 and $temp_row["dist"] > $temp_dist)
						{
							$part1[$regi_id][0] = $fila;
						}
						break;
					case NULL:
							$part1[$regi_id][0] = $fila;
						break;
				}				
			}else{
				$part1[$regi_id][0] = $fila;			
			}
			
			

		}
	}		
	echo count($part1);
	print_r($part1);


?>
