<?php
if( count($_POST) > -1 ) {
	include("../include/db.php");

	$jsondata = array();

	$rec_pat = $_POST["sel_reg"];
	$date_end = $_POST["date_end"];
	$rec_hours = ($_POST["rec_hours"] * 3600);
	
	$temp_date = str_replace("/", "-", $date_end);
	$end_date = strtotime($temp_date);
	$start_date = $end_date - $rec_hours;
	
	if($rec_pat > 0 && $start_date > 0 && $end_date > 0){		
	//	$date = date("Y-m-d  h:i:s", $stdate); 
		$sql = "SELECT
				/*DATE_FORMAT(from_unixtime(regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
				CONCAT(regi_latitud,',',regi_longitud) as pos, */
				CONCAT(RE.regi_longitud,' ',RE.regi_latitud) as pos2
				FROM `vehiculo` AS VE
				INNER JOIN `registro` AS RE ON RE.regi_vehi_id = VE.vehi_id
				WHERE VE.vehi_id = $rec_pat
				AND regi_fecha_posicion BETWEEN $start_date AND $end_date
				ORDER BY regi_id ASC";

				
		$sql = "SELECT 
				CONCAT(RE.regi_longitud,' ',RE.regi_latitud) as pos2 
				FROM 
				( 	SELECT
					vehiculo.vehi_id,
					vehiculo.vehi_patente
					FROM `vehiculo`
					WHERE vehi_id = $rec_pat
				) AS VE 
				INNER JOIN `registro` AS RE 
					ON RE.regi_vehi_id = VE.vehi_id 
					AND RE.regi_fecha_posicion BETWEEN $start_date AND $end_date
					ORDER BY RE.regi_fecha_posicion ASC";
		
		//$jsondata['msj_err'] = $sql;
		$temp = array();
		$i = 0;
		if ($resultado = $mysqli->query($sql)) {
			while ($fila = $resultado->fetch_object()) {
				if( @$temp[($i-1)] != $fila->pos2 ){
					$temp[$i++] = $fila->pos2;	
				}
			}
			$resultado->close();
			$jsondata['pos'] =  "((" . implode(",", $temp) . "))";
			//$s = serialize($jsondata['pos']);
		}
		$mysqli->close();
		$jsondata['msj_err'] = ( $i < 2 ) ? "No se han encontrado datos de unidad seleccionada" : NULL;
	}else{
		$jsondata['msj_err'] = "Falta el ingreso de informacion";	
	}
	
	//$jsondata['msj_err'] = $sql;
	
	header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();
}