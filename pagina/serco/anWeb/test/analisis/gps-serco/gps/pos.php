<?php
$pat_sel = $_POST['pat_sel'];

if( count($_POST) > 1 || $pat_sel != NULL ) {
	include("../include/db.php");


	unset($_POST['pat_sel']);
	
	if(!empty($pat_sel) && $pat_sel != "undefined" ){
		$patentes = $pat_sel;
	}else{
		foreach($_POST as $key => $valor){
				$temp[] = "'$key'";
		};
		$patentes = implode(',',$temp);
	}
	
	$verde = '#00FF00';
	$amarilla = '#FFFF00 ';
	$rojo = '#FF0000 ';
	$azul = '#0000FF';
	$negro = '#000';
	
	//			RE.regi_fecha_posicion AS fecha_unix,
	
	$sql = "SELECT 
			VE.vehi_id,
			RE.regi_latitud AS lat,
			RE.regi_longitud AS lng,
			DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
			RE.regi_azimut AS azimut,
			RE.regi_velocidad AS velocidad,
			(UNIX_TIMESTAMP() - RE.regi_fecha_recibido) AS dif_reg,
			(UNIX_TIMESTAMP() - RE.regi_fecha_posicion) AS dif_time,		
			VE.vehi_patente,
			GE.geoc_nombre
			FROM
				(SELECT 
				regi_vehi_id,
				MAX(regi_id) AS id_max
				FROM `registro` AS RE
				WHERE regi_vehi_id IN($patentes) 
				GROUP BY regi_vehi_id) AS TR
			INNER JOIN `registro` AS RE 
			ON TR.regi_vehi_id = RE.regi_vehi_id AND ( TR.id_max = RE.regi_id OR RE.regi_fecha_recibido > (UNIX_TIMESTAMP()-(60*20)) )
			INNER JOIN `vehiculo` AS VE ON TR.regi_vehi_id = VE.vehi_id
			LEFT JOIN `geocerca` AS GE ON GE.geoc_tipo IN(1,2,4,5,6) AND GE.geoc_visible = 1
			AND	ST_CONTAINS(
			GE.geoc_poligono, PointFromText( CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')' ) )) 
			ORDER BY TR.regi_vehi_id, RE.regi_fecha_posicion DESC, RE.regi_fecha_recibido ASC, GE.geoc_tipo DESC";
	
	$points_pat = array();
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			if( @!$points_pat[$fila["vehi_id"]]["st_move"] )
			{
				if(empty($points_pat[$fila["vehi_id"]])){
					$points_pat[$fila["vehi_id"]] = $fila;
					$points_pat[$fila["vehi_id"]]["st_move"] = ($fila["velocidad"] > 0);
				}else{
					$points_pat[$fila["vehi_id"]]["dif_time"] = $fila["dif_time"];
					//$points_pat[$fila["vehi_id"]]["date"] = $fila["fecha"];
					$points_pat[$fila["vehi_id"]]["st_move"] = ($fila["velocidad"] > 0);
				}
			}
		}
		$resultado->close();
	}

	$jsondata = array();
	$alert_geoc = "";
	if ( count($points_pat)>0) {
		foreach($points_pat as $valor) {
			$animation = ($pat_sel == $valor["vehi_id"]) ? 'BOUNCE' : 'DROP';
			$dif_time = $valor["dif_time"];
			$dif_reg = $valor["dif_reg"];
			$SymbolPath = false;
			$ln_color = "#33f";
							
			switch (true) {
				case $dif_reg > 86400:
					$color = $negro;
					$SymbolPath = true;
					$ln_color = "#ff8000";
					break;	
				case $dif_reg > 900:
					$color = $azul;
					break;	
				case $dif_time < 300:
					$color = $verde;
					break;
				case $dif_time < 1200:
					$color = $amarilla;
					break;
				default:
					$color = $rojo;
					break;
			}			

			if(!empty($valor["geoc_nombre"]))
			{
				$alert_geoc .= "<li>" . $valor["vehi_patente"] . ": " . $valor["geoc_nombre"] . "</li>";
				//$ln_color = "#03fbe8";
				$ln_color = "#f00";
			}			
						
			$pos[$valor["vehi_id"]] = array('lng'=>$valor["lng"], 
										'lat'=>$valor["lat"], 
										'animation'=>$animation, 
										'patente'=>$valor["vehi_patente"],
										'azimut'=>$valor["azimut"],
										'fecha'=>$valor["fecha"],
										'velocidad'=>$valor["velocidad"],
										'ln_color'=>$ln_color,
										'symbolpath' => $SymbolPath,
										'info'=>$valor["geoc_nombre"],
										'color'=>$color);
		}
		//print_r($pos);
		$jsondata["alert_geoc"] = $alert_geoc;
		$jsondata['markers'] = $pos;
	}
	//$jsondata["post"] = serialize($_POST);
	//$jsondata["post"] = $sql;
	$mysqli->close();
}else{
	$jsondata["post"] =  serialize($_POST);
}
	header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();
