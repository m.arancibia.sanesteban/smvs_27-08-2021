<?php

if( count($_POST) > 1 ) {
	include("../include/db.php");

	foreach($_POST as $key => $valor){
		if($key == "pat_sel"){
			$pat_sel = $valor;
		}elseif($valor == "on"){
			$temp[] = "'$key'";
		}
	};
	
	if(!empty($_POST['pat_sel']) && $_POST['pat_sel'] != "" ){
		$patentes = $_POST['pat_sel'];
	}else{
		$patentes = implode(',',$temp);
	}
	//$sql = "SELECT * FROM `ubicaciones` WHERE patente IN($patentes)";

	$verde = '#00FF00';
	$amarilla = '#FFFF00 ';
	$rojo = '#FF0000 ';
	$azul = '#0000FF';

	$sql = "SELECT
			VE.vehi_id,
			RE.regi_latitud AS lat,
			RE.regi_longitud AS lng,
			regi_fecha_posicion AS fecha
			FROM ( 
				SELECT
				Max(regi_id) AS ID
				FROM `registro`
				WHERE regi_vehi_id IN ($patentes) AND regi_id NOT IN (
				SELECT
				Max(regi_id)
				FROM `registro`
				WHERE regi_vehi_id IN ($patentes)
				GROUP BY regi_vehi_id) 
				GROUP BY regi_vehi_id
			) AS MAXR
			INNER JOIN `registro` AS RE ON RE.regi_id = MAXR.ID 
			INNER JOIN `vehiculo` AS VE ON VE.vehi_id = RE.regi_vehi_id";
			
	$time_patente = array();
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_object()) {
			$time_patente[$fila->vehi_id] = array(
										"lat"=>"$fila->lat",
										"lng"=>"$fila->lng",
										"fecha"=>"$fila->fecha"
										);
		}
		$resultado->close();
	}

	$sql = "SELECT
			VE.vehi_id,
			RE.regi_latitud AS lat,
			RE.regi_longitud AS lng,
			DATE_FORMAT(from_unixtime(regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
			regi_fecha_posicion AS fecha_unix,
			RE.regi_azimut AS azimut,
			RE.regi_velocidad AS velocidad,
			VE.vehi_patente
			FROM ( SELECT
			Max(regi_id) AS ID
			FROM `registro`
			WHERE regi_vehi_id IN ($patentes)
			GROUP BY regi_vehi_id ) AS MAXR
			INNER JOIN `registro` AS RE ON RE.regi_id = MAXR.ID 
			INNER JOIN `vehiculo` AS VE ON VE.vehi_id = RE.regi_vehi_id";
	
	$jsondata = array();
	//$jsondata["post"] = serialize($time_patente);
	
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_object()) {
			$animation = ($pat_sel == $fila->vehi_id) ? 'BOUNCE' : 'DROP';
			$patente = (string)$fila->vehi_id;
			$dif_date = 0;

			if ( isset($time_patente[$patente]))
			{	
				$data_temp = $time_patente[$patente];

				if(!empty($data_temp["fecha"]))
				{
					
					$fecha_a =  $fila->fecha_unix;
					$dif_date = ($fecha_a - $data_temp["fecha"]);
	
					if($dif_date < 300){
						$color = $verde;	
					}elseif($dif_date <1200){
						$color = $amarilla;
					}else{
						$color = $rojo;	
					}
				
				}else{
					$color = $rojo;
				}
				
			}else{
				$color = $rojo;	
			}
			
			
			$pos[$fila->vehi_id] = array('lng'=>$fila->lng, 
										'lat'=>$fila->lat, 
										'animation'=>$animation, 
										'patente'=>$fila->vehi_patente,
										'azimut'=>$fila->azimut,
										'fecha'=>$fila->fecha,
										'velocidad'=>$fila->velocidad,
										'color'=>$color,);
		}
		$resultado->close();
		$jsondata['markers'] = $pos;
    }
	$mysqli->close();

	header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();
}