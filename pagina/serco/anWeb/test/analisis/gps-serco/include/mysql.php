<?php
if(defined("MY_SQL"))
	exit();
else
	define("MY_SQL","mysql4");

class sql_db
{
	var $db_connect_id;
	var $query_result;
	var $row = array();
	var $rowset = array();
	var $num_queries = 0;

	function sql_db($sqlserver, $sqluser, $sqlpassword, $database)
	{
		$this->user = $sqluser;
		$this->password = $sqlpassword;
		$this->server = $sqlserver;
		$this->dbname = $database;
		//$this->db_connect_id = mysql_connect($this->server, $this->user, $this->password);
		$this->db_connect_id = new mysqli($this->server, $this->user, $this->password, $this->dbname);
		$this->db_connect_id->set_charset("utf8");
		
		return $this->db_connect_id;
		/*
		if( $this->db_connect_id )
		{
			if( $database != "" )
			{
				$dbselect = mysql_select_db($this->dbname);
				if( !$dbselect )
				{
					mysql_close($this->db_connect_id);
					$this->db_connect_id = $dbselect;
				}
			}

			return $this->db_connect_id;
		}
		else
		{
			return false;
		}*/
	}

	function sql_query($query = "",$line)
	{
		//
		// Remove any pre-existing queries
		//
		unset($this->query_result);

		if( $query != "" )
		{
			$this->num_queries++;
			//$this->query_result = mysql_query($query, $this->db_connect_id) or $this->message_error($line); $mysqli->query
			$this->query_result = $this->db_connect_id->query($query) or $this->message_error($line); 
		}

		if( $this->query_result )
		{
			//unset($this->row[$this->query_result]);
			//unset($this->rowset[$this->query_result]);
			return $this->query_result;
		}
		else
		{
			return false;
		}
	}

	function sql_numrows($query_id = 0)
	{
		$query_id = ( !$query_id ) ? $this->query_result : $query_id;
			
		return ( $query_id ) ? mysql_num_rows($query_id) : false;
	}

	function sql_fetchrow($query_id = 0)
	{
	
		$query_id = ( !$query_id ) ? $this->query_result : $query_id;

		if( $query_id )
		{
			$this->row[$query_id] = mysql_fetch_array($query_id, MYSQL_ASSOC);
			return $this->row[$query_id];
		}
		else
		{
			return false;
		}
	}
	
	function sql_fetchrowset($query_id = 0)
	{

		$query_id = ( !$query_id ) ? $this->query_result : $query_id;

		if( $query_id )
		{
			unset($this->rowset[$query_id]);
			unset($this->row[$query_id]);

			while($this->rowset[$query_id] = mysql_fetch_array($query_id, MYSQL_ASSOC))
			{
				$result[] = $this->rowset[$query_id];
			}

			return $result;
		}
		else
		{
			return false;
		}
	}
	
	
	function sql_affectedrows()
	{
		return ( $this->db_connect_id ) ? mysql_affected_rows($this->db_connect_id) : false;
	}
	
	function sql_nextid()
	{
		return ( $this->db_connect_id ) ? mysql_insert_id($this->db_connect_id) : false;
	}
	
	function sql_freeresult($query_id = 0)
	{
		$query_id = ( !$query_id ) ? $this->query_result : $query_id;

		if ( $query_id )
		{
			unset($this->row[$query_id]);
			unset($this->rowset[$query_id]);

			mysql_free_result($query_id);

			return true;
		}
		else
		{
			return false;
		}
	}

	function sql_close()
	{
		return ( $this->db_connect_id ) ? mysql_close($this->db_connect_id) : false;
	}
	
	function message_error($line)
	{
	$msg_error = $this->sql_error();
	if($msg_error[code] != 1062)
		die( 'Ha ocurrido un error en: ' . basename($_SERVER['PHP_SELF']) . ', linea: ' . $line.', '.$msg_error[message] );
	
	}
	
	function sql_error()
	{
		$result['message'] = mysql_error($this->db_connect_id);
		$result['code'] = mysql_errno($this->db_connect_id);

		return $result;
	}

}
?>