	function parsePolyStrings(ps) {
    	var i, j, lat, lng, tmp, tmpArr = {}, ad_lat = 0, ad_lng = 0;
        arr = [];
		var maxLat = null, minLat = null, maxLng = null, minLng = null;
        m = ps.match(/\([^\(\)]+\)/g);
		if (m !== null) {
			for (i = 0; i < m.length; i++) {
				tmp = m[i].match(/-?\d+\.?\d*/g);
				console.log(tmp);
				if (tmp !== null) {
					for (j = 0, tmpArr = []; j < tmp.length -2 ; j+=2) {						
						lng = Number(tmp[j]) ;
						lat = Number(tmp[j + 1]);
						//tmpArr.push(new google.maps.LatLng(lat, lng));
						tmpArr = {lat:lat, lng:lng};
						arr.push(tmpArr);
						arr[(j/2)] = tmpArr;
						//ad_lng = (lng + ad_lng);
						//ad_lat = (lat + ad_lat);
	
						// OBTENER MAXIMOS
						if(minLat>lat || minLat == null)
						{
							minLat = lat;
						}
						if(maxLat<lat || maxLat == null)
						{
							maxLat = lat;
						}
						if(minLng>lng || minLng == null)
						{
							minLng = lng;
						}
						if(maxLng<lng || maxLng == null)
						{
							maxLng = lng;
						}		
						// FIN MAXIMOS

					}
					//var n = j/2
					//pos = new google.maps.LatLng((ad_lat/n), (ad_lng/n));
					//arr.push(tmpArr);
				}
			}
			
			pos = new google.maps.LatLng(((maxLat+minLat)/2),((maxLng+minLng)/2));
			
			var GLOBE_WIDTH = 356; // a constant in Google's map projection
			var angle = maxLng - minLng;
			if (angle < 0) angle += 360
			var angle2 = maxLat - minLat;
			if (angle2 > angle) angle = angle2;
			
			var zoomfactor = (angle>0)? (Math.round(Math.log(960 * 360 / angle / GLOBE_WIDTH) / Math.LN2 - 0.7)) : 19;
			//alert(zoomfactor);	
		}
		return { arr:arr, pos:pos, zoom: zoomfactor};
	}