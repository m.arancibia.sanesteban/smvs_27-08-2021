	var map, markers = [], geoc = [], infoWindow = [], infoWindowG, recorrido = null;
	updatemyGps();
	
	function myMap() {
		var mapCanvas =  $("#map-canvas")[0];
		var myCenter = new google.maps.LatLng(-23.199948, -69.647242);		
		var mapOptions = {
			center: myCenter,
			zoom: 8,
		};
		map = new google.maps.Map(mapCanvas, mapOptions);
	}
		
	function updatemyGps(time) {
		window.setTimeout(function(){updatemyGps();},10000);
		console.log("update GPS");
		$("input#pat_sel[type=hidden]").attr("value",undefined);
		myGps();
	}
	
	function myRec() {
		var url = "gps/req.php";
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: $("form#recorrido").serialize(),
			success: function(data)
			{
				//alert(data.sql);
				var poli = parsePolyStrings(data.pos);
				map.setCenter(poli.pos); 
				map.setZoom(poli.zoom);				
				if(recorrido != null){
					recorrido.setPath(poli.arr);
				}else{
					var lineSymbol = {
          				path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
        			}
					
					recorrido = new google.maps.Polyline({   //Polyline
						path: poli.arr,
						map: map,
						icons: [{
							icon: lineSymbol,
							offset: '0%',
							repeat: '100px'
						}],
						geodesic: true,
				  		strokeColor: '#00FF00',
					  	strokeOpacity: 1.0,
					  	strokeWeight: 2
					});
					animateCircle(recorrido);		
				}
				
			}
		});
		
	}
	
	function animateCircle(line) {
    	var count = 0;
    	window.setInterval(function() {
      		count = (count + 1) % 3000;
			var icons = line.get('icons');
			var f = (count /20) + '%';
			icons[0].offset = (count /30) + '%';
			line.set('icons', icons);
		}, 150);
	}

	function myGeocerca() {
		var url = "gps/geo.php";
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: $("form#geocercas").serialize(),
			success: function(data)
			{
				//alert(data.post);
				$("form#geocercas ul#something input[type*='checkbox']").prop('checked', false);
				var color = data.color;
				var poli = parsePolyStrings(data.poligono);				
				geoc[data.id] = [];
				geoc[data.id]["center"] = poli.pos;
				map.setCenter( poli.pos ); 
				map.setZoom(poli.zoom);
				
				geoc[data.id]["polygon"] = new google.maps.Polygon({   //Polyline
				  paths: poli.arr, //path
				  map: map,
				  strokeColor: '#00FF00',
				  strokeOpacity: 1,
				  strokeWeight: 3,
				  fillColor: color,
				  fillOpacity: 0.3
				});
				infoWindowG = new google.maps.InfoWindow();
				(function(polygon, pos, data){
					google.maps.event.addListener( polygon, 'click', function(event) {
						var contentString = '<h5><b>GEOCERCA DATOS</b></h5>' +
							'<b>AREA: </b>	' + data.nombre + '<br>'+
							'<b>GC_ID: </b>	' + data.codigo + '<br>'+
							'<b>LATITUD: </b>	' + pos.lat() + '<br>'+
							'<b>LONGITUD: </b>	' + pos.lng() + '<br><br>';					
						infoWindowG.setContent(contentString);
						infoWindowG.setPosition(event.latLng);
						infoWindowG.open(map, polygon);
					});			
				})(geoc[data.id]["polygon"], poli.pos, data );
			}
		});
	}

	function myGps() {
		var url = "gps/pos.php";
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: $("form#patent").serialize(),
			success: function(data)
			{
				//alert(data.alert_geoc);
				if( data.alert_geoc != "" &&  data.alert_geoc != null &&  data.alert_geoc != undefined && data.alert_geoc != $("div ul#alert_geoc").html() )
				{
					$("div ul#alert_geoc").html(data.alert_geoc + $("div ul#alert_geoc").html());
					//alert($("div ul#alert_geoc").html());
					//alert("Se han cargado alertas");
					$("div#dialog  p").text("Se han cargado alertas");
					$( "#dialog" ).dialog();
				}
			
				$.each(data.markers, function(i, marker) 
				{
					if( $("form#patent ul li div input#" + i + "[type*='checkbox']:checked").length ==  1 )
					{	
						var myposition = new google.maps.LatLng(marker.lat, marker.lng);

						if(marker.animation == 'BOUNCE'){
							animation = google.maps.Animation.BOUNCE;
							map.panTo(myposition);
						}else{
							animation = null;		
						}
				
						contentString = "<div><strong>Patente " + marker.patente + "</strong><br/><b>" + marker.velocidad 
									+ " km/h</b><br/>" + marker.fecha + "</div>";
					  	var azimut = marker.azimut * 1;
					  	var color = marker.color;									
						var icon = {
									path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
									fillColor: color,
									fillOpacity: 100,
									strokeColor: '#33f',
									strokeWeight: 1,
									scale: 4,
									rotation: azimut};
									
						if (markers[i] != null) {
							markers[i].setPosition(myposition);
							markers[i].setIcon(icon);
							markers[i].setAnimation(animation);							
							if( infoWindow[i] != undefined){
								infoWindow[i].setContent(contentString);					
							}
 						}else{
							markers[i] = new google.maps.Marker({
								position: myposition,
								map: map,
								icon:icon,
								title: 'Patente: ' + marker.patente,
								animation: animation
							});
						 	infoWindow[i] = new google.maps.InfoWindow();
							(function(marker, contentString) {
								google.maps.event.addListener(marker, 'click', function() {										
									infoWindow[i].setContent(contentString);
									infoWindow[i].open(map, marker);
									map.setCenter(marker.getPosition());
								});
							})(markers[i], contentString);	
						}
					}			
				});			
			}
		});
	}	