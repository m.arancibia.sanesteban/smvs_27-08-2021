<!DOCTYPE html>
<html lang="en">
<head>
<title>GPS</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
    
  <script src="//code.jquery.com/jquery-1.12.4.js"></script>
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<script src="js/menu-desp.js"></script>
 	<script src="js/searchs.js"></script>
   	<script src="js/parsePoly.js"></script>
   	<script src="js/gps.js"></script>
 	<link href="css/style.css" rel="stylesheet" media="screen">

	<script src="./js/jquery-ui-timepicker-addon.js"></script>
	<link href="./css/jquery-ui-timepicker-addon.css" rel="stylesheet" media="screen">
    
    <script>
	//$(document).ready(function() {
	$( function() {		
	   	$( "#datepicker" ).datetimepicker({
			dateFormat: "dd-mm-yy",
			timeFormat: 'HH:mm'
		});

		searchp();
		searchg();
		$("ul li div input[type*='checkbox']").prop('checked', false);

		$("form#patent ul#not-these" ).on( "click", "label", function(){
			id_patente = $(this).attr("id").replace("_lb", "");;
			myposition = markers[id_patente].getPosition();
			map.panTo(myposition);	
		});
		$("form#geocercas ul#not-these" ).on( "click", "label", function(){
			id_geoc = $(this).attr("id").replace("_go", "");
			myposition = geoc[id_geoc]["center"];
			map.panTo(myposition);
		});
		
		
		$("form#recorrido" ).on( "click", "select#sel_reg", function(){
			myRec();	
		});
		
		$("form#geocercas" ).on( "click", "input[type=checkbox]", function(){
			var id_ul = $(this).parents("ul").attr("id");	
			console.log(id_ul)
			if ( id_ul == 'something' )
			{
				$(this).parent().attr("class","hidden");
				var id = "form#geocercas input[id="+ $(this).attr("name") +"_gch]";
				$(id).prop('checked',true);
				$(id).parent().attr("class","show");
				myGeocerca();
			}else if(id_ul == 'not-these'){
				$(this).parent().attr("class","hidden");
				var id_gc = $(this).attr("id").replace("_gch", "");
				var id = "form#geocercas input[id="+ id_gc +"_gc]"
				$(id).parent().attr("class","show");
				$(id).prop('checked',false);
				geoc[id_gc]['polygon'].setMap(null);
			}
		});
				
		$("form#patent" ).on( "click", "input[type=checkbox]", function(){				
			var id_ul = $(this).parents("ul").attr("id");	
			console.log(id_ul);
			if ( id_ul == 'something' )
			{
				$("input#pat_sel[type=hidden]").attr("value",$(this).attr("name"));
				$(this).parent().attr("class","hidden");
				var id = "form#patent input[id="+ $(this).attr("name") +"_id]";
				$(id).prop('checked',true);
				$(id).parent().attr("class","show");
				myGps();
				$("input#pat_sel[type=hidden]").attr("value","");
			}else if(id_ul == 'not-these'){
				$(this).parent().attr("class","hidden");
				var patente = $(this).attr("id").replace("_id", "");
				var id = "form#patent input[id="+ patente +"]";
				$(id).parent().attr("class","show");
				$(id).prop('checked',false);
				markers[patente].setMap(null);
				markers[patente] = null;
			}	
		});
	});
</script>
    
</head>
<body>



<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="col-md-9 col-sm-7"> <a class="navbar-brand" href="#">Sercoing</a> </div>
    <div class="col-md-3 col-sm-5" id="myNavbar"> 
      <!-- navbar-header  // collapse navbar-collapse
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Projects</a></li>
        <li><a href="#">Contact</a></li>
      </ul>-->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Log-out</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid content">
  <div class="row">

	<div class="container-fluid contenedor--div">
        <div class="icono--div">
            <div class="icono"><a href="#"><span class="raya"></span><span class="raya"></span><span class="raya"></span></a></div>
        </div>

        <div class="contenedor--menu">
        <div id="accordion" class="sidenav">

          <h3>Patentes</h3>         
          <div id="opcion1" class="opciones text-left">
            <div><input name="d" placeholder="Buscar Patente" id="search_p" type="text"></div>
            <form id="patent" class="list" action="#" method="post">
              <input type="hidden" name="pat_sel" id="pat_sel" />
    <?php
        include("./include/db.php");
        $sql = "SELECT
                max(vehi_id) as id,
                vehi_patente as patente
                FROM vehiculo
                GROUP BY patente
                ORDER BY 2 ASC";
        $li1 = null; 
        $li2 = null;
		$op_sel = null;
        if ($resultado = $mysqli->query($sql)) {
            while ($fila = $resultado->fetch_object()) {
				$op_sel .= '<option value="'.$fila->id.'">'.$fila->patente.'</option>\n';
                $li1 .= '    <li>
            <div class="show">
                <input type="checkbox" name="'.$fila->id.'" id="'.$fila->id.'" />
                <label for="'.$fila->id.'">'.$fila->patente.'</label>
            </div>
        </li>';
                $li2 .= '    <li>
            <div class="hidden">
                <input type="checkbox" id="'.$fila->id.'_id" />
                <label id="'.$fila->id.'_lb">'.$fila->patente.'</label>
            </div>
        </li>';
            }
            $resultado->close();
        }
        $mysqli->close();
    ?>
              <ul id="something">
                <?php echo $li1; ?>
              </ul>
              <ul id="not-these">
                <?php echo $li2; ?>
              </ul>
            </form>
          </div>
          
          <h3>Geocercas</h3>   
          <div id="opcion2" class="opciones text-left">
            <div><input name="g" id="search_g" placeholder="Buscar Geocerca" type="text"></div>
            <form id="geocercas" class="list" action="#" method="post">
    <?php
        include("./include/db.php");
            $sql = "SELECT
                GE.geoc_id AS id,
                GE.geoc_nombre,
                GE.geoc_codigo,
                GE.geoc_empr_id,
                EM.empr_nombre
                FROM `geocerca` AS GE
                INNER JOIN empresa AS EM ON GE.geoc_empr_id = EM.empr_id
                ORDER BY 4 ASC, 3 ASC";
        $li1 = null; 
        $li2 = null;
        $temp = null;
        if ($resultado = $mysqli->query($sql)) {
            while ($fila = $resultado->fetch_object()) {
                if($temp == null || $temp != $fila->geoc_empr_id ){
                $li1 .= '    <li id="title"><h5><strong>'.$fila->empr_nombre.'</strong></h5></li>';
                $temp = $fila->geoc_empr_id;
                }
                $li1 .= '    <li>
            <div class="show">
                <input type="checkbox" name="'.$fila->id.'" id="'.$fila->id.'_gc" />
                <label for="'.$fila->id.'_gc" title="'.$fila->geoc_nombre.'">'.$fila->geoc_codigo.'</label>
            </div>
        </li>';
                $li2 .= '    <li>
            <div class="hidden">
                <input type="checkbox" id="'.$fila->id.'_gch" />
                <label id="'.$fila->id.'_go" title="'.$fila->geoc_nombre.'">'.$fila->geoc_codigo.'</label>
            </div>
        </li>';
            }
            $resultado->close();
        }
        $mysqli->close();
    ?>
              <ul id="something">
                <?php echo $li1; ?>
              </ul>
              <ul id="not-these">
                <?php echo $li2; ?>
              </ul>
            </form>
          </div>
    
          <h3>Rutas</h3>
          <div id="opcion3" class="text-left">
          	
            <p id="">List RUTAS.</p>
          </div>
          
          <h3>Recorridos</h3>
          
          <div id="opcion4" class="text-left">
	          <form id="recorrido" class="list" action="#" method="post">
 	            <label for="rec_hours"><b>Tiempo de seguimiento</b></label>
                <div>      
                    <select name="rec_hours" id="rec_hours">
                        <option value="1">1 hora</option>
                        <option value="2">2 horas</option>
                        <option value="6">6 horas</option>
                        <option value="8">8 horas</option>
                        <option value="12">12 horas</option>
                        <option value="24">24 horas</option>
                        <option value="48">48 horas</option>
                    </select>
                </div>
                <label for="datepicker"><b>Fecha termino</b></label>
                <div><input type="text" id="datepicker" name="date_end"></div>
                <label for="sel_reg"><b>Patentes</b></label>
                <div>
                    <select name="sel_reg" size="10"  id="sel_reg">
                        <?php echo$op_sel; ?>
                    </select>	
                </div>
			</form>
          </div>          
          <h3>Alertas</h3>
          <div id="opcion5" class="text-left">
       		<ul id="alert_geoc"></ul>
          </div>
        </div>
    
        </div>
    </div>
   
    <div id="container-map" class="col-sm-12">
      <div id="map-canvas">maps</div>
    </div>
    
  </div>
</div>
<div id="dialog" title="Mensaje">
  <p>Se han cargado alertas</p>
</div>
<script>
	$( "#accordion" ).accordion({
		heightStyle: "fill",
		collapsible: true
	});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPSWiYwgsb-AHamk_0G6oAcTANacBVQzc&callback=myMap"></script>
</body>
</html>
