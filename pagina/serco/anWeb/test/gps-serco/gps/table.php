﻿<html>
<head>
<style>

  thead {
    color:green;
  }
  tbody {
    color:black;
  }
  tfoot {
    color:red;
  }
  table{
    width: auto;
    margin: 0 auto; 
  }
  table, th, td {
      border: 1px solid black;
      font-family: Calibri,sans-serif; 
      font-size: .9em;
      border-spacing: 0;
  }
  th{
    background-color: #8D0404;
    color: white;
  }
 

</style>
</head>

<body>

<table>
  <thead>
    <tr>
      <th>PPU</th>
      <th>CODGC</th>
      <th>IDGC</th>
      <th>NOMGC</th>
      <th>HORA INGRESO</th>
      <th>HORA SALIDA</th>
      <th>TIEMPO [MIN]</th>
      <th>DIST [KM]</th>
      <th>VEL PROM [KM/H]</th>
      <th>VEL MAX [KM/H]</th>
      <th>POINT</th>
      <th>HORA EXCESO</th>      
      <th>TIPO GC</th>      
    </tr>
  </thead>
  <tbody>
 <?php
 set_time_limit (3600);
 
 $patentes = array(
"JDDW32",
"JDDW34",
"DPTR82",
"JXZT23",
"JXWH88",
"DRHZ66",
"GJDP30",
"KDPX33",
"HWGK50",
"HBCX93",
"HKBT22",
"JPZC89",
"GHYK94",
"CFRG10",
"CRDP12",
"JLWC19",
"DPYD78",
"DRCX98",
"FYFP90",
"GHYK98",
"HHDP96",
"HBDP72",
"HBDS54",
"HHDP89",
"HHDP99",
"HWVX67",
"JPZC87",
"KBXC51",
"KJHP69",
"FCLX82",
"GZBF74",
"HCTR39",
"JGBB77",
"JWYK70",
"DSBB68"
);  // modificar patentes


  include("divisiones.php");  
  
  $date = "2018-12-18"; // modificar fechas 
 //$record = record($patentes, $date, true); //divisional - REPORTE ESPECIAL
 $record = record($patentes, $date, false); //todas las GC - REPORTE NORMAL 
  if(is_array($record)){
  foreach($record  as $var){
    if($var["geoc_nombre"] != "SxOP"){

?>
    <tr>
      <td><?php echo $var["patente"]; ?></td>
      <td><?php echo $var["geoc_id"]; ?></td>
      <td><?php echo $var["geoc_codigo"]; ?></td>
      <td><?php echo utf8_encode($var["geoc_nombre"]); ?></td>
      <td><?php echo $var["date_in"]; ?></td>
      <td><?php echo $var["date_out"]; ?></td>
      <td><?php echo $var["date_dif"]; ?></td>
      <td><?php echo str_replace(".",",",$var["dist_rec"]); ?></td>
      <td><?php echo str_replace(".",",",$var["promediovel"]); ?></td>
      <td><?php echo $var["max"]; ?></td>
      <td><?php echo $var["point"]; ?></td>
      <td><?php echo $var["hora"]; ?></td> 
      <td><?php echo $var["geoc_tipo"]; ?></td>       
  </tr>
<?php
    }
  }
  }
  /*
    }
  }*/
?> 
  </tbody>
</table>
</body>
</html>