<?php

class distance_geom {
    public $variable_km =  111.19457696;
 
    function Sum_array($positions) {
		//$LonLat = explode(',', $positions);
		$distance = $this->LineString($positions);
		list($num, $dec) = explode('.',$distance);
		if($num == 0 ){		
			$mtr = str_pad($dec, 5, "0", STR_PAD_RIGHT)/100;
			return round( $mtr ,2 ) . ' m';
		}else{
			return round( $distance ,3 ) . ' km';
		}	 
    }  
	
	function Glength($lat1, $lon1, $lat2, $long2) {
		$degrees = rad2deg(acos((sin(deg2rad($lat1))*sin(deg2rad($lat2))) + (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($lon1-$long2)))));
		$distance = $degrees * $this->variable_km;
		return round( $distance ,5 );
    }
	
	function LineString($LonLat){
		$ContLength = 0;
		foreach($LonLat as $pos){
			list($Lon,$Lat) = explode(' ', trim($pos));
			if( $Lat < $Lon )
			{
				die("ERROR: Formato no valido");
			}else if(isset($Lat_temp) && isset($Lon_temp) && ( $Lat != $Lat_temp || $Lon != $Lon_temp ))
			{
				$ContLength += $this->Glength($Lat, $Lon, $Lat_temp, $Lon_temp);
			}
			$Lat_temp = $Lat;
			$Lon_temp = $Lon;
		}
		return $ContLength;
	}	
}
//$distance = new distance_geom();
//$distance->Sum_array(" -32.8329580 -70.5985540 , -26.4408596 -69.4788711 , -26.4409276 -69.4787101 , -27.373327 -70.339894 ");

?>

