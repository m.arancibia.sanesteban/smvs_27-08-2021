var maxLat = null, minLat = null, maxLng = null, minLng = null;
	function reset_max_min() {
		maxLat = null; minLat = null; maxLng = null; minLng = null;
	}
	function parsePolyStrings(ps, op, resets) {
    	var i, j, lat, lng, tmp, tmpArr = {}, arr = [];
		$limit = (op) ? 2: 0;
		if(!resets)
		{
			reset_max_min();
		}
        m = ps.match(/\([^\(\)]+\)/g);
		if (m !== null) {
			for (i = 0; i < m.length; i++) {
				tmp = m[i].match(/-?\d+\.?\d*/g);
				//console.log(tmp);
				if (tmp !== null) {
					for (j = 0, tmpArr = []; j < tmp.length - $limit ; j+=2) {						
						lng = Number(tmp[j]) ;
						lat = Number(tmp[j + 1]);
						tmpArr = {lat:lat, lng:lng};	//tmpArr.push(new google.maps.LatLng(lat, lng));
						arr.push(tmpArr);				//arr[(j/2)] = tmpArr;
	
						// OBTENER MAXIMOS
						if(minLat>lat || minLat == null)
						{
							minLat = lat;
						}
						if(maxLat<lat || maxLat == null)
						{
							maxLat = lat;
						}
						if(minLng>lng || minLng == null)
						{
							minLng = lng;
						}
						if(maxLng<lng || maxLng == null)
						{
							maxLng = lng;
						}		
						// FIN MAXIMOS
					}
				}
			}
			
			pos = new google.maps.LatLng(((maxLat+minLat)/2),((maxLng+minLng)/2));
			
			var GLOBE_WIDTH = 356; // a constant in Google's map projection
			var angle = maxLng - minLng;
			if (angle < 0) angle += 360
			var angle2 = maxLat - minLat;
			if (angle2 > angle) angle = angle2;	
			var zoomfactor = (angle>0)? (Math.round(Math.log(960 * 360 / angle / GLOBE_WIDTH) / Math.LN2 - 0.7)) : 19;
		}
		return { arr:arr, pos:pos, zoom: zoomfactor};
	}