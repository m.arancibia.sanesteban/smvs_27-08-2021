<?php
if( count($_POST) > -1 ) {
	include("../include/db.php");

	$jsondata = array();

	foreach($_POST as $key => $valor){
		if($valor == "on"){
			$geoc_codigo = $key;
		}
	};
	if(!empty($geoc_codigo)){		
		$sql = "SELECT 
				geoc_nombre,
				geoc_color,
				geoc_max_speed,
				AsText(geoc_poligono) AS poligono,
				geoc_max_speed
			 FROM `geocerca` 
				WHERE geoc_tipo = 3 AND geoc_codigo = '$geoc_codigo'";
		$temp = array();
		$i = 0;
		if ($resultado = $mysqli->query($sql)) {
			while ($fila = $resultado->fetch_object()) {
				$temp[$i]['points'] =  $fila->poligono;
				$temp[$i]['color'] = $fila->geoc_color;
				$temp[$i]['max'] = $fila->geoc_max_speed;
				$temp[$i]['geoc_codigo'] = $geoc_codigo;
				$temp[$i++]['nombre'] =  $fila->geoc_nombre;
			}
			$jsondata['route'] = $temp;
			$resultado->close();
		}else{
			$jsondata['msj_err'] = "No se han encontrado datos";
		}
		$mysqli->close();		
	}else{
		$jsondata['msj_err'] = "Falta el ingreso de informacion";	
	}
	$jsondata['route'] = $temp;
	//$jsondata['post'] = $sql;
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();
}