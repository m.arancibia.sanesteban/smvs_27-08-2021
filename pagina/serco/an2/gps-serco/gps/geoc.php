<?php
	include("../include/db.php");
	include("distance.php");
	
	$sql = 'SELECT
	RE.regi_id,
	from_unixtime(RE.regi_fecha_posicion) AS fecha,
	RE.point,
	RE.regi_fecha_posicion AS unix,
	RE.regi_velocidad,
	GE.geoc_id,
	GE.geoc_codigo,
	GE.geoc_nombre,
	GE.geoc_tipo
  FROM `geocerca` AS GE
		INNER JOIN 
(	SELECT
	RE.regi_id,
	RE.regi_fecha_posicion,
	RE.regi_velocidad,
	CONCAT( RE.regi_longitud, " ", RE.regi_latitud ) AS point
	FROM `registro` AS RE
	WHERE RE.regi_vehi_id = 1700602 AND from_unixtime(RE.regi_fecha_posicion) BETWEEN "2018-08-28 00:00:00" AND "2018-08-28 23:59:59"
	ORDER BY 1 DESC
) AS RE ON 
	GE.geoc_tipo IN(0,1) 
	WHERE	ST_CONTAINS(
	geoc_poligono, PointFromText(CONCAT( "Point(", point, ")")))
	ORDER BY 2 ASC';
	
	$sql = 'SELECT
	RE.regi_id,
	from_unixtime(RE.regi_fecha_posicion) AS fecha,
	RE.point,
	RE.regi_fecha_posicion AS unix,
	RE.regi_velocidad,
	GE.geoc_id,
	GE.geoc_codigo,
	GE.geoc_nombre,
	GE.geoc_tipo
  FROM 
(	SELECT
	RE.regi_id,
	RE.regi_fecha_posicion,
	RE.regi_velocidad,
	CONCAT( RE.regi_longitud, " ", RE.regi_latitud ) AS point
	FROM `registro` AS RE
	WHERE RE.regi_vehi_id = 1700602 AND from_unixtime(RE.regi_fecha_posicion) BETWEEN "2018-08-28 00:00:00" AND "2018-08-28 23:59:59"
	ORDER BY 1 DESC
) AS RE
	LEFT JOIN `geocerca` AS GE ON 
	GE.geoc_tipo IN(0,1) 
	AND	ST_CONTAINS(
	geoc_poligono, PointFromText(CONCAT( "Point(", point, ")")))
	ORDER BY 2 ASC';

	$path = 0;
	$geoc_temp = NULL;
	$discharge = false;
	$first = true;
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			$geoc_tipo = ( $fila['geoc_tipo'] == NULL )? 99 : $fila['geoc_tipo'];
			$geoc_id = ( $fila['geoc_id'] == NULL )? 99 : $fila['geoc_id'];
					
			$bool1 = $geoc_tipo == 0 && $geoc_id != @$geoc_temp[$geoc_tipo]['geoc_id'];
			$bool2 = $geoc_tipo == 1 && $geoc_id != @$geoc_temp[$geoc_tipo]['geoc_id'] || $discharge ;
			$bool4 = @$geoc_temp[$geoc_tipo]['regi_velocidad'] == 0 && $fila['regi_velocidad']  > 0;
			$bool5 = @$geoc_temp[$geoc_tipo]['regi_velocidad'] > 0 && $fila['regi_velocidad']  == 0;
			$bool3 = $geoc_tipo == 99 && ( @$geoc_temp[$geoc_tipo]['geoc_id'] != NULL || $bool4 || $bool5 );
			
			if($bool3 || ( empty($fila['geoc_nombre']) && empty($fila['geoc_codigo']) )  ){
				$fila['geoc_codigo'] = "IN_ROUTE";
				$fila['geoc_nombre'] = ( $fila['regi_velocidad']  == 0)? "STOP":"MOVE";
			}
			
			if( $first || $bool1 || $bool2 || $bool3 ){
				$id_geoc[$geoc_id]['path'] = $path++;
				$id_geoc[$geoc_id]['op'] = 'STAR';
				$first = false;
				if( $bool2 ){
					$discharge = false; // activar en geocerca de descarga
				}
			}else{
				$id_geoc[$geoc_id]['op'] = 'END';
			}
			$record = $id_geoc[$geoc_id]['path'];
			$op = $id_geoc[$geoc_id]['op'];
			
			$polygons[$record][$op] = $fila;
			$polygons[$record]["points"][] =  $fila["point"];				
			
			$geoc_temp[$geoc_tipo] = $fila;
		}
	}	
	//print_r($polygons);

	//----------------------------------------------------------------------------
	/*
	$polygons = array();
	$geoc_temp = NULL;
	$record = 0;
	
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			if($geoc_temp['geoc_id'] != $fila['geoc_id'] OR $geoc_temp == NULL ){
				$record++;
				$polygons[$record]["STAR"] = $fila;
				$polygons[$record]["points"] = array();
			}
			if( $geoc_temp != NULL ){
				$polygons[$record]["END"] = $fila;			
			}
			if( $fila["regi_velocidad"] > 0 AND  $fila["geoc_tipo"] = 0){
				$polygons[$record]["points"][] =  $fila["point"];
			}
			$geoc_temp = $fila;
		}
	}*/
	$distance = new distance_geom();	

	foreach ($polygons as $key => $valor) {
		
		$value = $valor["STAR"];				
		
		if( count($valor["points"]) > 1 && $value["geoc_nombre"] != "STOP"){ //&& $value["geoc_id"] != NULL 
			$points_dist = $valor["points"];
			$dist_rec = $distance->Sum_array($points_dist);
		}else{
			$dist_rec = "0 m";
		}
		
		if(!isset($valor["END"])){
			$valor["END"] = $valor["STAR"];	
		}

		$data[$key]["geoc_id"] = $value["geoc_id"];
		$data[$key]["geoc_codigo"] = $value["geoc_codigo"];
		$data[$key]["geoc_nombre"] = $value["geoc_nombre"];
		$data[$key]["date_in"] = $value["fecha"];
		$data[$key]["date_out"] = $valor["END"]["fecha"];
		$data[$key]["date_dif"] =  round(($valor["END"]["unix"]-$value["unix"])/60) . " min";
		$data[$key]["dist_rec"] = $dist_rec;
	}
	print_r($data);
?>