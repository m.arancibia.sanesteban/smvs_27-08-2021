<?php

if( count($_POST) > -1 ) {
	include("../include/db.php");

	$rec_pat = $_POST["sel_reg"];
	$date_end = $_POST["date_end"];
	$rec_hours = ($_POST["rec_hours"] * 3600);
	
	$temp_date = str_replace("/", "-", $date_end);
	$end_date = strtotime($temp_date);
	$start_date = $end_date - $rec_hours;

	if($temp_date > 0 && $start_date > 0 && $end_date > 0){		
	//	$date = date("Y-m-d  h:i:s", $stdate); 
		$sql = "SELECT
				DATE_FORMAT(from_unixtime(regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
				CONCAT(regi_latitud,',',regi_longitud) as pos,
				CONCAT(regi_longitud,' ',regi_latitud) as pos2
				FROM `registro`
				WHERE
				registro.regi_vehi_id = $rec_pat
				AND regi_fecha_posicion BETWEEN $start_date AND $end_date
				ORDER BY regi_id ASC";
		
		
		$jsondata = array();
		$jsondata['sql'] = $sql;
		$temp = array();
		if ($resultado = $mysqli->query($sql)) {
			while ($fila = $resultado->fetch_object()) {
				//$jsondata['pos'][] = $fila->pos;
				$temp[] = $fila->pos2;
			}
			$resultado->close();
			$jsondata['pos'] =  "((" . implode(",", $temp) . "))";
		}
	}	
	$mysqli->close();
	
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();
	
}