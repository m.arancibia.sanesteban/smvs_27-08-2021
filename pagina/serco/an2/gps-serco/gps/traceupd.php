<?php
if( count($_POST) > -1 ) {
	include("../include/db.php");
	include("distance.php");
	
	$jsondata = array();

	$rec_pat = $_POST["sel_reg"];
	$date_end = $_POST["date_end"];
	$rec_hours = ($_POST["rec_hours"] * 3600);
	
	$temp_date = str_replace("/", "-", $date_end);
	$end_date = strtotime($temp_date);
	$start_date = $end_date - $rec_hours;
	
	$rec_pat = 1700566;
	$start_date = 1537498740 ;
	$end_date = 1537585140 ;
	
	if($rec_pat > 0 && $start_date > 0 && $end_date > 0){		

	$sql = "SELECT
	RE.regi_id,
	RE.vehi_patente,
	from_unixtime(RE.regi_fecha_posicion) AS fecha,
	RE.point,
	RE.regi_fecha_posicion AS unix,
	RE.regi_velocidad,
	GE.geoc_nombre,
	GE.geoc_id,
	GE.geoc_tipo,
	GE.geoc_codigo
  FROM 
	(SELECT
		RE.regi_id,
		VE.vehi_patente,
		RE.regi_fecha_posicion,
		RE.regi_velocidad,
		CONCAT( RE.regi_longitud, ' ', RE.regi_latitud ) AS point
		FROM `vehiculo` AS VE 
		INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id 
		AND RE.regi_fecha_posicion BETWEEN $start_date AND $end_date
		AND RE.regi_vehi_id = $rec_pat
	ORDER BY 1 DESC
		) AS RE 
LEFT JOIN `geocerca` AS GE ON 
			GE.geoc_tipo IN(2,4,5,6,7) AND GE.geoc_visible = 1
			AND	ST_CONTAINS(
			geoc_poligono, PointFromText(CONCAT( 'Point(', point, ')')))
			ORDER BY vehi_patente ASC, fecha ASC, regi_id ASC, geoc_tipo ASC";



	$path = 0;
	$geoc_temp = NULL;
	$discharge = false;
	$first = true;
	$polygons = array();
	$geoc_div = 1;
	
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc())
		{
			$geoc_tipo = ( $fila['geoc_tipo'] == NULL )? 99 : $fila['geoc_tipo'];
			$geoc_id = ( $fila['geoc_id'] == NULL )? 99 : $fila['geoc_id'];
			
			if($geoc_tipo != $geoc_div && isset($geoc_temp[1]) ){
				if( $geoc_temp[$geoc_div]['regi_id'] != $fila['regi_id'] ){			
					$path_div = $id_geoc[$geoc_temp[$geoc_div]['geoc_id']]['path'];
					$polygons[$path_div]['END'] = $geoc_temp[$geoc_div];
					// echo $geoc_temp[1]['regi_id'] ."  " . $fila['regi_id']. " /\n";
					unset($geoc_temp[$geoc_div]);
					$discharge = true;
				}
			}
					
			$bool1 = in_array($geoc_tipo, array(2,4,5,6)) && $geoc_id != @$geoc_temp[$geoc_tipo]['geoc_id'];  // E L I M I N A R
			
			$bool2 = $geoc_tipo == 1 && $geoc_id != @$geoc_temp[$geoc_tipo]['geoc_id'] || $discharge ;
			$bool4 = @$geoc_temp[$geoc_tipo]['regi_velocidad'] == 0 && $fila['regi_velocidad']  > 0;
			$bool5 = @$geoc_temp[$geoc_tipo]['regi_velocidad'] > 0 && $fila['regi_velocidad']  == 0;
			$bool3 = $geoc_tipo == 99 && ( @$geoc_temp[$geoc_tipo]['geoc_id'] != NULL || $bool4 || $bool5 || $discharge );
			
			
			$bool6 = (@$geoc_temp[$geoc_tipo]['vehi_patente'] !=  $fila['vehi_patente']);
			$bool7 = (( $path - @$id_geoc[$geoc_id]['path']) > 1 ) && $geoc_tipo > 1;
			
			if($bool3 || ( empty($fila['geoc_nombre']) && empty($fila['geoc_codigo']) )  ){
				$fila['geoc_codigo'] = "IN_ROUTE";
				$fila['geoc_nombre'] = ( $fila['regi_velocidad']  == 0)? "STOP" : "MOVE";
			}
	
			
			if( $first || $bool1 || $bool2 || $bool3 || $bool6 || $bool7 ){
				$id_geoc[$geoc_id]['path'] = $path++;
				$id_geoc[$geoc_id]['op'] = 'STAR';
				$first = false;
				if( $bool2 ){
					$discharge = false; // activar en geocerca de descarga
				}
			}else{
				$id_geoc[$geoc_id]['op'] = 'END';
			}
			$record = $id_geoc[$geoc_id]['path'];
			$op = $id_geoc[$geoc_id]['op'];
			
			$polygons[$record][$op] = $fila;
			$polygons[$record]["points"][] =  $fila["point"];
			if(!isset($polygons[$record]["velocidad"])){
				$polygons[$record]["velocidad"]["add"] = 0;
				$polygons[$record]["velocidad"]["nro"] = 0;
				$polygons[$record]["velocidad"]["max"] = 0;
				$polygons[$record]["velocidad"]["point"] = $fila["point"];
				$polygons[$record]["velocidad"]["hora"] = "";				
			}else if($fila["regi_velocidad"]>0){
				
				$polygons[$record]["velocidad"]["add"] +=  $fila["regi_velocidad"];
				$polygons[$record]["velocidad"]["nro"] ++;
				if( $fila["regi_velocidad"] > $polygons[$record]["velocidad"]["max"] )
				{
					$polygons[$record]["velocidad"]["max"] =  $fila["regi_velocidad"];
					$polygons[$record]["velocidad"]["point"] =  $fila["point"];
					$polygons[$record]["velocidad"]["hora"] =  date( "H:i:s", $fila["unix"]);//strtotime $fila["fecha"];
					
				}	
			}
			$geoc_temp["prev"] = $fila;
			$geoc_temp[$geoc_tipo] = $fila;
		}
		$resultado->close();
	}else{
			$jsondata['msj_err'] = ( $i < 2 ) ? "No se han encontrado datos de unidad seleccionada" : NULL;		
	}
	//print_r($id_geoc);
	//print_r($polygons);

	//----------------------------------------------------------------------------

	$distance = new distance_geom();	
	$temp_point = NULL;
	$temp_all = array();
	if(count($polygons)>0){		
		foreach($polygons as $key => $valor) {
		
			$value = $valor["STAR"];				
			
			if( count($valor["points"]) > 1 && $value["geoc_nombre"] != "SxTOP"){ //&& $value["geoc_id"] != NULL 
				$points_dist = $valor["points"];
				$dist_rec = $distance->Sum_array($points_dist);
			}else{
				$dist_rec = "0";
			}
			$velocidad = $valor["velocidad"];
			if(!isset($valor["END"])){
				$valor["END"] = $valor["STAR"];	
			}

			$data[$key]["patente"] = $value["vehi_patente"];
			$value["geoc_codigo"];
			$value["geoc_nombre"];
			$value["geoc_tipo"];
			$data[$key]["date_in"] = $value["fecha"];
			$data[$key]["date_out"] = $valor["END"]["fecha"];
			$data[$key]["date_dif"] =  round(($valor["END"]["unix"]-$value["unix"])/60);
			$data[$key]["dist_rec"] = $dist_rec;
			if($velocidad["nro"]>0)  $data[$key]["promediovel"] = round($velocidad["add"]/$velocidad["nro"],3);
			else $data[$key]["promediovel"] = 0;
			$data[$key]["max"] = $velocidad["max"];
			$data[$key]["point"] = $velocidad["point"];
			$data[$key]["hora"] = $velocidad["hora"];
			$end_point = ($temp_point != NULL) ? "$temp_point, " : '';
			$data[$key]["points"] = "((" . $end_point . implode(", ", $valor["points"]) . "))";
			$temp_point = end($points_dist);
			$temp_all = array_merge( $temp_all ,  $valor["points"]);
		}
		$jsondata["allpos"] = "((" . implode(", ", $temp_all) . "))";
		print_r($data);
		$jsondata['pos'] = $data;		
	}else{

		$jsondata['msj_err'] = "No se han encontrado datos de unidad seleccionada";	
	}

		$jsondata['msj_err'] = $sql;


		$mysqli->close();
	}else{
		$jsondata['msj_err'] = "Falta el ingreso de informacion";	
	}
	
	header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();
}