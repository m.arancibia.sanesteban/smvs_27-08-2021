<!DOCTYPE html>
<html>
<head>
<style>
 thead {
    color:green;
  }
  tbody {
    color:black;
  }
  tfoot {
    color:red;
  }
  table{
    width: auto;
    margin: 0 auto; 
  }
  table, th, td {
      border: 1px solid black;
      font-family: Calibri,sans-serif; 
      font-size: .9em;
      border-spacing: 0;
  }
  th{
    background-color: #8D0404;
    color: white;
  }
</style>
</head>

<body>

<table>
  <thead>
    <tr>
      <th>PPU</th>
      <th>GC_ID</th>
      <th>tipoVE</th>
      <th>FECHA</th>     
    </tr>
  </thead>
  <tbody>
<?php
	include("../include/db.php");

	$date_get = @$_GET["date"];
	$date_in = "2018-12-22";
	
	$date = (!empty($date_get)) ? $date_get : $date_in; 
	
	$sql ="SELECT
				VE.vehi_patente,
				VE.vehi_tive_id,				
				GE.geoc_id,
				FROM_UNIXTIME(MAX(RE.regi_fecha_posicion),'%Y-%m-%d') as fecha
				FROM `vehiculo` AS VE
				INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id
					AND RE.regi_fecha_posicion BETWEEN UNIX_TIMESTAMP('$date 00:00:00') AND UNIX_TIMESTAMP('$date 23:59:59')
					AND RE.regi_latitud != 0 AND RE.regi_longitud != 0
				INNER JOIN `gc_id` AS GI ON GI.gc_regi_id =  RE.regi_id
				INNER JOIN `geocerca` AS GE ON GE.geoc_id = GI.gc_geoc_id
					AND GE.geoc_visible = 1
					AND ( GE.geoc_tipo = 1 OR ( GE.geoc_tipo = 2 AND GE.geoc_codigo like '%OE%' ))
			GROUP BY VE.vehi_patente, VE.vehi_tive_id, GE.geoc_id, GE.geoc_codigo
			ORDER BY GE.geoc_empr_id ASC, 2 ASC, 1 ASC";
			
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc())
		{		
?>
    <tr>
      <td><?php echo $fila["vehi_patente"]; ?></td>
      <td><?php echo $fila["geoc_id"]; ?></td>
      <td><?php echo $fila["vehi_tive_id"]; ?></td>
       <!--1 = CAMION
      2 = CAMIONETA
      0 = NI-->
      <td><?php echo $fila["fecha"]; ?></td>
	</tr>
<?php
		}
	}
?> 
  </tbody>
</table>
</body>
</html>
