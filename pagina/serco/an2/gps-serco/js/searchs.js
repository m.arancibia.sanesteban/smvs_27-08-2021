	function searchg() {
		var busqueda = $('#search_g'),
		titulo = $('form#geocercas ul#something li div label');
		$(titulo).each(function(){
			var li = $(this);
			$(busqueda).keyup(function(){
				this.value = this.value.toLowerCase();
				$(li).parent().parent().hide();
				var txt = $(this).val();
				if($(li).text().toLowerCase().indexOf(txt) > -1 || $(li).attr("title").toLowerCase().indexOf(txt) > -1 || $(li).attr("id") == "title" ){					
					$(li).parent().parent().show();
				}
			});
		});
	}

	function searchp() {
		var busqueda = $('#search_p'),
		titulo = $('form#patent ul#something li div label');
		$(titulo).each(function(){
			var li = $(this);
			$(busqueda).keyup(function(){
				this.value = this.value.toLowerCase();
				$(li).parent().parent().hide();
				var txt = $(this).val();

				if($(li).text().toLowerCase().indexOf(txt) > -1){
					$(li).parent().parent().show();
				}
			});
		});
	}
