<?php 
	include("../include/db.php");
	include("../include/pointinline.php");
	include("distance.php");
	include("read.php");	
	//$patentes = "'CXSC74','DPLL84','DPRS16','DRHZ69'";
		$patentes = '"BGJK71",
"FGPK13",
"GFKJ18",
"HCKL25",
"HXPX88",
"CVPC58",
"CWHF69",
"DGRF89",
"DPFL27",
"DWBZ35",
"FPVV41",
"HBDP72",
"HHDP92",
"HHDP95",
"JPZC69",
"JYTK90",
"KBXC54",
"FCCL30",
"HCRH92",
"HYHL28",
"JCPB75",
"DPLL85",
"HSDZ53",
"KBBF51"
';
	$patentes = "'HCKL25','FGPK13'";
	
	$date_str = "2018-10-12";
	$date_end = "2018-10-12";
	
	$sql ="SELECT
				RE.regi_id,
				VE.vehi_patente,
				from_unixtime(RE.regi_fecha_posicion) AS fecha,
				RE.regi_fecha_posicion,
				RE.regi_latitud,
				RE.regi_longitud,
				RE.regi_velocidad,
			GE.geoc_id,
			GE.geoc_nombre,
			GE.geoc_codigo,
			GE.geoc_tipo,
			GE.geoc_max_speed
				FROM `vehiculo` AS VE 
				INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id 
					AND VE.vehi_patente IN($patentes)
					AND RE.regi_fecha_posicion BETWEEN UNIX_TIMESTAMP('$date_str 00:00:00') AND UNIX_TIMESTAMP('$date_end 23:59:59')
				LEFT JOIN `geocerca` AS GE ON 
					GE.geoc_visible = 1
					AND
					(
						(ST_CONTAINS(
						geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
						) AND GE.geoc_tipo IN(1,2,3,4,5,6,7,9))
						OR
						(CONTAINS(
						geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
						) AND GE.geoc_tipo IN(8))
					)
					ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";


	$pointLocation = new PointLocation();
	$part1 = array();
	$range_max =  0.030;
	$temp_end_gc = NULL;
	$temp_prev = array();
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc())
		{
			
			$regi_id = $fila['regi_id'];

			if( $fila['geoc_tipo'] == NULL and $temp_end_gc != NULL )
			{
				$fila['geoc_id'] = $part1[$temp_end_gc][0]["geoc_id"];
				$fila['geoc_nombre'] = $part1[$temp_end_gc][0]["geoc_nombre"];
				$fila['geoc_tipo'] = $part1[$temp_end_gc][0]["geoc_tipo"];
				$fila['geoc_codigo'] = $part1[$temp_end_gc][0]["geoc_codigo"];
				$fila['geoc_max_speed'] = $part1[$temp_end_gc][0]["geoc_max_speed"];			
			}

			if( $fila['geoc_tipo'] == 8 )
			{	
				$temp_dist = $pointLocation->Glength_all_point( $fila['geoc_id'], $fila['regi_longitud'] .' '. $fila['regi_latitud'] );
						
				if($temp_dist > $range_max || $temp_dist === NULL ){
					$fila['geoc_id'] = NULL;
					$fila['geoc_nombre'] = NULL;
					$fila['geoc_tipo'] = NULL;
					$fila['geoc_codigo'] = NULL;
					$fila['geoc_max_speed'] = NULL;
				}else{
					$fila["dist"] = $temp_dist;
				}
				//	$fila["dist"] = $temp_dist;
					
				
				if($fila['geoc_codigo'] != NULL )
				{
					$temp_end_gc = $regi_id;
				}elseif ( $temp_dist > 3 )
				{
					$temp_end_gc = NULL;
				}	
			}
						
			if( isset($part1[$regi_id][0])){
				$temp_row = $part1[$regi_id][0];
					
				switch ($temp_row["geoc_tipo"]) 
				{
					case 1:
						if( $fila["geoc_tipo"] == 2  ){
							$part1[$regi_id][] = $fila;
						}
						break;
					case 3:
						if($fila["geoc_tipo"] == 8 and $temp_dist <= $range_max){
							$part1[$regi_id][0] = $fila;
						}
						break;
					case 8:
						if($fila["geoc_tipo"] == 8 and $temp_row["dist"] > $temp_dist)
						{
							$part1[$regi_id][0] = $fila;
						}
						break;
					case NULL:
							$part1[$regi_id][0] = $fila;
						break;
				}				
			}else
			{
				$part1[$regi_id][0] = $fila;			
			}
		}
	}

	//print_r($part1);
	//die();

	//########################################################################################################################
	
	$it = new MiIterador($part1);
	$temp_i_move = 0;
	$temp_time_stop = 0;
	$moving_id = 0;
	$stop_id = 0;
	$bool_self_pat = false;
	while($it->valid())
	{
		$current = $it->current();
		$current = $current[0];
		
		if(isset($prev_temp) && $prev = @$prev_temp[$current["vehi_patente"]] )
		{
			$bool_self_pat = ($current["vehi_patente"] == $prev["vehi_patente"]);
			if($current["vehi_patente"] != $prev["vehi_patente"])
			{
				$temp_i_move = 0;
				$temp_time_stop = 0;
				$moving_id = 0;
				$stop_id = 0;

			}
			$lat_prev = $prev["regi_latitud"];
			$lon_prev = $prev["regi_longitud"];
			$lat_self = $current["regi_latitud"];
			$lon_self = $current["regi_longitud"];
			$current["dist_points"] = ($lat_prev!=$lat_self || $lon_prev!=$lon_self)?$pointLocation->Glength($lat_prev,$lon_prev,$lat_self,$lon_self):0;
			
			$time_prev =  $prev["regi_fecha_posicion"];
			$time_self =  $current["regi_fecha_posicion"];
			$current["time_dif"] =  $time_self - $time_prev; 
		}else{
			$current["dist_points"] = 0;
			$current["time_dif"] = 0;
		}
		$geoc_max_speed = $current["geoc_max_speed"];
		$regi_velocidad = $current["regi_velocidad"];
		$current["speed_dif"] = (!empty($geoc_max_speed)) ? $regi_velocidad - ( $geoc_max_speed + 5 ) : 0;		

		//################ SPEEDING
		if($current["speed_dif"] > 0)
		{
			$temp_i_speeding++;
			$temp_time_speeding += $current["time_dif"];		
			if( $temp_i_speeding >= 2 && $temp_time_speeding >= 60 ) {
				if($speeding_id === NULL){	
					$speeding_id = $it->update_row( ($temp_i_speeding - 1),"speeding");	
				}
				$current["speeding"] = $speeding_id;
			}
		}else if( !isset($speeding_id) || !empty($speeding_id) ) {  
			$temp_i_speeding = 0;
			$temp_time_speeding = 0;
			$speeding_id = NULL;		
		}
		//################ SPEEDING END
			
		//################ MOVING 
		
		if( $regi_velocidad > 0 || @$current["dist_points"] > 0.050 )
		{
			if($temp_i_move>0 ){
				$moving_id = $it->update_row( 0 ,"move");
			}
			if( $temp_time_stop <= 180 && $temp_time_stop > 0  ) {
				if($moving_id > 1){
					$moving_id = $it->update_row( ($temp_i_move) ,"move");	
				}
			}
			$current["move"] = $moving_id;
			$temp_time_stop = 0;
			$temp_i_move = 0;

		}else{  
			if( ($moving_id + 1) > $stop_id ){
				$stop_id = ($moving_id + 1);
			}
			$current["stop"] = $stop_id;
			$temp_i_move++;
			$temp_time_stop += $current["time_dif"];	
		}
		
	
		//################ MOVING END	
		
		$it->update_self_array($current);
		$prev_temp[$current["vehi_patente"]] = $current;
		$it->next();
	}
	
	//print_r($it->get_array());
	//die();
	
	//########################################################################################################################

	$moving = array();
	foreach($it->get_array() as $key => $var){
		$date_self = $var[0];
		$id_moving = ((isset($date_self["move"]))? "m" . $date_self["move"] : "s" . $date_self["stop"]) . "_" . $date_self["geoc_id"] ;
		
		
		if(!isset($moving[$id_moving]["ppu"])){
			$moving[$id_moving]["ppu"] = $date_self["vehi_patente"];
			$moving[$id_moving]["geoc_id"] = $date_self["geoc_id"];
			$moving[$id_moving]["geoc_nombre"] = $date_self["geoc_nombre"];
			$moving[$id_moving]["geoc_tipo"] = $date_self["geoc_tipo"];
			$moving[$id_moving]["finicio"] = $date_self["fecha"];
			$moving[$id_moving]["time"] = 0;
			$moving[$id_moving]["lenght"] = 0;
			$moving[$id_moving]["promVel"] = 0;
			$moving[$id_moving]["num_rows"] = 0;
		}
		$moving[$id_moving]["time"] += @$date_self["time_dif"];
		$moving[$id_moving]["lenght"] += @$date_self["dist_points"];
		$moving[$id_moving]["promVel"] += $date_self["regi_velocidad"];
		$moving[$id_moving]["point"] = $date_self["regi_latitud"] .", " . $date_self["regi_longitud"] ;
		$moving[$id_moving]["num_rows"]++;
		
		$moving[$id_moving]["ffin"] = $date_self["fecha"];
		$moving[$id_moving]["move"] = @$date_self["move"];
		$moving[$id_moving]["stop"] = @$date_self["stop"];
		
		//print_r($date_self);
	}

	//print_r($moving);

	/*
	$it->current_size();
	$it->prev();
	*/
?>
<!DOCTYPE html>
<html>
<head>
<style>
 thead {
    color:green;
  }
  tbody {
    color:black;
  }
  tfoot {
    color:red;
  }
  table{
    width: auto;
    margin: 0 auto; 
  }
  table, th, td {
      border: 1px solid black;
      font-family: Calibri,sans-serif; 
      font-size: .9em;
      border-spacing: 0;
  }
  th{
    background-color: #8D0404;
    color: white;
  }
</style>
</head>

<body>

<table>
  <thead>
    <tr>
      <th>ppu</th>
      <th>idGC</th>
      <th>nombreGC</th>
      <th>Inicio</th>
      <th>Fin</th>
      <th>Tiempo</th>
      <th>Distancia</th>
      <th>Velocidad PROM</th>
      <th>Move</th>
      <th>Stop</th>
      <th>Point</th>
      <th>TipoGC</th>

    
    </tr>
  </thead>
  <tbody>
<?php

	foreach($moving as $var){
		echo "<tr>";

		echo "<td>" . $var["ppu"] . "</td>";
		echo "<td>" . $var["geoc_id"] . "</td>";
		echo "<td>" . $var["geoc_nombre"] . "</td>";
		echo "<td>" . $var["finicio"] . "</td>";
		echo "<td>" . $var["ffin"] . "</td>";
		echo "<td>" . $var["time"] . "</td>";
		echo "<td>" . $var["lenght"] . "</td>";
		echo "<td>" . ($var["promVel"]/ $var["num_rows"]) . "</td>";
		echo "<td>" . $var["move"] . "</td>";
		echo "<td>" . $var["stop"] . "</td>";
		echo "<td>" . $var["point"] . "</td>";
		echo "<td>" . $var["geoc_tipo"] . "</td>";
		
		


		echo "</tr>";
	}

?>  </tbody>
</table>
</body>
</html>
