<?php
//date_default_timezone_set('Chile/Continental');

if( count($_POST) > -1 ) {
	include("../include/db.php");

	//$pat_sel = $_POST[''];

	$pat_sel = $_POST['pat_sel'];
	unset($_POST['pat_sel']);
	
	
	

	if(!empty($pat_sel) && $pat_sel != "undefined" ){
		$patentes = $pat_sel;
	}else{
		foreach($_POST as $key => $valor){
				$temp[] = "'$key'";
		};
		$patentes = implode(',',$temp);
	}
	
	$verde = '#00FF00';
	$amarilla = '#FFFF00 ';
	$rojo = '#FF0000 ';
	$azul = '#0000FF';
	
	/*

	$sql = "SELECT
			VE.vehi_id,
			RE.regi_latitud AS lat,
			RE.regi_longitud AS lng,
			regi_fecha_posicion AS fecha,
			MAXR.regi_id1
			FROM ( 
				SELECT
				Max(regi_id) AS regi_id2,
				MAX.regi_id1
				FROM `registro` REG
				INNER JOIN (
					SELECT
					regi_vehi_id,
					Max(regi_id) AS regi_id1
					FROM `registro`
					WHERE regi_vehi_id IN ($patentes)
					GROUP BY regi_vehi_id
				) AS MAX ON REG.regi_vehi_id = MAX.regi_vehi_id AND REG.regi_id <> regi_id1
				GROUP BY regi_id1			
			) AS MAXR
			INNER JOIN `registro` AS RE ON RE.regi_id = MAXR.regi_id2 
			INNER JOIN `vehiculo` AS VE ON VE.vehi_id = RE.regi_vehi_id";
			
	$time_patente = array();
	$regi_id = array();
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_object()) {
			$time_patente[$fila->vehi_id] = array(
										"lat"=>"$fila->lat",
										"lng"=>"$fila->lng",
										"fecha"=>"$fila->fecha"
										);
			$regi_id[] = $fila->regi_id1;
		}
		$resultado->close();
	}

	$sql = "SELECT
			VE.vehi_id,
			RE.regi_latitud AS lat,
			RE.regi_longitud AS lng,
			DATE_FORMAT(from_unixtime(regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
			regi_fecha_posicion AS fecha_unix,
			RE.regi_azimut AS azimut,
			RE.regi_velocidad AS velocidad,
			VE.vehi_patente
			FROM ( SELECT
			Max(regi_id) AS ID
			FROM `registro`
			WHERE regi_vehi_id IN ($patentes)
			GROUP BY regi_vehi_id ) AS MAXR
			INNER JOIN `registro` AS RE ON RE.regi_id = MAXR.ID 
			INNER JOIN `vehiculo` AS VE ON VE.vehi_id = RE.regi_vehi_id";
	*/	
	
	// GEOCERCAS
		
	require("../include/in_geoc.php");

	$sql = "SELECT
			geoc_codigo,
			AsText(geoc_poligono) AS poligono
			FROM `geocerca`";

    $converted_points = array();
	$polygons = array();	
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_object()) {
			$polygons = explode("((", $fila->poligono);
			$number = count($polygons)-1;
			for ( $n = 1; $n < count($polygons); $n++ )
			{
				$polygon = str_replace("))","",$polygons[1]);
				$points_str = explode(",",$polygon);
				foreach ($points_str as &$valor) {	
					$points = explode(" ",$valor);
					$lon  = $points[0];
					$lat  = $points[1];   
					$converted_points[$fila->geoc_codigo][] = "$lat,$lon";				
				}
			}
		}
		$resultado->close();
	}

	// FIN GEOCERCAS
	
	//DATE_FORMAT(CONVERT_TZ(from_unixtime(RE.regi_fecha_posicion),'+00:00','-03:00'), '%d-%m-%Y %H:%i:%s') AS fecha,

	//RE.regi_vehi_id IN (-100,650,348,264)
	
	$sql = "SELECT
			RE.regi_fecha_posicion AS fecha_unix,
			VE.vehi_id,
			RE.regi_latitud AS lat,
			RE.regi_longitud AS lng,
			DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
			RE.regi_azimut AS azimut,
			RE.regi_velocidad AS velocidad,
			VE.vehi_patente,
			if(  MAXR.regi_id1 = MAXR.regi_id2, null,(RE.regi_fecha_posicion - RE2.regi_fecha_posicion) )  AS dif_time
			FROM ( 
				SELECT
				REG.regi_vehi_id,
				MAX.regi_id1,
				Max(REG.regi_id) AS regi_id2
				FROM (
					SELECT
					regi_vehi_id,
					Max(regi_id) AS regi_id1,
					count(regi_vehi_id) AS num_rows
					FROM `registro`
					WHERE regi_vehi_id IN ($patentes)
					GROUP BY regi_vehi_id
				) AS MAX
				INNER JOIN `registro` REG	ON REG.regi_vehi_id = MAX.regi_vehi_id AND ( REG.regi_id <> regi_id1 OR num_rows = 1 )
				GROUP BY regi_id1, regi_vehi_id	
			) AS MAXR
			INNER JOIN `registro` AS RE ON RE.regi_vehi_id = MAXR.regi_vehi_id AND RE.regi_id = MAXR.regi_id1 
			INNER JOIN `vehiculo` AS VE ON VE.vehi_id = RE.regi_vehi_id
			LEFT JOIN `registro` AS RE2 ON RE2.regi_vehi_id = MAXR.regi_vehi_id AND RE2.regi_id = MAXR.regi_id2";		
	
	
			
	
	$jsondata = array();
	$alert_geoc = "";
	//$jsondata["post"] = serialize($_POST);
	//$jsondata["post"] = $patentes;
	if ($resultado = $mysqli->query($sql)) {
		$pointLocation = new pointLocation();
		while ($fila = $resultado->fetch_object()) {
			$animation = ($pat_sel == $fila->vehi_id) ? 'BOUNCE' : 'DROP';
	

			$dif_time = $fila->dif_time;
			switch (true) {
				case $dif_time == null:
					$color = $azul;
					break;				
				case $dif_time < 300:
					$color = $verde;
					break;
				case $dif_time < 1200:
					$color = $amarilla;
					break;
				default:
					$color = $rojo;
					break;
			}
			
		    $geoc_pos = $fila->lat . ", " . $fila->lng;
			foreach ($converted_points as $clave => $valor) {
				$info = $pointLocation->pointInPolygon( $geoc_pos, $valor);
				if($info == "inside")
				{
					$alert_geoc .= "<li>" . $fila->vehi_patente . ": $clave</li>";
				}
			}
			
			$pos[$fila->vehi_id] = array('lng'=>$fila->lng, 
										'lat'=>$fila->lat, 
										'animation'=>$animation, 
										'patente'=>$fila->vehi_patente,
										'azimut'=>$fila->azimut,
										'fecha'=>$fila->fecha,
										'velocidad'=>$fila->velocidad,
										'info'=>$info,
										'color'=>$color);
										

					
		}
		$resultado->close();
		$jsondata["alert_geoc"] = $alert_geoc;
		$jsondata['markers'] = $pos;
    }
	$mysqli->close();

	header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();
}