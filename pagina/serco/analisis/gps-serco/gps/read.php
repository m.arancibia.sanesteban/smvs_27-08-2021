<?php
class MiIterador implements Iterator
{
    private $var = array();
	private $speeding = 0;
	private $moving = 0;
    
    public function __construct($array)
    {
        if (is_array($array)) {
            $this->var = $array;
			$this->rewind();
        }
    }

    public function rewind()
    {
        //echo "rebobinando\n";
        reset($this->var);
    }

    public function current()
    {
        $var = current($this->var);
       	//echo "actual: var\n";
		//print_r($var);
        return $var;
    }

    public function key()
    {
        $var = key($this->var);
        //echo "clave: $var\n";
        return $var;
    }
		
    public function prev()
    {
        $var = prev($this->var);
        //echo "anterior: var\n";
		//print_r($var);
        return $var;
    }
	public function reset_speeding()
    {
		$this->speeding = 0;
		return 0;
	}
	
	public function reset_move()
    {
		$this->moving = 1;
		return 1;
	}
	
	public function update_row($numrows, $option)
    {
		$key = $this->key();
		$pos = NULL;
		$i = 0;
		if( $option=="speeding"){
			$this->speeding++;
		}else if( $option == "move" && $numrows == 0 ){
			$this->moving++;
		}
		
		if( $numrows > 0 )
		{
			while($pos != $key){
				if( $i++ < $numrows )
				{
					$this->prev();
					if( $option=="speeding"){
						$this->update_prev_array("speeding", $this->speeding);
					}else if( $option=="move"){
						$this->update_prev_array("move", $this->moving);
					}
				}else{
					$this->next();				
				}
				$pos = $this->key();
			}
		}
        return ( $option=="speeding")? $this->speeding : $this->moving;
    }
	
    public function next()
    {
        $var = next($this->var);
        //echo "siguientes: var\n";
		//print_r($var);
        return $var;
    }

    public function valid()
    {
        $clave = key($this->var);
        $var = ($clave !== NULL && $clave !== FALSE);
        //echo "válido: $var\n";
        return $var;
    }
	
    public function size()
    {
        $var = count($this->var);
        //echo "size: $var\n";
        return $var;
    }
    
	public function update_self_array($current)
    {
		$clave = key($this->var);
		$this->var[$clave][0] = $current;
        return true;
  	}
	
	public function update_prev_array($key, $val)
    {
		$clave = key($this->var);
		$this->var[$clave][0][$key] = $val;
		if($key = "move"){
			unset($this->var[$clave][0]["stop"]);
		}
		return true;
  	}
		
	public function get_array()
    {
        return $this->var;
   }
   
	public function current_size()
    {
        $var = count(current($this->var));
        //echo "current_size: $var\n";
        return $var;
    }
}

?>
