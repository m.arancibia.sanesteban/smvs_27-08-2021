﻿<html>
<head>
<style>

  thead {
    color:green;
  }
  tbody {
    color:black;
  }
  tfoot {
    color:red;
  }
  table{
    width: auto;
    margin: 0 auto; 
  }
  table, th, td {
      border: 1px solid black;
      font-family: Calibri,sans-serif; 
      font-size: .9em;
      border-spacing: 0;
  }
  th{
    background-color: #8D0404;
    color: white;
  }
 

</style>
<title>CAMIONETAS.DMH</title>
</head>

<body>

<table>
  <thead>
    <tr>
      <th>PPU</th>
      <th>CODGC</th>
      <th>IDGC</th>
      <th>NOMGC</th>
      <th>HORA INGRESO</th>
      <th>HORA SALIDA</th>
      <th>TIEMPO [MIN]</th>
      <th>DIST [KM]</th>
      <th>VEL PROM [KM/H]</th>
      <th>VEL MAX [KM/H]</th>
      <th>POINT</th>
      <th>HORA EXCESO</th>      
      <th>TIPO GC</th>      
    </tr>
  </thead>
  <tbody>
 <?php
 set_time_limit (3600);
 
 $patentes = array(
"FWZH53",
"HDYH90",
"HDYJ14",
"HDYJ15",
"HDYJ16",
"HDYJ17",
"HDYJ23",
"HDYJ25",
"HDYJ26",
"HDYJ27",
"HDYJ28",
"HDYJ30",
"HDYJ33",
"HDYJ60",
"HDYJ62",
"HDYJ64",
"HDYJ67",
"HDYJ73",
"HDYJ74",
"HDYJ76",
"HDYJ78",
"HDYJ80",
"HDYJ81",
"HDYJ83",
"HDYJ88",
"HDYJ89",
"HDYJ92",
"HDYJ96",
"HDYJ99",
"HDYK11",
"HDYK14",
"HDYK45",
"HDYK47",
"HDYK49",
"HDYV72",
"HDYV73",
"HDYV74",
"HDYV75",
"HDYV79",
"HDYW50",
"HDYW51",
"HDYW53",
"HDZB13",
"HDZB61",
"HDZB62",
"HFXB75",
"HFXB76",
"JKLR63",
"JKPL81",
"JKPL82",
"JKPL83",
"JKPL96",
"JKPL98",
"JKPP74",
"JKPP75",
"JKPP81",
"JKPP82",
"JKPP83",
"JKPP84",
"JYZL37",
"JYZS27",
"JYZS28",
"JZHS20",
"JZHS47",
"JZHS52",
"JZLV18",
"JZLV31",
"JZLV33",
"JZLV35",
"JZLV36",
"JZLV38",
"JZLV40",
"JZLV44",
"JZLV45",
"JZLV49",
"JZLV51",
"JZLV52",
"JZLV58"
);  // modificar patentes


	include("divisiones.php");	
	
  $date_in = "2018-11-18"; // modificar fechas 
	$date_get = $_GET["fecha"];
	$date = (!empty($date_get)) ? $date_get : $date_in; 

  //$record = record($patentes, $date, true); //divisional - REPORTE ESPECIAL
  $record = record($patentes, $date, false); //todas las GC - REPORTE NORMAL 
  if(is_array($record)){
  foreach($record  as $var){
    if($var["geoc_nombre"] != "SxOP"){

?>
    <tr>
      <td><?php echo $var["patente"]; ?></td>
      <td><?php echo $var["geoc_id"]; ?></td>
      <td><?php echo $var["geoc_codigo"]; ?></td>
      <td><?php echo utf8_encode($var["geoc_nombre"]); ?></td>
      <td><?php echo $var["date_in"]; ?></td>
      <td><?php echo $var["date_out"]; ?></td>
      <td><?php echo $var["date_dif"]; ?></td>
      <td><?php echo str_replace(".",",",$var["dist_rec"]); ?></td>
      <td><?php echo str_replace(".",",",$var["promediovel"]); ?></td>
      <td><?php echo $var["max"]; ?></td>
      <td><?php echo $var["point"]; ?></td>
      <td><?php echo $var["hora"]; ?></td> 
      <td><?php echo $var["geoc_tipo"]; ?></td>       
  </tr>
<?php
    }
  }
  }
  /*
    }
  }*/
?> 
  </tbody>
</table>
</body>
</html>