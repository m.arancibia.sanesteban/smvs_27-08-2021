		function myMap() {
		var mapCanvas =  $("#map-canvas")[0];
		var myCenter = new google.maps.LatLng(-23.199948, -69.647242);		
		var mapOptions = {
			center: myCenter,
			zoom: 8,
		};
		map = new google.maps.Map(mapCanvas, mapOptions);	
		mkcluster = new MarkerClusterer(map, null, {imagePath: './images/m'});
	}

/*	function updatemyGps(time) {
		window.setTimeout(function(){updatemyGps();},20000);
		console.log("update GPS");
		myGps(null);
	}*/
	var funcion_error = function(xhr, error){
		var msj = (error=="timeout")? "<br>ERROR: Sin conexión con el servidor" : "<br>ERROR: Problema de sintaxis || " + error ;
		popup_msj.html( msj );
		popup_msj.dialog('open');
	};
	
	function myGpsX(pat_sel) {
		var url = "gps/pos.php";
		alert("llego");	
		$.ajax({
			url: url, 
			success: function(result){	
			alert(result);
		}});
	};

	function myGps(pat_sel) {
		
	if(pat_sel == null) {
		console.log("update GPS ENTRO");
		console.log($("form#patent").serialize());
	}
	
		var url = "gps/pos.php";
		$.ajax({
			type: "POST",
			dataType: "text",
			url: url,
			data: $("form#patent").serialize() + "&pat_sel=" +  pat_sel,
			//timeout:10000,
			error: funcion_error,
			success: function(data)
			{
				var data = JSON.parse(data);
				if(pat_sel == null) {
					console.log("update GPS-CARGO");
				}else{
					//alert(data.post);
				}
				if( data.alert_geoc != "" && data.alert_geoc != null && data.alert_geoc != undefined )
				{
					var tbody = $('table tbody#alert_geoc');
					$.each(data.alert_geoc, function(i, marker) {
						var tr_pat = tbody.find("tr#"+i);

						if( tr_pat.length == 0 || (tr_pat.attr("class").replace(" updsel","") != $(marker).attr("class")) )
						{
							if( $(marker).attr("class") != tr_pat.attr("class") && tr_pat.attr("class") != undefined ) {
								tr_pat.remove();
							}
							if( $(marker).attr("class") == "npatDIV" ) {
								tbody.prepend(marker);
							} else {
								tbody.append(marker);
							}
						}else if( tr_pat.length > 0 || (tr_pat.attr("class") == $(marker).attr("class")) ) {
							var MARKER = $(marker);
							
							if( tr_pat.attr("name") != $(marker).attr("name") && tr_pat.attr("class") == "npatDIV" ) {
								//alert(marker);
								MARKER.addClass("updsel");
								popup_msj.html( "<br>Revisar notificaciones"); //Se han cargado
								popup_msj.dialog('open');
								setTimeout("popup_msj.dialog('close')", 1000);
							};
							tr_pat.replaceWith(MARKER);
								//alert(tr_pat.attr("class"));
						}
					});					
				}

				if( data.err != undefined )
				{
					$( "label#" + data.idpat ).css('color','red');
					popup_msj.html( "<br>" + data.err);
					popup_msj.dialog('open');
					setTimeout("popup_msj.dialog('close')", 2000);
				}

				$.each(data.markers, function(i, marker)
				{
					if( $("form#patent ul li input#" + i + "_sl[type*='checkbox']:checked").length == 1 )
					{
						var myposition = new google.maps.LatLng(marker.lat, marker.lng);
						if(marker.animation == 'BOUNCE') {
							animation = google.maps.Animation.BOUNCE;
							map.panTo(myposition);
							//console.log("center GPS");
						} else {
							animation = null;
						}
						var gcString = (marker.info != "" )? "<br/><strong> " + marker.geoc_tipo + " : </strong> " + marker.info : '';
						contentString = "<div><strong>Patente " + marker.patente + "</strong><br/><b>" + marker.velocidad
									+ " km/h</b>"
									+ "<br/><strong>Fecha ultimo REG:</strong> " + marker.fecha
									+ gcString
									+ "</div>";
					  	var azimut = parseInt(marker.azimut);
					  	var color = marker.color;
						var ln_color = marker.ln_color;
					var SymbolPath = (marker.symbolpath == true)? google.maps.SymbolPath.FORWARD_OPEN_ARROW : google.maps.SymbolPath.FORWARD_CLOSED_ARROW;	
						var icon = {
									path: SymbolPath,
									fillColor: color,
									fillOpacity: 100,
									strokeColor: ln_color,
									strokeWeight: 1.7,
									scale: 4,
									rotation: azimut
						};
						if (markers[i] != null) {
							markers[i].setPosition(myposition);
							markers[i].setIcon(icon);
							markers[i].setAnimation(animation);		
							if( infoWindow[i] != undefined) {
								infoWindow[i].setContent(contentString);	
							}
 						}else{
							markers[i] = new google.maps.Marker({
								position: myposition,
								icon:icon,
								zIndex: 112,
								title: 'Patente: ' + marker.patente,
								animation: animation
							});
						 	infoWindow[i] = new google.maps.InfoWindow();
							createInfoWindow( markers[i], contentString, infoWindow[i]);
							
							var clase = $("div#btn_group button i").attr("class");					
							if( clase == "fa fa-object-group" ) {
								mkcluster.addMarker(markers[i]);
							} else {
								markers[i].setMap(map);
							}
						}
					}
				});
			}
		});
	}

	function myGeocerca() {
		var url = "gps/geo.php";
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: $("form#geoc").serialize(),
			success: function(data)
			{
				$("form#geoc div#something input[type*='checkbox']").prop('checked', false);
				var color = data.color;
				var poli = parsePolyStrings(data.poligono, true, false);				
				geoc[data.id] = [];
				geoc[data.id]["center"] = poli.pos;
				map.setCenter(poli.pos); 
				map.setZoom(poli.zoom);

				var zindex = (data.tipo==1) ? 110 : 111;
								
				geoc[data.id]["zoom"] = poli.zoom;
				geoc[data.id]["polygon"] = new google.maps.Polygon({
				  paths: poli.arr,
				  map: map,
				  zIndex: zindex,
				  strokeColor: '#00FF00',
				  strokeOpacity: 1,
				  strokeWeight: 3,
				  fillColor: color,
				  fillOpacity: 0.3
				});
				
				var contentString = '<h5><b>GEOCERCA DATOS</b></h5>' +
					'<b>AREA: </b>	' + data.nombre + '<br>'+
					'<b>GC_ID: </b>	' + data.codigo + '<br>'+
					'<b>LATITUD: </b>	' + poli.pos.lat() + '<br>'+
					'<b>LONGITUD: </b>	' + poli.pos.lng() + '<br>';

				createInfoWindow( geoc[data.id]["polygon"], contentString);
			}
		});
	}

	function route(idrt) {
		var url = "gps/route.php";
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: "&idrt=" + idrt,
			success: function(data)
			{
				var points = data.route;
				console.log(points);
				//alert(data.post);
				reset_max_min();
				routes[idrt] = [];
				points.forEach(function(elemento) {
					poli = parsePolyStrings(elemento.points, false, true);
					var contentString = '<h5><b>DATOS RUTA</b></h5>' +
						'<b>RUTA: </b>	' + elemento.geoc_codigo + '<br>'+
						'<b>NOMBRE: </b>	' + elemento.nombre + '<br>'+
						'<b>VELOCIDAD MAX: </b>	' + elemento.max + '<br>';
					routes[idrt].push(lineString(poli.arr, elemento.color, contentString, 300, true) );
				});
				routes[idrt]["zoom"] = poli.zoom;
				routes[idrt]["center"] = poli.pos;
				map.setCenter(poli.pos); 
				map.setZoom(poli.zoom);			
			}
		});
	}

	function createInfoWindow(marker, content, infoWindow) {
		if(infoWindow == undefined) {
			infoWindow = new google.maps.InfoWindow();		
		}
		infoWindow.setContent(content);
		google.maps.event.addListener(marker, 'click', function(event) {
			infoWindow.setPosition(event.latLng);
			infoWindow.open(map, marker);
			//map.setCenter(event.latLng);
		});
	}
	
	function lineString(poly, color, contentString, zIndex, symbol) {
		if(!symbol) {
			var lineSymbol = {
				path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
				fillColor: color,
				fillOpacity: 1
			};	
		} else {
			var lineSymbol = "";
		}
		var lineOptions = {
		  	strokeOpacity: 0.9,   
			strokeWeight: 2.5,
			geodesic: true,
			zIndex: zIndex,
			strokeColor: color,	 
			icons: [ {
				icon: lineSymbol,
				offset: '0%',
				repeat: '120px',
				fillColor: '#00FF00' 
			} ]   
		}
		var rec = new google.maps.Polyline(lineOptions);
		rec.setMap(map); 
		rec.setPath(poly);
		createInfoWindow(rec, contentString);		
		return rec;
	}
	
	
	
	
	
	
	
	
	
	function myRec(id_pat) {
		var url = "gps/trace.php";
		popup_pb.dialog("open");
		$.ajax({
			type: "POST",	
			dataType: "JSON",
			url: url,
			data: $("form#recorrido").serialize(),
			error: function(jqXHR, textStatus, errorThrown) {
				closeDownload();
				popup_msj.html("<h3>Error</h3> Sin conexión con el servidor, contactar con Webmáster");
				popup_msj.dialog({
					autoOpen: true,
				   	buttons: {
						Aceptar: function() {$( this ).dialog( "close" );}
					}}
				);
    		},
			success: function(data)
			{
				//alert(data.msj_err);
				closeDownload();
				if(data.msj_err != null ){
					popup_msj.text(data.msj_err);
					popup_msj.dialog({
						autoOpen: true,
					   	buttons: {
							Aceptar: function() {$( this ).dialog( "close" );}
						}}
					);
					return;
				}
				pat_rec = data.pat;
				
				//	popup_rec = $ id=('<div"datrec"></div>').appendTo('div#main');
				popup_rec.html(data.popup);
				popup_rec.dialog('open');
				
				recorrido.forEach(function(elemento,i) {
					elemento["line"].setMap(null);
				});
				recorrido.splice(0,recorrido.length);

				points = data.pos;
				var poli;
				reset_max_min();

				//console.log(points);
				points.forEach(function(elemento,i) {
					
					var array_points = elemento.points;
					var string_points = "((" + array_points.join(", ") + "))";
					poli = parsePolyStrings(string_points, false, true);
					
					var contentString = '<h5><b>DATOS RECORRIDO</b></h5>' +
					'<b>Fecha ingreso: </b>	' + elemento.date_in + '<br>'+
					'<b>Fecha salida: </b>	' + elemento.date_out + '<br>'+
					'<b>Tiempo Min: </b>	' + Math.round(elemento.time_rec/60)+ '<br>'+
					'<b>Distancia rec: </b>	' +parseFloat(elemento.dist_rec).toFixed(2) + '<br>';

					//recorrido.push(lineString(poli.arr, elemento.color, contentString, 900 )); //'#00FF00'
					var temp_rec = [];
					temp_rec["line"] = lineString(poli.arr, elemento.color, contentString, 900 );
					temp_rec["zoom"] = poli.t_zoom;
					temp_rec["pos"] = poli.t_pos;
					recorrido[i] = temp_rec;
				});
				zoom = poli.zoom;
				map.setCenter(poli.pos); 
				map.setZoom(zoom);
			}
		});
	}	