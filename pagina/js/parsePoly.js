var maxLat = null, minLat = null, maxLng = null, minLng = null;
	function reset_max_min() {
		maxLat = null; minLat = null; maxLng = null; minLng = null;
	}
	function parsePolyStrings(ps, op, resets) {
    	var i, j, lat, lng, tmp, tmpArr = {}, arr = [];
		var t_maxLat = null, t_minLat = null, t_maxLng = null, t_minLng = null;
		$limit = (op) ? 2: 0;
		if(!resets)
		{
			reset_max_min();
		}
        m = ps.match(/\([^\(\)]+\)/g);
		if (m !== null) {
			for (i = 0; i < m.length; i++) {
				tmp = m[i].match(/-?\d+\.?\d*/g);
				//console.log(tmp);
				if (tmp !== null) {
					for (j = 0, tmpArr = []; j < tmp.length - $limit ; j+=2) {						
						lng = Number(tmp[j]) ;
						lat = Number(tmp[j + 1]);
						tmpArr = {lat:lat, lng:lng};	//tmpArr.push(new google.maps.LatLng(lat, lng));
						arr.push(tmpArr);				//arr[(j/2)] = tmpArr;
	
						// OBTENER MAXIMOS
						if(minLat>lat || minLat == null)
						{
							minLat = lat;
						}
						if(maxLat<lat || maxLat == null)
						{
							maxLat = lat;
						}
						if(minLng>lng || minLng == null)
						{
							minLng = lng;
						}
						if(maxLng<lng || maxLng == null)
						{
							maxLng = lng;
						}		
						// FIN MAXIMOS

						// OBTENER MAXIMOS T
						if(t_minLat>lat || t_minLat == null)
						{
							t_minLat = lat;
						}
						if(t_maxLat<lat || t_maxLat == null)
						{
							t_maxLat = lat;
						}
						if(t_minLng>lng || t_minLng == null)
						{
							t_minLng = lng;
						}
						if(t_maxLng<lng || t_maxLng == null)
						{
							t_maxLng = lng;
						}		
						// FIN MAXIMOS T					
					}
				}
			}
			
			pos = new google.maps.LatLng(((maxLat+minLat)/2),((maxLng+minLng)/2));
			t_pos = new google.maps.LatLng(((t_maxLat+t_minLat)/2),((t_maxLng+t_minLng)/2));
					
			var GLOBE_WIDTH = 356; // a constant in Google's map projection
			var angle = maxLng - minLng;
			if (angle < 0) angle += 360
			var angle2 = maxLat - minLat;
			if (angle2 > angle) angle = angle2;	
			var zoomfactor = (angle>0)? (Math.round(Math.log(960 * 360 / angle / GLOBE_WIDTH) / Math.LN2 - 0.7)) : 19;

			var angle = t_maxLng - t_minLng;
			if (angle < 0) angle += 360
			var angle2 = t_maxLat - t_minLat;
			if (angle2 > angle) angle = angle2;	
			var t_zoomfactor = (angle>0)? (Math.round(Math.log(960 * 360 / angle / GLOBE_WIDTH) / Math.LN2 - 0.7)) : 19;

		}
		return { arr:arr, pos:pos, zoom: zoomfactor, t_pos: t_pos, t_zoom: t_zoomfactor};
	}