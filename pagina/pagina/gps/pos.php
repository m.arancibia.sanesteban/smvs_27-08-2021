<?php
//sleep(2);

	include("../include/db.php");

	$XPOST = array(
		'pat_sel'=>"null",
		76=>"on",
		79=>"on",
		13=>"on");


	//$jsondata["post"] = serialize($_POST);
	$jsondata = array();
	$alert_geoc = array();
	
	if( count($_POST) > 1 ) // || @$pat_sel != "undefined"
	{
		
			
		$pat_sel = $_POST['pat_sel'];
		unset($_POST['pat_sel']);
	
		foreach($_POST as $key => $valor){
			$temp[] = "$key";
		};
		$patSel = implode(',',$temp);	
		
		$patentes = (!empty($pat_sel) && $pat_sel != "null" )? $pat_sel : $patSel ;
		$time_upd = 1;

	}else{
		$patSel = 0;	
		$time_upd = 15;
	}


	//$patentes = "396,450,89,565,2,4,6,7,89,86,34,678,99,75,1700615,1511";
	//$patSel = $patentes
	//$patentes = "45";
	
	/*$patentes = "339,719,658,282,344,41,808,18,188,384,234,340,1511,56,660,45,565,337,263,1700459,816,1,750,751,1700467,341,1700535,40,87,362,562,112,1700548,1700546,1700498,762,694,86,648,256,132,133,134,135,1700501,346,136,137,480,716,189,89,354,20,274,226,227,228,229,230,231,232,233,673,190,61,421,28,1700809,706,113,138,139,140,141,278,321,114,589,396,840,223,191,29,489,416,703,641,251,1700541,373584,279,1700559,1700627,1700626,411,270,1700778,275,1200270,392,607,1700741,388,587,549,116,235,1700556,1700554,84,1700530,21271,264,633,259,260,753,76,252,2370,639"*/;
	
	//$patentes = "1700791";		

				
	$sql = "SELECT
							RE.regi_id,
						  	VE.vehi_id,
							VE.vehi_tive_id,
							RE.regi_latitud AS lat,
							RE.regi_longitud AS lng,
							DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%H:%i:%s') AS fecha, #%d-%m-%Y 
							RE.regi_azimut AS azimut,
							RE.regi_velocidad AS velocidad,
							(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_recibido AS SIGNED)) AS dif_reg,
							(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_posicion AS SIGNED)) AS dif_time,
							VE.vehi_patente,
							GE.geoc_nombre,
							GE.geoc_tipo,
							GE.geoc_id
			 FROM `registro` AS RE 
			INNER JOIN `vehiculo` AS VE ON RE.regi_vehi_id = VE.vehi_id AND VE.vehi_tive_id = 1
			AND RE.regi_fecha_posicion > (UNIX_TIMESTAMP()-(60*$time_upd))
			AND RE.regi_vehi_id NOT IN($patSel)
			INNER JOIN `gc_id` AS GI ON RE.regi_id = GI.gc_regi_id
			INNER JOIN `geocerca` AS GE ON GE.geoc_id = GI.gc_geoc_id
				AND GE.geoc_visible = 1 AND GE.geoc_tipo IN(1,6,10)
				ORDER BY RE.regi_id DESC, GE.geoc_tipo DESC";

	//$jsondata["post"] = $sql;

	if($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			if(empty($alert_geoc[$fila['vehi_id']])) {
				$alert_geoc[$fila['vehi_id']] = "<tr class='patDIV' name='$fila[geoc_id]' id='$fila[vehi_id]'>
						<td id=\"ah\">$fila[vehi_patente]</td>
						<td>" . htmlentities($fila["geoc_nombre"]) . "</td>
						<td>$fila[fecha]</td>
					</tr>";				
			}
		}
		$resultado->free();
	}	
				
	//print_r($alert_geoc);	
	//die();	

if( !empty($patentes) ) // || @$pat_sel != "undefined"
{
	
	$verde = '#00FF00';
	$amarilla = '#FFFF00';
	$rojo = '#FF0000';
	$azul = '#0000FF';
	$negro = '#000';
	$celeste = "#6699FF";
	
	$set = "SELECT GROUP_CONCAT(products SEPARATOR ',') as id FROM
			(	SELECT
				MAX(regi_id) AS regi_id
				FROM `registro`
				WHERE regi_vehi_id IN($patentes) 
				GROUP BY regi_vehi_id
			) AS RE";

	$set = "SELECT
				MAX(regi_id) AS regi_id
				FROM `registro`
				WHERE regi_vehi_id IN($patentes) 
				GROUP BY regi_vehi_id";
	
	$ids = 0;	
	if($resultado = $mysqli->query($set)) {
		while ($fila = $resultado->fetch_assoc()) {
			$temp_ids[] = $fila['regi_id'];
		}
		if(isset($temp_ids)){
			$ids = implode(',',$temp_ids);
		}
	}

	$sql = "SELECT
				RE.regi_id,
				VE.vehi_id,
				VE.vehi_tive_id,
				RE.regi_latitud AS lat,
				RE.regi_longitud AS lng,
				DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
				RE.regi_azimut AS azimut,
				RE.regi_velocidad AS velocidad,
				(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_recibido AS SIGNED)) AS dif_reg,
				(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_posicion AS SIGNED)) AS dif_time,
				VE.vehi_patente,
				IF( GE.geoc_tipo=9, GE.geoc_codigo, GE.geoc_nombre) AS geoc_nombre,
				GE.geoc_tipo,
				GE.geoc_id
				FROM `registro` AS RE 
				INNER JOIN `vehiculo` AS VE ON RE.regi_vehi_id = VE.vehi_id
				#AND RE.regi_vehi_id IN($patentes)
				AND ( RE.regi_id IN ($ids) OR RE.regi_fecha_posicion > (UNIX_TIMESTAMP()-(60*23)) )
					LEFT JOIN `gc_id` AS GI ON RE.regi_id = GI.gc_regi_id
					LEFT JOIN `geocerca` AS GE ON GE.geoc_id = GI.gc_geoc_id
						AND GE.geoc_visible = 1
				HAVING vehi_id IN($patentes)
				ORDER BY RE.regi_vehi_id, RE.regi_fecha_posicion DESC, RE.regi_fecha_recibido ASC, GE.geoc_tipo DESC";
	
	$points_pat = array();

	if($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			if( @!$points_pat[$fila["vehi_id"]]["st_move"] ) {
				if(empty($points_pat[$fila["vehi_id"]])) {
					$points_pat[$fila["vehi_id"]] = $fila;
				} else {
					$points_pat[$fila["vehi_id"]]["dif_time"] = $fila["dif_time"];
					//$points_pat[$fila["vehi_id"]]["date"] = $fila["fecha"];
				}
				$points_pat[$fila["vehi_id"]]["st_move"] = ($fila["velocidad"] > 0);
			}
		}
		$resultado->free();
	}
	$mysqli->close();

	if( count($points_pat)>0) {
		foreach($points_pat as $valor) {
			$animation = ($pat_sel == $valor["vehi_id"]) ? 'BOUNCE' : 'DROP';
			$dif_time = $valor["dif_time"];
			$dif_reg = $valor["dif_reg"];
			$SymbolPath = false;
			$ln_color = "#33f";
							
			switch (true) {
				case $dif_reg > 86400:
					$color = $negro;
					$SymbolPath = true;
					$ln_color = "#ff8000";
					break;
				case ( $dif_time-$dif_reg ) > 3600:
					$color = $celeste;
					$SymbolPath = true;
					$ln_color = "#333";
					break;						
				case $dif_reg > 900:
					$color = $azul;
					break;
				case $dif_time < 300:
					$color = $verde;
					break;
				case $dif_time < 1200:
					$color = $amarilla;
					break;
				default:
					$color = $rojo;
					break;
			}
						
			if(!empty($valor["geoc_nombre"]) and $valor["vehi_tive_id"] == 1 and 
			in_array($valor["geoc_tipo"], array(1,2,6,10)) )
			//$pat_sel == "null" and ($valor["geoc_tipo"] == 1 || $valor["geoc_tipo"] == 2))
			{
				//$alert_geoc .= "<tr id='" . $valor["vehi_id"] . "'><td>" . $valor['vehi_patente'] . "</td><td>" . htmlentities($valor["geoc_nombre"]) 
				//			 . "</td><td>" . $valor["fecha"] . "</td></tr>";
				$alert_geoc[$valor['vehi_id']] = "<tr class='npatDIV' name='$valor[geoc_id]' id='$valor[vehi_id]'>
						<td id=\"ah\">$valor[vehi_patente]</td>
						<td>" . htmlentities($valor["geoc_nombre"]) . "</td>
						<td>$valor[fecha]</td>
					</tr>";
 
				$ln_color = "#f00";
			}			
						
			$pos[$valor["vehi_id"]] = array('lng'=>$valor["lng"], 
										'lat'=>$valor["lat"], 
										'animation'=>$animation, 
										'patente'=>$valor["vehi_patente"],
										'azimut'=>$valor["azimut"],
										'fecha'=>$valor["fecha"],
										'velocidad'=>$valor["velocidad"],
										'geoc_tipo'=>($valor["geoc_tipo"]==9)? 'Ruta' : 'Geocerca',
										'ln_color'=>$ln_color,
										'symbolpath' => $SymbolPath,
										'info'=>htmlentities($valor["geoc_nombre"]),
										'color'=>$color);
		}

		$jsondata['markers'] = $pos;
	} else {
		$jsondata["err"] = "No se encontraron datos de unidad seleccionada";
		//	$jsondata["idpat"] = $pat_sel;	
	}
}

$jsondata["alert_geoc"] = $alert_geoc;
header('Content-type: application/json; charset=utf-8');
echo json_encode($jsondata);

?>