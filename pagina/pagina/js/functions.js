	var map, mkcluster, markers = [], geoc = [], routes = [], infoWindow = [], recorrido = [], zoom, pat_rec;
	
	function progress() {
		var val = progressbar.progressbar( "value" ) || 0;
		if ( val > 95 ) {
			 val = 0;
		}
		progressbar.progressbar( "value", val + Math.floor( Math.random() * 3 ) );
		progressTimer = setTimeout( progress, 60 );
	}

	closeDownload = function closeDownload() {
		clearTimeout( progressTimer );
		popup_pb.dialog( "close" );
		progressbar.progressbar( "value", false );
	}
	
	function form_select(pref) {
		var form = "form#" + pref;
		$(form)[0].reset();  // RESERT FORM
		search_label(pref.charAt(0));  // INIT BUSQUEDA
		$(form).on( "click", "input[type=checkbox]", function() {
			var id_ul = $(this).parents("div").attr("id"); //	console.log(id_ul) modificado UL
			if ( id_ul == 'something' )
			{
				$(this).parent().hide();
				$(this).parent().css('visibility', 'hidden');
				var id = form + " input[id=" + $(this).attr("name") + "_sl]";
				$(id).prop('checked',true);
				$(id).parent().show();
				if( ul = $(this).parent().parent().attr("name") ){					
					$( form + " div.accordion" ).accordion({
						active: parseInt(ul)
					});
				}				
				switch(pref) {
					case "patent":
						myGps( $(this).attr("name") );
						break;				
					case "geoc":
						myGeocerca( $(this).attr("name") );
						break;				
					case "routes":
						route( $(this).attr("name") );
						break;
				} 
			} else {
				$(this).parent().hide();
				var id_input = $(this).attr("id").replace("_sl", "");
				var id = form + " input[id="+ id_input + "_" + pref + "]"
				$(id).prop('checked',false);
				$(id).parent().show();
				$(id).parent().css('visibility', 'visible');
				//alert(id_input);
				switch(pref) {
					case "routes":
						routes[id_input].forEach(function(elemento,i) {
							elemento.setMap(null);
						});
						//routes.splice(0,routes.length);
						delete routes[id_input];
						break;
					case "patent":			
						var clase = $("div#btn_group button i").attr("class");
						if(clase == "fa fa-object-group"){
							mkcluster.removeMarker(markers[id_input]);
						}else{
							markers[id_input].setMap(null);
						}
						delete markers[id_input];	
						break;		
					case "geoc":
						var id_gc = $(this).attr("id").replace("_sl", "");
						geoc[id_gc]['polygon'].setMap(null);						
						break;						
									
				}
			}
		});
	}