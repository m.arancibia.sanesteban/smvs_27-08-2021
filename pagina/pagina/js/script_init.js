$(function(){

	$("form#patent")[0].reset();
	$("form#geoc")[0].reset();
	$("form#routes")[0].reset();
	//$("form#recorrido")[0].reset();
	
/*	form_select("patent");
	form_select("geoc");
	form_select("routes");
	searchtr();*/
	//updatemyGps();
	var updatemyGps = setInterval(function(){myGps(null)},20000);
	myGps(null);

	
    $( "div.accordion" ).accordion( {
		active: 0,
		animate: 400,
   		collapsible: true,
		//event: "mouseover",		
		heightStyle: "content",
		maxHeight: 140,
      	maxWidth: 200		
    });
	
	$("div.icono--div").click(function() {
		var level = this;
		$( "#mySidenav" ).fadeToggle( "fast", "linear", function() {
			level.style.marginLeft = ( level.style.marginLeft == "300px" ) ? "0" : "300px";
		});
	});
	
	$("div#btn_group button").click(function() {
		if ($("div#btn_group button i.fa").attr('class') == "fa fa-object-group") {
			$("div#btn_group button i.fa").removeClass("fa-object-group").addClass("fa-object-ungroup");
		} else {
			$("div#btn_group button i.fa").removeClass("fa-object-ungroup").addClass("fa-object-group");
		}
	});

	var widthWIN = $(window).width()
	popup_rec = $('<div id="datrec"></div>').appendTo('div#main');
	popup_rec.dialog({
        position: {
                my: 'left top',
                at: 'top',
                of: $('div#main')
            },	
		autoOpen: false,
		appendTo: 'div#main',
		resizable: false,
		fluid: true,
		modal: false,
		title: 'Datos Recorrido',
		width: ( widthWIN < 400 ) ? 300 : ( widthWIN - 300 ),
		height:350
	}).parent().draggable({
            containment: 'div#main'
	});

	popup_msj = $('<div id="popup"></div>').appendTo('body');
	popup_msj.dialog( {
		title: 'Mensaje',
		autoOpen: false,
		width:400,
		minHeight:200,
		show: {
			effect: "blind",
			duration: 200
		},
		hide: {
			effect: "explode",
			duration: 200
		},
		modal: true,
		open: function() {
			$(".ui-widget-overlay").css({
				opacity: 0.5,
				filter: "Alpha(Opacity=70)",
				backgroundColor: "white"
			});
		},
		buttons: {
        	Cerrar: function() {
          		$( this ).dialog( "close" );
        	}
      	}	  
	});
	
	popup_pb = $('<div title="Cargando Recorridos"><br><div id="progressbar"></div></div>').appendTo('body');
	dialog_pb = popup_pb.dialog({
		autoOpen: false,
		closeOnEscape: false,
		resizable: false,
		modal: true,
		open: function() {
			progressTimer = setTimeout( progress, 500 );
			$(".ui-widget-overlay").css({
				opacity: 0.7,
				filter: "Alpha(Opacity=70)",
				backgroundColor: "white"
			});
		}
	});

	progressbar = $( "#progressbar" ),
    	progressbar.progressbar({
			value: false,
			complete: function() {
				closeDownload();
			}
	});

	$( "#datepicker" ).datetimepicker({
		dateFormat: "dd-mm-yy",
		timeFormat: "HH:mm",
		currentText: "Nueva",
		closeText: "Cargar",
		timeText: "Horario",
		hourText: "Hora",
		minuteText: "Minutos",				
	});	
	
	$("div#btn_group" ).on( "click", "button", function(){		
		var clase = ($(this).children("i").attr("class"));
		mkcluster.clearMarkers();
		if(clase == "fa fa-object-group"){
			$.each(Object.keys(markers), function(key, value){
				if(markers[value].getMap()!= null){
					markers[value].setMap(null);
					mkcluster.addMarker(markers[value]);
				}
			})
		}else{
			$.each(Object.keys(markers), function(key, value){
				markers[value].setMap(map);
			})
		}
	});	
	
	var on_patent = function(){
		id_patente = $(this).attr("id");
		if($(this).attr("class") != "patDIV" && $(this).attr("class") != "patDIV updsel" ) {
			myposition = markers[id_patente].getPosition();
			map.panTo(myposition);
			if( map.getZoom() < 12) {
				map.setZoom(12);
			}
			markers[id_patente].setAnimation(google.maps.Animation.DROP);
			//setTimeout(function(){ markers[id_patente].setAnimation(null); },1000);
		}else{
			var id1 = "form#patent input[id=" + id_patente + "_patent]";
			$(id1).parent().css('visibility', 'hidden');
			$(id1).prop('checked',true);
			var id2 = "form#patent input[id=" + id_patente + "_sl]";
			$(id2).prop('checked',true);
			$(id2).parent().show();			
			$("form#patent div.accordion" ).accordion({
					active: parseInt(1)
			});
			myGps(id_patente);			
		}
	}
	
	$("form#patent div#not-these" ).on( "click", "label", on_patent );	
	$("table tbody#alert_geoc" ).on( "click", "tr", on_patent );		

	$("form#geoc div#not-these" ).on( "click", "label", function(){
		id_geoc = $(this).attr("id");
		map.panTo(geoc[id_geoc]["center"]);
		map.setZoom(geoc[id_geoc]["zoom"]);
	});

	$("form#routes div#not-these" ).on( "click", "label", function(){
		id_route = $(this).attr("id");
		routes[id_route]
		map.panTo(routes[id_route]["center"]);
		map.setZoom(routes[id_route]["zoom"]);
	});
	
	
	
	$("form#recorrido" ).on( "change", "select#sel_reg", function(){
		myRec();
	});

	$("form#recorrido" ).on( "dblclick", "select#sel_reg", function(){
		myRec();	
	});
		
	$("form#recorrido" ).on( "click", "select#sel_reg", function(){
		var pat_temp = $(this).val();
		console.log(pat_rec +"=="+pat_temp);
		if(recorrido.length > 0 && pat_rec == pat_temp ){
			popup_rec.dialog('open');
		}	
	});	

	$("div#datrec").on( "click", "table tbody tr", function(){
		id_rec = $(this).attr('id')
		map.panTo(recorrido[id_rec]["pos"]);
		map.setZoom(recorrido[id_rec]["zoom"]);
		//rec = recorrido[id_rec]["line"];
	});
	
	$("form#recorrido" ).on( "click", "input#maps_rec_clean", function(){
		recorrido.forEach(function(elemento,i) {
			elemento["line"].setMap(null);
		});
		recorrido.splice(0,recorrido.length);
		popup_rec.dialog('close');
	});
				
});