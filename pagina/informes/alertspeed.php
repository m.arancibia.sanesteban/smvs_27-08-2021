<?php
	require("../include/db.php");
	$sql = "SELECT * FROM `gravedad`";
	if($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			$gravedad[$fila["grv_nombre"]]["str"] = $fila["grv_str"];
			$gravedad[$fila["grv_nombre"]]["end"] = $fila["grv_end"];
		}
		$resultado->free();	
	}

	$sql = "SELECT
			RE.regi_latitud,
			RE.regi_longitud,
			DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha, 
			RE.regi_velocidad,
			GE.geoc_id,
			GE.geoc_nombre,
			IF(GE.geoc_tipo=9, GE.geoc_codigo, GE.geoc_nombre) AS nombre,			
			GE.geoc_codigo,
			GE.geoc_max_speed,
			VE.vehi_patente,
			VE.vehi_tive_id,
			GCI.gc_spd,
			TP.tive_nombre,
			RE.regi_vehi_id
			FROM `gc_id` AS GCI 
			INNER JOIN registro AS RE ON GCI.gc_regi_id = RE.regi_id AND GCI.gc_spd IN (1,2)
			INNER JOIN geocerca AS GE ON GCI.gc_geoc_id = GE.geoc_id AND GE.geoc_m_b_speed IS NULL 
			INNER JOIN vehiculo AS VE ON VE.vehi_id = RE.regi_vehi_id AND ( VE.vehi_empr_id = 5 OR VE.vehi_tive_id = 2 )
			INNER JOIN tipo_vehiculo AS TP ON VE.vehi_tive_id = TP.tive_id
			ORDER BY vehi_patente ASC, regi_fecha_posicion ASC,  geoc_tipo DESC, gc_spd DESC
	#		GROUP BY VE.vehi_patente";
	
	$alert_temp = array();
	$alert_spd = array();

	if($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			$tolerancia = 5; 
			$max_speed = ( $fila["geoc_max_speed"] > 90 && $fila["vehi_tive_id"] == 1 ) ? 90 : $fila["geoc_max_speed"];
			$dif_vel = ( $fila["regi_velocidad"] - $max_speed - $tolerancia);
			$calgrav = round((($dif_vel*100) / $fila["geoc_max_speed"]),2);
			$indgrav = NULL;
			
			foreach ( $gravedad as $key => $valor){
				if($valor["str"] <= $calgrav && $valor["end"] >= $calgrav ){
					$indgrav = $key;
				}
			}
			if($fila["gc_spd"] > 1 ){
				$alert_spd[$fila["regi_vehi_id"]]["prev"] = true;
				
			}else if( !isset($alert_temp[$fila["vehi_patente"]] )  || @$alert_spd[$fila["regi_vehi_id"]]["vel"] < $dif_vel)
			{
				$prev =  utf8_encode($fila["vehi_patente"]) . (($alert_spd[$fila["regi_vehi_id"]]["prev"]) ? '&nbsp;**' : NULL );
				
				$alert_spd[$fila["regi_vehi_id"]]["vel"] = $dif_vel;
				$alert_temp[$fila["vehi_patente"]] = "
				<tr style='color:#333333;text-align:center;background:#f2f2f2'>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>$prev</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>" . $fila["tive_nombre"] . "</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>" . utf8_encode($fila["fecha"]) . "</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>" . utf8_encode($fila["nombre"]) . "</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'><a href=\"https://www.google.com/maps/place/" . 
							$fila["regi_latitud"] . "," . $fila["regi_longitud"] . "\">Visualizar en mapa
						</a></td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>Exceso Velocidad</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>" . $fila["geoc_max_speed"] . 
						($fila["geoc_max_speed"]!=$max_speed? "/$max_speed" : '') . "</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>" . $fila["regi_velocidad"] . "</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>$indgrav</td>
				</tr>";				
			}
		}
		$resultado->free();

		//$vehi_ids = implode(',',array_keys($alert_spd));
		//die();
			
		$sql = "UPDATE `gc_id` SET `gc_spd` = NULL
				WHERE gc_spd = 2";
		$mysqli->query($sql);

		$sql = "UPDATE `gc_id` SET `gc_spd`= 2
				WHERE gc_spd = 1";
		$mysqli->query($sql);
		$mysqli->close();
					
		if( count($alert_temp) > 0 )
		{
			$alert_geoc = implode('',$alert_temp);
			
			//print_r($alert_temp);
			//die();
			
			ob_start();
			include("./reporte/send_contacto.php");
			$content = ob_get_contents();
			ob_end_clean();
			

								
			include('../include/mime/Mail.php');
			include('../include/mime/mime.php');
			$mime = new Mail_mime($crlf);
			$text = 'www.sercoing.cl';
			$crlf = "\n";

						
			$hdrs = array ('From' => "Reporte Sercoing Ltda<reporte@sercoing.cl>",
			'Cc' => "Reporte Sercoing Ltda<reporte@smvscodelco.sercoing.cl>",
			'Subject' => "Alerta Exceso de Velocidad - DMH");			
						
						
			//$destinatario = array("Soporte TI<soporteti@sercoing.cl>");	
			//$destinatario = array("Daniel Donoso Mura<ddonoso@sercoing.cl>","Julio Muñoz<jmunoz@sercoing.cl>");	
			$destinatario = array("John Fritz<JFrit003@codelco.cl>");	
			$mime->setTXTBody($text);
			$mime->setHTMLBody($content);
			


			ob_start();
				include("./reporte/excel.php");
			$content = ob_get_contents();
			ob_end_clean();
			$file = fopen("./reporte/informe.xls", "w");
			fwrite($file, $content . PHP_EOL);
			fclose($file);
			
			$mime->addAttachment("./reporte/informe.xls", "application/vnd.ms-excel");
			
			$mimeparams['text_encoding']="8bit";
			$mimeparams['text_charset']="UTF-8";
			$mimeparams['html_charset']="UTF-8";
			$mimeparams['head_charset']="UTF-8"; 

			
			$body = $mime->get($mimeparams);
			$hdrs = $mime->headers($hdrs);
			$mail = &Mail::factory('mail');
			//$res = $mail->send($destinatario, $hdrs, $body);
			//echo (PEAR::isError($res))? 'Error enviando el email' : 'Enviado con exito';		
		}
	}
?>