<?php

	session_start();

	require("../include/db.php");
	$sql = "SELECT gc_regi_id FROM `gc_id` 
			ORDER BY `gc_regi_id` DESC
			LIMIT 1";
	if($resultado = $mysqli->query($sql)) {
		$fila = $resultado->fetch_assoc();
		echo "$_SESSION[id] == $fila[gc_regi_id]";
		if($_SESSION['id'] == $fila['gc_regi_id']){
			
			include('../include/mime/Mail.php');
			include('../include/mime/mime.php');
			$crlf = "\n";
			$mime = new Mail_mime($crlf);
						
			$hdrs = array ('From' => "Reporte Sercoing Ltda<reporte@sercoing.cl>",
			'Subject' => "ERROR DE TIGER GC_ID");			
			$destinatario = array("Daniel Donoso Mura<ddonoso@sercoing.cl>","Julio Muñoz<jmunoz@sercoing.cl>");		
			$mime->setTXTBody('www.sercoing.cl');
			$mime->setHTMLBody("ERROR TABLA GC_ID");
			
			$mimeparams['text_encoding']="8bit";
			$mimeparams['text_charset']="UTF-8";
			$mimeparams['html_charset']="UTF-8";
			$mimeparams['head_charset']="UTF-8"; 
			
			$body = $mime->get($mimeparams);
			$hdrs = $mime->headers($hdrs);
			$mail = &Mail::factory('mail');
			$res = $mail->send($destinatario, $hdrs, $body);
			echo (PEAR::isError($res))? 'Error enviando el email' : 'Enviado con exito';		
						
		}
		$_SESSION['id'] = $fila["gc_regi_id"];
		$resultado->free();
	}
	$mysqli->close();
?> 