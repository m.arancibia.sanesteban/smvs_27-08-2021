<?php

	require('../include/config.php');
	
	$mysqli = new mysqli($dbhost, $dbuser, $dbpasswd, $dbname);

	if ($mysqli->connect_errno) { 
			include('../include/mime/Mail.php');
			include('../include/mime/mime.php');
			$crlf = "\n";
			$mime = new Mail_mime($crlf);
						
			$hdrs = array ('From' => "Reporte Sercoing Ltda<reporte@sercoing.cl>",
			'Subject' => "ERROR DE CONEXION DB SMVS");			
			$destinatario = array("Daniel Donoso Mura<ddonoso@sercoing.cl>",
			                      "Daniel Donoso Mura<d.donosomura@gmail.com>",
			                      //"Patricio Palma Lobos<ppalma@sercoing.cl>",
			                      "Julio Muñoz<jmunoz@sercoing.cl>");		
			$mime->setTXTBody('www.sercoing.cl');
			$mime->setHTMLBody("ERROR DE CONEXION Motor DB SMVS " . mysqli_connect_errno() . "//" . mysqli_connect_error());
			
			$mimeparams['text_encoding']="8bit";
			$mimeparams['text_charset']="UTF-8";
			$mimeparams['html_charset']="UTF-8";
			$mimeparams['head_charset']="UTF-8"; 
			
			$body = $mime->get($mimeparams);
			$hdrs = $mime->headers($hdrs);
			$mail = &Mail::factory('mail');
			$res = $mail->send($destinatario, $hdrs, $body);
			echo (PEAR::isError($res))? 'Error enviando el email' : 'Enviado con exito';	
			exit();	
	}else{
		echo "conex exitosa";
		$mysqli->close();
	}
?> 