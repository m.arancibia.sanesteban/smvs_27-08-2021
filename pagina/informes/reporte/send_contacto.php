<html>
<head>
<title>Informe SMVS</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style>
body {
	padding: 0px;
	margin: 0px;
}
</style>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="#000000" bgcolor="#FFFFFF">
<div align="center">
  <table border="0" cellpadding="0" cellspacing="0" width="800" height="19">
    <tr>
      <td width="99%" height="19"><p align="center" style="font-size: 7pt;color:#444444; font-weight: bold; font-family: Verdana, Arial, Helvetica, sans-serif;line-height: 13px;"> <span style="color:#444444">Para asegurar la llegada de nuestros correos,<br />
          por favor agregue</span> <span><a href="mailto:reporte@sercoing.cl" style="color:#FF6600" title="Informaciones">reporte@sercoing.cl</a></span> <span style="color:#444444">a su libreta de direcci&oacute;nes de correos.</span> </p></td>
    </tr>
    <tr>
      <td width="553" align="left"><hr size="1" color="#BECCCC"></td>
    </tr>
  </table>
  <table border="0" cellpadding="0" style="border-collapse: collapse" width="800">
    <tr>
      <td valign="top" width="660">
      	<table align="center" cellSpacing=0 cellpadding=0 width=800 border=0>
          <tbody>
          	<tr style="padding:0;margin:0;border:0">
            	<td width=660 style="padding:0;margin:0;border:0" height=9><p style="text-align:justify;text-indent:30px;"><hr></p></td>
			</tr>
            <tr style="padding:0;margin:0;border:0">
              <td style="padding:0;margin:0;border:0">
              	<table cellSpacing=0 cellpadding=0 width=800 style="padding:-5px 0; margin:-5px 0;">
                    <tr>
                      <td style="padding-right: 20px; padding-left: 20px; padding-bottom: 10px; font: 12px Verdana; color: #212121; padding-top: 10px; text-align: left" width=458>
         		<p style="text-align:center; font-weight:bold; font-size:17px; color:#5C82AB; text-align:center">&quot;Alertas de exceso de velocidad&quot;
                </p>
       			<p style="font-weight:bold; font-size:14px; color:#5C82AB">Seg&uacute;n siguiente informe:</p>

	<table border=0 cellspacing=0 cellpadding=0 style='border-collapse:collapse;font-size:9pt;font-family:"Verdana",sans-serif;'>
 		<tr style='color:#FFFFFF;text-align:center;background:#4A4A4A'>
			<th style='border:solid #b2b2b2 1pt;padding:6pt'><b>Patente</b></th>
			<th style='border:solid #b2b2b2 1pt;padding:6pt'><b>Tipo vehiculo</b></th>           
            <th style='border:solid #b2b2b2 1pt;padding:6pt'><b>Fecha</b></th>
            <th style='border:solid #b2b2b2 1pt;padding:6pt'><b>Ubicaci&oacute;n</b></th>
            <th style='border:solid #b2b2b2 1pt;padding:6pt'><b>Punto GPS</b></th>
            <th style='border:solid #b2b2b2 1pt;padding:6pt'><b>Tipo Alerta</b></th>
            <th style='border:solid #f2f2f2 1pt;padding:6pt'><b>Velocidad Permitida</b></th>
            <th style='border:solid #f2f2f2 1pt;padding:6pt'><b>Velocidad registrada</b></th>
            <th style='border:solid #f2f2f2 1pt;padding:6pt'><b>Gravedad</b></th>
		</tr>        
                    		<?php echo $alert_geoc ?>
	</table>
    <p style="font-weight:bold; font-size:10px; color:#F00">** Veh&iacute;culo con faltas consecutivas de alertas anteriores</p>
    
                        </td>
                    </tr>
                </table></td>
            </tr>
			<tr style="padding:0;margin:0;">
            	<td width=600 style="padding:0;margin:0;" height=9><p style="text-align:justify;text-indent:30px;"><hr></p></td>
            </tr>
          </tbody>
        </table>
 	  </td>
    </tr>
  </table>
  <table border="0" cellpadding="0" style="border-collapse: collapse" width="553" id="table3">
    <tr style="">
      <td width="553" align="left"><p align="center" style="font-size: 7pt; font-weight: bold; font-family: Verdana, Arial, Helvetica, sans-serif;	line-height: 13px;"> <span style="color:#444444">Este correo electr&oacute;nico ha sido enviado 
      en forma autom&aacute;tica por</span> <span style="color:#FF6644">reporte@sercoing.cl</span> </td>
    </tr>
    <tr>
      <td width="553" align="left"><hr size="1" color="#BECCCC"></td>
    </tr>
    <tr>
      <td width="553" align="left"><p align="center" style="font-size: 7pt;color:#444444; font-weight: bold; font-family: Verdana, Arial, Helvetica, sans-serif;	line-height: 13px;">&quot;SMVS SERCOING LTDA - CODELCO DMH&quot;<br />
          Los Andes - Chile</p></td>
    </tr>
  </table>
</div>
</body>
</html>