<?php

	require('../include/config.php');
	$dbname = 'serco_dev';
	$mysqli = new mysqli($dbhost, $dbuser, $dbpasswd, $dbname);
	
	
	if ($mysqli->connect_errno) {  //mysqli_connect_errno()
		printf("Falló la conexión con el servidor"); //_ %s\n", mysqli_connect_error()
		exit();
	}

	$sql = "SELECT UNIX_TIMESTAMP(UPDATE_TIME) AS UPDATE_TIME FROM information_schema.tables
			WHERE  TABLE_SCHEMA = 'serco_dev' AND TABLE_NAME = 'registro'";
			
	$resultado = $mysqli->query($sql);
	$fila = $resultado->fetch_assoc();
	$timeDif = time() - $fila["UPDATE_TIME"];	
	$resultado->free();
	
	$sql = "SELECT UNIX_TIMESTAMP(UPDATE_TIME) AS UPDATE_TIME FROM information_schema.tables
			WHERE  TABLE_SCHEMA = 'serco_dev' AND TABLE_NAME = 'registros'";
			
	$resultado = $mysqli->query($sql);
	$fila = $resultado->fetch_assoc();
	echo $timeDif_s = time() - $fila["UPDATE_TIME"];	
	$resultado->free();	
	
	$mysqli->close();


	if ( $timeDif > 120 || $timeDif_s > 150) { 
	
			include('../include/mime/Mail.php');
			include('../include/mime/mime.php');
			$crlf = "\n";
			$mime = new Mail_mime($crlf);
						
			$hdrs = array ('From' => "Reporte Sercoing Ltda<reporte@sercoing.cl>",
			'Subject' => "ERROR DE WS - OUTLINE");	
			
			if ($timeDif_s > 150) {
			$destinatario = array("Daniel Donoso Mura<ddonoso@sercoing.cl>",
			                      "Daniel Donoso Mura<d.donosomura@gmail.com>",
			                      "Hernan Cañas<hernan@hosted.cl>",
			                      "Julio Muñoz<jmunoz@sercoing.cl>");				  
			} else {				
			$destinatario = array("Daniel Donoso Mura<ddonoso@sercoing.cl>",
			                      "Daniel Donoso Mura<d.donosomura@gmail.com>",
			                      //"Patricio Palma Lobos<ppalma@sercoing.cl>",
			                      "Julio Muñoz<jmunoz@sercoing.cl>");
			}

			$mime->setTXTBody('www.sercoing.cl');
			$table = ($timeDif_s > 150) ? "s" : "";
		$mime->setHTMLBody("ERROR DE WS SIN INGRESO ULTIMOS 3 MINUTOS ( t.Registro$table )" . mysqli_connect_errno() . "//" . mysqli_connect_error());
			
			$mimeparams['text_encoding']="8bit";
			$mimeparams['text_charset']="UTF-8";
			$mimeparams['html_charset']="UTF-8";
			$mimeparams['head_charset']="UTF-8"; 
			
			$body = $mime->get($mimeparams);
			$hdrs = $mime->headers($hdrs);
			$mail = &Mail::factory('mail');
			$res = $mail->send($destinatario, $hdrs, $body);
			echo (PEAR::isError($res))? 'Error enviando el email' : 'Enviado con exito';	
			exit();	
	}
?> 