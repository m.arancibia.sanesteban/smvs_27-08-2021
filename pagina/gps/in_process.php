<?php
	require("../include/db.php");

	$sql = "SELECT
						RE.regi_vehi_id,
						MAX(RE.regi_id) regi_id
					FROM registro AS RE
					INNER JOIN gc_id AS GCI ON RE.regi_id = GCI.gc_regi_id
					INNER JOIN geocerca AS GE ON GCI.gc_geoc_id = GE.geoc_id
					WHERE RE.regi_fecha_posicion > (UNIX_TIMESTAMP()-(180))
								AND GE.geoc_item IN (21,22,23,31,32,33)
					GROUP BY RE.regi_vehi_id";

	$process = array();					
	if($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
				$process[$fila["regi_vehi_id"]] = $fila["regi_id"];
		}
		$resultado->free();	
	}
	
	$ids = implode(',', array_keys($process)); 	
	$sql = "UPDATE `vehiculo` SET `vehi_regi_id` = CASE vehi_id \n"; 
	foreach ($process as $id => $valor) { 
		$sql .= sprintf("WHEN %d THEN %d \n", $id, $valor); 
	} 
	$sql .= "END WHERE vehi_id IN ($ids)"; 
	
	$mysqli->query($sql);
	$mysqli->close();
	echo $sql;
?>