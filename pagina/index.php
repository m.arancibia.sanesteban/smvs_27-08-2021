<?php
	require './include/smarty-3.1.33/libs/Smarty.class.php';
	require './include/db.php';
 
	$smarty = new Smarty;
	$smarty->force_compile = false;
	$smarty->debugging = false;
	$smarty->caching = false;
	$smarty->cache_lifetime = 120;
	
	$div_priv = array(0,3,4,5);
	$seccion_priv = array("mov","geoc","rut","rec","not");

	
	$smarty->assign("seccion_priv",$seccion_priv);
	$smarty->assign("hacpan",(count($seccion_priv)*34) );
		
	$sql = 'SELECT
			MAX(vehi_id) AS id,
			vehi_patente AS patente,
			vehi_tive_id AS tive
			FROM vehiculo
			WHERE LEFT(vehi_patente, 1) != "@"
				GROUP BY patente, vehi_tive_id
				ORDER BY 3 ASC, 2 ASC';	
	$moviles = array();
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_object()) {
			$moviles[$fila->tive][$fila->id] = $fila->patente;			
		}
   		$resultado->close();
	}
	$smarty->assign("moviles",$moviles);	
		
	$sql = 'SELECT
			GE.geoc_codigo
			FROM `geocerca` AS GE
				WHERE geoc_tipo IN(8,9)
				GROUP BY geoc_codigo
				ORDER BY 1 ASC';	
	$route = array();
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_object()) {
			$routes[] = utf8_encode($fila->geoc_codigo);		
		}
   		$resultado->close();
	}	
	$smarty->assign("routes",$routes);
	
	$sel_priv = implode(',',$div_priv);
	$smarty->assign("haclist",(count($div_priv)*20) );
		
	$sql = "SELECT
    		GE.geoc_id,
            GE.geoc_nombre,
            GE.geoc_codigo,
			IFNULL(EM.empr_nombre,'VARIAS') AS empr_nombre
            FROM `geocerca` AS GE
	        	LEFT JOIN empresa AS EM ON GE.geoc_empr_id = EM.empr_id 
				WHERE geoc_tipo IN(1,2,3,4,5,6,7,10) AND ( geoc_empr_id IN($sel_priv) OR geoc_empr_id IS NULL ) 
				ORDER BY 4 ASC, 3 ASC";	
	$geoc = array();
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_object()) {
			$id = $fila->empr_nombre;
			$geoc[$id][$fila->geoc_id]['nombre'] = utf8_encode($fila->geoc_nombre);			
			$geoc[$id][$fila->geoc_id]['codigo'] = utf8_encode($fila->geoc_codigo);			
		}
   		$resultado->close();
	}
	$mysqli->close();	
	//SSSprint_r($geoc);
	$smarty->assign("geoc",$geoc);	
	$smarty->display('index.tpl');
?>