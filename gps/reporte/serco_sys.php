<?php

	include("../include/db.php");
	
	$sql = "SELECT * FROM `gravedad`";
	if($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			$gravedad[$fila["grv_nombre"]]["str"] = $fila["grv_str"];
			$gravedad[$fila["grv_nombre"]]["end"] = $fila["grv_end"];
		}
		$resultado->free();	
	}

	$sql = "SELECT
			RE.regi_latitud,
			RE.regi_longitud,
			DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha, 
			RE.regi_velocidad,
			GE.geoc_id,
			GE.geoc_nombre,
			GE.geoc_codigo,
			GE.geoc_max_speed,
			VE.vehi_patente
			FROM `gc_id` AS GCI 
			INNER JOIN registro AS RE ON GCI.gc_regi_id = RE.regi_id AND GCI.gc_spd = TRUE
			INNER JOIN geocerca AS GE ON GCI.gc_geoc_id = GE.geoc_id
			INNER JOIN vehiculo AS VE ON VE.vehi_id = RE.regi_vehi_id";

	$alert_geoc = "";
	if($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			$tolerancia = ( $fila["geoc_id"] == 1 ) ? 0 : 5; 
			$dif_vel = ( $fila["regi_velocidad"] - $fila["geoc_max_speed"] - $tolerancia);
			$calgrav = round((($dif_vel*100) / $fila["geoc_max_speed"]),2);
			$indgrav = NULL;
			
			foreach ( $gravedad as $key => $valor){
				if($valor["str"] <= $calgrav && $valor["end"] >= $calgrav ){
					$indgrav = $key;
				}
			}	 
			 
			$alert_geoc .= "
				<tr style='color:#333333;text-align:center;background:#f2f2f2'>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>" . htmlentities($fila["vehi_patente"]) . "</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>" . htmlentities($fila["fecha"]) . "</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>" . htmlentities($fila["geoc_nombre"]) . "</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'><a href=\"https://www.google.com/maps/place/" . 
							$fila["regi_latitud"] . "," . $fila["regi_longitud"] . "\">Visualizar en mapa
						</a></td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>Exceso Velocidad</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>" . $fila["geoc_max_speed"] . "</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>" . $fila["regi_velocidad"] . "</td>
						<td style='border:solid #b2b2b2 1pt;padding:6pt'>$indgrav</td>
				</tr>";				
		}
		$resultado->free();

		$sql = "UPDATE `gc_id` SET `gc_spd`= NULL ";
		$mysqli->query($sql);
		$mysqli->close();
		
		if(!empty($alert_geoc))
		{
			ob_start();
			include("./reporte/send_contacto.php");
			$content = ob_get_contents();
			ob_end_clean();
						
			include('../include/mime/Mail.php');
			include('../include/mime/mime.php');
			$mime = new Mail_mime($crlf);
			$text = 'www.sercoing.cl';
			$crlf = "\n";
			$hdrs = array('From' => "Reportes Sercoing Ltda<reporte@sercoing.cl>",
						'Subject'=> "Alerta Exceso de Velocidad");
						
			$hdrs = array ('From' => "Reportes Sercoing Ltda<reporte@sercoing.cl>",
			'Cc' => "Daniel Donoso Mura<ddonoso@sercoing.cl>",
			'BCC' => "Daniel Donoso Mura<ddonoso@sercoing.cl>",
			'Subject' => "Alerta Exceso de Velocidad");			
						
						
			$destinatario = array("Patricio Vega Araya<pvega017@codelco.cl>,Hernan Guzman Silva<hguzm003@codelco.cl>,Jorge Santiago Carpio<jsant033@codelco.cl>,Daniel Donoso Mura<ddonoso@sercoing.cl>");	
			$mime->setTXTBody($text);
			$mime->setHTMLBody($content);
			
			ob_start();
				include("./reporte/excel.php");
			$content = ob_get_contents();
			ob_end_clean();
			$file = fopen("./reporte/informe.xls", "w");
			fwrite($file, $content . PHP_EOL);
			fclose($file);
			
			$mime->addAttachment("./reporte/informe.xls", "application/vnd.ms-excel");
			
			$mimeparams['text_encoding']="8bit";
			$mimeparams['text_charset']="UTF-8";
			$mimeparams['html_charset']="UTF-8";
			$mimeparams['head_charset']="UTF-8"; 
			
			$body = $mime->get($mimeparams);
			$hdrs = $mime->headers($hdrs);
			$mail = &Mail::factory('mail');
			$res = $mail->send($destinatario, $hdrs, $body);
			echo (PEAR::isError($res))? 'Error enviando el email' : 'Enviado con exito';		
		}
	}
?>