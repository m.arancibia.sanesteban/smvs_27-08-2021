<?php 

error_reporting(0);

if( count($_POST) > -1 ) 
{
	include("../include/class_all_search.php");
	$gps = new gps_serco();	

	$jsondata = array();
	
	$rec_pat = @$_POST["sel_reg"];
	$date_end = @$_POST["date_end"];
	$rec_hours = (@$_POST["rec_hours"] * 3600);
	
	$temp_date = str_replace("/", "-", $date_end);
	$end_date = strtotime($temp_date);
	$start_date = $end_date - $rec_hours;
	
	$jsondata['data'] = "$temp_date, $end_date";	
/*	$rec_pat = 1700566;
	$start_date = 1537498740;
	$end_date = 1537585140;

	$rec_pat = 135;
	$start_date =1544580000;
	$end_date = 1544583600;
*/
	if($rec_pat > 0 && $start_date > 0 && $end_date > 0) {

		$gps->format_vars($rec_pat, $start_date, $end_date);
		$vars = $gps->exec_sql(false);
		//print_r($vars);

		$jsondata['pos'] = $vars;

		$tr = "";
		$id_tr = -1;
		foreach($vars as $var){

			$id_tr++;

			if(!isset($var['st'])) continue;

			$icon = ( @$var['st'] == "exceso") ? "glyphicon-hdd speeding" : "glyphicon-minus-sign stop";
			$rpy = (@$var['replay']>2)? "<span id=\"replay\">(".$var['replay'].")</span>":"";

			$tr .= "<tr id=\"$id_tr\">			
				<th id=\"ahb\" scope=\"row\"><span class=\"glyphicon $icon \"></span>".$rpy."</th>
				<td>" . $var['date_in'] . "</td>
				<td>" . ($var['geoc_codigo1']) . "</td>
				<td>" . $var['date_out'] . "</td>
				<td>" . ($var['geoc_codigo2']) . "</td>			
				<td>" . $var['dist_rec'] . "&nbsp;KM</td>
				<td>" . round($var['time_rec']/60) . "&nbsp;min</td>			
				<td>" . round($var['speed_dif']/$var['rows']) . "</td>	
				<td>" . $var['max_speed'] . "</td>				
				<td>" . round($var['geoc_max_speed']/$var['rows']) . "</td>	
				</tr>\n\t\t\t";
		}
		
		// class=\"table table-hover table-dark fixed\"
		$jsondata['popup'] = "<div id=\"constrainer\">
		<table>
	  	<thead>
			<tr>
			  <th id=\"ahh\">Evento</th>		
			  <th>Inicio Evento</th>
			  <th>Geoc Inicio</th>
			  <th>Fin Evento</th>
			  <th>Geoc Fin</th>
			  <th>Distancia</th>
			  <th>Duracion</th>
			  <th>Velocidad %</th>
			  <th>Velocidad MAX</th>		  
			  <th>Velocidad Perm %</th>
	   		</tr>
	  	</thead>
		<tbody>
				$tr
		</tbody>
		</table>
		</div>";
				
		$jsondata['pat'] = $rec_pat;

	}else{
			$jsondata['msj_err'] = "Falta el ingreso de informacion";	
	}
	//print_r($jsondata);
	header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();		
}
?>
