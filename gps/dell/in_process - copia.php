<?php

	require("../include/db.php");
	
	$tpgc = array(  11=>1,
					12=>1,
					21=>1,
					31=>1,
					23=>1,
					33=>1,
					22=>1,
					32=>1);

	$sql = "SELECT
			VE.vehi_id,
			GE.geoc_item,
			GE.geoc_id
			FROM vehiculo AS VE
			INNER JOIN registro AS RE ON VE.vehi_regi_id = RE.regi_id
			INNER JOIN gc_id AS GCI ON RE.regi_id = GCI.gc_regi_id
			INNER JOIN geocerca AS GE ON GCI.gc_geoc_id = GE.geoc_id
			WHERE regi_fecha_posicion > (UNIX_TIMESTAMP()-(60*60*24)) AND vehi_tive_id = 1
			HAVING geoc_item IN (11,12,21,22,23,31,32,33)
			ORDER BY VE.vehi_id ASC, GE.geoc_item DESC";

	$prev = array();	
	$descargas = array(22,32);	
	if($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			if(!isset($prev[$fila["vehi_id"]]) && !in_array($fila["geoc_item"],$descargas) ){
				$prev[$fila["vehi_id"]] = $fila["geoc_id"];
			}
		}
		$resultado->free();
	}
	echo count($prev);
	print_r($prev);

	$sql = "SELECT
			RE.regi_id,
			RE.regi_vehi_id,
			GE.geoc_item
			FROM
				(SELECT
						regi_id,
						regi_vehi_id,
						regi_fecha_posicion
					FROM registro
					WHERE regi_fecha_posicion BETWEEN (UNIX_TIMESTAMP()-(60*10.5)) AND UNIX_TIMESTAMP()
					#HAVING regi_vehi_id IN(16) 
				)AS RE
					INNER JOIN gc_id AS GCI ON RE.regi_id = GCI.gc_regi_id
					INNER JOIN  geocerca AS GE ON GCI.gc_geoc_id = GE.geoc_id
					HAVING geoc_item IN (11,12,21,22,23,31,32,33) 
					#GROUP BY RE.regi_vehi_id, geoc_item
					ORDER BY RE.regi_vehi_id ASC, regi_fecha_posicion ASC";

	$process = array();					
	if($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			$bool1 = !isset($prev[$fila["regi_vehi_id"]]);
			$bool2 = $tpgc[$fila["geoc_item"]] > @$tpgc[@$prev[$fila["regi_vehi_id"]]];
			
			if($bool1 || $bool2) {
				$process[$fila["regi_vehi_id"]] = $fila["regi_id"];
			}
		}
		$resultado->free();	
	}
	
	$ids = implode(',', array_keys($process)); 	
	$sql = "UPDATE `vehiculo` SET `vehi_regi_id` = CASE vehi_id \n"; 
	foreach ($process as $id => $valor) { 
		$sql .= sprintf("WHEN %d THEN %d \n", $id, $valor); 
	} 
	$sql .= "END WHERE vehi_id IN ($ids)"; 
	
	//$mysqli->query($sql);
	$mysqli->close();
	echo $sql;
?>