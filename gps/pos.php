<?php
//sleep(2);

include("../include/db.php");
include_once("../desarrollo/class/viajeModel.php");

date_default_timezone_set("America/Santiago");

//$jsondata["post"] = serialize($_POST);
$jsondata = array();
$jsondata['success'] = "dfaklsdfjalfjñla";
$alert_geoc = array();

include('../include/functions.php');
$session = charge_session();
if(is_array($session)) {
	$jsondata['success'] = true;

	if( count($_POST) > 1 ) {
		$pat_sel = $_POST['pat_sel'];
		unset($_POST['pat_sel']);

		foreach($_POST as $key => $valor) {
			$temp[] = "$key";
		}

		$patSel = implode(',',$temp);	

		$patentes = (!empty($pat_sel) && $pat_sel != "null" )? $pat_sel : $patSel ;
		$time_upd = 1;

	} else {
		$patSel = 0;	
		$time_upd = 15;
	}

	require("../include/config_priv.php");

	$sel_priv_geo = implode(',',$priv_gc[$div_user]);
	$sel_priv_pat = implode(',',$priv_ttes_id[$div_user]);

	$sql = "SELECT
			RE.regi_id,
			VE.vehi_id,
			VE.vehi_tive_id,
			RE.regi_latitud AS lat,
			RE.regi_longitud AS lng,
			DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%H:%i:%s') AS fecha, #%d-%m-%Y 
			RE.regi_azimut AS azimut,
			RE.regi_velocidad AS velocidad,
			(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_recibido AS SIGNED)) AS dif_reg,
			(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_posicion AS SIGNED)) AS dif_time,
			VE.vehi_patente,
			GE.geoc_nombre,
			GE.geoc_tipo,
			GE.geoc_id,
			GE.geoc_empr_id
		FROM `registro` AS RE 
		INNER JOIN `vehiculo` AS VE ON RE.regi_vehi_id = VE.vehi_id AND VE.vehi_tive_id = 1
			AND RE.regi_fecha_posicion > (UNIX_TIMESTAMP()-(60*$time_upd))
			AND RE.regi_vehi_id NOT IN($patSel) AND VE.vehi_empr_id IN($sel_priv_pat)
		INNER JOIN `gc_id` AS GI ON RE.regi_id = GI.gc_regi_id
		INNER JOIN `geocerca` AS GE ON GE.geoc_id = GI.gc_geoc_id
			AND GE.geoc_visible = 1 AND GE.geoc_tipo IN(1,6,10)
		HAVING geoc_empr_id IN($sel_priv_geo)
		ORDER BY RE.regi_id DESC, GE.geoc_tipo DESC";


	/*if($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc()) {
			if(empty($alert_geoc[$fila['vehi_id']])) {
				$alert_geoc[$fila['vehi_id']] = "<tr class='patDIV' name='$fila[geoc_id]' id='$fila[vehi_id]'>
					<td id=\"ah\">$fila[vehi_patente]</td>
					<td>" . htmlentities($fila["geoc_nombre"]) . "</td>
					<td>$fila[fecha]</td>
					</tr>";				
			}
		}
		$resultado->free();
	}*/

	// traer despachos activos
	$m_viajes = new viajeModel();
	$date_now = new DateTime();
	$now = $date_now->getTimestamp();
	$since = $now - (60 * 60 * 48);
	$horas = (60 * 60 * 2);
	$notis = array();

	$divisiones = array(
		4 => 'SITT',
		5 => 'CAMI',
	);

	$id_vehis = array();
	if(!empty($patentes)) {
		$id_vehis = array_map('intval', explode(',', $patentes));
	}

	$data_desp_sel = array();

	$viajes = $m_viajes->get_viajes_despachados();
	foreach ($viajes as $viaje) {
		$estado = '';
		// descartar viajes terminados hace mas de 2 horas
		if (in_array($viaje['estado'], array(-1, -2))){
			if (($viaje['f_posicion_des'] + $horas) > $now) {
				$estado = 'Terminado';
			} else{
				continue;
			}
		}elseif ($viaje['estado'] < -3) {
			if (($viaje['f_salida'] + ($horas * 6)) > $now) {
				$estado = 'Cancelado';
			} else{
				continue;
			}
		}elseif ($viaje['estado'] == 1) {
			$estado = 'Informado';
		}else{
			$estado = 'En ruta';
		}

		$date_format = date('d-m-Y H:i:s', $viaje['f_salida']);

		/*$notis[$divisiones[$viaje['division']]][$viaje['vehi_id']] = "<tr class='patDIV' id='{$viaje['vehi_id']}'>
				<td id=\"ah\">{$viaje['patente']}</td>
				<td>{$date_format}</td>
				<td>{$m_viajes->set_index_geoc($viaje, 'ori')}</td>
				<td>{$m_viajes->set_index_geoc($viaje, 'des')}</td>
				<td>{$estado}</td>
			</tr>";*/

		if (in_array($viaje['vehi_id'], $id_vehis)) {
			$data_desp_sel[$viaje['vehi_id']] = array(
				'f_despacho' => $date_format,
				'origen' => $m_viajes->set_index_geoc($viaje, 'ori'),
				'destino' => $m_viajes->set_index_geoc($viaje, 'des'),
				'estado' => $estado
			);
		}
	}


	if( !empty($patentes) ) {

		$verde = '#00FF00';
		$amarilla = '#FFFF00';
		$rojo = '#FF0000';
		$azul = '#0000FF';
		$negro = '#000';
		$celeste = "#6699FF";

		$set = "SELECT
				MAX(regi_id) AS regi_id
			FROM `registro`
			WHERE regi_vehi_id IN($patentes) 
			GROUP BY regi_vehi_id";

		$ids = 0;	
		if($resultado = $mysqli->query($set)) {
			while ($fila = $resultado->fetch_assoc()) {
				$temp_ids[] = $fila['regi_id'];
			}

			if(isset($temp_ids)) {
				$ids = implode(',',$temp_ids);
			}
		}

		$sql = "SELECT
				RE.regi_id,
				VE.vehi_id,
				VE.vehi_tive_id,
				RE.regi_latitud AS lat,
				RE.regi_longitud AS lng,
				DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
				RE.regi_azimut AS azimut,
				RE.regi_velocidad AS velocidad,
				(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_insercion AS SIGNED)) AS dif_reg,
				(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_posicion AS SIGNED)) AS dif_time,
				VE.vehi_patente,
				IF( GE.geoc_tipo=9, GE.geoc_codigo, GE.geoc_nombre) AS geoc_nombre,
				GE.geoc_tipo,
				GE.geoc_id
			FROM `registro` AS RE
			INNER JOIN `vehiculo` AS VE ON RE.regi_vehi_id = VE.vehi_id
				AND ( RE.regi_id IN ($ids) OR RE.regi_fecha_posicion > (UNIX_TIMESTAMP()-(60*23)) )
			LEFT JOIN `gc_id` AS GI ON RE.regi_id = GI.gc_regi_id
			LEFT JOIN `geocerca` AS GE ON GE.geoc_id = GI.gc_geoc_id
				AND GE.geoc_visible = 1
			HAVING vehi_id IN($patentes)
			ORDER BY RE.regi_vehi_id, RE.regi_fecha_posicion DESC, RE.regi_fecha_recibido ASC, GE.geoc_tipo DESC";

		$points_pat = array();
		if($resultado = $mysqli->query($sql)) {
			while ($fila = $resultado->fetch_assoc()) {
				if( @!$points_pat[$fila["vehi_id"]]["st_move"] ) {
					if(empty($points_pat[$fila["vehi_id"]])) {
						$points_pat[$fila["vehi_id"]] = $fila;
					} else {
						$points_pat[$fila["vehi_id"]]["dif_time"] = $fila["dif_time"];
					}
					$points_pat[$fila["vehi_id"]]["st_move"] = ($fila["velocidad"] > 0);
				}
			}
			$resultado->free();
		}
		$mysqli->close();

		if( count($points_pat)>0) {
			foreach($points_pat as $valor) {
				$animation = ($pat_sel == $valor["vehi_id"]) ? 'BOUNCE' : 'DROP';
				$dif_time = $valor["dif_time"];
				$dif_reg = $valor["dif_reg"];
				$SymbolPath = false;
				$ln_color = "#33f";

				switch (true) {
					case $dif_reg > 86400:
						$color = $negro;
						$SymbolPath = true;
						$ln_color = "#ff8000";
						break;

					case ( $dif_time-$dif_reg ) > 3600:
						$color = $celeste;
						$SymbolPath = true;
						$ln_color = "#333";
						break;

					case $dif_reg > 900:
						$color = $azul;
						break;

					case $dif_time < 300:
						$color = $verde;
						break;

					case $dif_time < 1200:
						$color = $amarilla;
						break;

					default:
						$color = $rojo;
						break;
				}

				if(!empty($valor["geoc_nombre"]) and $valor["vehi_tive_id"] == 1 and in_array($valor["geoc_tipo"], array(1,2,6,10)) ) {
					
					$alert_geoc[$valor['vehi_id']] = "<tr class='npatDIV' name='$valor[geoc_id]' id='$valor[vehi_id]'>
						<td id=\"ah\">$valor[vehi_patente]</td>
						<td>" . htmlentities($valor["geoc_nombre"]) . "</td>
						<td>$valor[fecha]</td>
						</tr>";
					$ln_color = "#f00";
				}			

				$pos[$valor["vehi_id"]] = array(
					'lng'=>$valor["lng"], 
					'lat'=>$valor["lat"], 
					'animation'=>$animation, 
					'patente'=>$valor["vehi_patente"],
					'azimut'=>$valor["azimut"],
					'fecha'=>$valor["fecha"],
					'velocidad'=>$valor["velocidad"],
					'geoc_tipo'=>($valor["geoc_tipo"]==9)? 'Ruta' : 'Geocerca',
					'ln_color'=>$ln_color,
					'symbolpath' => $SymbolPath,
					'info'=>htmlentities($valor["geoc_nombre"]),
					'color'=>$color,
				);

				if (array_key_exists($valor["vehi_id"], $data_desp_sel)) {
					$pos[$valor["vehi_id"]]['f_despacho'] = $data_desp_sel[$valor["vehi_id"]]['f_despacho'];
					$pos[$valor["vehi_id"]]['origen'] = $data_desp_sel[$valor["vehi_id"]]['origen'];
					$pos[$valor["vehi_id"]]['destino'] = $data_desp_sel[$valor["vehi_id"]]['destino'];
					$pos[$valor["vehi_id"]]['estado'] = $data_desp_sel[$valor["vehi_id"]]['estado'];
					$pos[$valor["vehi_id"]]['despacho'] = true;
				}else{
					$pos[$valor["vehi_id"]]['despacho'] = false;
				}
			}

			$jsondata['markers'] = $pos;
		} else {
			$jsondata["err"] = "No se encontraron datos de unidad seleccionada";
			$jsondata["idpat"] = $pat_sel;	
		}
	}

}else{
	$jsondata['success'] = false;	
}

/*$jsondata["alert_geoc"] = $alert_geoc;
$jsondata["notificaciones"] = $notis;*/
// $jsondata["notificaciones"] = $alert_geoc;
if (array_key_exists('SITT', $notis)) {
	$jsondata["alert_geoc"] = $notis['SITT'];	
}

/*if (array_key_exists('CAMI', $notis)) {
	$jsondata["alert_geoc_cami"] = $notis['CAMI'];
}*/
header('Content-type: application/json; charset=utf-8');
echo json_encode($jsondata);

?>