$(document).ready(function(){

    let config_table = {
        // "dom": 'BSfrtp',
        "processing": true,
        "serverSide": false,
        // "rowId": "id_vehiculo", 
        "oLanguage": {
            "sSearch": "Buscar:"
        },
        "language": {
            "emptyTable": "No hay equivalencias para mostrar."
        },
        "ajax": {
            "url": "../ajax/dt_equivalencias.php",
            "type": "POST"
        },
        "rowId": "id_row",
        "columns": [
            { "data": "id" },
            { "data": "geo_origen" },
            { "data": "geo_target" },
            { "data": "nombre" },
            {
                "data": "estado",
                "render":  (data, type, row, meta) => data == 1 ? 'Activa' : 'Inactiva'
            },
            {
                "data": "id",
                "render":  (data, type, row, meta) => `<a data-id="${data}" class="btn_edit"><i class="fa fa-edit"></a>`
            }
        ],
        "initComplete": function(settings, json) {
            init_buttons()
        },
    }

    let tabla_viajes = $("#table_equiv").DataTable(config_table)
    tabla_viajes.on('draw', function() {
        init_buttons();
      });
    $('#form_equiv').hide()

    const init_buttons = () => {
        $("#cb_geo").combobox()
        $("#cb_geo_target").combobox()
        $(".btn_edit").on('click', function (){
            let id = $(this).data('id')
            getEquiv(id)
        })
    }
    

    const getEquiv = async (id) => {
        try {
            let url = '../ajax/equivalencias.php?op=get'
            let body = new FormData()
            body.append('id', id)
            const res = await fetch(url, {
                method: 'POST',
                body: body
            })
            const data = await res.json()
            create_form(data, 'update')
            console.log(data)
        } catch (error) {
            console.log(error)
        }

    }

    $("#btn_new").click(function (e) {
        e.preventDefault()
        create_form(null, 'create')
    })

    const create_form = (eq, op) => {
        $("#txt_op").val(op)
        if (op == 'update') {
            $("#txt_id").val(eq.id)
            $("#cb_geo").val(eq.id_geo_original)
            $("#cb_geo_target").val(eq.id_geo_target)
            $("#txt_nombre").val(eq.nombre)
            $("#cb_activa").val(eq.activa)
            $("#modal_label").text('Editar Equivalencia')
            $("#box_activa").show()
            updateCombo(eq.id_geo_original, eq.id_geo_target)
        }else if (op == 'create'){
            $("#cb_geo").val(-1)
            $("#cb_geo_target").val(-1)
            updateCombo(-1, -1)
            $("#box_activa").hide()
            $("#modal_label").text('Crear Equivalencia');
            $("#txt_nombre").val('');
        }
        $('#modal_equiv').modal('show')
        $("#form_equiv").show()
    }

    const updateCombo = () => {
        label_geo = $('select[name="geo_ori"] option:selected').text()
        label_geo_target = $('select[name="geo_tar"] option:selected').text()
        $('.input-pesao:first').val(label_geo)
        $('.input-pesao:last').val(label_geo_target)
    }

    const boton = document.getElementById('btn_equiv')
    const form = document.getElementById('form_equiv')

    boton.addEventListener('click', (e) => {send_data(e)})

    const send_data = async (e) => {
        e.preventDefault()
        try {
            let op = $("#txt_op").val()
            let url = `../ajax/equivalencias.php?op=${op}`
            let body = new FormData(form)
            const res = await fetch(url, {
                method: 'POST',
                body: body
            })
            const data = await res.json()
            console.log('done')
            tabla_viajes.ajax.reload()
            tabla_viajes.draw()
            init_click_editar()
        } catch (error) {
            console.log(error)
        }
    }


    /* TODO:
    - pedir el objeto del id a editar
    - iniciar ojeto en el formulario
    - enviar formulario para actualizar

    -crear funciones que creen formulario, para editar y crear
    -crear destructor de formulario

    -crear modal
     */

    /* document.querySelectorAll(".btn_edit").forEach(item =>{
        item.addEventListener('click', (e) => {
            console.log(item)
            console.log('loco')
        })
    })
    
    const editar = (e) => {
        e.preventDefault()
        console.log('boton')
    } */
})