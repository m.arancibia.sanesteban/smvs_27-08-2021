
	var all_data = "";
	Chart.defaults.global.legend.labels.usePointStyle = true;

	$(function(){
		var ctx_1 = $('#chart-legend-top').get(0).getContext('2d');
		window.myPie1 = new Chart(ctx_1, config_1);

		var ctx_2 = $('#chart-legend-right').get(0).getContext('2d');
		window.myPie2 = new Chart(ctx_2, config_2);		

		var ctx_3 = $('#chart-legend-bottom').get(0).getContext('2d');
		window.myPie3 = new Chart(ctx_3, config_3);

		var ctx_4 = $('#chart-legend-left').get(0).getContext('2d');
		window.myPie4 = new Chart(ctx_4, config_4);

		var ctx_5 = $('#chart-legend-dmh').get(0).getContext('2d');
		window.myPie5 = new Chart(ctx_5, config_div);

		var ctx_6 = $('#chart-legend-dch').get(0).getContext('2d');
		window.myPie6 = new Chart(ctx_6, config_div);

		var ctx_7 = $('#chart-legend-dsal').get(0).getContext('2d');
		window.myPie7 = new Chart(ctx_7, config_div);

		var ctx_8 = $('#chart-legend-ext').get(0).getContext('2d');
		window.myPie8 = new Chart(ctx_8, config_div);

	});
			
	function myChart() {
		var url = "./ajax/chart.php";
		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			data: $("form#form_chart").serialize(),
			success: function(data)
			{
				if( data.msj != undefined ){
					//alert(data.msj);
					popup_msj.html( "<br>" + data.msj); //Se han cargado
					popup_msj.dialog('open');
					setTimeout("popup_msj.dialog('close')", 700);
				}else{
					config_1.data.labels = eval( "[" + data.y1 + "]" );			
					config_1.data.datasets[0].data = eval( "[" + data.x1 + "]" );
					window.myPie1.update();

					config_2.data.labels = eval( "[" + data.y2 + "]" );		
					config_2.data.datasets[0].data = eval( "[" + data.x2 + "]" );
					window.myPie2.update();
					
					barChartData.labels = eval( "[" + data.empresa_grav + "]" );
					barChartData.datasets[0].data = eval( "[" + data.tgravedad["Gravisimo"] + "]" );
					barChartData.datasets[1].data = eval( "[" + data.tgravedad["Grave"] + "]" );
					barChartData.datasets[2].data = eval( "[" + data.tgravedad["Leve"] + "]" );
					config_3.data = barChartData;
					window.myPie3.update();

					all_data =  data.analisis;
					$("div#table_detail_emp").html(data['table_detail_emp']);
					$("#resumen_graf div.group").html(data['table_group_grav']);
					$("#exceso_emp div.group").html(data['table_group_emp']);
					

					
					var temporal = joinJson(data.divisiones.DCH);
					config_div.data.labels = temporal['key'];
					config_div.data.datasets[0].data = temporal['data'];
					window.myPie5.update();
				}
			}
		})
	}
	
	function joinJson($array) {
		var temp={}, temp1=[], temp2=[], temp3=[], suma = 0;
		var formater = new Intl.NumberFormat("de-DE");
		$.each($array,function(key,data) {
			temp1.push(key);
			temp2.push(data);	
			suma += data;
            temp3 += '<div class="input-group">'+
                  	'<p class="input-group-addon bg-orange">'+key+'</p>'+
                 	'<span class="input-group-addon">'+formater.format(data)+'</span>'+
                	'</div>';		
		
		});	
		temp3 += '<div class="input-group">'+
        		'<p class="input-group-addon bg-orange">Total</p>'+
                '<span class="input-group-addon">'+formater.format(suma)+'</span>'+
                '</div>';
		temp['key'] = temp1;
		temp['data'] = temp2;
		temp['group'] = temp3;
		return temp;
	}
	
	var tooltips_pie = {
		enabled: true,
		mode: 'label',
		callbacks: {
			title: function (tooltipItem, data) { 
				return data.labels[tooltipItem[0].index]; 
			},
			label: function(tooltipItems, data) {
				//console.log();
				var total = eval(data.datasets[tooltipItems.datasetIndex].data.join("+"));
				var amount = data.datasets[0].data[tooltipItems.index];
				var promedio = Math.round((amount * 100) / total);
				return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' ( ' + parseFloat(amount * 100 / total).toFixed(2) + '% )';
				//data.labels[tooltipItems.index]
			}
		}
	}

	var config_1 = {
		type: 'pie',
		data: {
			datasets: [{
				backgroundColor: [
					window.chartColors.red,
					window.chartColors.orange,
					window.chartColors.yellow
				],
				label: 'Gravedad'
			}],
		},
		options: {
			/*title: {
				display: true,
				text: 'CANTIDAD / GRAVEDAD DE EXCESO DE VELOCIDAD'
			},*/
			responsive: true,
			'onClick' : function (evt, item) {
				//alert(config_1.data.labels[item[0]._index]);
			},
			legend: {
				display: true,
				position: 'right',
				labels: {
				  boxWidth: 40,
				  fontColor: 'black'
				}
			},
			tooltips: tooltips_pie
		}
	};
	
	var config_2 = {
		type: 'pie',
		data: {
			datasets: [{
				backgroundColor: [
					"#0000ff",
					"#ffc0cb",
					"#008000",
					"#ffff00",
					"#ff0000",
					"#000080",
					"#00ffff",
					"#ff7f50",
					"#a52a2a",
				],
				label: 'EXCESO POR EMPRESA',
				borderColor: 'rgba(255,255,255,0.5)',
				borderWidth: 1				
			}],
		},
		options: {
			/*title: {
				display: true,
				text: 'EXCESO POR EMPRESA'
			},*/
			responsive: true,
			'onClick' : function (evt, item) {
				//alert(config_2.data.labels[item[0]._index]);
			},
			legend: {
				display: true,
				position: 'top',
				labels: {
				  boxWidth: 40,
				  fontColor: 'black'
				}
			},
			tooltips: tooltips_pie
		}			
	};

	var barChartData = {
		datasets: [{
			label: 'Gravisimo',
			backgroundColor: window.chartColors.red,
		},{
			label: 'Grave',
			backgroundColor: window.chartColors.orange,
		},{
			label: 'Leve',
			backgroundColor: window.chartColors.yellow,
		}]
	};

	var tooltips_bar = {
		mode: 'index',
		intersect: false,
		yAlign: 'bottom',
		itemSort: function(a, b) {
			return b.datasetIndex - a.datasetIndex
		},						
		callbacks: {
			title: function (tooltipItem, data) { return data.labels[tooltipItem[0].index]; },
			label: function (tooltipItem, data) {
				var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				var total = 0;
				for (i = 0; i < data.datasets.length; i++) {
					total += parseInt(data.datasets[i].data[tooltipItem.index]);
				}
				return amount + ' ( ' + parseFloat(amount * 100 / total).toFixed(2) + '% )';
			}
		},
	};	
	
	var config_3 = {
		type: 'bar',
		data: barChartData,
		options: {
			/*title: {
				display: true,
				text: 'DETALLE FALTAS POR EMPRESA'
			},*/
			tooltips: tooltips_bar,
			'onClick' : function (evt, item) {
				var data = all_data[config_2.data.labels[item[0]._index]];
				var patentes = data['patentes'];
				var grvs = data['grvs'];
				var grav = data['grav'];
				var leve = data['leve'];
				$("div#table_detail_pat").html(data['table']);				
				window.myPie4.data = {
					labels: patentes.split(','),
					datasets: [{
						label: 'Gravisimo',
						backgroundColor: window.chartColors.red,
						data: grvs.split(',')
					},{
						label: 'Grave',
						backgroundColor: window.chartColors.orange,
						data: grav.split(',')
					},{
						label: 'Leve',
						backgroundColor: window.chartColors.yellow,
						data: leve.split(',')
					}]
				};
				window.myPie4.update();
			},							
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					scaleLabel: { 
						display: true,
						labelString: 'EMPRESAS',
						fontSize: 10,
					}
				}],
				yAxes: [{
					stacked: true,
					scaleLabel: { 
						display: true,
						labelString: '# FALTAS',
						fontSize: 10,
					}
				}]
			}
		}
	};
	
	var config_4 = {
		type: 'bar',
		options: {
			/*title: {
				display: true,
				text: 'DETALLE FALTAS POR PATENTE'
			},*/
			tooltips: tooltips_bar,			
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					scaleLabel: { 
						display: true,
						labelString: 'PATENTES',
						fontSize: 10,
					}
				}],
				yAxes: [{
					stacked: true,
					scaleLabel: { 
						display: true,
						labelString: '# FALTAS / GRAVEDAD',
						fontSize: 10,
					}
				}]
			}
		}
	};
	
	var config_div = {
		type: 'pie',
		data: {
			datasets: [{
				backgroundColor: [
					window.chartColors.red,
					window.chartColors.orange,
					window.chartColors.yellow
				],
				label: 'Gravedad'
			}],
		},
		options: {
			legend: {
				display: true,
				position: 'right',
				labels: {
				  boxWidth: 40,
				  fontColor: 'black'
				}
			},
			tooltips: tooltips_pie
		}
	};