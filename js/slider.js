(function($){
$.fn.Slider = function(options) {
    var defaults = {
        speed: 400,
    };
    var settings = $.extend({}, defaults, options);
    var Slider = function(settings) {
        this.o = this;
        this.speed = options.speed;
        this.slider = $('#main-slider');
        this.children = this.slider.children();
        this.activeSlide = this.children.first().addClass('active');
        this.slideNumber = this.children.length;
        this.sliderOuterWrapper;
        this.sliderWrapper;
        this.sliderArrow;
        this.actualPosition;
        this.outerWidth;

        function changeSlide(elem) {
            this.nextSlide = elem.next();
            this.prevSlide = elem.prev();
        };

        this.init = function() {

            // Variables
            //––––––––––––––––––
					
            var outerWidth = window.innerWidth;
			this.outerWidth = outerWidth
            this.actualPosition = 0;

            this.slider.children().remove();
            this.slider.html('<div class="slider-wrapper-outer"></div><div class="slider-controls"></div>');

            this.sliderOuterWrapper = this.slider.find('.slider-wrapper-outer');
            this.sliderOuterWrapper.html('<div class="slider-wrapper"></div>');

            this.sliderWrapper = this.sliderOuterWrapper.find('.slider-wrapper');
            this.sliderWrapper.html(this.children);

            this.children.addClass('slider-item').css('width', outerWidth);
            this.children = this.sliderWrapper.children();
			
            this.sliderArrow = {
                right : this.slider.find('.arrow.arrow-right'),
                left : this.slider.find('.arrow.arrow-left'),
				goto0 : $('.arrow-goto0'),
				goto1 : $('.arrow-goto1'),
				goto2 : $('.arrow-goto2'),
            };

            this.sliderWrapper.css({
                'width': this.slideNumber * outerWidth,
                'transform': "translate3d(0px, 0px, 0px)",
            });

            events(this);
        };
        var events = function(el) {

            var ob = el.o;
            // Slider Right
            el.sliderArrow.right.click(function() {
                ob.goToNextSlide();
            });
            // Slider Left
            el.sliderArrow.left.click(function() {
                ob.goToPrevSlide();
            });
            // goto Left
            el.sliderArrow.goto0.click(function() {
                ob.goToSlide(0);
				$("li.active").removeClass('active');
				$(this).parent().addClass('active');			
            });
            el.sliderArrow.goto1.click(function() {
                ob.goToSlide(1);
				$("li.active").removeClass('active');
				$(this).parent().addClass('active');
            });
            el.sliderArrow.goto2.click(function() {
                ob.goToSlide(2);
				$("li.active").removeClass('active');
				$(this).parent().addClass('active');
            });
            // Resize
            $(window).resize(function() {
                console.log('resize');
                ob.resize();
            });
        };
        var setActiveSlide = function(el, direction) {
            el.activeSlide = el.sliderWrapper.find('.active').removeClass('active');
            if (direction == 'next')
                el.activeSlide = new changeSlide(el.activeSlide).nextSlide;
            else if (direction == 'prev')
                el.activeSlide = new changeSlide(el.activeSlide).prevSlide;
			else
				el.activeSlide = direction
            el.activeSlide.addClass('active');
        }
		
        this.goToSlide = function(num) {
			
			var marginLeft = $( "div.icono--div").css("margin-left");
			//alert(marginLeft);
			if(num == 0){
                $("#switch_desp").show()
				$( "div.icono--div").show();
				if( marginLeft == "300px" ){
					$( "#mySidenav").show();
				}
			}else{
				$( "div.icono--div").hide();
				if( marginLeft == "300px" ){
					$( "#mySidenav").hide();
                }
                $(".desp-btn").css('margin-right', '50px');
                $(".side-bar-right").css('width', '3px');
                $("#switch_desp").hide()
			}
            this.actualPosition =  ( -this.outerWidth * num );
            this.sliderWrapper.css({
                'transition': 'all 1000ms',
                'transform': "translate3d(" + parseInt(this.actualPosition) + "px, 0px, 0px)",
            });
           	this.sliderWrapper.find('.active').removeClass('active');
          	this.activeSlide = this.children.eq( num ).addClass('active');
        };
		
        this.goToNextSlide = function() {
            // Set actual childrens
            this.children = this.sliderWrapper.children();

            // Core Function
            this.actualPosition = ( this.actualPosition - outerWidth );
            this.sliderWrapper.css({
                'transition': 'all ' + this.speed + 'ms',
                'transform': "translate3d(" + parseInt(this.actualPosition) + "px, 0px, 0px)",
            });
            if ( this.activeSlide.index() + 1 == this.slideNumber ) {
                // Go To Start
                this.goToStart();
            } else {
                // Change active slide
                setActiveSlide(this, 'next');
            }
        };
        this.goToPrevSlide = function() {

            // Set actual childrens
            this.children = this.sliderWrapper.children();
            this.actualPosition = this.actualPosition + outerWidth;
            this.sliderWrapper.css({
                'transition': 'all ' + this.speed + 'ms',
                'transform': "translate3d(" + parseInt(this.actualPosition) + "px, 0px, 0px)",
            });
            if ( this.activeSlide.index() == 0 ) {
                // Go To End
                this.goToEnd();
            } else {
                // Change active slide
                setActiveSlide(this, 'prev');
            }
        };
        this.goToStart = function() {

            this.actualPosition = 0;
            this.sliderWrapper.css({
                'transition': 'all 1000ms',
                'transform': "translate3d(" + parseInt(this.actualPosition) + "px, 0px, 0px)",
            });
            this.sliderWrapper.find('.active').removeClass('active');
            this.activeSlide = this.children.first().addClass('active');
        }
        this.goToEnd = function() {

            this.actualPosition = -(this.slideNumber - 1) * outerWidth;
            this.sliderWrapper.css({
                'transition': 'all 1000ms',
                'transform': "translate3d(" + parseInt(this.actualPosition) + "px, 0px, 0px)",
            });
            this.sliderWrapper.find('.active').removeClass('active');
            this.activeSlide = this.children.last().addClass('active');
        };
        this.resize = function() {
            outerWidth = $(window).outerWidth();
            this.actualPosition = ((-this.activeSlide.index()) * outerWidth);

            this.sliderWrapper.css({
                'width': this.slideNumber * outerWidth,
                'transform': "translate3d(" + this.actualPosition + "px, 0px, 0px)",
                'transition': 'all 0ms'
            });
            this.children.css({
                'width': outerWidth,
                'transition': 'all 0ms'
            });
        };
        this.init();
    };
    var slider = new Slider(options);
}
})( jQuery );