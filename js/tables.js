$(document).ready(function(){
	// inicializacion datatable cami
	var tablacami = $("#desp_cami").DataTable(init_dt("ajax/dt_despachos.php?div=5"));

	$(tablacami.table().body()).addClass('pointer');

	$('#desp_cami tbody').on('click', 'tr', function () {
		draw_pos(tablacami, this);
    });

	// inicializacion datatable sitt
    var tablasitt = $("#desp_sitt").DataTable(init_dt("ajax/dt_despachos.php?div=4"));

	$(tablasitt.table().body()).addClass('pointer');

	$('#desp_sitt tbody').on('click', 'tr', function () {
		draw_pos(tablasitt, this);
    });

	// retorna objeto de configuracion de dt, recibe el endpoint a consumir
    function init_dt(endpoint){
    	return {
			"dom": 'BSfrtp',
			"paging": false,
			"processing": true,
	        "serverSide": false,
	        "rowId": "id_vehiculo", 
	        "oLanguage": {
				"sSearch": "Buscar:"
			},
			"language": {
				"emptyTable": "Sin viajes informados."
			},
	        "ajax": {
	            "url": endpoint,
	            "type": "POST"
	        },
	        "columns": [
	            { "data": "patente" },
	            { "data": "despachado" },
	            { "data": "origen" },
	            { "data": "destino" },
	            { "data": "estado" },
	        ],
	        "createdRow": function(row, data, dataIndex) {
				set_status_class(row, data, dataIndex);
			}
    	}
    }

    // dibuja la posicion del vehiculo seleccionado
    function draw_pos(tabla, i){
    	var data = tabla.row( i ).data();
        var id_patente = data.id_vehiculo;

		var id1 = "form#patent input[id=" + id_patente + "_patent]";
		$(id1).parent().css('visibility', 'hidden');
		$(id1).prop('checked',true);
		var id2 = "form#patent input[id=" + id_patente + "_sl]";
		$(id2).prop('checked',true);
		$(id2).parent().show();			
		$("form#patent div.accordion" ).accordion({
			active: parseInt(1)
		});
		myGps(id_patente);
		tabla.ajax.reload();
		tabla.draw();
    }

    // setea la clase de estado del viaje
    function set_status_class(row, data, dataIndex){
    	$(row).addClass('patDIV');
    	if (data.estado == 'Terminado') {
    		$(row).addClass('info');
    	}else if (data.estado == 'Cancelado') {
    		$(row).addClass('danger');
    	}else if (data.estado == 'Informado') {
    		$(row).addClass('warning');
		}else if (data.estado == 'En ruta') {
			$(row).addClass('success');
    	}
    }
});
