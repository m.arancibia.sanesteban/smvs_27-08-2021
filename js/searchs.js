	function search_label(div_id) {
		switch(div_id) {
		  case 'r':
		  	form = 'routes';
			break;
		  case 'p':
		  	form = 'patent';
			break;
		  case 'g':
		  	form = 'geoc';
			break;
		}
		var busqueda = $('#search_' + div_id),
		titulo = $('form#'+ form +' div#something ul li label');
		$(titulo).each(function(){
			var li = $(this);
			$(busqueda).keyup(function(){
				//this.value = this.value.toLowerCase();
				$(li).parent().hide();
				var txt = $(this).val().toLowerCase();
				var bool = ( $(li).attr("title").toLowerCase().indexOf(txt) > -1 && $(li).attr("title") != "");
				if( ($(li).text().toLowerCase().indexOf(txt) > -1 || bool) && ($(li).parent().css('visibility') == "visible") ){
					$(li).parent().show();
				}
			});
		});
	}
			
	function searchtr() {
		var busqueda = $('#search_tp'),
		titulo = $('form#recorrido div select#sel_reg option');
		$(titulo).each(function(){
			var li = $(this);
			$(busqueda).keyup(function(){
				this.value = this.value.toLowerCase();
				var txt = $(this).val();
				$(li).hide();
				if($(li).text().toLowerCase().indexOf(txt) > -1){
					$(li).show();
				}
			});
		});
	}