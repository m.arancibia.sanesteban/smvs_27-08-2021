	Chart.defaults.global.legend.labels.usePointStyle = true;

	var all_data_pat = "", all_data_div = "";
	var color = new Array( "yellow", "orange", "coral", "green", "lime", "purple", "red", "blue", "pink");
	var color_hex = ['#ffcd56','#ff9f40','#ff6384','#4bc0c0','#66ff6e','#9966ff','#ff3c3c','#007dd1','#ff66ff'];
	var formater = new Intl.NumberFormat("de-DE");

	$(function(){
		var ctx_1 = $('#chart-legend-top').get(0).getContext('2d');
		window.myPie1 = new Chart(ctx_1, config_1);
			
		var ctx_2 = $('#chart-legend-right').get(0).getContext('2d');
		window.myPie2 = new Chart(ctx_2, config_2);		

		var ctx_3 = $('#chart-legend-bottom').get(0).getContext('2d');
		window.myPie3 = new Chart(ctx_3, config_3);

		var ctx_4 = $('#chart-legend-left').get(0).getContext('2d');
		window.myPie4 = new Chart(ctx_4, config_4);

		var ctx_5 = $('#chart-legend-div').get(0).getContext('2d');
		window.myPie5 = new Chart(ctx_5, config_div);

		var ctx_6 = $('#chart-legend-div-grv').get(0).getContext('2d');
		window.myPie6 = new Chart(ctx_6, config_div_grv);
		
	});
	
	function myChart() {
		var url = "./ajax/chart.new.php";		
		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			data: $("form#form_chart").serialize(),
			success: function(data)
			{
				if( data.msj != undefined ){
					popup_msj.html( "<br>" + data.msj); //Se han cargado
					popup_msj.dialog('open');
					setTimeout("popup_msj.dialog('close')", 700);
				}else{			
					var temporal_1 = joinJsonPie(data.grv);					
					config_1.data.labels = temporal_1['key'];			
					config_1.data.datasets[0].data = temporal_1['data'];
					window.myPie1.update();
					$("#resumen_grv div.group").html(temporal_1.group);

					var temporal_2 = joinJsonPie(data.emp);
					config_2.data.labels = temporal_2['key'];		
					config_2.data.datasets[0].data = temporal_2['data'];
					window.myPie2.update();
					$("#exceso_emp div.group").html(temporal_2.group);
					
					var temporal_3 = joinJsonBar(data.emp_grv);
					barChartData.labels = temporal_3.key;
					barChartData.datasets[0].data = temporal_3.data["Gravisimo"];
					barChartData.datasets[1].data = temporal_3.data["Grave"];
					barChartData.datasets[2].data = temporal_3.data["Leve"];
					config_3.data = barChartData;
					window.myPie3.update();

					all_data_pat =  data.pat_grv;
					all_data_div =  data.div;
					$("div#table_detail_emp").html(data['table_detail_emp']);
					var temporal_5 = joinJsonPie(data.div_grv);
					config_div.data.labels = temporal_5['key'];
					config_div.data.datasets[0].data = temporal_5['data'];
					window.myPie5.update();
					$("#div div.group").html(temporal_5.group);

					$("div#table_detail_pat").html("");				
					window.myPie4.data = {
						labels: [],
						datasets: []
					};
					window.myPie4.update();
					
					$("#div_grv div.group").html("");
					config_div_grv.data.labels = [];		
					config_div_grv.data.datasets[0].data = [];	
					window.myPie6.update();

				}
			}
		})
	}
	
	function joinJsonBar(array) {
		var temp={}, temp1=[], temp2=[], temp3=[], suma = 0;
		var color = color;
		var bg_color = 0;
		$.each(array,function(key,data) {
			temp1.push(key);			
			$.each(data,function(key_data,data_data) {
				if(typeof temp2[key_data] == 'undefined') {
					temp2[key_data] = [];
				}
				temp2[key_data].push(data_data);
			});
		});
		temp['key'] = temp1;
		temp['data'] = temp2;
		return temp;
	}	
	
		
	function joinJsonPie(array) {
		var temp={}, temp1=[], temp2=[], temp3=[], suma = 0;
		var bg_color = 0;
		$.each(array,function(key,data) {
			temp1.push(key);
			temp2.push(data);	
			suma += data;
            temp3 += '<div class="input-group">'+
                  	'<p class="input-group-addon bg-'+color[bg_color++]+'">'+key+'</p>'+
                 	'<span class="input-group-addon">'+formater.format(data)+'</span>'+
                	'</div>';	
		});
		if(Object.keys(array).length==3){
			temp3 += '<div class="input-group">'+
        		'<p class="input-group-addon bg-lime">Total</p>'+
                '<span class="input-group-addon">'+formater.format(suma)+'</span>'+
                '</div>';
		}
		temp['key'] = temp1;
		temp['data'] = temp2;
		temp['group'] = temp3;
		return temp;
	}	
	
	var tooltips_pie = {
		mode: 'nearest',
		enabled: true,
		intersect: true,
		shadow: false,
		callbacks: {
			title: function (tooltipItem, data) { 
				return data.labels[tooltipItem[0].index]; 
			},
			label: function(tooltipItems, data) {
				var total = eval(data.datasets[tooltipItems.datasetIndex].data.join("+"));
				var amount = data.datasets[0].data[tooltipItems.index];
				var promedio = Math.round((amount * 100) / total);
				return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' ( ' + parseFloat(amount * 100 / total).toFixed(1) + '% )';
			}
		}
	}
	
	var tooltips_bar = {
		mode: 'index',
		intersect: false,
		yAlign: 'bottom',
		itemSort: function(a, b) {
			return b.datasetIndex - a.datasetIndex
		},						
		callbacks: {
			title: function (tooltipItem, data) { return data.labels[tooltipItem[0].index]; },
			label: function (tooltipItem, data) {
				var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				var total = 0;
				for (i = 0; i < data.datasets.length; i++) {
					total += parseInt(data.datasets[i].data[tooltipItem.index]);
				}
				var porcentaje = amount + ' ( ' + parseFloat(amount * 100 / total).toFixed(2) + '% )';
				return  amount + (  (amount>0)? ' ( ' + parseFloat(amount * 100 / total).toFixed(1) + '% )' : "" ); 				
			}
		},
	};

	function config_base() {
		var base = {
			type: 'pie',
			data: {
				datasets: [{}],
			},
			options: {
				responsive: true,
				//events: false,
				animation: {
					duration:500,
					easing: "linear"
				},
				legend: {
					display: true,
					labels: {
					  boxWidth: 400,
					  fontColor: 'black'
					}
				},

				tooltips: tooltips_pie,
			},
			plugins: {
				afterDatasetDraw: function(chart) {
					var ctx = chart.chart.ctx;
					ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
					ctx.textAlign = 'center';
					ctx.textBaseline = 'bottom';
					chart.data.datasets.forEach(function (dataset) {
						for (var i = 0; i < dataset.data.length; i++) {
							var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
							total = dataset._meta[Object.keys(dataset._meta)[0]].total,
							mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius),
							start_angle = model.startAngle,
							end_angle = model.endAngle,
							mid_angle = start_angle + (end_angle - start_angle)/2;

							var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
							var div = ((Math.round(dataset.data[i]/total*100)) / 10 ) + 1.1 ;
							mid_radius = mid_radius / div ;
							var x = mid_radius * Math.cos(mid_angle);
							var y = mid_radius * Math.sin(mid_angle);
							if(dataset.data[i] != 0 && dataset._meta[Object.keys(dataset._meta)[0]].data[i].hidden != true ) {
								ctx.fillStyle = '#DADADA';
								ctx.fillText(percent, model.x + x+1, model.y + y + 11);
								ctx.fillStyle = '#333';
								ctx.fillText(percent, model.x + x, model.y + y + 10);
							}
						}
					});  
				}
			},
		};
		return base;
	}
	
	var config_1 = config_base();
	config_1.data.datasets[0].backgroundColor = color_hex;
	config_1.options.legend.position = 'right';
	
	var config_2 = config_base();
	config_2.data.datasets[0].backgroundColor = color_hex;
	config_2.data.datasets[0].borderColor = 'rgba(255,255,255,0.5)';
	config_2.data.datasets[0].borderWidth = 1;
	config_2.options.legend.position = 'top';
		
	var config_div = config_base();
	config_div.data.datasets[0].backgroundColor = color_hex;
	config_div.data.datasets[0].borderColor = 'rgba(255,255,255,0.5)';
	config_div.data.datasets[0].borderWidth = 1;
	config_div.options.legend.position = 'top';
	config_div.options.onClick = function (evt, item) {
		if( item.length > 0) { 
			var data = all_data_div[config_div.data.labels[item[0]._index]];
			var temporal_div = joinJsonPie(data);
			config_div_grv.data.labels = temporal_div['key'];		
			config_div_grv.data.datasets[0].data = temporal_div['data'];	
			window.myPie6.update();	
			$("#div_grv div.group").html(temporal_div.group);				
		}
	};
	
	var config_div_grv = config_base();
	config_div_grv.data.datasets[0].backgroundColor = color_hex;
	config_div_grv.options.legend.position = 'top';	
	
	var barChartData = {
		datasets: [{
			label: 'Gravisimo',
			backgroundColor: window.chartColors.red,
		},{
			label: 'Grave',
			backgroundColor: window.chartColors.orange,
		},{
			label: 'Leve',
			backgroundColor: window.chartColors.yellow,
		}]
	};

	var config_3 = {
		type: 'bar',
		data: barChartData,
		options: {
			tooltips: tooltips_bar,
			'onClick' : function (evt, item) {
				if( item.length > 0) {
					var data = all_data_pat[config_2.data.labels[item[0]._index]];
					$("div#table_detail_pat").html(data['table']);				
					window.myPie4.data = {
						labels: data['patentes'],
						datasets: [{
							label: 'Gravisimo',
							backgroundColor: window.chartColors.red,
							data: data['Gravisimo']
						},{
							label: 'Grave',
							backgroundColor: window.chartColors.orange,
							data:  data['Grave']
						},{
							label: 'Leve',
							backgroundColor: window.chartColors.yellow,
							data: data['Leve']
						}]
					};
					window.myPie4.update();
				}
			},							
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					scaleLabel: { 
						display: true,
						labelString: 'EMPRESAS',
						fontSize: 10,
					}
				}],
				yAxes: [{
					stacked: true,
					scaleLabel: { 
						display: true,
						labelString: '# FALTAS',
						fontSize: 10,
					}
				}]
			}
		}
	};
	
	var config_4 = {
		type: 'bar',
		options: {
			tooltips: tooltips_bar,			
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					scaleLabel: { 
						display: true,
						labelString: 'PATENTES',
						fontSize: 10,
					}
				}],
				yAxes: [{
					stacked: true,
					scaleLabel: { 
						display: true,
						labelString: '# FALTAS / GRAVEDAD',
						fontSize: 10,
					}
				}]
			}
		}
	};
