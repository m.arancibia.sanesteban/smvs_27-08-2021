$(document).ready(function(){
    const button = document.getElementById('btn_analyze')
    const button_tabla = document.getElementsByClassName('link-tabla')

    let config_table = {
        // "dom": 'BSfrtp',
        "processing": true,
        "serverSide": false,
        // "rowId": "id_vehiculo", 
        "oLanguage": {
            "sSearch": "Buscar:"
        },
        "language": {
            "emptyTable": "Sin viajes para Analizar."
        },
        "ajax": {
            "url": "ajax/dt_analyzer.php?op=sitt",
            "type": "POST"
        },
        "columns": [
            {
                "data": "id",
                "render": (data, type, row, meta) => `<input type="checkbox" name="cb_viaje" value="${data}">`
            },
            { "data": "id" },
            { "data": "vehiculo" },
            { "data": "estado" },
            { "data": "origen" },
            { "data": "destino" },
            { "data": "salida" },
            { "data": "insert" },
            { "data": "reg_origen" },
            { "data": "reg_destino" },
            { "data": "obs" }
        ],
    }

    let tabla_viajes = $("#table_analyzer").DataTable(config_table);

    button.addEventListener('click', (e) => {analizar(e)})

    Array.from(button_tabla).forEach((boton) => {
        const tabla = boton.dataset.tabla
        boton.addEventListener('click', (e) => cambio_tabla(e, tabla))
    })

    const cambio_tabla = (e, tabla) => {
        e.preventDefault()
        console.log(tabla)
        tabla_viajes.destroy()
        config_table.ajax.url = `ajax/dt_analyzer.php?op=${tabla}`
        tabla_viajes = $("#table_analyzer").DataTable(config_table);
    }

    const analizar = (e) => {
        e.preventDefault()
        const viajes = document.getElementsByName('cb_viaje')
        const viajes_seleccionados = []
        viajes.forEach ((viaje) => {
            if (viaje.checked) {
                viajes_seleccionados.push(viaje.value)
            }
        })
        
        let url = 'ajax/analizar_viajes.php'
        let data = new FormData()
        data.append('viajes', JSON.stringify(viajes_seleccionados))

        fetch(url, {
            method: 'POST',
            body: data
        }).then(res => res.json())
        .catch(error => console.error(error))
        .then(response => {
            console.log(response);
        })
        console.log(viajes_seleccionados)
    }
})