$(document).ready(function() {

    // Basic Functions
    //------------------------------
    function windowWidth() {
        var winWidth = $(window).width();
        // console.log(winWidth);
        return winWidth;
    };
    function windowHeight() {
        var winHeight = $(window).height();
        // console.log(winHeight);
        return winHeight;
    };

    // Slider Init
    //------------------------------
    //$('body').css('height', windowHeight());

    $('#main-slider').Slider({
        speed: 1000
    });

	//$('.arrow-goto0').click();
    // Functions onResize
    //------------------------------
    $(window).resize(function() {
       $('body').css('height', windowHeight());
    });
})
