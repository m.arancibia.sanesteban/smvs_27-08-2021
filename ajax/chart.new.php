<?php
	require("admin.php");
	
	$date =  @$_POST["date"];
	$opt_report =  @$_POST["opt_report"];
	//$date = "2019/4/9";
	//$opt_report = 2;
	switch ($opt_report) {
    case 1:
        $fechas = info_day($date);
		$msj = "l dia";
        break;
    case 2:
        $fechas = info_week($date);
		$msj = " la semana";
        break;
    case 3:
       	$fechas = info_month($date);
		$msj = "l mes";
		break;
	default:
		$fechas = array();

		break;
	}
	if(count($fechas)>0) { 
		//$txt = unserialize(fread($myfile,filesize($file)));
		//fclose($myfile);
		//print_r(info_mes($date));

		require("../include/db.php");
		require('../include/functions.php');
		$session = charge_session();
		
		$sql = "SELECT
				geoc_id,
				LEFT(geoc_codigo,3) AS COD
				FROM `geocerca` 
				HAVING COD IN ('DSA','DMH','DCH')
				ORDER BY geoc_id";
		if($resultado = $mysqli->query($sql)) {
			while ($fila = $resultado->fetch_assoc()) {
				$geoc[$fila["geoc_id"]] = $fila["COD"];
			}
			$resultado->free();
		}
		
		// INICIALIZAR VARIABLES
		$tgravedad = array('Leve'=>0,'Grave'=>0,'Gravisimo'=>0);
		$grv = $tgravedad;	
		//$div_user = "ALL";
		require("../include/config_priv.php");
				
		if($div_user=="ALL") {
			$div = array('DCH'=>$tgravedad,'DMH'=>$tgravedad,'DSAL'=>$tgravedad,'EXTERNA'=>$tgravedad);
			$div_grv = array('DCH'=>0,'DMH'=>0,'DSAL'=>0,'EXTERNA'=>0);			
		}else{
			$div = array($div_user=>$tgravedad,'EXTERNA'=>$tgravedad);
			$div_grv = array($div_user=>0,'EXTERNA'=>0);	
		}

		$read_file = false;

		foreach($fechas as $date) {
			$file = "../var_dump/sp/$date.php";	
			if(@$myfile = fopen( $file, "r")) {  //or die("Unable to open file!");
				$read_file = true;
				$txt = unserialize(fread($myfile,filesize($file)));
				fclose($myfile);
				
		// INICIO CALCULO GENERAL
		foreach($txt as $var) {			
			$key = ($var[5] == NULL)? 'SIN_INFORMACION' : $var[5]; 
			$name_emp = ucwords(strtolower($key));
			
			$ndiv = !isset($geoc[$var[4]]) ? 'EXTERNA': $geoc[$var[4]];
				
			if(!((in_array($ndiv,array_keys($div_grv)) || $div_user=="ALL") && in_array(array_search($key,$list_ttes),$priv_ttes["$div_user"]) ) ){
				continue;
			};
				
			$div[$ndiv][$var[6]]++;
			$div_grv[$ndiv]++;
			
			@$emp[$name_emp]++; 
			$grv[$var[6]]++; 
			if(!isset($emp_grv[$name_emp])){
				$emp_grv[$name_emp] = $tgravedad;
			}
			$emp_grv[$name_emp][$var[6]]++;
			
			switch ($var[6]) {
				case "Leve":
					$puntos = 1;
					break;
				case "Grave":
					$puntos = 2;
					break;
				case "Gravisimo":
					$puntos = 3;
					break;
			}
			@$emp_pat[$name_emp]["$var[0]"]["puntos"] += $puntos;
			@$emp_pat[$name_emp]["$var[0]"]["$var[6]"]++;			
		}
		// FIN CALCULO GENERAL
			}
		}
				
		// INICIO ORDEN SCORE
		if($read_file == true)
		{
			$grv_pat = array();
			$contador = 0;
			foreach($emp_pat as $emp_tmp => $pat ) {
				$temp_data = array();
				foreach($pat as $key => $data ) {
					$puntos = -$data["puntos"];
					unset($data['puntos']);
					$temp_data[$puntos][$key] = $data;
				}
				ksort($temp_data);
				$grv_pat["patentes"] = array();
				foreach($temp_data as $pat ) {
					foreach($pat as $key => $data ) {
						if(count($grv_pat["patentes"])<30){
							$grv_pat["patentes"][] = $key;		
							foreach( array_keys($tgravedad) as $grv_tmp ) {
								$grv_pat[$grv_tmp][] = (isset($data[$grv_tmp])) ? $data[$grv_tmp] : 0;
							}
						}
					}
				}
				
				
				$grv_pat["table"] = "
				<table class=\"table table-condensed table-bordered\">
						<thead>
							<tr>
								<th>T° Gravedad</th>
								<th>".implode("</th>\n\t\t\t\t\t\t\t<th>",$grv_pat["patentes"])."</th>             
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Leve</td>
								<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$grv_pat["Leve"])."</td>             
							</tr>
							<tr>
								<td>Grave</td>
								<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$grv_pat["Grave"])."</td>             
							</tr>
							<tr>
								<td>Gravisimo</td>
								<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$grv_pat["Gravisimo"])."</td>             
						  </tr>
						</tbody>
					</table>";			
				$pat_grv[$emp_tmp] = $grv_pat;
				$grv_pat = array();
			}
			// FIN ORDEN SCORE
			ksort($emp);
			ksort($emp_grv);
			
			
			$all_gravedad = array();
			foreach($emp_grv as $grv_temp){
				foreach($grv_temp as $key => $var ){
					$all_gravedad[$key][] = (isset($var)) ? $var : 0;
				}
			}
			
			$jsondata["table_detail_emp"] =  "
				<table class=\"table table-condensed table-bordered\">
						<thead>
							<tr>
								<th>Gravedad</th>
								<th>".implode("</th>\n\t\t\t\t\t\t\t<th>",array_keys($emp))."</th>             
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Leve</td>
								<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$all_gravedad["Leve"])."</td>             
							</tr>
							<tr>
								<td>Grave</td>
								<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$all_gravedad["Grave"])."</td>             
							</tr>
							<tr>
								<td>Gravisimo</td>
								<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$all_gravedad["Gravisimo"])."</td>             
						  </tr>
						</tbody>
					</table>";
					
	
			$jsondata["grv"] = $grv;
			$jsondata["div"] = $div;
			$jsondata["div_grv"] = $div_grv;
			$jsondata["emp"] = $emp;
			$jsondata["emp_grv"] = $emp_grv;
			$jsondata["pat_grv"] = $pat_grv;
	
			//print_r($jsondata);
			//die();
		}else{
			$jsondata["msj"] = "NO se encontro informacion de$msj requerido";			
		}
	}else{
		$jsondata["msj"] = "Hubo un error en los datos seleccionados";	
	}

	header('Content-type: application/json; charset=utf-8');
	echo json_encode($jsondata);

?>