<?php
	$date = str_replace("/", "", @$_POST["date"]);
	//$date = "20190416";
	$file = "../var_dump/sp/$date.php";
	if(@$myfile = fopen( $file, "r")) {  //or die("Unable to open file!");
	 
		$txt = unserialize(fread($myfile,filesize($file)));
		fclose($myfile);
	
		//print_r($txt);
		//die();
		require("../include/db.php");
		
		$sql = "SELECT
				geoc_id,
				LEFT(geoc_codigo,3) AS COD
				FROM `geocerca` 
				HAVING COD IN ('DSA','DMH','DCH')
				ORDER BY geoc_id";
		if($resultado = $mysqli->query($sql)) {
			while ($fila = $resultado->fetch_assoc()) {
				$geoc[$fila["geoc_id"]] = $fila["COD"];
			}
			$resultado->free();	
		}

			
		$grafico = array();
		$gravedad = array("'Gravisimo'"=>0,"'Grave'"=>0,"'Leve'"=>0);
		$tgravedad = array('Leve'=>0,'Grave'=>0,'Gravisimo'=>0);		
		$divisiones = array('DCH'=>$tgravedad,'DMH'=>$tgravedad,'DSAL'=>$tgravedad,'EXTERNA'=>$tgravedad);
		
		foreach($txt as $var) {			
			
			$key = ($var[5] == NULL)? "SIN_INFORMACION" : $var[5]; 
			$div = !isset($geoc[$var[4]]) ? "EXTERNA": $geoc[$var[4]];		
			$divisiones[$div][$var[6]]++;
						
			// G E N E R A L 
	
			@$empresa["'$key'"]++; 
			@$gravedad["'$var[6]'"]++; 
			@$empresa_grav["'$key'"][$var[6]]++;
			
			switch ($var[6]) {
				case "Leve":
					$puntos = 1;
					break;
				case "Grave":
					$puntos = 2;
					break;
				case "Gravisimo":
					$puntos = 3;
					break;
			}
			@$patentes["$key"]["$var[0]"]["puntos"] += $puntos;
			@$patentes["$key"]["$var[0]"]["'$var[6]'"]++;
		}
		
		$jsondata["empresa_grav"] = implode(',', array_keys($empresa_grav));
		$jsondata["divisiones"] = $divisiones;
		//print_r($gravedad);
		//die();
		
		$grv_pat = array();
		$contador = 0;
		foreach($patentes as $emp => $pat ) {
			$temp_data = array();
			foreach($pat as $key => $data ) {
				$puntos = -$data["puntos"];
				unset($data['puntos']);
				$temp_data[$puntos][$key] = $data;
			}
			ksort($temp_data);
			$grv_pat["patentes"] = array();
			foreach($temp_data as $pat ) {
				foreach($pat as $key => $data ) {
					if(count($grv_pat["patentes"])<20){
						$grv_pat["patentes"][] = $key;		
						foreach($gravedad as $grv => $null ) {
							$grv_pat[$grv][] = (isset($data[$grv])) ? $data[$grv] : 0;
						}
					}
				}
			}
			
			
			$analisis[$emp]["table"] =  "
			<table class=\"table table-condensed table-bordered\">
                	<thead>
						<tr>
							<th>GRAVEDAD</th>
							<th>".implode("</th>\n\t\t\t\t\t\t\t<th>",$grv_pat["patentes"])."</th>             
                  		</tr>
                	</thead>
                	<tbody>
						<tr>
							<td>Leve</td>
							<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$grv_pat["'Leve'"])."</td>             
						</tr>
						<tr>
							<td>Grave</td>
							<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$grv_pat["'Grave'"])."</td>             
						</tr>
						<tr>
							<td>Gravisimo</td>
							<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$grv_pat["'Gravisimo'"])."</td>             
                      </tr>
                    </tbody>
         		</table>";	
			
			$analisis[$emp]["patentes"] = implode(',',$grv_pat["patentes"]);
			$analisis[$emp]["grvs"] = implode(',',$grv_pat["'Gravisimo'"]);
			$analisis[$emp]["grav"] = implode(',',$grv_pat["'Grave'"]);
			$analisis[$emp]["leve"] = implode(',',$grv_pat["'Leve'"]);
			$grv_pat = array();
			
		}
		$jsondata["analisis"] = $analisis;

		$all_gravedad = array();
		foreach($tgravedad as $grv => $null ){
			foreach($empresa_grav as $var){
				$all_gravedad[$grv][] = (isset($var[$grv])) ? $var[$grv] : 0;
			}
		}
	
		foreach($all_gravedad as $key => $grvd){
			$tgravedad[$key] = implode(',', $grvd);
		}

		$jsondata["table_detail_emp"] =  "
			<table class=\"table table-condensed table-bordered\">
                	<thead>
						<tr>
							<th>GRAVEDAD</th>
							<th>".implode("</th>\n\t\t\t\t\t\t\t<th>",array_keys($patentes))."</th>             
                  		</tr>
                	</thead>
                	<tbody>
						<tr>
							<td>Leve</td>
							<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$all_gravedad["Leve"])."</td>             
						</tr>
						<tr>
							<td>Grave</td>
							<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$all_gravedad["Grave"])."</td>             
						</tr>
						<tr>
							<td>Gravisimo</td>
							<td>".implode("</td>\n\t\t\t\t\t\t\t<td>",$all_gravedad["Gravisimo"])."</td>             
                      </tr>
                    </tbody>
         		</table>";

		$jsondata["table_group_grav"] = '<div class="input-group">
                  <p class="input-group-addon bg-pink">Gravisimo</p>
                  <span class="input-group-addon">' . number_format($gravedad["'Gravisimo'"], 0, ',', '.') . '</span>
                </div> 
                <div class="input-group">
                  <p class="input-group-addon bg-orange">Grave</p>
                  <span class="input-group-addon">' . number_format($gravedad["'Grave'"], 0, ',', '.'). '</span>
                </div>
                <div class="input-group">
                  <p class="input-group-addon bg-yellow">Leve</p>
                  <span class="input-group-addon">' . number_format($gravedad["'Leve'"], 0, ',', '.') . '</span>
                </div>
                <div class="input-group">           
                  <p class="input-group-addon bgr-red">Total</p>
                  <span class="input-group-addon ">' . number_format(($gravedad["'Gravisimo'"] + $gravedad["'Grave'"] + $gravedad["'Leve'"]), 0, ',', '.') . '</span>
                </div>';			
		
		
		$color = array("blue", "pink", "green", "yellow", "red", "navy", "aqua", "coral", "brown", "lime");
	
		$c = 0;
		$jsondata["table_group_emp"] = "";
		foreach($empresa as $key => $num){
			$jsondata["table_group_emp"] .= '
				<div class="input-group">
                  <p class="input-group-addon bgr-'.$color[$c++].'">' . ucwords(strtolower(str_replace("'", "", $key ))) . '</p>
                  <span class="input-group-addon">' . number_format($num, 0, ',', '.') . "</span>
                </div>\n\t\t";
		}		
				
		$jsondata["tgravedad"] = $tgravedad;

		$jsondata["x1"] = implode(",",$gravedad);
		$jsondata["y1"] = implode(",",array_keys($gravedad));
		$jsondata["x2"] = implode(",",$empresa);
		$jsondata["y2"] = implode(",",array_keys($empresa));
				
	}else{
		$jsondata["msj"] = "NO se encontro informacion en la fecha requerida";	
	}

	header('Content-type: application/json; charset=utf-8');
	echo json_encode($jsondata);

?>