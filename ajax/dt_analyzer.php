<?php
// web service datatable analizador
require_once '../desarrollo/class/viajeModel.php';
date_default_timezone_set("America/Santiago");

$op = $_GET['op'];

$viajes;
$estados;

switch ($op) {
	case 'sitt':
		$viajes = viajeModel::get_viajes_analizando(4);
		$estados = viajeModel::get_estados();
		break;
		
	case 'cami_1':
		$viajes = viajeModel::get_viajes_analizando(5);
		$estados = viajeModel::get_estados();
		break;
		
	case 'cami_2':
		$viajes = viajeModel::get_viajes_analizando(5, true);
		$estados = viajeModel::get_estados(true);
		break;
	
	default:
		# code...
		break;
}


$out = [];
$data = [];

foreach ($viajes as $viaje) {
    $data[] = [
        'id'          => $viaje['id_viaje'],
        'vehiculo'    => $viaje['patente'],
        'estado'      => set_label_estado($viaje['estado'], $estados),
        'origen'      => set_index_geoc($viaje, 'ori'),
        'destino'     => set_index_geoc($viaje, 'des'),
        'salida'      => format_date($viaje['f_salida']),
        'insert'      => format_date($viaje['f_insert']),
        'reg_origen'  => $viaje['reg_origen'],
        'reg_destino' => $viaje['reg_destino'],
        'obs'         => $viaje['obs']
    ];
}

$total_rec = count($data);
$total_filter = count($data);

$out = array(
	'recordsTotal' => $total_rec,
	'recordsFiltered' => $total_filter,
	'data' => $data
);

echo json_encode($out);

function set_label_estado($st, $estados)
{
    $str_estado = array_search($st, $estados);
    $str_estado = str_replace('_', ' ', $str_estado);
    return ucfirst($str_estado);
}

function set_index_geoc($viaje, $op) {
	if (!empty($viaje[$op . '_nombre'])) {
		return $viaje[$op . '_nombre'];
	}

	if (!empty($viaje[$op . '_localidad_cm'])) {
		return $viaje[$op . '_localidad_cm'];
	}

	if (!empty($viaje[$op . '_localidad'])) {
		return $viaje[$op . '_localidad'];
	}

	if (!empty($viaje[$op . '_codigo'])) {
		return $viaje[$op . '_codigo'];
	}

	return "Sin Nombre";
}

function format_date($val) {
	return date('d-m-Y \<\b\r\> H:i:s', $val);
}