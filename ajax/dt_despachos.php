<?php

/**
 * clase para datatables
 */

require_once '../desarrollo/class/viajeModel.php';

$division = $_GET['div'];
// $division = 5;

$m_viajes = new viajeModel();
$date_now = new DateTime();
$now = $date_now->getTimestamp();
$since = $now - (60 * 60 * 48);
$horas = (60 * 60 * 2);

$viajes = $m_viajes->get_viajes_despachados($since, $division);

$out = array();
$data = array();

foreach ($viajes as $k => $viaje) {
	$estado = format_estado($viaje['estado'], $viaje['f_salida'], $viaje['f_posicion_des']);

	if (!$estado) {
		continue;
	}

	$data[] = array(
		'id_vehiculo' => $viaje['vehi_id'],
		'patente' => $viaje['patente'],
		'despachado' => format_date($viaje['f_salida']),
		'origen' => set_index_geoc($viaje, 'ori'),
		'destino' => set_index_geoc($viaje, 'des'),
		'estado' => $estado
	);
}

$total_rec = count($data);
$total_filter = count($data);

$out = array(
	'recordsTotal' => $total_rec,
	'recordsFiltered' => $total_filter,
	'data' => $data
);

echo json_encode($out);

function format_date($val) {
	// return date('d-m-Y H:i:s', $val);
	return date('d-m-Y \<\b\r\> H:i:s', $val);
}

function format_estado($estado, $salida, $posicion_des) {
	global $horas, $now;
	$st = '';
	// descartar viajes terminados hace mas de 2 horas
	if (in_array($estado, array(-1, -2))){
		if (($posicion_des + $horas) > $now) {
			$st = 'Terminado';
		} else{
			$st = false;
		}
	}elseif ($estado < -3) {
		if (($salida + ($horas * 6)) > $now) {
			$st = 'Cancelado';
		} else{
			$st = false;
		}
	}elseif ($estado == 1) {
		$st = 'Informado';
	}else{
		$st = 'En ruta';
	}
	return $st;
}

function set_index_geoc($viaje, $op) {
	if (!empty($viaje[$op . '_nombre'])) {
		return $viaje[$op . '_nombre'];
	}

	if (!empty($viaje[$op . '_localidad_cm'])) {
		return $viaje[$op . '_localidad_cm'];
	}

	if (!empty($viaje[$op . '_localidad'])) {
		return $viaje[$op . '_localidad'];
	}

	if (!empty($viaje[$op . '_codigo'])) {
		return $viaje[$op . '_codigo'];
	}

	return "Sin Nombre";
}