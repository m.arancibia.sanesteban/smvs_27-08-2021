<?php
require_once '../desarrollo/class/viajeModel.php';
require_once '../desarrollo/class/analyzer.php';

$id_viajes = json_decode($_POST['viajes'], true);

foreach ($id_viajes as $id_viaje) {
    $viaje = viajeModel::load($id_viaje);
    if ($viaje->estado == 4) {
        $res = Analyzer::scan_viaje_sitt($viaje);
    } else {
        $res = Analyzer::scan_viaje_cami($viaje);
    }
}