<?php
require_once '../desarrollo/class/equivalencia.php';

$op = $_GET['op'];

if ($op == 'get') {
    $id = $_POST['id']; 
    $equiv = Equivalencia::get_from_id($id);
    echo json_encode($equiv);
} else {
    $id = $_POST['id'] == -1 ? NULL : $_POST['id']; 
    $geo_original = $_POST['geo_ori'];
    $geo_target = $_POST['geo_tar'];
    $nombre = $_POST['nombre'];
    
    $equiv = new Equivalencia($id, $geo_original, $geo_target, $nombre);
    $query= '';
    if ($op == 'create') {
        if ($geo_original != $geo_target) {
            $query = $equiv->save();
        }
    } elseif ($op == 'update') {
        $equiv->activa = $_POST['activa'];
        $query = $equiv->update();
    }
    $out = [
        'status' => 'ok',
        'query'  => $query,
    ];

    echo json_encode($out);
}