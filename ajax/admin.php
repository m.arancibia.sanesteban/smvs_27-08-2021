<?php

	function info_day($date){ //22 to 21
		$days[] = str_replace("/", "", @$_POST["date"]);
		return $days;
	}
	
	function info_month($date){ //22 to 21
		list($year,$mes,$day) = explode("/",$date);
		
		if($day>21) {
			$mes_start = $mes;
		}else{
			if ($mes > 1) {
				$mes_start = --$mes;
			}else {
				$mes_start = 12;
				$year--;
			}
		}
		$day_end = cal_days_in_month(CAL_GREGORIAN, $mes_start, $year);
		$day_end_temp = $day_end;
				
		for( $day = 22; $day<=$day_end; $day++)
		{	
			$mes_temp = str_pad($mes_start,2,"0", STR_PAD_LEFT);
			$day_temp = str_pad($day,2,"0", STR_PAD_LEFT);
			$dat_temp = "$year/$mes_temp/$day_temp";		
			if(  strtotime(date("Y/m/d")) <= strtotime($dat_temp)) {
				break;
			};
			
			$days[] = "$year$mes_temp$day_temp";
						
			if($day == $day_end_temp){
				$day = 0;
				$day_end = 21;
				if ($mes_start < 12) {
					$mes_start++;
				}else {
					$mes_start = 1;
					$year++;
				}
			}	
		}			
		return $days;	
	}

	function info_week($date) { //V to J
		$unix_time = strtotime($date);
		$fecha = new DateTime();
		$dias = array(-2,-1,0,1,2,3,4);
		$nday = date('N',strtotime($date));	
		foreach ($dias as $valor) {
			$delta = ($nday>=5)? 7 : 0;
			$valor = ($delta + $valor) - $nday ;
			$timestamp = $unix_time + ( $valor * 86400);
			$fecha->setTimestamp($timestamp);
			$days[] = $fecha->format('Ymd');
		}
		return $days;
	}

?>