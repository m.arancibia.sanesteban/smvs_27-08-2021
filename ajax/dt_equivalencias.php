<?php
// web service datatable analizador
require_once '../desarrollo/class/equivalencia.php';
date_default_timezone_set("America/Santiago");

$out = [];
$data = [];
$equivalencias = Equivalencia::get_all_tabla();

foreach ($equivalencias as $equiv) {
    $data[] = [
        'id'         => $equiv['eq_id'],
        'geo_origen' => "{$equiv['eq_geo']} - " . set_index_geoc($equiv, 'gco'),
        'geo_target' => "{$equiv['eq_geot']} - " . set_index_geoc($equiv, 'gct'),
        'nombre'     => $equiv['eq_nombre'],
		'estado'     => $equiv['eq_activa'],
		'id_row'     => "eq_{$equiv['eq_id']}",
    ];
}

$total_rec = count($data);
$total_filter = count($data);

$out = array(
	'recordsTotal' => $total_rec,
	'recordsFiltered' => $total_filter,
	'data' => $data
);

echo json_encode($out);

function set_index_geoc($equiv, $op) {
	if (!empty($equiv[$op . '_nombre'])) {
		return $equiv[$op . '_nombre'];
	}

	if (!empty($equiv[$op . '_localidad_cm'])) {
		return $equiv[$op . '_localidad_cm'];
	}

	if (!empty($equiv[$op . '_localidad'])) {
		return $equiv[$op . '_localidad'];
	}

	if (!empty($equiv[$op . '_codigo'])) {
		return $equiv[$op . '_codigo'];
	}

	return "Sin Nombre";
}