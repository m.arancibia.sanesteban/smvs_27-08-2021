<?php
include_once "class/geocercaModel.php";
$geos = geocercaModel::get_all();


echo '<div style="display: none" class="">';
// print_r($geos);
echo '</div>';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Equivalencias Geocercas</title>

    <!-- styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->

    <!-- scripts -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <script src="../js/combobox.js"></script>

    <style>
        .ui-autocomplete{
            z-index: 20000;
        }
    </style>
</head>
<body>
    <div class="container content-equiv">
        <h1>Equivalencias</h1>
        <h4>Tabla</h4>
        <div class="row">
            <table id="table_equiv" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Geocera Origen</th>
                        <th>Geocerca Objetivo</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary" id="btn_new">Nuevo</button>
        </div>
        <?php if(false): ?>
        <h4>Modal</h4>
        <div id="form-equiv-modal" class="modal-form">
            <div class="row">
                <form action="" id="form_equiv">
                    <input type="hidden" name="id" id="txt_id" value="-1">
                    <input type="hidden" name="op" id="txt_op" value="">
                    <div class="form-group">
                        <label for="">Geocerca Origen:</label>
                        
                        <select name="geo_ori" id="cb_geo">
                            <option value="-1">Seleccione Geocerca</option>
                        <?php foreach ($geos as $geo): ?>
                            <option value="<?= $geo->id ?>"><?= $geo->id ?> - <?= $geo->label ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Geocerca Objetivo:</label>
                        <select name="geo_tar" id="cb_geo_target">
                            <option value="-1">Seleccione Geocerca</option>
                        <?php foreach ($geos as $geo): ?>
                            <option value="<?= $geo->id ?>"><?= $geo->id ?> - <?= $geo->label ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Nombre Equivalencia:</label>
                        <input type="text" name="nombre" id="txt_nombre" class="form-control">
                    </div>
                    <div class="form-group" id="box_activa" style="display: none">
                        <label for="">Estado:</label>
                        <select class="form-control" name="activa" id="cb_activa">
                            <option value="1">Activa</option>
                            <option value="0">Inactiva</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input id="btn_equiv" type="submit" class="btn btn-primary" value="Guardar">
                    </div>
                </form>
            </div>
        </div>
        <?php endif; ?>
    </div>


    <!-- Modal -->
    <?php if(true): ?>
    <div class="modal fade" id="modal_equiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_label"> title</h4>
            </div>
            <div class="modal-body">
                <div id="form-equiv-modal" class="modal-form">
                    <div class="container" style="width: 100%">
                        <div class="row">
                            <form action="" id="form_equiv">
                                <input type="hidden" name="id" id="txt_id" value="-1">
                                <input type="hidden" name="op" id="txt_op" value="">
                                <div class="form-group">
                                    <label for="">Geocerca Origen:</label>
                                    
                                    <select name="geo_ori" id="cb_geo">
                                        <option value="-1">Seleccione Geocerca</option>
                                    <?php foreach ($geos as $geo): ?>
                                        <option value="<?= $geo->id ?>"><?= $geo->id ?> - <?= $geo->label ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Geocerca Objetivo:</label>
                                    <select name="geo_tar" id="cb_geo_target">
                                        <option value="-1">Seleccione Geocerca</option>
                                    <?php foreach ($geos as $geo): ?>
                                        <option value="<?= $geo->id ?>"><?= $geo->id ?> - <?= $geo->label ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Nombre Equivalencia:</label>
                                    <input type="text" name="nombre" id="txt_nombre" class="form-control">
                                </div>
                                <div class="form-group" id="box_activa" style="display: none">
                                    <label for="">Estado:</label>
                                    <select class="form-control" name="activa" id="cb_activa">
                                        <option value="1">Activa</option>
                                        <option value="0">Inactiva</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_equiv">Guardar</button>
            </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <script src="../js/table_equiv.js"></script>
</body>
</html>