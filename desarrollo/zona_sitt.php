<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../include/PHPMailer/Exception.php';
require '../include/PHPMailer/PHPMailer.php';
require '../include/PHPMailer/SMTP.php';

require('../include/config.php');
$mysqli = new mysqli($dbhost, $dbuser, $dbpasswd, $dbname);
$mysqli->set_charset("utf8");

/* comprobar la conexión */
if ($mysqli->connect_errno) {  //mysqli_connect_errno()
    printf("Falló la conexión con el servidor"); //_ %s\n", mysqli_connect_error()
    exit();
}

$mail = new PHPMailer(true);

$fin =mysqli_query($mysqli,"SELECT R.regi_id FROM registro R WHERE R.regi_fecha_insercion < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 MINUTE))ORDER BY R.regi_id DESC LIMIT 1");
$inicio = mysqli_query($mysqli,"SELECT R.regi_id  FROM registro R WHERE R.regi_fecha_insercion < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 11 MINUTE)) ORDER BY R.regi_id DESC LIMIT 1");

$row=mysqli_fetch_array($fin);
$fin1=$row['regi_id'];

$row=mysqli_fetch_array($inicio);
$inicio1=$row['regi_id'];

$desde=mysqli_query($mysqli, "SELECT R.regi_fecha_insercion FROM registro R WHERE R.regi_id = $inicio1");
$row=mysqli_fetch_array($desde);
$desde1=date('d/m/Y H:i:s', $row['regi_fecha_insercion']);

$hasta=mysqli_query($mysqli, "SELECT R.regi_fecha_insercion FROM registro R WHERE R.regi_id = $fin1");
$row=mysqli_fetch_array($hasta);
$hasta1=date('d/m/Y H:i:s', $row['regi_fecha_insercion']);


$consulta = mysqli_query($mysqli,
"SELECT
	VIAJEAUX.vi_id AS ID_SMVS,
	V.vi_fsalida AS INFORMADO,
	R.PATENTE AS PATENTE,
	MAX( R.REGISTRO ) AS REGISTRO,
	RE.regi_fecha_posicion  AS FECHA_LAST_POS,
	C.cond_rut AS RUT_COND,
	C.cond_nombre AS CONDUCTOR,
	E.empr_nombre AS EMPRESA,
	GE.geoc_nombre AS ORIGEN,
	GD.geoc_nombre AS DESTINO,
	TC.tica_nombre AS CARGA,
	R.ZONA_URBANA AS ZONA,
	CONCAT( R.LATITUD, ',', R.LONGITUD ) AS LAT_LONG
FROM
	(
	SELECT
		GC.gc_regi_id AS REGISTRO,
		G.geoc_nombre AS ZONA_URBANA,
		R.regi_latitud AS LATITUD,
		R.regi_longitud AS LONGITUD,
		COUNT( GC.gc_regi_id ) AS CR,
		V.vehi_patente AS PATENTE,
		V.vehi_id AS ID_VEHI 
	FROM
		gc_id GC
		INNER JOIN geocerca G ON G.geoc_id = GC.gc_geoc_id
		INNER JOIN registro R ON R.regi_id = GC.gc_regi_id
		INNER JOIN vehiculo V ON V.vehi_id = R.regi_vehi_id 
	WHERE
		GC.gc_regi_id BETWEEN $inicio1 AND $fin1
		AND G.geoc_item = 40 
	GROUP BY
		GC.gc_regi_id 
	HAVING
		CR = 1 
	) R
	INNER JOIN ( SELECT V.vi_id, V.vi_vehi_id FROM viajes V WHERE V.vi_st = 12 AND V.vi_div = 4 ) VIAJEAUX ON VIAJEAUX.vi_vehi_id = R.ID_VEHI
	INNER JOIN viajes V ON VIAJEAUX.vi_id = V.vi_id
	INNER JOIN conductor C ON C.cond_id = V.vi_cond_id
	INNER JOIN empresa E ON E.empr_id = V.vi_empr_id
	INNER JOIN geocerca GE ON GE.geoc_id = V.vi_geoc_id_ori
	INNER JOIN geocerca GD ON GD.geoc_id = V.vi_geoc_id_dest
	INNER JOIN tipo_carga TC ON TC.tica_id = V.vi_tica_id 
	INNER JOIN registro RE ON RE.regi_id = R.REGISTRO 
GROUP BY
	R.PATENTE 
ORDER BY
	R.PATENTE,
	R.REGISTRO");

$content='<html>
<head>
<style type="text/css">
body { 
	font: 14px/1.4 Verdana, Serif; 
}
table { 
	width: 100%; 
	border-collapse: collapse; 
    }
		tr:nth-of-type(odd) { 
		background: #eee; 
	}
	th { 
		background: #333; 
		color: white; 
		font-weight: bold; 
        font: 12px/1.4 Verdana, Serif;
    }
    td  {
        font: 11px/1.4 Verdana, Serif;
    }
	td, th { 
		padding: 6px; 
		border: 1px solid #ccc; 
		text-align: center; 
    }
</style>
</head>
<body>
Estimado usuario, informamos a usted que los siguientes móviles han ingresado a una zona urbana en los pasados 10 minutos, comprendidos entre el ';
$d='<b><i>'.$desde1.'</b></i>';
$h='<b><i>'.$hasta1.'</b></i>';
$cont='<br><br>
<table >
<tr> 
<th>ID SMVS</th>
  <th>HORA DESPACHO DESDE SITT</th>
  <th>PATENTE</th>
  <th>RUT</th>
  <th>CONDUCTOR</th>
  <th>EMPRESA</th>
  <th>ORIGEN</th>
  <th>DESTINO</th>
  <th>CARGA</th>
  <th>ZONA URBANA</th>

</tr>';
$link="https://maps.google.com/?q=";
$body1="";
foreach($consulta as $key => $value){
    $body1 .= "<tr><td >".$value['ID_SMVS']."</td>";
    $body1 .= "<td >".date('d/m/Y H:i:s',$value['INFORMADO'])."</td>";
    $body1 .= "<td >".substr($value['PATENTE'],0,4)."-".substr($value['PATENTE'],4,6)."</td>";
    $body1 .= "<td >".$value['RUT_COND']."</td>";
    $body1 .= "<td >".$value['CONDUCTOR']."</td>";
    $body1 .= "<td >".$value['EMPRESA']."</td>";
    $body1 .= "<td >".$value['ORIGEN']."</td>";
    $body1 .= "<td >".$value['DESTINO']."</td>";
    $body1 .= "<td >".strtoupper($value['CARGA'])."</td>";
    $body1 .= "<td >".$value['ZONA']."</td>";
    // $body1 .= "<td >".date('d/m/Y H:i:s',$value['INFORMADO'])."</td>";
    // $body1 .= "<td ><a href=".$link.$value['LAT_LONG'].">Ver</a></td></tr>";
}
$content1='</table><br><br>
<strong>*Este correo es generado de manera automática desde Plataforma SMVS.</strong><br><br>
Atte<br>
<strong>Reportes SMVS Codelco</strong><br>
Departamento TI | Casa Central<br>
------------------------------------------------------------------------<br>
+ 56 34  229 0795<br>
reporte@smvscodelco.sercoing.cl<br>
www.sercoing.cl<br>
Los Naranjos 13, Los Andes, Valparaíso<br>
------------------------------------------------------------------------<br>
<img src="cid:imagen" width="300" height="100"></body></html>';

if ($body1 == "") {

    echo 'Sin información';
    // try {
    //     //Server settings
    //     $mail->SMTPDebug = 0;                     			 //Enable verbose debug output
    //     $mail->isSMTP();                                            //Send using SMTP
    //     $mail->Host       = 'mail.sercoing.cl';     		//Set the SMTP server to send through 
    //     $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    //     $mail->Username   = 'reporte@smvscodelco.sercoing.cl'; 	//SMTP username 
    //     $mail->Password   = 'PS&mC#Qw&et5;xt';              	//SMTP password   
    //     $mail->SMTPSecure = 'tls';            			 
    //     $mail->Port       = '587';  
    
    //     //Recipients
    //     $mail->setFrom('reporte@smvscodelco.sercoing.cl', 'Reportes SMVS Codelco - Sercoing Ltda.'); //Correo desde el cual se envia el correo
    //     //$mail->addAddress('javieracortes666@gmail.com', 'Javiera');     //Añadir destinatario
    //     // $mail->addAddress('m.arancibia.sanesteban@gmail.com', 'Marco Arancibia');
    //     $mail->addAddress('marancibia@sercoing.cl', 'Marco Arancibia');
    //     $mail->addAddress('jcortes@sercoing.cl', 'Javiera Cortes');
    //     $mail->addAddress('ddonoso@sercoing.cl', 'Daniel Donoso');
    
    //     //Content
    //     $mail->isHTML(true); 
    //     $mail->msgHTML(true);                                   
    //     $mail->CharSet = 'UTF-8';
    //     $mail->Subject = 'Vehículos en Zona Urbana';   //Asunto
    //     $mail->Body ='Sin información'.'<br>'.$content1;
    //     $mail->AddEmbeddedImage('../images/imagen.jpg', 'imagen', 'imagen.jpg');
    //     $mail->send();
    //     echo 'Mensaje enviado correctamente';
    // } catch (Exception $e) {
    //     echo "El mensaje no pudo ser enviado: {$mail->ErrorInfo}";
    // }
    }
 else {

    try {
        //Server settings
        $mail->SMTPDebug = 0;                     			 //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = 'mail.sercoing.cl';     		//Set the SMTP server to send through 
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = 'reporte@smvscodelco.sercoing.cl'; 	//SMTP username 
        $mail->Password   = 'PS&mC#Qw&et5;xt';              	//SMTP password   
        $mail->SMTPSecure = 'tls';            			 
        $mail->Port       = '587';  
    
   //Recipients
   $mail->setFrom('reporte@smvscodelco.sercoing.cl', 'Reportes SMVS Codelco - Sercoing Ltda.'); //Correo desde el cual se envia el correo
//    $mail->addAddress('jadan001@codelco.cl','Adán Castillo José Luis (Codelco-DMH)' );
   $mail->addAddress('caray078@codelco.cl','Araya Pizarro Cristian Andres (Codelco-DMH)' );
   $mail->addCC('administrador@altoloaag.cl', 'Eduardo Varas' );
   $mail->addCC('psaez004@codelco.cl', 'Saez Parra Pedro Gabriel (Codelco-DMH)');
   $mail->addCC('rmoli010@codelco.cl','Molina Araya Richard (Codelco-DMH)');
//    // $mail->addCC('ppalma@sercoing.cl', 'Patricio Palma Lobos');
   $mail->addBCC('reporte@smvscodelco.sercoing.cl', 'Reportes SMVS Codelco - Sercoing Ltda.');
   // $mail->addAddress('jcortes@sercoing.cl', 'Javiera Cortes');
   // $mail->addAddress('ddonoso@sercoing.cl', 'Daniel Donoso');
    
        //Content
        $mail->isHTML(true); 
        $mail->msgHTML(true);                                 
        $mail->CharSet = 'UTF-8';
        $mail->Subject = 'Vehículos en Zona Urbana';   //Asunto
        $mail->Body =$content.$d." y el ".$h.".".$cont.$body1.$content1;
        $mail->AddEmbeddedImage('../images/imagen.jpg', 'imagen', 'imagen.jpg');
        $mail->send();
        echo 'Mensaje enviado correctamente';
    } catch (Exception $e) {
        echo "El mensaje no pudo ser enviado: {$mail->ErrorInfo}";
    }
    
}
?>
