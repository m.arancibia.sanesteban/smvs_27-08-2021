<?php

	include("../include/db.php");
	
	$sql = "SELECT
			vi_id,
			vi_vehi_id,
			vi_fsalida,
			vi_geoc_id_ori,
			vi_regi_id_ori,
			vi_geoc_id_dest,
			vi_regi_id_dest
			FROM `viajes`
			WHERE vi_regi_id_ori IS NULL OR vi_regi_id_dest IS NULL AND vi_st = 1";

	$result = $mysqli->query($sql);
	if($result->num_rows > 0) {
		while($registros = $result->fetch_assoc())
		{
			$vi_id = $registros["vi_id"];
			$vehi_id = $registros["vi_vehi_id"];			
			$fsalida = $registros["vi_fsalida"];			
			$geoc_id_ori = $registros["vi_geoc_id_ori"];			
			$regi_id_ori = $registros["vi_regi_id_ori"];			
			$geoc_id_dest = $registros["vi_geoc_id_dest"];			
			$regi_id_dest = $registros["vi_regi_id_dest"];

			if( $regi_id_ori == NULL ) {
				$sql = "SELECT
					RE.regi_fecha_posicion
					FROM `viajes` AS VI
					INNER JOIN `registro` AS RE ON VI.vi_regi_id_ori = RE.regi_id AND RE.regi_vehi_id = 471
					ORDER BY VI.vi_id DESC
					LIMIT 1";
				$result_prev = $mysqli->query($sql);
				$reg_prev = $result_prev->fetch_assoc();
				$tdif = ($result_prev->num_rows > 0)? (time()-$reg_prev["regi_fecha_posicion"]) : 43200 ;
				$idreg = search_geo( $vehi_id, $geoc_id_ori, 43200 );
				if($idreg>0) {
					$sql = "UPDATE `viajes` SET `vi_regi_id_ori` = '$idreg' WHERE (`vi_id` = $vi_id)";
					$mysqli->query($sql);					
				}
			};
			
			if( $regi_id_dest == NULL  ) {
				//echo "$vehi_id, $geoc_id_dest <br>";
				$idreg = search_geo( $vehi_id, $geoc_id_dest, 700 );
				if($idreg>0) {
					$sql = "UPDATE `viajes` SET `vi_regi_id_dest` = '$idreg' WHERE (`vi_id` = $vi_id)";
					$mysqli->query($sql);					
				}
			};
			
		}
		$result->free();
	}




	function search_geo( $idpat=NULL, $idgeo=NULL, $tdif ) {
		global $mysqli;
		if( $idpat != NULL && $idgeo != NULL ) {
			
			$sql = "SELECT 
			MIN(RE.regi_id) AS regi_id
			FROM
			(
				SELECT 
				regi_id,
				regi_vehi_id
				FROM `registro` AS RE
				INNER JOIN 
				(
					SELECT 
					RE.regi_fecha_posicion - 43200 AS PS1,
					RE.regi_fecha_posicion AS PS2
					FROM
					(
						SELECT
						RE.regi_id
						FROM `registro` AS RE
						INNER JOIN `gc_id` AS GC ON RE.regi_id = GC.gc_regi_id
						AND RE.regi_fecha_posicion > (UNIX_TIMESTAMP()-$tdif)
						AND RE.regi_vehi_id = $idpat AND GC.gc_geoc_id = $idgeo
						ORDER BY RE.regi_id DESC
						LIMIT 1
					) 
					AS REG INNER JOIN `registro` AS RE ON RE.regi_id = REG.regi_id
				)
				AS PV ON RE.regi_fecha_posicion BETWEEN PV.PS1 AND PV.PS2
				HAVING regi_vehi_id = $idpat
			) 
			AS RE
			INNER JOIN `gc_id` AS GC ON RE.regi_id = GC.gc_regi_id AND GC.gc_geoc_id = $idgeo
			ORDER BY 1 ASC
			LIMIT 1";
			//die($sql);
			$result = $mysqli->query($sql);
			if($result->num_rows > 0) {
				$registros = $result->fetch_assoc();
				$result->free();
				return $registros["regi_id"];
			}
		}
		return false;
	}


?>