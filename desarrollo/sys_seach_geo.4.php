<?php

	include("../include/db.php");
	
	$sql = "SELECT
			vi_id,
			vi_vehi_id,
			vi_fsalida,
			vi_st,
			vi_geoc_id_ori,
			vi_regi_id_ori,
			vi_geoc_id_dest,
			vi_regi_id_dest
			FROM `viajes`
			WHERE #(vi_regi_id_ori = 0 OR vi_regi_id_dest = 0 ) AND 
			vi_st > 0 AND vi_fsalida IS NOT NULL";

	$resultAllOT = $mysqli->query($sql);
	
	if($resultAllOT->num_rows > 0) {
		while($registros = $resultAllOT->fetch_assoc())
		{
			$vi_id = $registros["vi_id"];
			$vehi_id = $registros["vi_vehi_id"];			
			$fsalida = $registros["vi_fsalida"];
			$st = $registros["vi_st"];
			$geoc_id_ori = $registros["vi_geoc_id_ori"];			
			$regi_id_ori = $registros["vi_regi_id_ori"];			
			$geoc_id_dest = $registros["vi_geoc_id_dest"];			
			$regi_id_dest = $registros["vi_regi_id_dest"];

			if( $regi_id_dest == 0 && in_array($st, array(1,2) ) ) {

				if( $st == 1 && ($regi_id_ori == 0 || $regi_id_ori == "" )) {
					$sql = "SELECT
							RE.regi_fecha_posicion
							FROM `viajes` AS VI
							INNER JOIN `registro` AS RE ON VI.vi_regi_id_ori = RE.regi_id AND VI.vi_st = 0 AND RE.regi_vehi_id = $vehi_id
							ORDER BY VI.vi_id DESC
							LIMIT 1";
					$result_Prev = $mysqli->query($sql);
					$reg_viejes = $result_Prev->fetch_assoc();
					$result_Prev->free();
					$prev_reg = $reg_viejes["regi_fecha_posicion"];

					$str = ($prev_reg > $fsalida)? $prev_reg : $fsalida-43200;
					$end = $fsalida;
					$vi_st = 1;
				}else if($st == 1){
					$str = $fsalida ;
					$end = 'UNIX_TIMESTAMP()';
					$vi_st = 2;
				}else{
					$sql_regi_prev = "SELECT regi_fecha_posicion FROM `registro`
							WHERE regi_id = $regi_id_ori";
					$resultStr = $mysqli->query($sql_regi_prev);
					$registros = $resultStr->fetch_assoc();
					$resultStr->free();
					$str = $registros["regi_fecha_posicion"];
					$end = 'UNIX_TIMESTAMP()';
					$vi_st = 2;
				}

				$sqlBetween = "SELECT 
								RE.regi_fecha_posicion-43200 AS PS1,
								RE.regi_fecha_posicion AS PS2
									FROM
									(
										SELECT
										regi_id,
										regi_vehi_id,
										regi_fecha_posicion
										FROM `registro` AS RE
										WHERE regi_fecha_posicion BETWEEN $str AND $end
										HAVING regi_vehi_id = $vehi_id
										ORDER BY regi_id DESC
									) AS RE
									INNER JOIN `gc_id` AS GC ON RE.regi_id = GC.gc_regi_id
									AND GC.gc_geoc_id = $geoc_id_ori
								LIMIT 1";
				//die($sqlBetween);
				$resultBetween = $mysqli->query($sqlBetween);
				$idreg = 0;
				if($resultBetween->num_rows > 0) {
					$registros = $resultBetween->fetch_assoc();
					$resultBetween->free();
					$PS1 = $registros["PS1"];
					$PS2 = $registros["PS2"];
					
					if($PS1>0 && $PS2>0) {
						$idreg = search_geo( $vehi_id, $geoc_id_ori, $PS1, $PS2 );						
					}	
				}

				$timesearch = ($idreg>0)? $PS2 : $end ;
				$sql_regi_prev = "SELECT regi_id FROM `registro`
							WHERE regi_fecha_posicion< $timesearch AND regi_vehi_id=$vehi_id
							ORDER BY regi_id DESC
							LIMIT 1";
				$resultPrev = $mysqli->query($sql_regi_prev);
				$registros = $resultPrev->fetch_assoc();
				$resultPrev->free();

				if( $idreg>0 ) {
					$vi_st = 3;
					$temp_dest = ", `vi_regi_id_dest` = '$registros[regi_id]'";
				} else {
					$idreg = $registros["regi_id"];
					$temp_dest = "";
				}
				if($idreg>0){
				$sql = "UPDATE `viajes` SET `vi_st`='$vi_st', `vi_regi_id_ori`='$idreg' $temp_dest WHERE (`vi_id`='$vi_id')";
				echo "$sql<br>";
				$mysqli->query($sql);
				}

			} else if( $regi_id_ori != 0  && in_array($st, array(2,3) ) ) {
				
				if($regi_id_dest>0){
					$sql_regi_prev = "SELECT regi_fecha_posicion FROM `registro`
								WHERE regi_id = $regi_id_dest";
					$resultStr = $mysqli->query($sql_regi_prev);
					$registros = $resultStr->fetch_assoc();
					$resultStr->free();				
					$str = $registros["regi_fecha_posicion"];
				}else{
					$str = $fsalida;				
				}
				$end = 'UNIX_TIMESTAMP()';
				$vi_st = 2;

				$sqlBetween = "SELECT 
								RE.regi_fecha_posicion-43200 AS PS1,
								RE.regi_fecha_posicion AS PS2
									FROM
									(
										SELECT
										regi_id,
										regi_vehi_id,
										regi_fecha_posicion
										FROM `registro` AS RE
										WHERE regi_fecha_posicion BETWEEN $str AND $end
										HAVING regi_vehi_id = $vehi_id
										ORDER BY regi_id DESC
									) AS RE
									INNER JOIN `gc_id` AS GC ON RE.regi_id = GC.gc_regi_id
									AND GC.gc_geoc_id = $geoc_id_dest
								LIMIT 1";
				//die($sqlBetween);
				$resultBetween = $mysqli->query($sqlBetween);
				$idreg = 0;
				if($resultBetween->num_rows > 0) {
					$registros = $resultBetween->fetch_assoc();
					$resultBetween->free();
					$PS1 = $registros["PS1"];
					$PS2 = $registros["PS2"];

					if($PS1>0 && $PS2>0) {	
						$idreg = search_geo( $vehi_id, $geoc_id_dest, $PS1, $PS2 );
					}	
				}

				if($idreg>0) {
					$vi_st = 0;
				}else{
					$sql_regi_prev = "SELECT regi_id FROM `registro`
							WHERE regi_fecha_posicion<$end AND regi_vehi_id=$vehi_id
							ORDER BY regi_id DESC
							LIMIT 1";
					$resultPrev = $mysqli->query($sql_regi_prev);
					$registros = $resultPrev->fetch_assoc();
					$resultPrev->free();				
					$idreg = $registros["regi_id"]; ;									
				}
				$sql = "UPDATE `viajes` SET `vi_st`='$vi_st', `vi_regi_id_dest`='$idreg' WHERE (`vi_id`='$vi_id')";
				echo "$sql<br>";
				$mysqli->query($sql);

			};
		}
		$resultAllOT->free();
	}

	function search_geo( $idpat=NULL, $idgeo=NULL, $PS1, $PS2 ) {
		global $mysqli;	
		if( $idpat != NULL && $idgeo != NULL ) {
			$sql = "SELECT
					MIN(RE.regi_id) AS regi_id
					FROM
					(
						SELECT 
						regi_id,
						regi_vehi_id
						FROM `registro`
							WHERE regi_fecha_posicion BETWEEN $PS1 AND $PS2
							HAVING regi_vehi_id = $idpat
					) AS RE
					INNER JOIN `gc_id` AS GC ON RE.regi_id = GC.gc_regi_id AND GC.gc_geoc_id = $idgeo
					ORDER BY 1 ASC
					LIMIT 1";					
			$result = $mysqli->query($sql);
			if($result->num_rows > 0) {
				$registros = $result->fetch_assoc();
				$result->free();
				return $registros["regi_id"];
			}
		}
		return false;
	}
?>