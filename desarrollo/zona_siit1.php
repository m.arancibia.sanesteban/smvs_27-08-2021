<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../include/PHPMailer/Exception.php';
require '../include/PHPMailer/PHPMailer.php';
require '../include/PHPMailer/SMTP.php';

require('../include/config.php');
$mysqli = new mysqli($dbhost, $dbuser, $dbpasswd, $dbname);
$mysqli->set_charset("utf8");

/* comprobar la conexión */
if ($mysqli->connect_errno) {  //mysqli_connect_errno()
    printf("Falló la conexión con el servidor"); //_ %s\n", mysqli_connect_error()
    exit();
}

$mail = new PHPMailer(true);

$fin =mysqli_query($mysqli,"SELECT R.regi_id FROM registro R WHERE R.regi_fecha_insercion < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 MINUTE))ORDER BY R.regi_id DESC LIMIT 1");
$inicio = mysqli_query($mysqli,"SELECT R.regi_id  FROM registro R WHERE R.regi_fecha_insercion < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 11 MINUTE)) ORDER BY R.regi_id DESC LIMIT 1");

$row=mysqli_fetch_array($fin);
$fin1=$row['regi_id'];

$row=mysqli_fetch_array($inicio);
$inicio1=$row['regi_id'];

$consulta = mysqli_query($mysqli,
"SELECT
	V.vi_id AS id, 
	V.vi_fsalida AS salida,  
	VE.vehi_patente AS patente, 
	C.cond_nombre AS conductor, 
	EM.empr_nombre AS empresa, 
	GE.geoc_nombre AS geocerca,
	GD.geoc_nombre As geocerca1,
	TC.tica_nombre AS tica,
	G.geoc_nombre AS geoc
FROM
	viajes V
	INNER JOIN vehiculo AS VE ON V.vi_vehi_id = VE.vehi_id
	INNER JOIN registro R ON R.regi_vehi_id = VE.vehi_id
	INNER JOIN gc_id GC ON GC.gc_regi_id = R.regi_id
	INNER JOIN geocerca G ON G.geoc_id = GC.gc_geoc_id
	INNER JOIN conductor C ON C.cond_id = V.vi_cond_id
	INNER JOIN geocerca GE ON GE.geoc_id = V.vi_geoc_id_ori
	INNER JOIN geocerca GD ON GD.geoc_id = V.vi_geoc_id_dest
	INNER JOIN tipo_carga TC ON TC.tica_id = V.vi_tica_id 
	INNER JOIN empresa EM ON EM.empr_id = V.vi_empr_id 
WHERE
	V.vi_id IN ( SELECT V.vi_id FROM viajes V WHERE V.vi_st = 12 AND V.vi_div = 4 ) 
	AND R.regi_id BETWEEN $inicio1 AND $fin1 
	AND G.geoc_item = 40 
GROUP BY
	V.vi_id 
ORDER BY
	V.vi_id");

$content='<html>
<head>
<style type="text/css">
body { 
	font: 14px/1.4 Arial, Serif; 
}
table { 
	width: 100%; 
	border-collapse: collapse; 
    
    }
		tr:nth-of-type(odd) { 
		background: #eee; 
	}
	th { 
		background: #333; 
		color: white; 
		font-weight: bold; 
        font: 12px/1.4 Arial, Serif;
        
	}
    td  {
        font: 10px/1.4 Arial, Serif;
    }
	td, th { 
		padding: 6px; 
		border: 1px solid #ccc; 
		text-align: center; 
        
	}
</style>
</head>
<body>
Los siguientes móviles han ingresado en una zona urbana en los pasados 10 minutos:<br><br>
<table >
<tr> 
<th>Viaje</th>
  <th>INFORMADO</th>
  <th>PATENTE</th>
  <th>CONDUCTOR</th>
  <th>EMPRESA</th>
  <th>ORIGEN</th>
  <th>DESTINO</th>
  <th>CARGA</th>
  <th>ZONA URBANA</th>
</tr>';

$body1="";
foreach($consulta as $key => $value){
    $body1 .= "<tr><td >".$value['id']."</td>";
    $body1 .= "<td >".date('d/m/Y H:i:s',$value['salida'])."</td>";
    $body1 .= "<td >".substr($value['patente'],0,4)."-".substr($value['patente'],4,6)."</td>";
    $body1 .= "<td >".$value['conductor']."</td>";
    $body1 .= "<td >".$value['empresa']."</td>";
    $body1 .= "<td >".$value['geocerca']."</td>";
    $body1 .= "<td >".$value['geocerca1']."</td>";
    $body1 .= "<td >".$value['tica']."</td>";
    $body1 .= "<td >".$value['geoc']."</td></tr>";
}
$content1='</table><br><br>
NOTA: Este correo es generado de manera automática por plataforma SMVS.<br><br>
Atte<br>
Reportes SMVS Codelco<br>
Departamento TI | Casa Central<br>
------------------------------------------------------------------------<br>
+ 56 34  229 0795<br><br>
reporte@smvscodelco.sercoing.cl<br>
www.sercoing.cl<br><br>
Los Naranjos 13, Los Andes, Valparaíso<br>
------------------------------------------------------------------------<br>
<img src="cid:imagen.jpg" width="300" height="100"></body></html>';

if ($body1 == "") {
 echo "Sin Información";
    }
else {

    try {
        //Server settings
        $mail->SMTPDebug = 0;                     			 //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = 'mail.sercoing.cl';     		//Set the SMTP server to send through 
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = 'reporte@smvscodelco.sercoing.cl'; 	//SMTP username 
        $mail->Password   = 'PS&mC#Qw&et5;xt';              	//SMTP password   
        $mail->SMTPSecure = 'tls';            			 
        $mail->Port       = '587';  
    
        //Recipients
        $mail->setFrom('reporte@smvscodelco.sercoing.cl', 'Reportes SMVS Codelco - Sercoing Ltda.'); //Correo desde el cual se envia el correo
        //$mail->addAddress('javieracortes666@gmail.com', 'Javiera');     //Añadir destinatario
        // $mail->addAddress('m.arancibia.sanesteban@gmail.com', 'Marco Arancibia');
        $mail->addAddress('marancibia@sercoing.cl', 'Marco Arancibia');
        $mail->addAddress('jcortes@sercoing.cl', 'Javiera Cortes');
        // $mail->addAddress('ddonoso@sercoing.cl', 'Daniel Donoso');
    
        //Content
        $mail->isHTML(true);                                  
        $mail->CharSet = 'UTF-8';
        $mail->Subject = 'Vehículos en Zona Urbana';   //Asunto
        $mail->Body =$content.$body1.$content1;
        $mail->AddEmbeddedImage('../images/imagen.jpg', 'imagen');
        $mail->send();
        echo 'Mensaje enviado correctamente';
    } catch (Exception $e) {
        echo "El mensaje no pudo ser enviado: {$mail->ErrorInfo}";
    }
    
    
}
?>

