<?php
/*
script para cancelar viajes antiguos

*/
require_once("class/viajeModel.php");
require_once("class/geocercaModel.php");

$now = new DateTime();
$now_stamp = $now->getTimestamp();
$since = $now_stamp - (60 * 60 * 24);

$m_viajes = new viajeModel();

// viajes sin origen sitt
cerrar_viajes(-12, 'origen', $m_viajes->get_viajes_act_custom($since, null, false, false, 4));

// viajes sin destino sitt
cerrar_viajes(-12, 'destino', $m_viajes->get_viajes_act_custom($since, null, true, false, 4));

// viajes sin origen cami carga informado
cerrar_viajes(-12, 'origen', $m_viajes->get_viajes_act_custom($since, 1, false, false, 5));

// viajes sin origen cami carga por descargar
cerrar_viajes(-12, 'origen', $m_viajes->get_viajes_act_custom($since, 12, false, false, 5));

// sin destino cami carga informado
cerrar_viajes(-12, 'destino', $m_viajes->get_viajes_act_custom($since, 1, true, false, 5));

// sin destino cami carga por descargar
cerrar_viajes(-12, 'destino', $m_viajes->get_viajes_act_custom($since, 12, true, false, 5));

// sin origen cami descarga informado 
cerrar_viajes(-22, 'origen', $m_viajes->get_viajes_act_custom($since,  2, false, false, 5));

// sin origen cami descarga por descargar 
cerrar_viajes(-22, 'origen', $m_viajes->get_viajes_act_custom($since,  22, false, false, 5));

// sin destino cami descarga informado
cerrar_viajes(-22, 'destino', $m_viajes->get_viajes_act_custom($since, 2, true, false, 5));

// sin destino cami descarga por descargar
cerrar_viajes(-22, 'destino', $m_viajes->get_viajes_act_custom($since, 22, true, false, 5));

// viajes sin fecha de salida
cerrar_resto($m_viajes->get_viajes_aux($since - (60 * 60 * 24)));

function cerrar_viajes($st, $tipo, $set) {
	global $now_stamp;
	$date_str = date('d_m_y__H_i_s', $now_stamp);
	$file = fopen("log/cerrados/{$date_str}.txt", 'ab');
	foreach ($set as $viaje) {
		global $m_viajes;
		$regi_ori = empty($viaje['reg_ori']) ? -1 : $viaje['reg_ori'];
		$regi_des = empty($viaje['reg_des']) ? -1 : $viaje['reg_des'];
		$m_geoc = new geocercaModel($viaje['geo_des']);
		$m_geoc->load_geoc();
		$obs = '';
		if ($m_geoc->has_poligono()) {
			$obs = "Sin {$tipo} en Geoc {$m_geoc->id} - {$m_geoc->get_label_geoc()}";
		} else {
			$obs = "Geoc sin poligono: {$m_geoc->id} - {$m_geoc->get_label_geoc()}";
		}

		$obs_trim = substr($obs, 0, 64);
		$m_viajes->id = $viaje['id'];
		$query_text = $m_viajes->cerrar($st, $obs_trim, $regi_ori, $regi_des);
		$query = preg_replace("/[\r\n|\n|\r]+/", ' ', $query_text);
		$clean_query = preg_replace('/\s\s+/', ' ', $query);

		fwrite($file, "{$clean_query}\n");
	}
	fclose($file);
}

function cerrar_resto($data_set)
{
	global $now_stamp;
	$date_str = date('d_m_y__H_i_s', $now_stamp);
	$file = fopen("log/cerrados/{$date_str}.txt", 'ab');
	foreach ($data_set as $viaje) {
		global $m_viajes;
		$regi_ori = empty($viaje['reg_ori']) ? -1 : $viaje['reg_ori'];
		$regi_des = empty($viaje['reg_des']) ? -1 : $viaje['reg_des'];
		$obs = 'Viaje sin fecha de salida informada';
		$m_viajes->id = $viaje['id'];
		$m_viajes->estado = $viaje['estado'];
		$st  = $m_viajes->get_estado_cancel();
		$query_text = $m_viajes->cerrar($st, $obs, $regi_ori, $regi_des);
		$query = preg_replace("/[\r\n|\n|\r]+/", ' ', $query_text);
		$clean_query = preg_replace('/\s\s+/', ' ', $query);

		fwrite($file, "{$clean_query}\n");
	}
	fclose($file);
}
