<?php

require_once("../include/db.php");

class Equivalencia
{
    public $id;
    public $id_geo_original;
    public $id_geo_target;
    public $nombre;
    public $activa;

    public function __construct($id = null, $geo_original = null , $geo_target = null, $nombre = null) {
        $this->id = $id;
        $this->id_geo_original = $geo_original;
        $this->id_geo_target = $geo_target;
        $this->nombre = $nombre;
    }

    public function save()
    {
        global $mysqli;
        $query = "INSERT INTO equivalencia_geocerca
                (equiv_id_geo, equiv_id_geo_target, equiv_nombre) 
            VALUES (
                {$this->id_geo_original}, 
                {$this->id_geo_target},
                '{$this->nombre }'
            );";
        echo $query;
        $mysqli->query($query);
        return true;
    }

    public function update()
    {
        global $mysqli;
        $query = "UPDATE equivalencia_geocerca 
            SET 
                equiv_id_geo = {$this->id_geo_original},
                equiv_id_geo_target = {$this->id_geo_target},
                equiv_nombre = '{$this->nombre}',
                equiv_activa = {$this->activa}
            WHERE
                equiv_id = {$this->id}";
        $mysqli->query($query);
        return $query;
    }

    public static function get_from_id($id)
    {
        global $mysqli;
        $query = "SELECT *
            FROM equivalencia_geocerca
            WHERE equiv_id = {$id}";
        $res = $mysqli->query($query);
        if ($res->num_rows > 0) {
            $row = $res->fetch_object();
            $equivalencia = new Equivalencia(
                $row->equiv_id,
                $row->equiv_id_geo,
                $row->equiv_id_geo_target,
                $row->equiv_nombre
            );
            $equivalencia->activa = $row->equiv_activa;
            return $equivalencia;
        }
        return $false;
    }

    public static function get_all_tabla()
    {
        global $mysqli;
        $query = "SELECT
                eq.equiv_id AS eq_id,
                eq.equiv_id_geo AS eq_geo,
                eq.equiv_id_geo_target AS eq_geot,
                eq.equiv_nombre AS eq_nombre,
                eq.equiv_activa AS eq_activa,
                gco.geoc_nombre as gco_nombre,
                gco.geoc_codigo as gco_codigo,
                gco.nom_localidad as gco_localidad,
                gco.nom_localidad_cm as gco_localidad_cm,
                gct.geoc_nombre as gct_nombre,
                gct.geoc_codigo as gct_codigo,
                gct.nom_localidad as gct_localidad,
                gct.nom_localidad_cm as gct_localidad_cm
            FROM equivalencia_geocerca eq
            JOIN geocerca gco ON eq.equiv_id_geo = gco.geoc_id
            JOIN geocerca gct ON eq.equiv_id_geo_target = gct.geoc_id";
        $res = $mysqli->query($query);
        return $res->fetch_all(MYSQLI_ASSOC);
    }

    public static function get_all($format = false)
    {
        global $mysqli;
        $query = "SELECT * FROM equivalencia_geocerca WHERE equiv_activa = 1;";
        $res = $mysqli->query($query);
        if ($format) {
            return self::get_all_format($res);
        }else{
            return $res;
        }
    }

    public static function get_all_format($data)
    {
        $equivs = [];

        while ($row = $data->fetch_object()){            
            $equivs[$row->equiv_id_geo] = $row;
        }
        return $equivs;
    }

    public static function get_from_original($id_geo)
    {
        global $mysqli;
        $query = "SELECT * 
            FROM equivalencia_geocerca 
            WHERE equiv_id_geo = {$id_geo}";
        $res = $mysqli->query($query);
        if ($res->num_rows > 0) {
            $row = $res->fetch_object();
            $equivalencia = new Equivalencia(
                $row->equiv_id,
                $row->equiv_id_geo,
                $row->equiv_id_geo_target,
                $row->equiv_nombre
            );
            return $equivalencia;
        }
        return $false;
    }

    public function validar()
    {
        if ($this->equiv_exist) {
            return false;
        }
    }

    public function equiv_exist()
    {
        global $mysqli;
        $query;
    }
}


