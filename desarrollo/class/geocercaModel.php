<?php 
/**
 * clase modelo geocerca
 * TODO: cambiar fetch de query a objetos para normalizar el proyecto                           
 */

require_once("../include/db.php");

class geocercaModel {

	public $id;
	public $nombre;
	public $color;
	public $poligono;
	public $visible;
	public $div_id;
	public $empresa_id;
	public $codigo;
	public $maxima_velocidad;
	public $tipo;
	public $item;
	public $localidad;
	public $nom_localidad;
	public $nom_localidad_cm;
	public $label;
	
	function __construct($id) {
		$this->id = $id;
	}

	public static function get_all()
	{
		global $mysqli;
		$query = "SELECT
				geoc_id,
				geoc_nombre,
				geoc_codigo,
				nom_localidad,
				nom_localidad_cm
			FROM geocerca";
		$res = $mysqli->query($query);
		return self::format_geo_list($res);		
	}

	// TODO: crear funcion estatica que retorne objeto con geocerca cargada
	public function load_geoc(){
		global $mysqli;
		$query = "SELECT *
			FROM geocerca
			WHERE geoc_id = {$this->id}";
		$res = $mysqli->query($query);
		$data = $res->fetch_assoc();

		$this->nombre = !empty($data['geoc_nombre']) ? $data['geoc_nombre'] : 'no data';
		$this->color = 	!empty($data['geoc_color']) ? $data['geoc_color'] : 'no data';
		$this->poligono = !empty($data['geoc_poligono']) ? $data['geoc_poligono'] : 'no data';
		$this->visible = !empty($data['geoc_visible']) ? $data['geoc_visible'] : 'no data';
		$this->div_id = !empty($data['geoc_div_id']) ? $data['geoc_div_id'] : 'no data';
		$this->codigo = !empty($data['geoc_codigo']) ? $data['geoc_codigo'] : 'no data';
		$this->nom_localidad = !empty($data['nom_localidad']) ? $data['nom_localidad'] : 'no data';
		$this->nom_localidad_cm = !empty($data['nom_localidad_cm']) ? $data['nom_localidad_cm'] : 'no data';
		$this->label = self::get_label_geoc();
	}

	public static function format_geo_list($res)
	{
		$geos = [];
		while ($row = $res->fetch_array()) {
			$geo = new geocercaModel($row['geoc_id']);
			$geo->load_from_row($row);
			$geos[] = $geo;
		}
		return $geos;
	}

	public function load_from_row($data)
	{
		$this->nombre = !empty($data['geoc_nombre']) ? $data['geoc_nombre'] : 'no data';
		$this->color = 	!empty($data['geoc_color']) ? $data['geoc_color'] : 'no data';
		$this->poligono = !empty($data['geoc_poligono']) ? $data['geoc_poligono'] : 'no data';
		$this->visible = !empty($data['geoc_visible']) ? $data['geoc_visible'] : 'no data';
		$this->div_id = !empty($data['geoc_div_id']) ? $data['geoc_div_id'] : 'no data';
		$this->codigo = !empty($data['geoc_codigo']) ? $data['geoc_codigo'] : 'no data';
		$this->nom_localidad = !empty($data['nom_localidad']) ? $data['nom_localidad'] : 'no data';
		$this->nom_localidad_cm = !empty($data['nom_localidad_cm']) ? $data['nom_localidad_cm'] : 'no data';
		$this->label = self::get_label_geoc();
	}

	public function has_poligono($value='') {
		if (empty($this->poligono)) {
			global $mysqli;
			$query = "SELECT geoc_poligono AS poligono 
				FROM geocerca 
				WHERE geoc_id = {$this->id}";
			$res = $mysqli->query($query);
			$data = $res->fetch_assoc();
			if ($data['poligono'] == null) {
				return false;
			}else{
				return true;
			}
		} elseif ($this->poligono == "no data"){
			return false;
		} else {
			return true;
		}
	}

	public function get_label_geoc() {
		if ($this->nombre != 'no data') {
			return $this->nombre;
		}

		if ($this->nom_localidad_cm != 'no data') {
			return $this->nom_localidad_cm;
		}

		if ($this->nom_localidad != 'no data') {
			return $this->nom_localidad;
		}

		if ($this->codigo != 'no data') {
			return $this->codigo;
		}

		//retornar codigo geocerca
		return "Sin Nombre";
		
	}
}