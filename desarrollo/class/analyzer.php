<?php
/**
 * clase del analizador
 * 
 * TODO:
 * revisar metodos de laclase viaje model
 * encapsular el proyecto
 * evitar paso masivo de parametros innecesarios si el codigo tuviera mejor estructura
 */

include_once("viajeModel.php");
include_once("vehiculoModel.php");
include_once("geocercaModel.php");
include_once("equivalencia.php");
include_once("../include/const.php");

class Analyzer
{

	public static function solve_equivalencias()
	{
		$equivalencias = Equivalencia::get_all(true);
		$destinos = array_keys($equivalencias);
		$viajes = viajeModel::get_activos_equiv($destinos);
		if ($viajes->num_rows > 0) {
			while ($row = $viajes->fetch_object()){
				$i = $row->vi_geoc_id_dest;
				$viaje = new viajeModel($row->vi_id);
				$viaje->load_from_row($row);
				$viaje->apply_equivalencia($equivalencias[$i]);
			}
			define('DESTINOS', $destinos);
		}
	}
	
	public static function apply_equivalencia($viaje, $equivalencia)
	{
		$obs = "{$equivalencias[$i]->equiv_id_geo_target} - $equivalencias[$i]->equiv_nombre .%e=$equivalencias[$i]->equiv_id";
		$viaje->geo_destino_id = $equivalencias[$i]->equiv_id_geo_target;
		$viaje->obs = $obs;
		$viaje->update();

	}

	public static function solve_viajes_duplicados()
	{
		$vehiculos_dup = self::get_ids_dup();
		foreach ($vehiculos_dup as $vehi) {
			$res = self::resolve_duplicados($vehi);
		}
	}

	public static function analyze_sitt()
	{
		$viajes = viajeModel::get_activos_sitt();
		if (!$viajes) {
			return false;
		}
		if ($viajes->num_rows > 0) {
			while ($row = $viajes->fetch_object()) {
				$viaje = new viajeModel($row->vi_id);
				$viaje->load_from_row($row);
				$res = self::scan_viaje_sitt($viaje);
				var_dump($res);
			}
		}
	}

	public static function analyze_cami($descargas)
	{
		$viajes = viajeModel::get_activos(5, $descargas);
		if (!$viajes) {
			return false;
		}
		if ($viajes->num_rows > 0) {
			while ($row = $viajes->fetch_object()) {
				$viaje = new viajeModel($row->vi_id);
				$viaje->load_from_row($row);
				$res = self::scan_viaje_cami($viaje);
				var_dump($res);
			}
		}
	}

	public static function scan_viaje_cami(&$viaje)
	{
		if (defined('DESTINOS')) {
			$destinos = DESTINOS;
			if (in_array($viaje->geo_destino_id, $destinos)) {
				$viaje->apply_equivalencia();
			}
		}

		// valida que el vehiculo tenga data y que el viaje tenga destino alcanzable (poligono)
		if ($viaje->estado == $viaje->get_estado('informado')) {
			// cierra viaje si vehiculo no tiene data
			if (!self::scan_data_vehiculo($viaje, new vehiculoModel($viaje->vehi_id))) {
				return $viaje->id;
			}
	
			/* cierra viaje si vehiculo no tiene destino identificado
			- verificar que descargas tambien se dirigen a destinos identificados (?
			- verificar listado de destinos identificados (?*/
			if (!in_array($viaje->geo_destino_id, $viaje->get_destinos_identificados())) {
				$viaje->set_data_cerrados('cancelado_smvs');
				$viaje->obs = 'DESTINO NO IDENTIFICADO';
				$viaje->update();
				return $viaje->id;
			}

			/* replicar finsert en fsalida si el vehiculo tiene data y destino identificado
			- setear estado analizando */
			if (empty($viaje->time_salida)) {
				$viaje->time_salida = $viaje->time_insert;
			}
			$viaje->set_estado('analizando');
			$viaje->update();
			return $viaje->id;
		}

		// buscar y setea origen del viaje
		if ($viaje->estado == $viaje->get_estado('analizando') && empty($viaje->reg_origen_id)) {
			return self::scan_origen($viaje);
		}
		
		// busca y setea destino del viaje y cierra este
		if ($viaje->estado == $viaje->get_estado('por_descargar') && !empty($viaje->reg_origen_id) && empty($viaje->reg_destino_id)) {
			return self::scan_destino($viaje);
		}

		/* TODO: realizar acciones si viajes no encuentra registro de origen en determinado tiempo */
	}

	public static function scan_viaje_sitt(&$viaje)
	{
		if (defined('DESTINOS')) {
			$destinos = DESTINOS;
			if (in_array($viaje->geo_destino_id, $destinos)) {
				$viaje->apply_equivalencia();
			}
		}

		if ($viaje->estado == $viaje->get_estado('informado')) {
			// cierra viaje si vehiculo no tiene data
			if (!self::scan_data_vehiculo($viaje, new vehiculoModel($viaje->vehi_id))) {
				return $viaje->id;
			}
			
			// cierra viaje si geocerca destino no tiene poligono
			if (!self::scan_geocerca($viaje, new geocercaModel($viaje->geo_destino_id))) {
				return $viaje->id;
			}
			
			// viaje con geocerca identificada y vehiculo con data, se pasa en estado analizando para encontrar origen
			$viaje->set_estado('analizando');
			$res = $viaje->update();
			return $viaje->id;
		}

		// buscar y setea origen del viaje
		if ($viaje->estado == $viaje->get_estado('analizando') && empty($viaje->reg_origen_id)) {
			return self::scan_origen($viaje);
		}

		// busca y setea destino del viaje y cierra este
		if ($viaje->estado == $viaje->get_estado('por_descargar') && !empty($viaje->reg_origen_id) && empty($viaje->reg_destino_id)) {
			return self::scan_destino($viaje);
		}
	}

	public static function scan_data_vehiculo(&$viaje, $vehiculo)
	{
		$data_vehiculo = $vehiculo->has_data();
		if (!$data_vehiculo['data']) {
			$viaje->set_data_cerrados('movil_sin_data');
			$viaje->obs = $data_vehiculo['obs'];
			$viaje->update();
			return false;
		}else {
			return true;
		}
	}

	public static function scan_geocerca(&$viaje, $geocerca)
	{
		if (!$geocerca->has_poligono()) {
			$viaje->set_data_cerrados('cancelado_smvs');
			$geocerca->load_geoc();
			$str_obs = "Geocerca sin poligono: {$geocerca->id} - {$geocerca->label}";
			$viaje->obs = substr($str_obs, 0, 65);
			$res = $viaje->update();
			return false;
		} else {
			return true;
		}
	}

	public static function scan_origen(&$viaje)
	{
		$origen = $viaje->get_registro_origen();
		if ($origen) {
			$viaje->reg_origen_id = $origen;
			$viaje->set_estado('por_descargar');
			$viaje->update();
			return $viaje->id;
		} else {
			return false;
		}
	}

	public static function scan_destino(&$viaje)
	{
		$destino = $viaje->get_registro_destino();
		if ($destino) {
			$id_equivalencia = $viaje->get_codigo_equivalencia();
			if ($id_equivalencia) {
				$equivalencia = Equivalencia::get_from_id($id_equivalencia);
				$viaje->geo_destino_id = $equivalencia->id_geo_original;
				$split = explode(".%e=", $viaje->obs);
				$viaje->obs = $split[0];
			}
			$viaje->reg_destino_id = $destino;
			$viaje->set_estado('terminado');
			$viaje->update();
			return $viaje->id;
		} else {
			return false;
		}
	}


	// TODO: terminar metodo para cerrar automaticamente los viajes reabiertos
	public static function cerrar_reactivados()
	{
		$viajes = viajeModel::get_viajes_duplicados();
		if (!$viajes) {
			return false;
		}
		if ($viajes->num_rows > 0) {
			while ($row = $viajes->fetch_object()) {
				$viaje = new viajeModel($row->vi_id);
				$viaje->load_from_row($row);
			}
		}
	}

	public static function get_ids_dup()
	{
		$m_viaje = new viajeModel();
		$dups = $m_viaje->get_viajes_duplicados();
		$ids = [];
		foreach ($dups as $vehi) {
			$ids[] = $vehi['id_vehiculo'];
		}

		return $ids;
	}

	public static function resolve_duplicados($vehi_id)
	{
		$m_vehiculo = new vehiculoModel($vehi_id);
		$data_vehiculo = $m_vehiculo->has_data();
		$dataset_viajes = $m_vehiculo->get_viajes_activos();
		$viajes = [];


		// cargar array de objetos de viaje
		if ($dataset_viajes->num_rows > 0) {
			while ($dataset_viaje = $dataset_viajes->fetch_object()) {
				$viaje = new viajeModel($dataset_viaje->vi_id);
				$viaje->load_from_row($dataset_viaje);
				$viajes[] = $viaje;
			}
		}

		// cerrar si vehiculo no tiene data
		if (!$data_vehiculo['data']) {
			foreach ($viajes as $viaje) {
				$viaje->set_data_cerrados('movil_sin_data');
				$viaje->obs = $data_vehiculo['obs'];
				$viaje->update();
			}
			return false;
		}

		$len = count($viajes);
		if ($len < 2) {
			return false;
		}

		// cierra viajes anteriores dejando solo dos para trabajar
		for ($i=2; $i < $len; $i++) {
			self::cancelar_duplicado($viajes[$i]);
		}

		$t0 = $viajes[0]->time_salida ? $viajes[0]->time_salida : $viajes[0]->time_insert;
		$t1 = $viajes[1]->time_salida ? $viajes[1]->time_salida : $viajes[1]->time_insert;

		if ($viajes[0]->div == $viajes[0]->div) {
			// evaluar geoc_ids de origen, replantear logica
			if ($viajes[0]->div == 4) {
				$en_division = $m_vehiculo->ultimo_geoc(ID_GEOC_DMH);

				$t1 += (60 * 60 * 1); // tolerancia de una hora
				if ($t1 < $en_division) {
					self::cancelar_duplicado($viajes[1]);
				} else {
					self::cancelar_duplicado($viajes[0]);
				}
			// viajes cami
			} else {
				$diff = $t0 - $t1;
				/*evaluar con geoc de origen, no poligonos pero igual*/
				if ($diff > (60 * 60 * 3)) {
					self::cancelar_duplicado($viajes[1]);
				} else {
					self::cancelar_duplicado($viajes[0]);
				}
			}
		} else {
			self::cancelar_duplicado($viajes[1]);
		}

		return false;
	}


	// TODO: llegado a erste punto el vehiculo si tiene data, replantear logica del analizador para que no revise la data del vehiculo en cada ciclo de analisis
	public static function cancelar_duplicado(&$viaje)
	{
		for ($i=0; $i < 4; $i++) { 
			if ($viaje->estado < 0) {
				return false;
			}

			if ($viaje->div == 4) {
				self::scan_viaje_sitt($viaje);
			} else {
				self::scan_viaje_cami($viaje);
			}
		}
		$viaje->set_data_cerrados('cancelado_smvs');
		$obs = "Vehiculo con multiples viajes en curso.";
		$viaje->update();
		$viaje->id;
	}
}