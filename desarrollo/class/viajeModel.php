<?php
/**
 * modelo de clase viaje
 */

require_once("../include/db.php");
require_once("vehiculoModel.php");

class viajeModel {

	public $id;
	public $vehi_id;
	public $geo_origen_id;
	public $geo_destino_id;
	public $time_insert;
	public $time_salida;
	public $estado;
	public $reg_origen_id;
	public $reg_destino_id;
	public $div;
	public $obs;

	function __construct($id = null) { 
		$this->id = $id;
	}

	public static function load($vi_id)
	{
		global $mysqli;
		$query = "SELECT * 
			FROM viajes
			WHERE vi_id = {$vi_id};";
		$res = $mysqli->query($query);
		if ($res->num_rows > 0) {
			$row = $res->fetch_assoc();
			$viaje = new viajeModel($row['vi_id']);
			$viaje->vehi_id = $row['vi_vehi_id'];
			$viaje->geo_origen_id = $row['vi_geoc_id_ori'];
			$viaje->geo_destino_id = $row['vi_geoc_id_dest'];
			$viaje->time_insert = $row['vi_finsert'];
			$viaje->time_salida = $row['vi_fsalida'];
			$viaje->estado = $row['vi_st'];
			$viaje->reg_origen_id = $row['vi_regi_id_ori'];
			$viaje->reg_destino_id = $row['vi_regi_id_dest'];
			$viaje->div = $row['vi_div'];
			$viaje->obs = $row['vi_obs'];

			return $viaje;
		} else {
			return false;
		}
	}

	public function load_from_row($row)
	{
		$this->vehi_id        = $row->vi_vehi_id;
		$this->geo_origen_id  = $row->vi_geoc_id_ori;
		$this->geo_destino_id = $row->vi_geoc_id_dest;
		$this->time_insert    = $row->vi_finsert;
		$this->time_salida    = $row->vi_fsalida;
		$this->estado         = $row->vi_st;
		$this->reg_origen_id  = $row->vi_regi_id_ori;
		$this->reg_destino_id = $row->vi_regi_id_dest;
		$this->div            = $row->vi_div;
		$this->obs            = $row->vi_obs;
	}

	public function set_data_cerrados($estado)
	{
		self::set_estado($estado);
		$this->reg_origen_id = $this->reg_origen_id ? : -1;
		$this->reg_destino_id = $this->reg_destino_id ? : -1;
	}

	public static function get_activos($division, $descarga = false)
	{
		if ($division == 5 && $descarga) {
			$estados = [2, 21, 22, 23];
		}else{
			$estados = [1, 11, 12, 13];
		}

		$estados_str = implode(',', $estados);
		global $mysqli;
		$query = "SELECT
				vi_id,
				vi_vehi_id,
				vi_finsert,
				vi_fsalida,
				vi_st,
				vi_geoc_id_ori,
				vi_regi_id_ori,
				vi_geoc_id_dest,
				vi_regi_id_dest,
				vi_div,
				vi_obs
			FROM `viajes`
			WHERE 
				vi_st IN ({$estados_str}) AND
				vi_div = {$division}" ;
		$res = $mysqli->query($query);
		return $res;
	}

	public static function get_activos_sitt()
	{
		$estados = [1, 11, 12, 13];

		$estados_str = implode(',', $estados);
		global $mysqli;
		$query = "SELECT
				vi_id,
				vi_vehi_id,
				vi_finsert,
				vi_fsalida,
				vi_st,
				vi_geoc_id_ori,
				vi_regi_id_ori,
				vi_geoc_id_dest,
				vi_regi_id_dest,
				vi_div,
				vi_obs
			FROM `viajes`
			WHERE 
				vi_st IN ({$estados_str}) AND
				vi_fsalida IS NOT NULL AND
				vi_div = 4" ;
		$res = $mysqli->query($query);
		return $res;
	}
	
	// TODO: borrar fsalida not null (?
	public static function get_activos_equiv($destinos)
	{
		$destinos_str = implode(',', $destinos);
		global $mysqli;
		$query = "SELECT
				vi_id,
				vi_vehi_id,
				vi_finsert,
				vi_fsalida,
				vi_st,
				vi_geoc_id_ori,
				vi_regi_id_ori,
				vi_geoc_id_dest,
				vi_regi_id_dest,
				vi_div,
				vi_obs
			FROM `viajes`
			WHERE 
				vi_st > 0 AND
				vi_fsalida IS NOT NULL AND
				vi_geoc_id_dest IN ({$destinos_str})";
		$res = $mysqli->query($query);
		echo $query;
		return $res;
	}

	public static function get_reactivados()
	{
		global $mysqli;
		$query = "SELECT *
			FROM viajes
			WHERE
				vi_regi_id_dest = -1 AND
				vi_st > 0;" ;
		$res = $mysqli->query($query);
		return $res;
	}

	public static function get_destinos_identificados()
	{
		return [
			237, // Mejillones
			245, // DSAL
			323, // Montecristo
			328, // Cerro Dominador
			329, // Fundicion alto Norte
			331, // Mercosur la Negra
			334, // Puerto Angamos
			336, // Puerto Barquitos
			350, // Tocopilla
			351  // Trafigura Mercosur
		];
	}

	public static function get_estados($descarga = false)
	{
		if ($descarga) {
			return [
				'informado'      => 2,
				'por_cargar'     => 21,
				'por_descargar'  => 22,
				'analizando'     => 23,
				'terminado'      => -2,
				'movil_sin_data' => -20,
				'fuera_de_plazo' => -21,
				'cancelado_smvs' => -22
			];
		}
		return [
			'informado'      => 1,
			'por_cargar'     => 11,
			'por_descargar'  => 12,
			'analizando'     => 13,
			'terminado'      => -1,
			'movil_sin_data' => -10,
			'fuera_de_plazo' => -11,
			'cancelado_smvs' => -12
		];
	}

	public function find_registro($inicio, $fin, $geo)
	{
		global $mysqli;
		$query = "SELECT
				reg.re_id as reg_id
			FROM (
				SELECT
					regi_id as re_id,
					regi_fecha_posicion as re_fecha,
					regi_vehi_id AS re_vehi_id
				FROM
					registro
				WHERE
					regi_vehi_id = {$this->vehi_id} AND 
					regi_fecha_posicion BETWEEN {$inicio} AND {$fin}
			) AS reg
			JOIN gc_id gi ON gi.gc_regi_id = reg.re_id
			WHERE
				gi.gc_geoc_id = {$geo}
			ORDER BY reg.re_fecha DESC;";
		
		return $mysqli->query($query);
	}

	// ajustar parametros de inicio y fin para viajes sitt:
	// - viajes informados tarde(el vehiculo ya dejo la division)
	public function get_registro_origen($intento = 1, $start = null)
	{
		global $mysqli;
		$geoc_divisional = $this->div == 4 ? ID_GEOC_DMH : ID_GEOC_DCH;
		$geos[] = $this->geo_origen_id;
		$geos[] = $geoc_divisional;
		$inicio;
		$fin;
		switch ($intento) {
			case 1:
				$inicio = $this->time_salida - (60 * 5);
				$fin = $this->time_salida + (60 * 5);
				break;
				
			case 2:
				$inicio = $start - 900;
				$fin = $start + 60;
				break;
		}

		$regi = null;

		// busca registro en origen, si no encuentra en geoc divisional, registros se almacenan en variable regi
		foreach ($geos as $geo) {
			$regi = self::find_registro($inicio, $fin, $geo);
			if ($regi->num_rows > 0) {
				break;
			}
		}

		// si encuentra registros retorna el indice del mecio del total de registros
		$num_registros = $regi->num_rows;
		if ($num_registros > 0) {
			$registros = $regi->fetch_all(MYSQLI_ASSOC);
			$index_registro = (round($num_registros / 2) - 1);
			return $registros[$index_registro]['reg_id'];
		// realiza un segundo intento si el viaje fue informado cuando el vehiculo ya dejo la division
		} else {
			if ($this->div == 4 && $intento == 1) {
				$vehiculo = new vehiculoModel($this->vehi_id);
				$fecha_origen = $vehiculo->ultimo_geoc($this->geo_origen_id);
				$diff = $this->time_salida - $fecha_origen;
				if (empty($fecha_origen) || $diff > 43200) {
					$fecha_origen = $vehiculo->ultimo_geoc($geoc_divisional);
					$diff = $this->time_salida - $fecha_origen;
					if (empty($fecha_origen) || $diff > 43200) {
						return false;
					}
				}
				return self::get_registro_origen(2, $fecha_origen);
			} else {
				return false;
			}
			return false;
		}
	}

	public function get_registro_destino()
	{
		global $mysqli;
		$query = "SELECT
				reg.re_id as reg_id
			FROM (
				SELECT
					regi_id as re_id,
					regi_fecha_posicion as re_fecha,
					regi_vehi_id AS re_vehi_id
				FROM
					registro
				WHERE
					regi_vehi_id = {$this->vehi_id} AND 
					regi_fecha_posicion > {$this->time_salida}
			) AS reg
			JOIN gc_id gi ON gi.gc_regi_id = reg.re_id
			WHERE
				gi.gc_geoc_id = {$this->geo_destino_id} 
			ORDER BY reg.re_fecha DESC;";

		$res = $mysqli->query($query);
		if ($res) {
			$num_registros = $res->num_rows;
			if ($num_registros > 2) {
				$registros = $res->fetch_all(MYSQLI_ASSOC);
				return $registros[$num_registros - 1]['reg_id'];
			}
		}
		return false;
	}

	public static function get_viajes_duplicados(){
		global $mysqli;
		$query = "SELECT 
				dup.id_vehiculo as id_vehiculo,
				ve.vehi_patente as patente,
				dup.cantidad_viajes as cantidad_viajes
			FROM (
				SELECT
					vi.vi_vehi_id AS id_vehiculo,
					COUNT(vi.vi_vehi_id) AS cantidad_viajes
				FROM
					viajes AS vi
				WHERE 
					vi_st > 0
				GROUP BY vi.vi_vehi_id
			) AS dup
			JOIN vehiculo ve ON dup.id_vehiculo = ve.vehi_id
			WHERE dup.cantidad_viajes > 1";

		$res = $mysqli->query($query);
		return $res->fetch_all(MYSQLI_ASSOC);
	}

	// public function get_
	// TODO: Cambiar por viajes despachados
	public function get_viajes_abiertos() {
		global $mysqli;
		$query = "SELECT
				vi_id AS id,
				vi_vehi_id AS vehiculo_id,
				vi_fsalida AS f_salida,
				vi_st AS estado,
				vi_geoc_id_ori AS geo_ori,
				vi_regi_id_ori AS reg_ori,
				vi_geoc_id_dest AS geo_des,
				vi_regi_id_dest AS reg_des,
				vi_div AS division
			FROM viajes
			WHERE 
				vi_st > 0 AND 
				vi_fsalida IS NOT NULL
			ORDER BY vi_fsalida DESC";

		$res = $mysqli->query($query);
		return $res;
	}

	public function get_viajes_despachados($since = 0, $division = null) {
		$str_div = "TRUE";
		if (!empty($division)) {
			$str_div = "vi.vi_div = {$division}";
		}

		global $mysqli;
		$query = "SELECT
				vi.vi_id as id_viaje,
				vi.vi_vehi_id as vehi_id,
				ve.vehi_patente as patente,
				gco.geoc_nombre as ori_nombre,
				gco.geoc_codigo as ori_codigo,
				gco.nom_localidad as ori_localidad,
				gco.nom_localidad_cm as ori_localidad_cm,
				gcd.geoc_nombre as des_nombre,
				gcd.geoc_codigo as des_codigo,
				gcd.nom_localidad as des_localidad,
				gcd.nom_localidad_cm as des_localidad_cm,
				vi.vi_fsalida as f_salida,
				vi.vi_div as division,
				vi.vi_st as estado,
				reg.regi_fecha_posicion as f_posicion_des
			FROM viajes vi
			JOIN vehiculo ve ON ve.vehi_id = vi.vi_vehi_id
			JOIN geocerca gco ON gco.geoc_id = vi.vi_geoc_id_ori
			JOIN geocerca gcd ON gcd.geoc_id = vi.vi_geoc_id_dest
			LEFT JOIN registro reg ON reg.regi_id = vi.vi_regi_id_dest
			WHERE
				vi_fsalida > {$since} AND
				vi_fsalida IS NOT NULL AND
				{$str_div}
			ORDER BY vi_fsalida DESC";

		$res = $mysqli->query($query);
		return $res->fetch_all(MYSQLI_ASSOC);
	}

	public static function get_viajes_analizando($division, $descarga = false) {
		$estados = '1, 11, 12, 13';
		if ($descarga) {
			$estados = '2, 21, 22, 23';
		}

		$salida_null = "TRUE";
		if ($division == 4) {
			$salida_null = "vi_fsalida IS NOT NULL";
		}

		global $mysqli;
		$query = "SELECT
				vi.vi_id as id_viaje,
				vi.vi_vehi_id as vehi_id,
				ve.vehi_patente as patente,
				gco.geoc_nombre as ori_nombre,
				gco.geoc_codigo as ori_codigo,
				gco.nom_localidad as ori_localidad,
				gco.nom_localidad_cm as ori_localidad_cm,
				gcd.geoc_nombre as des_nombre,
				gcd.geoc_codigo as des_codigo,
				gcd.nom_localidad as des_localidad,
				gcd.nom_localidad_cm as des_localidad_cm,
				vi.vi_fsalida as f_salida,
				vi.vi_finsert as f_insert,
				vi.vi_regi_id_ori as reg_origen,
				vi.vi_regi_id_dest as reg_destino,
				vi.vi_div as division,
				vi.vi_st as estado,
				vi.vi_obs as obs,
				reg.regi_fecha_posicion as f_posicion_des
			FROM viajes vi
			JOIN vehiculo ve ON ve.vehi_id = vi.vi_vehi_id
			JOIN geocerca gco ON gco.geoc_id = vi.vi_geoc_id_ori
			JOIN geocerca gcd ON gcd.geoc_id = vi.vi_geoc_id_dest
			LEFT JOIN registro reg ON reg.regi_id = vi.vi_regi_id_dest
			WHERE
				vi_st IN ({$estados}) AND
				vi_div = {$division} AND
				{$salida_null}
			ORDER BY vi_id DESC";

		$res = $mysqli->query($query);
		return $res->fetch_all(MYSQLI_ASSOC);
	}

	public function set_index_geoc($viaje, $op) {
		if (!empty($viaje[$op . '_nombre'])) {
			return $viaje[$op . '_nombre'];
		}

		if (!empty($viaje[$op . '_localidad_cm'])) {
			return $viaje[$op . '_localidad_cm'];
		}

		if (!empty($viaje[$op . '_localidad'])) {
			return $viaje[$op . '_localidad'];
		}

		if (!empty($viaje[$op . '_codigo'])) {
			return $viaje[$op . '_codigo'];
		}

		return "Sin Nombre";
	}

	public function get_viajes_act_custom($since = null, $state = null, $origen = false, $destino = false, $division = null) {

		if (empty($since)) {
			$now = new DateTime();
			$since = $now->getTimestamp();		
		}

		$cadenas = array(
			'state' => '',
			'origen' => '',
			'destino' => '',
			'division' => ''
		);

		if (empty($state)) {
			$cadenas['state'] = "vi_st > 0"; 
		} else {
			$cadenas['state'] = "vi_st = {$state}";
		}

		if ($origen) {
			$cadenas['origen'] = "vi_regi_id_ori IS NOT NULL";
		} else {
			$cadenas['origen'] = "TRUE"; 
		}

		if ($destino) {
			$cadenas['destino'] = "vi_regi_id_dest IS NOT NULL";
		} else {
			$cadenas['destino'] = "TRUE"; 
		}

		if (empty($division)) {
			$cadenas['division'] = "TRUE"; 
		} else {
			$cadenas['division'] = "vi_div = {$division}";
		}

		global $mysqli;
		$query = "SELECT
				vi_id AS id,
				vi_vehi_id AS vehi_id,
				vi_fsalida AS salida,
				vi_st AS estado,
				vi_geoc_id_ori AS geo_ori,
				vi_regi_id_ori AS reg_ori,
				vi_geoc_id_dest AS geo_des,
				vi_regi_id_dest AS reg_des,
				vi_div AS division
			FROM viajes
			WHERE 
				{$cadenas['state']} AND 
				vi_fsalida < {$since} AND
				vi_fsalida IS NOT NULL AND
				{$cadenas['origen']} AND
				{$cadenas['destino']} AND
				{$cadenas['division']}";

		$res = $mysqli->query($query);
		return $res->fetch_all(MYSQLI_ASSOC);
	}

	public function get_viajes_aux($since)
	{
		global $mysqli;
		$query = "SELECT
				vi_id AS id,
				vi_vehi_id AS vehi_id,
				vi_fsalida AS salida,
				vi_st AS estado,
				vi_geoc_id_ori AS geo_ori,
				vi_regi_id_ori AS reg_ori,
				vi_geoc_id_dest AS geo_des,
				vi_regi_id_dest AS reg_des,
				vi_div AS division
			FROM viajes
			WHERE 
				vi_finsert < {$since} AND
				vi_finsert IS NOT NULL AND
				vi_st > 0";

		$res = $mysqli->query($query);
		return $res->fetch_all(MYSQLI_ASSOC);
	}

	/* Nombre set debido a que retorna valor
	 * TODO: cambiar por set y asignar valor en el objeto*/
	public function get_estado_cancel()
	{
		$sts = [
			-12 => [1, 11, 12, 13],
			-22 => [2, 21, 22, 23]
		];
		foreach ($sts as $k => $stat) {
			if (in_array($this->estado, $stat)) {
				return $k;
			}
		}
		return false;
	}

	public function set_estado($estado)
	{
		$this->estado = self::get_estado($estado);
	}

	public function get_estado($estado)
	{
		$estados = [
			'carga' => [
				'informado'      => 1,
				'por_cargar'     => 11,
				'por_descargar'  => 12,
				'analizando'     => 13,
				'terminado'      => -1,
				'movil_sin_data' => -10,
				'fuera_de_plazo' => -11,
				'cancelado_smvs' => -12
			],
			'descarga' => [
				'informado'      => 2,
				'por_cargar'     => 21,
				'por_descargar'  => 22,
				'analizando'     => 23,
				'terminado'      => -2,
				'movil_sin_data' => -20,
				'fuera_de_plazo' => -21,
				'cancelado_smvs' => -22
			]
		];

		$tipo = in_array($this->estado, [1, 11, 12, 13]) ? 'carga' : 'descarga';
		return $estados[$tipo][$estado];
	}

	public function apply_equivalencia($equivalencia = null)
	{
		if (is_null($equivalencia)) {
			$equivalencia = Equivalencia::get_from_original($this->geo_destino_id);
		}
		if (!$equivalencia) {
			return false;
		}
		$obs = "{$equivalencia->equiv_id_geo_target} - {$equivalencia->equiv_nombre} .%e={$equivalencia->equiv_id}";
		$this->geo_destino_id = $equivalencia->equiv_id_geo_target;
		$this->obs = $obs;
		$this->update();
	}

	public function get_codigo_equivalencia()
	{
		$nedle = ".%e=";
		$split = explode($nedle, $this->obs);
		if (array_key_exists(1, $split)) {
			return $split[1];
		} else {
			return false;
		}
	}

	/* TODO: usar estado y obs desde propiedades del objeto,
	 * -nombre del metodo por update*/
	public function cerrar($state, $obs = null, $regi_ori, $regi_des) {
		global $mysqli;
		$query = "UPDATE viajes 
			SET 
				vi_st = {$state}, 
				vi_obs = '{$obs}',
				vi_regi_id_ori = {$regi_ori},
				vi_regi_id_dest = {$regi_des}
			WHERE
				vi_id = {$this->id}";

		// echo $query . "<br>";
		$mysqli->query($query);
		return $query;
	}

	public function update()
	{
		global $mysqli;

		$this->time_salida = $this->time_salida ? : "NULL";
		$this->estado = $this->estado ? : "NULL";
		$this->reg_origen_id = $this->reg_origen_id ? : "NULL";
		$this->reg_destino_id = $this->reg_destino_id ? : "NULL";
		$this->obs = $this->obs ? : ".";
		
		$query = "UPDATE viajes 
			SET 
				vi_fsalida = {$this->time_salida},
				vi_st = {$this->estado},
				vi_regi_id_ori = {$this->reg_origen_id},
				vi_regi_id_dest = {$this->reg_destino_id},
				vi_geoc_id_dest = {$this->geo_destino_id},
				vi_obs = '{$this->obs}'
			WHERE
				vi_id = {$this->id};";

		$mysqli->query($query);
		return $query;
	}
}