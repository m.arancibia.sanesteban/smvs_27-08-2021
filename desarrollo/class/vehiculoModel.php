<?php
/**
 * clase modelo de vehiculo
 */

require_once("../include/db.php");

class vehiculoModel {

	public $id;
	public $patente;
	public $nombre;
	public $visible;
	public $empresa_id;
	public $equipo_id;

	function __construct($id = null, $patente = null) {
		$this->id = $id;
		$this->patente = $patente;
	}

	public function get_viajes($since = null) {
		global $mysqli;
		$query = "SELECT
				vi.vi_id as id_viaje,
				ve.vehi_patente as patente,
				gco.geoc_id as ori_geo_id,
				gcd.geoc_id as des_geo_id,
				gco.geoc_nombre as ori_nombre,
				gco.geoc_codigo as ori_codigo,
				gco.nom_localidad as ori_localidad,
				gco.nom_localidad_cm as ori_localidad_cm,
				gcd.geoc_nombre as des_nombre,
				gcd.geoc_codigo as des_codigo,
				gcd.nom_localidad as des_localidad,
				gcd.nom_localidad_cm as des_localidad_cm,
				vi.vi_finsert as f_insert,
				vi.vi_fsalida as f_salida,
				vi.vi_regi_id_ori as reg_origen,
				vi.vi_regi_id_dest as reg_destino,
				vi.vi_div as division
			FROM viajes vi
			JOIN vehiculo ve ON ve.vehi_id = vi.vi_vehi_id
			JOIN geocerca gco ON gco.geoc_id = vi.vi_geoc_id_ori
			JOIN geocerca gcd ON gcd.geoc_id = vi.vi_geoc_id_dest
			WHERE
				vi_vehi_id = {$this->id} AND
				vi_st > 0
			ORDER BY vi.vi_id ASC";
		$res = $mysqli->query($query);
		return $res->fetch_all(MYSQLI_ASSOC);
	}

	// TODO: asignar indice del arreglo para evaluar el valor a retornar
	public function set_index_geoc($viaje, $op) {
		if (!empty($viaje[$op . '_nombre'])) {
			return $viaje[$op . '_nombre'];
		}

		if (!empty($viaje[$op . '_localidad_cm'])) {
			return $viaje[$op . '_localidad_cm'];
		}

		if (!empty($viaje[$op . '_localidad'])) {
			return $viaje[$op . '_localidad'];
		}

		if (!empty($viaje[$op . '_codigo'])) {
			return $viaje[$op . '_codigo'];
		}

		//retornar codigo geocerca
		return "Sin Nombre";	
	}

	// evalua que el movil tenga data registrada
	public function has_data($rango = 172800) {
		global $mysqli;
		$query = "SELECT regi_fecha_posicion AS fecha
			FROM registro
			WHERE regi_vehi_id = {$this->id} 
			ORDER BY regi_fecha_posicion DESC
			LIMIT 1";
		$res = $mysqli->query($query);
		if ($res->num_rows > 0) {
			$data = $res->fetch_assoc();
			$now = new DateTime();
			$t_now = $now->getTimestamp();
			if ($data['fecha'] < ($t_now - $rango)){
				$ultimo = date('d-m-y H:i:s', $data['fecha']);
				return array(
					'data' => false,
					'obs'  => "MOVIL SIN DATA | ULTIMO REGISTRO {$ultimo}"
				);
			}else{
				return array(
					'data' => true
				);
			}
		}else{
			return array(
				'data' => false,
				'obs'  => "MOVIL NO REGISTRADO"
			);
		}
	}

	public function ultimo_geoc($geoc)
	{
		$date_now = new DateTime();
		$now = $date_now->getTimestamp();
		$since = $now - (60 * 60 * 48);
		global $mysqli;
		$query = "SELECT
				reg.re_fecha as re_fecha
			FROM (
				SELECT
					regi_id as re_id,
			        regi_fecha_posicion as re_fecha,
			        regi_vehi_id AS re_vehi_id
			    FROM
					registro
				WHERE
					regi_vehi_id = {$this->id} AND 
			        regi_fecha_posicion > {$since}
			) AS reg
			JOIN gc_id gi ON gi.gc_regi_id = reg.re_id
			WHERE
				gi.gc_geoc_id = {$geoc}
			ORDER BY reg.re_fecha DESC LIMIT 1;";
		$res = $mysqli->query($query);
		if ($res->num_rows > 0) {
			return $res->fetch_assoc()['re_fecha'];
		} else {
			return false;
		}
	}	
	
	public function get_viajes_activos()
	{
		global $mysqli;
		$query = "SELECT *
			FROM
				viajes
			WHERE
				vi_vehi_id = {$this->id} AND
				vi_st > 0
			ORDER BY vi_finsert ASC";
		return $mysqli->query($query);
	}
}