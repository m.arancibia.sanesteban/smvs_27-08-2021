<?php
require_once("class/analyzer.php");

// Resolver viajes duplicados
Analyzer::solve_viajes_duplicados();

// Analizar viajes tipo carga de cami
Analyzer::analyze_cami(false);

// Analizar viajes tipo descarga de cami
Analyzer::analyze_cami(true);