<?php

	include("../include/class_all_search.php");
	$gps = new gps_serco();	
	$gps->format_vars_class("2019-04-07");
	$vars = $gps->exec_sql();
	$moving = $vars[0];
	$speeding = $vars[1];
?>
<!DOCTYPE html>
<html>
<head>
<style>
 thead {
    color:green;
  }
  tbody {
    color:black;
  }
  tfoot {
    color:red;
  }
  table{
    width: auto;
    margin: 0 auto; 
  }
  table, th, td {
      border: 1px solid black;
      font-family: Calibri,sans-serif; 
      font-size: .9em;
      border-spacing: 0;
  }
  th{
    background-color: #8D0404;
    color: white;
  }
</style>

  <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    var tabs = $( "#tabs" ).tabs();
    tabs.find( ".ui-tabs-nav" ).sortable({
      axis: "x",
      stop: function() {
        tabs.tabs( "refresh" );
      }
    });
  } );
  </script>
  
</head>
<body>

<div id="tabs">
  <ul>
    <li><a href="#tabs-1">SPEEDING</a></li>
    <li><a href="#tabs-2">MOVING_STOP</a></li>
  </ul>
  <div id="tabs-1">
<table>
  <thead>
    <tr>
      <th>PPU</th>
      <th>IDev</th>
      <th>INICIOEv</th>
      <th>FINEv</th>
      <th>DURACIONEv [MIN]</th>
      <th>DISTEv [KM]</th>
      <th>VELX [KM/H]</th>
      <th>POINT_INI</th>     
      <th>POINT_FIN</th>    
      <th>IDGC_INI</th>     
      <th>IDGC_FIN</th>        
      <th>VELPERM_INI [KM/H]</th>     
      <th>VELPERM_FIN [KM/H]</th> 
		<!--INICIO DATOS FALTA-->
      <th>DIFMAX_FALTA [KM/H]</th>     
      <th>VELMAX_FALTA [KM/H]</th>
      <th>IDGC_FALTA</th>
      <th>POINT_FALTA</th>
      <th>VELPERM_FALTA [KM/H]</th>
		<!--FIN DATOS FALTA-->
		<!--INICIO VEL MAX REGISTRADA-->
      <th>VELMAX_TRAMO [KM/H]</th>     
      <th>DIFMAX_VELMAX_TRAMO [KM/H]</th>
      <th>IDGC_VELMAX</th>
      <th>POINT_VELMAX</th>
      <th>VELPERM_VELMAX [KM/H]</th> 
      	<!--FIN VEL MAX REGISTRADA-->     
	</tr>
  </thead>
  <tbody>
<?php
	foreach($speeding as $var){
		echo "<tr>";
		echo "<td>" . $var["ppu"] . "</td>";
		echo "<td>" . $var["speeding"] . "</td>";
		echo "<td>" . $var["finicio"] . "</td>";
		echo "<td>" . $var["ffin"] . "</td>";
		echo "<td>" . number_format(($var["time"] / 60), 2, ',', '.') . "</td>";
		echo "<td>" . number_format($var["lenght"], 2, ',', '.') . "</td>";
		echo "<td>" . round($var["promVel"]/ $var["num_rows"]) . "</td>";
		echo "<td>" . $var["point_start"] . "</td>";
		echo "<td>" . $var["point_end"] . "</td>";
		echo "<td>" . $var["gc_start"] . "</td>";
		echo "<td>" . $var["gc_end"] . "</td>";		
		echo "<td>" . $var["vel_start"] . "</td>";
		echo "<td>" . $var["vel_end"] . "</td>";

		echo "<td>" . $var["speed_dif"] . "</td>";
		echo "<td>" . $var["dif_vel"] . "</td>";
		echo "<td>" . $var["dif_geoc_id"] . "</td>";
		echo "<td>" . $var["dif_point"] . "</td>";
		echo "<td>" . $var["dif_max_speed"] . "</td>";

		echo "<td>" . $var["speed"] . "</td>";
		echo "<td>" . $var["speed_speed_dif"] . "</td>";
		echo "<td>" . $var["speed_geoc_id"] . "</td>";
		echo "<td>" . $var["speed_point"] . "</td>";
		echo "<td>" . $var["speed_max_speed"] . "</td>";
		echo "</tr>";
	}
?>  </tbody>
</table>
  </div>
  <div id="tabs-2">

<table>
  <thead>
    <tr>
      <th>ID1</th>
	  <th>ID2</th>
	  <th>PPU</th>
      <th>ID_GC</th>
      <th>NOM_GC</th>
      <th>INIEv</th>
      <th>FINEv</th>
      <th>DURACION [MIN]</th>
      <th>DISTANCIA [KM/H]</th>
      <th>VELPROM</th>
      <th>IDMOV</th>
      <th>IDSTOP</th>
      <th>LASTPOINT</th>
      <th>TIPOGC</th>
   </tr>
  </thead>
  <tbody>
<?php
	
	$temp_id = NULL;
	foreach($moving as $var){
		$id = $var["move"] . "to" . $var["stop"];
		if( !isset($patente) || @$patente != $var["ppu"] ) {
			$i_e = 1;
			$i_a = 1;
			$patente = $var["ppu"];
		} else if( $temp_id != $id ) {
			$i_a = 1;
			$i_e++;
		}
		
		echo "<tr>";
		echo "<td>" . $i_e . "</td>";
		echo "<td>" . $i_a++. "</td>";		
		echo "<td>" . $var["ppu"] . "</td>";
		echo "<td>" . $var["geoc_id"] . "</td>";
		echo "<td>" . utf8_encode($var["geoc_nombre"]) . "</td>";
		echo "<td>" . $var["finicio"] . "</td>";
		echo "<td>" . $var["ffin"] . "</td>";
		echo "<td>" . number_format(($var["time"]/60), 2, ',', '.'). "</td>";
		echo "<td>" . number_format(($var["lenght"]), 2, ',', '.') . "</td>";
		echo "<td>" . round($var["promVel"]/ $var["num_rows"]) . "</td>";
		echo "<td>" . $var["move"] . "</td>";
		echo "<td>" . $var["stop"] . "</td>";
		echo "<td>" . $var["point"] . "</td>";
		echo "<td>" . $var["geoc_tipo"] . "</td>";
		echo "<td>" . $var["op_azumit_vel"] . "</td>";
		echo "</tr>";
		
		$temp_id = $id;
	}

?>  </tbody>
</table>
  </div>
</div>
</body>
</html>