<?php

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../include/PHPExcel-1.8/Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Sercoing LTDA")
							 ->setLastModifiedBy("Sercoing LTDA")
							 ->setTitle("Office 2007 XLSX  Document")
							 ->setSubject("Office 2007 XLSX Document")
							 ->setDescription("Test document for Office 2007 XLSX")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("result file");

	$fecha = date( "Y-m-d", strtotime( " - 1 days"  ) );
	//$fecha = "2021-06-05";
	include("../include/class_all_search.php");
	$gps = new gps_serco();
	//Camiones
	$gps->format_vars_class(1);
	//Camionetas
	//$gps->format_vars_class(2);

	$vars = $gps->exec_sql();
	/*$moving = $vars[0];
	$speeding = $vars[1];*/
	//print_r($vars);
	//die();

	$hmoving = array("ID1","ID2","PPU","ID_GC","NOM_GC","INIEv","FINEv","DURACION [MIN]","DISTANCIA [KM/H]","VELPROM","IDMOV","IDSTOP","LASTPOINT",
					"TIPOGC","ID_VH","MAX_VEL","IDTRV");
	
	$hspeeding = array("PPU","IDev","INICIOEv","FINEv","DURACIONEv [MIN]","DISTEv [KM]","VELX [KM/H]","POINT_INI","POINT_FIN","IDGC_INI",
						"IDGC_FIN","VELPERM_INI [KM/H]","VELPERM_FIN [KM/H]","DIFMAX_FALTA [KM/H]","VELMAX_FALTA [KM/H]","IDGC_FALTA",
						"POINT_FALTA","VELPERM_FALTA [KM/H]","VELMAX_TRAMO [KM/H]","DIF MAX_VELMAX_TRAMO [KM/H]","IDGC_VELMAX","POINT_VELMAX",
						"VELPERM_VELMAX [KM/H]","EPNOM","GRAVEDAD","ID_VH","IDTRV");
					
	$hin_process = array("PPU","Tiempo","FI","FS","ID");

	$hdata_travels = array("PPU","FI","FS","ID");

	if( $vars[0] != 0 ){		
		$temp_id = NULL;
		foreach($vars[0] as $var){
			$id = $var["move"] . "to" . $var["stop"];
			if( !isset($patente) || @$patente != $var["ppu"] ) {
				$i_e = 1;
				$i_a = 1;
				$patente = $var["ppu"];
			} else if( $temp_id != $id ) {
				$i_a = 1;
				$i_e++;
			}
			
			$moving[] = array(
			0 => $i_e,
			1 => $i_a++,		
			2 => $var["ppu"],
			3 => $var["geoc_id"],
			4 => utf8_encode($var["geoc_nombre"]),
			5 => $var["finicio"],
			6 => $var["ffin"],
			7 => number_format(($var["time"]/60), 2, ',', '.'),
			8 => number_format(($var["lenght"]), 2, ',', '.'),
			9 => ($var["promVel"]== 0)? "0" : round($var["promVel"]/$var["num_rows"]) , //round
			10 => $var["move"],
			11 => $var["stop"],
			12 => $var["point"],
			13 => $var["geoc_tipo"],
			14 => $var["id_vh"],
			15 => $var["max_vel"],
			16 => $var["id_travel"]);
			
			$temp_id = $id;
		}
	}	
	
	if( $vars[1] != 0 ){

		$var = NULL;
		foreach($vars[1] as $var){
			$speeding[] = array(
			0 => $var["ppu"],
			1 => $var["speeding"],
			2 => $var["finicio"],
			3 => $var["ffin"],
			4 => number_format(($var["time"] / 60), 2, ',', '.'),
			5 => number_format($var["lenght"], 2, ',', '.'),
			6 => round($var["promVel"]/ $var["num_rows"]),
			7 => $var["point_start"],
			8 => $var["point_end"],
			9 => $var["gc_start"],
			10 => $var["gc_end"],	
			11 => $var["vel_start"],
			12 => $var["vel_end"],
	
			13 => $var["speed_dif"],  //***
			14 => $var["dif_vel"],
			15 => $var["dif_geoc_id"],
			16 => $var["dif_point"],
			17 => $var["dif_max_speed"], //****
	
			18 => $var["speed"],
			19 => $var["speed_speed_dif"],
			20 => $var["speed_geoc_id"],
			21 => $var["speed_point"],
			22 => $var["speed_max_speed"],
			23 => $var["emp"],
			24 => $var["gravedad"],		
			25 => $var["id_vh"],
			26 => $var["id_travel"]);
			
			$speeding_file[] = array(
			0 => $var["ppu"],
			1 => $var["id_vh"],
			2 => number_format(($var["time"] / 60), 2, ',', '.'),
			3 => number_format($var["lenght"], 2, ',', '.'),
			4 => $var["dif_geoc_id"],
			5 => $var["emp"],
			6 => $var["gravedad"]
			);
		}
	}

	$div = array();
	foreach($vars[2]  as $key => $var){
		foreach($var as $dv){
			$temp = array();
			$temp[] = $key;
			$temp[] = $dv;
			$temp[] = $fecha;
			$div[] = $temp;
		}
	}
	
	$in_process = array();
	foreach($vars[3] as $var){
		$in_process[] = array(
		0 => $var["vi_vehi_id"],
		1 => $var["date_diff"],
		2 => $var["fi"],
		3 => $var["fs"],
		4 => $var["id"]);
	};
	
	if( $vars[4] != 0 ){
		foreach($vars[4] as $var){
			$data_travels[] = array(
			0 => $var["vi_vehi_id"],
			1 => $var["fi"],
			2 => $var["fs"],
			3 => $var["id"]);
		};
	}
	$nfile = str_replace("-", "", $fecha);



	
	$myfile = fopen("./var_dump/pt/$nfile.php", "w") or die("Unable to open file!");
	$txt = serialize($vars[2]);
	fwrite($myfile, $txt);
	fclose($myfile);	
	
	/*print_r($speeding);
	die();*/
$objPHPExcel->getActiveSheet()->setTitle('MV');
$objPHPExcel->getActiveSheet()->fromArray($hmoving, null, 'A1');

if(isset($moving)){
	$myfile = fopen("./var_dump/mv/$nfile.php", "w") or die("Unable to open file!");
	$txt = serialize($moving);
	fwrite($myfile, $txt);
	fclose($myfile);

	$objPHPExcel->getActiveSheet()->fromArray($moving, null, 'A2');
}

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setTitle('EV');
$objPHPExcel->getActiveSheet()->fromArray($hspeeding, null, 'A1');

if(isset($speeding)){
	$myfile = fopen("./var_dump/sp/$nfile.php", "w") or die("Unable to open file!");
	$txt = serialize($speeding_file);
	fwrite($myfile, $txt);
	fclose($myfile);
	
	$objPHPExcel->getActiveSheet()->fromArray($speeding, null, 'A2');
}

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(2);
$objPHPExcel->getActiveSheet()->setTitle('DV');
$objPHPExcel->getActiveSheet()->fromArray($div, null, 'A2');


$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(3);
$objPHPExcel->getActiveSheet()->setTitle('IN_PROCESS');
$objPHPExcel->getActiveSheet()->fromArray($hin_process, null, 'A1');
$objPHPExcel->getActiveSheet()->fromArray($in_process, null, 'A2');

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(4);
$objPHPExcel->getActiveSheet()->setTitle('Fechas Viaje');
$objPHPExcel->getActiveSheet()->fromArray($hdata_travels, null, 'A1');

if(isset($data_travels)){
	$objPHPExcel->getActiveSheet()->fromArray($data_travels, null, 'A2');
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="01simple.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>