<?php
require_once("class/analyzer.php");

// Resolver viajesgit status duplicados
Analyzer::solve_viajes_duplicados();

// Analizar viajes de sitt
Analyzer::analyze_sitt();

// Analizar viajes tipo carga de cami
Analyzer::analyze_cami(false);

// Analizar viajes tipo descarga de cami
Analyzer::analyze_cami(true);