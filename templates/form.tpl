            <form id="geoc">       
              <div id="something" class="accordion list list_ul">
{if isset($geoc['DCH'])}             
                <h3>DCH</h3>
                <ul name="0">    
{foreach key=key from=$geoc['DCH'] item=dch}                
					<li>
						<input type="checkbox" name="{$key}" id="{$key}_geoc" />
						<label for="{$key}_geoc" title="{$dch['nombre']}">{$dch['codigo']}</label>
			        </li>                
{/foreach}
                </ul>
{/if}
{if isset($geoc['DMH'])}
                <h3>DMH</h3>
                <ul name="1">
{foreach key=key from=$geoc['DMH'] item=dmh}       
					<li>
						<input type="checkbox" name="{$key}" id="{$key}_geoc" />
						<label for="{$key}_geoc" title="{$dmh['nombre']}">{$dmh['codigo']}</label>
			        </li>                
{/foreach}
                </ul>
{/if}
{if isset($geoc['DSAL'])}
                <h3>DSAL</h3>
                <ul name="2">
{foreach key=key from=$geoc['DSAL'] item=dsal}                
					<li>
						<input type="checkbox" name="{$key}" id="{$key}_geoc" />
						<label for="{$key}_geoc" title="{$dsal['nombre']}">{$dsal['codigo']}</label>
			        </li>                
{/foreach}
                </ul>
{/if}
                <h3>VARIAS</h3>
                <ul name="3">
{foreach key=key from=$geoc['OTRAS'] item=varias}                
					<li>
						<input type="checkbox" name="{$key}" id="{$key}_geoc" />
						<label for="{$key}_geoc" title="{$varias['nombre']}">{$varias['codigo']}</label>
			        </li>                
{/foreach}
                </ul>
              </div>
              <div id="not-these">
                <div class="acction">
                  <p>seleccionado</p>
                </div>
                <div class="accordion list list_ul">
{if isset($geoc['DCH'])}                  
                  <h3>DCH</h3>
                  <ul>
{foreach key=key from=$geoc['DCH'] item=dch}                  
					<li>
						<input type="checkbox" id="{$key}_sl" />
						<label id="{$key}" title="{$dch['nombre']}">{$dch['codigo']}</label>
                    </li>
{/foreach}                                  
                  </ul>
{/if}
{if isset($geoc['DMH'])}
                  <h3>DMH</h3>
                  <ul>
{foreach key=key from=$geoc['DMH'] item=dmh}                  
					<li>
						<input type="checkbox" id="{$key}_sl" />
						<label id="{$key}" title="{$dmh['nombre']}">{$dmh['codigo']}</label>
                    </li>
{/foreach}
                  </ul>
{/if}
{if isset($geoc['DSAL'])}       
                  <h3>DSAL</h3>
                  <ul>
{foreach key=key from=$geoc['DSAL'] item=dsal}                  
					<li>
						<input type="checkbox" id="{$key}_sl" />
						<label id="{$key}" title="{$dsal['nombre']}">{$dsal['codigo']}</label>
                    </li>
{/foreach}                  
{/if}
                  </ul>
                  <h3>VARIAS</h3>
                  <ul>
{foreach key=key from=$geoc['OTRAS'] item=varias}                   
					<li>
						<input type="checkbox" id="{$key}_sl" />
						<label id="{$key}" title="{$varias['nombre']}">{$varias['codigo']}</label>
                    </li>
{/foreach}
                  </ul>
                </div>
              </div>
            </form>