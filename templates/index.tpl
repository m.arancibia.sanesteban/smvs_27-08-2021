<!DOCTYPE html>
<html lang="es">
<head>
<title>.:: SMVS | SERCOING ::.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/x-icon" href="https://sercoing.cl/firma/serco.ico" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="./js/dist/Chart.min.js"></script>
<script src="./js/utils.js"></script>
<script src="./js/chart.new.js"></script> 

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="./js/jquery-ui-timepicker-addon.js"></script>
<script src="./js/extra.js"></script>

<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

<link href="./css/jquery-ui-timepicker-addon.css" rel="stylesheet" media="screen">

<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">

<link rel="stylesheet" href="./css/overflow-table.css">

<link rel="stylesheet" href="./css/accordion-panel.css">
<link rel="stylesheet" href="./css/accordion-list.css">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/grafico.css">


<style>
div.left_menu div.ui-accordion-content {
	height: calc( 100% - {$hacpan}px );
}
form#geoc div.list_ul ul{
	height:100%;
  	height:calc(100% - {$haclist}px);
}
</style>

<!-- CSS
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="./css/slider.css"> 
<!-- JAVASCRIPT Slider
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <script src="./js/slider.js"></script>
    <script src="./js/config.js"></script>
  
<!-- JAVASCRIPT Otros
–––––––––––––––––––––––––––––––––––––––––––––––––– -->   
<script src="./js/ES_es.js"></script>
<script src="./js/functions.js"></script>
<script src="./js/script_init.js"></script>
<script src="./js/searchs.js"></script>
<script src="./js/markerclusterer.js"></script>
<script src="./js/parsePoly.js"></script>
<script src="./js/gps.js"></script>

<script type="text/JavaScript">

	$(function(){
	{if in_array("mov", $seccion_priv)}
		form_select("patent");
	{/if}
	{if in_array("geoc", $seccion_priv)}	
		form_select("geoc");
	{/if}
	{if in_array("rut", $seccion_priv)}
		form_select("routes");
	{/if}
	{if in_array("rec", $seccion_priv)}
		searchtr();
	{/if}
	
});

</script>
   
</head>
<body>
  {literal}
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> 
        <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <div class="navbar-brand">
          <img src="http://sercoing.cl/firma/SercoingLogo_100x150.png" alt="SMVS">
          <a class="btn" href="#">SMVS-SERCOING LTDA</a>
        </div>      
      </div>
      <div id="myNavbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#" class="arrow-goto0"><i class="fa fa-map"></i> Mapa</a></li>
          <li><a href="#" class="arrow-goto1"><i class="fa fa-line-chart"></i> Reporte</a></li>
          <li><a href="#" class="arrow-goto2"><i class="fa fa-cog"></i> Administracion</a></li>
          <li><a href="javascript:"><i class="fa fa-cog"></i> Prueba</a></li>
        </ul>
      {/literal}
        <ul class="nav navbar-nav navbar-right">
          <li class="user"><p class="align-middle">{$user}<!--Julio Muñoz--></p></li>
          <li><a href="./?logout=1"><i class="fa fa-sign-out"></i> Log-out</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <section>
    <div class="container-fluid text-center">
      <div class="row content">
        <div class="icono--div">
          <div class="icono">
            <a href="#" class="borderr">
              <span class="raya"></span>
              <span class="raya"></span>
              <span class="raya"></span>
            </a>
          </div>
        </div>

        <div class="desp-btn">
          <div id="switch_desp" class="icono-r borderr">
            <a class="" href="#">
              <i id="desp_ico" class="fa fa-truck"></i>
            </a>
          </div>
        </div>

        


        <div id="mySidenav" class="sidenav">
          <div class="left_menu accordion">
          {*if in_array("not", $seccion_priv)*}        
          {if false}        
            <h3> <i class="fa fa-warning"></i> Despachos</h3>
            <div id="notialert" class="panel table-responsive ">
       	      <div id="constrainer">
                <div class="left_menu accordion">
                  <h3>SITT</h3>
                  <table class="table table-hover table-dark fixed table-condensed table-bordered no-padd"> 
                    <thead>
                      <tr>
                        <th id="ah">Movil</th>
                        <th>Despachado</th>
                        <th>Origen</th>
                        <th>Destino</th>
                        <th>Estado</th>
                      </tr>
                    </thead>
                    <tbody id="alert_geoc">
                    </tbody>
                  </table>
                  
                  <h3>CAMI</h3>
                  <table class="table table-hover table-dark fixed table-condensed table-bordered no-padd"> 
                    <thead>
                      <tr>
                        <th id="ah">Movil</th>
                        <th>Despachado</th>
                        <th>Origen</th>
                        <th>Destino</th>
                        <th>Estado</th>
                      </tr>
                    </thead>
                    <tbody id="alert_geoc_cami">
                  
                    </tbody>
                  </table>     
                </div>
              </div>
            </div>
          {/if}
          {if in_array("mov", $seccion_priv)}
            <h3> <i class="fa fa-car"></i> Moviles</h3>
            <div id="opcion1" class="panel">         
           	  <div class="div-group">        
                <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                  <input name="pt" id="search_p" type="text" class="form-control" placeholder="Buscar patente">
                </div>
                <div id="btn_group">
	                <button type="button" class="btn btn-info btn-xs"><i class="fa fa-object-group" style="font-size:20px"></i></button>
                </div>
           	  </div>  
              <form id="patent">       
                <div id="something" class="accordion list list_ul">
                  <h3>CAMIONETAS</h3>
                  <ul name="0">
                  {if isset($moviles[2])}
                    {foreach key=key from=$moviles[2] item=camioneta}
				              <li>
                        <input type="checkbox" name="{$key}" id="{$key}_patent" />
                        <label for="{$key}_patent" title="">{$camioneta}</label>
                      </li>                                 
                    {/foreach}
                  {/if}
                  </ul>
                  <h3>CAMIONES</h3>
                  <ul name="1">
                  {foreach key=key from=$moviles[1] item=camion}
                    <li>
                      <input type="checkbox" name="{$key}" id="{$key}_patent" />
                      <label for="{$key}_patent" title="">{$camion}</label>
                    </li>                                       
                  {/foreach}
                  </ul>
                </div>
                <div id="not-these">
                  <div class="acction">
                    <p>seleccionado</p>
                  </div>
                  <div class="accordion list list_ul">
                    <h3>CAMIONETAS</h3>
                    <ul>
                    {if isset($moviles[2])}                  
                      {foreach key=key from=$moviles[2] item=camioneta}
                        <li>
                          <input type="checkbox" id="{$key}_sl" />
                          <label id="{$key}">{$camioneta}</label>
                        </li>                    
                      {/foreach}
                    {/if}
                    </ul>
                    <h3>CAMIONES</h3>
                    <ul>
                    {foreach key=key from=$moviles[1] item=camion}
                      <li>
                        <input type="checkbox" id="{$key}_sl" />
                        <label id="{$key}">{$camion}</label>
                      </li>                    
                    {/foreach}
                    </ul>
                  </div>
                </div>
              </form>
            </div>
          {/if}
          {if in_array("geoc", $seccion_priv)}
            <h3> <i class="fa fa-map-marker"></i> Geocercas</h3>
            <div id="opcion2" class="panel">         
           	  <div class="div-group">        
                <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                  <input name="gc" id="search_g" type="text" class="form-control" placeholder="Buscar Geocerca">
                </div>
           	  </div>  
              <form id="geoc">       
                <div id="something" class="accordion list list_ul">
                {assign var="id_ac" value=0}
                {foreach key=div from=$geoc item=all_geoc}                
                  <h3>{$div}</h3>
                  <ul name="{$id_ac}">    
                  {foreach key=key from=$all_geoc item=dch}                
                    <li>
						          <input type="checkbox" name="{$key}" id="{$key}_geoc" />
						          <label for="{$key}_geoc" title="{$dch['nombre']}">{$dch['codigo']}</label>
                    </li>                
                  {/foreach}
                  {assign var="id_ac" value = $id_ac + 1}   
                  </ul>
                {/foreach}
                </div>
                <div id="not-these">
                  <div class="acction">
                    <p>seleccionado</p>
                  </div>
                  <div class="accordion list list_ul">
                  {if isset($geoc['DCH'])}                  
                    <h3>DCH</h3>
                    <ul>
                    {foreach key=key from=$geoc['DCH'] item=dch}                  
                      <li>
						            <input type="checkbox" id="{$key}_sl" />
                        <label id="{$key}" title="{$dch['nombre']}">{$dch['codigo']}</label>
                      </li>
                    {/foreach}                                  
                    </ul>
                  {/if}
                  {if isset($geoc['DMH'])}
                    <h3>DMH</h3>
                    <ul>
                    {foreach key=key from=$geoc['DMH'] item=dmh}                  
                      <li>
						            <input type="checkbox" id="{$key}_sl" />
                        <label id="{$key}" title="{$dmh['nombre']}">{$dmh['codigo']}</label>
                      </li>
                    {/foreach}
                    </ul>
                  {/if}
                  {if isset($geoc['DSAL'])}       
                    <h3>DSAL</h3>
                    <ul>
                    {foreach key=key from=$geoc['DSAL'] item=dsal}                  
                      <li>
  						          <input type="checkbox" id="{$key}_sl" />
  						          <label id="{$key}" title="{$dsal['nombre']}">{$dsal['codigo']}</label>
                      </li>
                    {/foreach}                  
                    </ul>
                  {/if}
                    <h3>VARIAS</h3>
                    <ul>
                    {foreach key=key from=$geoc['VARIAS'] item=varias}                   
                      <li>
    						        <input type="checkbox" id="{$key}_sl" />
    					          <label id="{$key}" title="{$varias['nombre']}">{$varias['codigo']}</label>
                      </li>
                    {/foreach}
                    </ul>
                  </div>
                </div>
              </form>
            </div>
          {/if}
          {if in_array("rut", $seccion_priv)}      
            <h3> <i class="fa fa-road"></i> Rutas</h3>
            <div id="opcion3" class="panel">
              <div class="div-group">        
                <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                  <input name="rt" id="search_r" type="text" class="form-control" placeholder="Buscar ruta">
                </div>
           	  </div>
              <form id="routes">
                <div id="something" class="list list_li">
                  <ul class="ui-accordion-content ui-widget-content">
                  {foreach from=$routes item=route}
                    <li>
                      <input type="checkbox" name="{$route}" id="{$route}_routes" />
                      <label for="{$route}_routes" title="">{$route}</label>
      				      </li>
                  {/foreach}
                  </ul>
                </div>
                <div id="not-these">
                  <div class="acction">
                    <p>seleccionado</p>
                  </div>
                  <div class="list list_li">
                    <ul class="ui-accordion-content ui-widget-content">
                    {foreach from=$routes item=route}
                      <li>
                        <input type="checkbox" id="{$route}_sl" />
                        <label id="{$route}" >{$route}</label>
                      </li>
                    {/foreach}
                    </ul>
                  </div>
                </div>
              </form>
            </div>
          {/if}
          {if in_array("rec", $seccion_priv)}    
            <h3> <i class="fa fa-map-o"></i> Recorridos</h3>
            <div id="opcion4" class="panel">
 	            <form id="recorrido" class="list" action="#" method="post">
 	              <label for="rec_hours"><b>Tiempo de seguimiento</b></label>
                  <div>      
                    <select name="rec_hours" id="rec_hours">
                      <option value="1">1 hora</option>
                      <option value="2">2 horas</option>
                      <option value="3">3 horas</option>
                      <option value="4">4 horas</option>
                      <option value="6">6 horas</option>
                      <option value="8">8 horas</option>
                      <option value="12">12 horas</option>
                      <option value="24">24 horas</option>
                      <option value="48">48 horas</option>
                    </select>
                  </div>
                  <label for="datepicker"><b>Fecha termino</b></label>
                  <div>
	                  <input type="text" id="datepicker" name="date_end" placeholder="&#xF073;&nbsp;Seleccione fecha">
                  </div>
                  <label for="sel_reg"><b>Patentes</b></label>
                  <div id="trace">
                    <div>
                      <input name="tp" id="search_tp" placeholder="&#xf1b9;&nbsp;Busque vehiculo" type="text">
                    </div>
                    <select name="sel_reg" id="sel_reg" size="11" style="min-height:100px">
                    {if isset($moviles[2])}
                    	<optgroup label="Camionetas">
                      {foreach key=key from=$moviles[2] item=camioneta}
                        <option value="{$key}">{$camioneta}</option>                   
                      {/foreach}
                      </optgroup>
                    {/if}
                      <optgroup label="Camiones">
                      {foreach key=key from=$moviles[1] item=camion}
                        <option value="{$key}">{$camion}</option>                    
                      {/foreach}
                      </optgroup>
                    </select>
                  </div>
                <div>
                  <input type="button" id="maps_rec_clean" value="Limpiar">
                </div>
              </form>
            </div>
          {/if}
          </div>
        </div>

        <div id="side_bar_r" class="side-bar-right">
          <h4>Viajes Informados</h4>
          <div id="viajes_despachados" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="viajes_sitt">
                <a data-toggle="collapse" data-parent="#viajes_despachados" href="#tabla_sitt" aria-expanded="false" aria-controls="tabla_sitt">
                  <h5 class="panel-title">
                    SITT
                  </h5>
                </a>
              </div>
              <div id="tabla_sitt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="viajes_sitt">
                <div class="panel-body">
                  <table id="desp_sitt" class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th>Patente</th>
                        <th class="th-despacho">Despachado</th>
                        <th>Origen</th>
                        <th>Destino</th>
                        <th>Estado</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="viajes_cami">
                <a data-toggle="collapse" data-parent="#viajes_despachados" href="#tabla_cami" aria-expanded="false" aria-controls="tabla_cami">
                  <h5 class="panel-title">
                    CAMI
                  </h5>
                </a>
              </div>
              <div id="tabla_cami" class="panel-collapse collapse" role="tabpanel" aria-labelledby="viajes_cami">
                <div class="panel-body">
                  <table id="desp_cami" class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th>Patente</th>
                        <th class="th-despacho">Despachado</th>
                        <th>Origen</th>
                        <th>Destino</th>
                        <th>Estado</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

<div id="main-slider">
    <div id="main" class="slide slide-1 ">
		<div id="map-canvas">&nbsp;</div>
 	</div>
    <div id="grafico" class="slide slide-2 text-left">
        <div id="ln1" class="row">
            <div id="calendar" class="col-sm-3">
                <div class="well">
					<form id="form_chart" action="./" method="post">
                    <p class="title"><span class="label label-primary">Seleccione Fecha/Opcion</span>
                      <select id="opt_report" name="opt_report">
                        <option value="1" selected>Diaria</option>
                        <option value="2">Semanal</option>
                        <option value="3">Mensual</option>
                      </select>
                    </p>
                    <div class="calendario">
                        <input type="hidden" name="date" id="date">
                        <div id="datepicker_report"></div>
                    </div>
					</form>
                </div>
            </div>
            <div id="resumen_grv" class="col-sm-4">
                <div class="well">
                    <p class="title"><span class="label label-primary">CANTIDAD / GRAVEDAD DE EXCESO DE VELOCIDAD</span></p>
                    <div class="chart-pie-container">
                        <canvas id="chart-legend-top"></canvas>
                    </div>
                    <div class="group"></div>                     
                </div>
            </div>
            <div id="exceso_emp" class="col-sm-5">
                <div class="well">
                    <p class="title"><span class="label label-primary">EXCESO POR EMPRESA</span></p>
                     <div class="chart-pie-container">
                        <canvas id="chart-legend-right" height="140vw"></canvas>
                    </div>
                    <div class="group"></div>
                </div>
            </div>
        </div>
        <div id="ln2" class="row">
			<div class="col-sm-6">
                <div class="well">
                    <p class="title"><span class="label label-primary">DETALLE FALTAS POR EMPRESA</span></p>
                    <div class="chart-bar-container">
                        <canvas id="chart-legend-bottom" height="90vw"></canvas>
                    </div>
                    <div id="table_detail_emp" class="table-responsive">   	
                    </div>            
                </div>
            </div>         
            <div id="div" class="col-sm-3">
                <div class="well">
                    <p class="title"><span class="label label-primary">CANTIDAD / GRAVEDAD - DIVISION</span></p>
                    <div class="chart-pie-container">
                        <canvas id="chart-legend-div" height="210vw"></canvas>
                    </div>
                    <div class="group"></div>                     
                </div>
            </div>
             <div id="div_grv" class="col-sm-3">
                <div class="well">
                    <p class="title"><span class="label label-primary">CANTIDAD / GRAVEDAD - DETALLE DIVISION</span></p>
                    <div class="chart-pie-container">
                        <canvas id="chart-legend-div-grv" height="200vw"></canvas>
                    </div>
                    <div class="group"></div>                     
                </div>
            </div>          
        </div>
        <div id="ln3" class="row">   
            <div class="col-sm-12">
                <div class="well">
                    <p class="title"><span class="label label-primary">DETALLE FALTAS POR PATENTE - TOP 30</span></p>
                    <div class="chart-bar-container">
                        <canvas id="chart-legend-left" height="200vw"></canvas>
                    </div>
                    <div id="table_detail_pat" class="table-responsive">   	
                    </div>
                </div>
            </div>
        </div>
   	</div>
     	
  	<div id="panel_admin" class="slide slide-3">
	  	<div class="container content-admin">
			  <header>
          <h2>ADMINISTRACION</h2>
        </header>
        <section>
          <div class="row">
            <!-- menu admin -->
            <div class="col-md-3">
              <ul>
                <li>Analizador</li>
                <ul>
                  <li><a class="link-tabla" data-tabla="sitt">Viajes SITT</a></li>
                  <li><a class="link-tabla" data-tabla="cami_1">Viajes CAMI Cargas</a></li>
                  <li><a class="link-tabla" data-tabla="cami_2">Viajes CAMI Descargas</a></li>
                </ul>
              </ul>
            </div>
            <div class="col-md-9">
              <div class="table-cont">
                <table id="table_analyzer" class="table table-striped table-hover">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Id Viaje</th>
                      <th>Vehiculo</th>
                      <th>Estado</th>
                      <th>Origen</th>
                      <th>Destino</th>
                      <th>Fecha Insercion</th>
                      <th>Fecha Salida</th>
                      <th>Registro Origen</th>
                      <th>Registro Destino</th>
                      <th>Observacion</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                </table>
              </div>
              <div class="">
                <button class="btn btn-primary" id="btn_analyze">Analizar</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    
</div>     

    </div>
  </div>
</section>
<footer class="container-fluid text-center">
  <p>SERCOING LTDA. - SMVS © 2020</p>
</footer>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPSWiYwgsb-AHamk_0G6oAcTANacBVQzc&callback=myMap"></script>

<script src="./js/tables.js"></script>
<script src="./js/table_analyzer.js"></script>
</body>
</html>
