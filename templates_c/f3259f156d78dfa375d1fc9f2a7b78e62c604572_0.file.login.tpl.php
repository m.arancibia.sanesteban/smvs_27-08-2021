<?php
/* Smarty version 3.1.33, created on 2019-03-28 12:44:29
  from 'C:\xampp\htdocs\prueba\smvs\pagina\templates\login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c9cebdd6a8ad5_62167319',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3259f156d78dfa375d1fc9f2a7b78e62c604572' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prueba\\smvs\\pagina\\templates\\login.tpl',
      1 => 1553787868,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c9cebdd6a8ad5_62167319 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>SMVS | Codelco VP - DCH</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content="" name="description" />
<meta content="" name="author" />
<link rel="shortcut icon" href="favicon.ico">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<?php echo '<script'; ?>
 src="//code.jquery.com/jquery-1.12.4.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"><?php echo '</script'; ?>
>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
<?php echo '<script'; ?>
 src="//code.jquery.com/ui/1.12.1/jquery-ui.js"><?php echo '</script'; ?>
>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="css/login/style.css" rel="stylesheet" />
<link href="css/login/blue.css" rel="stylesheet" />
<link rel="stylesheet" href="css/font-awesome.min.css">
<style>
.error-message {
	font-size: 15px;
	color: #333;
}
</style>
</head>
<body class="pace-top bg-white">
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<div id="page-container" class="fade">
  <div class="login login-with-news-feed">
    <div class="news-feed">
      <div class="news-image"> <img src="images/login/background.jpg" data-id="login-cover-image" alt="" /> </div>
      <div class="news-caption">
        <h4 class="caption-title"><i class="fa fa-map-o text-theme"></i> SMVS - SERCOING LTDA</h4>
        <p> Servicio Control GPS | Codelco VP - DCH </p>
      </div>
    </div>
    <div class="right-content">
      <div class="login-header" style="padding: 40px 60px 0">
        <div class="brand text-center"> <img src="images/login/logoSercoing.png" style="height: 125px;" /> </div>
      </div>
      <div class="login-content">
        <form id="contact-form" action="gps/login.php" method="post" novalidate class="margin-bottom-0">
          <div class="form-group m-b-15">
            <input type="text" class="form-control input-lg requiredField" style="font-size:20px;text-transform:uppercase" placeholder="RUT" maxlength="12" name="username" id="username" class="requiredField" data-error-empty="INGRESE RUT" />
          </div>
          <div class="form-group m-b-15">
            <input type="password" class="form-control input-lg requiredField" style="font-size:20px;text-transform:uppercase" placeholder="Contraseña" maxlength="12" name="password" id="password" class="requiredField" data-error-empty="INGRESE CONTRASEÑA"  />
          </div>
          <div class="row m-b-15 text-center">
            <button type="submit" class="btn btn-primary btn-lg" data-error-message="INGRESAR" data-sending-message="ESPERE..." data-ok-message="BIENVENIDO"><i class="fa fa-sign-in"></i> INGRESAR</button>
          </div>
          <br />
          <div id="form-messages" class="error-mensaje alert alert-danger hidden "></div>
          <input type="hidden" name="submitted" id="submitted" value="true" />
          
          <!-- <div class="row">
                            <div class="col-xs-12">
                                <div class="alert alert-warning">
                                    <i class="fa fa-warning"></i>&nbsp;@ViewBag.Error
                                </div>
                            </div>
                        </div> -->
          
          <hr />
          <p class="text-center text-inverse"> &copy; Copyright SERCOING LTDA <br />
          </p>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo '<script'; ?>
>
$(document).ready(function() {
	//SACAR
	$("#username").val('16305996-7');
	$("#password").val('prueba123');
	//SACAR

	$.when($('#page-loader').addClass('hide')).done(function() {
		$('#page-container').addClass('in');
	});
			
	$('#contact-form').submit(function () {
		var buttonCopy = $('#contact-form button').html(),
		errorMessage = $('#contact-form button').data('error-message'),
        sendingMessage = $('#contact-form button').data('sending-message'),
		okMessage = $('#contact-form button').data('ok-message'),
		hasError = false;
						
		$('#contact-form .error-message').remove();

		$('.requiredField').each(function () {
           	if ($.trim($(this).val()) == '') {
               	var errorText = $(this).data('error-empty');
               	$('#form-messages').append('<p class="error-message" style="display:none;margin:0"><i class="fa fa-times"></i>&nbsp;' + errorText + '</p>').find('.error-message').fadeIn('fast');
               	$(this).addClass('inputError');
               	hasError = true;
           	} 
		});
        if (hasError) {
			$('#contact-form button').html('<i class="fa fa-times"></i>&nbsp;' + errorMessage);
			$('#contact-form button').addClass('btn-danger');
			$("#form-messages").removeClass("hidden");
            setTimeout( function(){
                $('#contact-form button').html(buttonCopy).removeClass('btn-danger');
            }, 2000);
		}else{
            $('#contact-form button').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;' + sendingMessage);
            var formInput = $(this).serialize();
            $.post( $(this).attr('action'), formInput, function (data) {
     			if(data==-1)
				{
					$('#contact-form button').html('<i class="fa fa-times"></i>ERROR');
					$("#form-messages").removeClass("hidden").html("<p class='error-message'>RUT O CONTRASEÑA INCORRECTOS</p>");
					setTimeout(function () {
						$('#contact-form button').html(buttonCopy); 
						$("#username").val("");
						$("#password").val("");                    
					}, 2000);
				}
				if(data==1){								
					$(location).attr('href','./');								
				}
            });
        }
		return false;
	});
});
<?php echo '</script'; ?>
>
</body>
</html><?php }
}
