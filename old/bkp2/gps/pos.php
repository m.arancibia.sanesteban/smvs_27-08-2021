<?php

$jsondata = array();
	
if( count($_POST) > 1  ) // || @$pat_sel != "undefined"
{
	include("../include/db.php");

	$pat_sel = $_POST['pat_sel'];
	unset($_POST['pat_sel']);
		
	if(!empty($pat_sel) && $pat_sel != "undefined" ){
		$patentes = $pat_sel;
	}else{
		foreach($_POST as $key => $valor){
				$temp[] = "$key";
		};
		$patentes = implode(',',$temp);
	}
	
	//$patentes = "396,450,89,565,2,4,6,7,89,86,34,678,99,75,1700615";
	//$patentes = "1700791";	
		
	$verde = '#00FF00';
	$amarilla = '#FFFF00';
	$rojo = '#FF0000';
	$azul = '#0000FF';
	$negro = '#000';

	$sql = "SELECT 
			VE.vehi_id,
			RE.regi_latitud AS lat,
			RE.regi_longitud AS lng,
			DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
			RE.regi_azimut AS azimut,
			RE.regi_velocidad AS velocidad,
			(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_recibido AS SIGNED)) AS dif_reg,
			(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_posicion AS SIGNED)) AS dif_time,
			VE.vehi_patente,
			GE.geoc_nombre
			FROM
				(SELECT 
				regi_vehi_id,
				MAX(regi_id) AS id_max
				FROM `registro` AS RE
				WHERE regi_vehi_id IN($patentes) 
				GROUP BY regi_vehi_id) AS TR
			INNER JOIN `registro` AS RE 
			ON TR.regi_vehi_id = RE.regi_vehi_id AND ( TR.id_max = RE.regi_id OR RE.regi_fecha_recibido > (UNIX_TIMESTAMP()-(60*23)) )
			INNER JOIN `vehiculo` AS VE ON TR.regi_vehi_id = VE.vehi_id
			LEFT JOIN `geocerca` AS GE ON GE.geoc_tipo IN(1,2,4,5,6) AND GE.geoc_visible = 1
			AND	ST_CONTAINS(
			GE.geoc_poligono, PointFromText( CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')' ) )) 
			ORDER BY TR.regi_vehi_id, RE.regi_fecha_posicion DESC, RE.regi_fecha_recibido ASC, GE.geoc_tipo DESC";
			
	$sql = "SELECT 
			VE.vehi_id,
			RE.regi_latitud AS lat,
			RE.regi_longitud AS lng,
			DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
			RE.regi_azimut AS azimut,
			RE.regi_velocidad AS velocidad,
			(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_recibido AS SIGNED)) AS dif_reg,
			(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_posicion AS SIGNED)) AS dif_time,
			VE.vehi_patente,
			GE.geoc_nombre,
			GE.geoc_tipo
			FROM
				(SELECT 
				regi_vehi_id,
				MAX(regi_id) AS id_max
				FROM `registro` AS RE
				WHERE regi_vehi_id IN($patentes) 
				GROUP BY regi_vehi_id) AS TR
			INNER JOIN `registro` AS RE 
			ON TR.regi_vehi_id = RE.regi_vehi_id AND ( TR.id_max = RE.regi_id OR RE.regi_fecha_posicion > (UNIX_TIMESTAMP()-(60*23)) )
			INNER JOIN `vehiculo` AS VE ON TR.regi_vehi_id = VE.vehi_id
				LEFT JOIN `gc_id` AS GI ON RE.regi_id = GI.gc_regi_id
				LEFT JOIN `geocerca` AS GE ON GE.geoc_id = GI.gc_geoc_id
					AND GE.geoc_visible = 1
			ORDER BY TR.regi_vehi_id, RE.regi_fecha_posicion DESC, RE.regi_fecha_recibido ASC, GE.geoc_tipo DESC";

	
	$set = "SELECT GROUP_CONCAT(products SEPARATOR ',') as id FROM
			(	SELECT
				MAX(regi_id) AS products
				FROM `registro`
				WHERE regi_vehi_id IN($patentes) 
				GROUP BY regi_vehi_id
			) AS RE;";
	
	$ids = ($resultado=$mysqli->query($set)) ? $resultado->fetch_assoc() : 0;

	$sql = "SELECT 
				VE.vehi_id,
				VE.vehi_tive_id,
				RE.regi_latitud AS lat,
				RE.regi_longitud AS lng,
				DATE_FORMAT(from_unixtime(RE.regi_fecha_posicion), '%d-%m-%Y %H:%i:%s') AS fecha,
				RE.regi_azimut AS azimut,
				RE.regi_velocidad AS velocidad,
				(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_recibido AS SIGNED)) AS dif_reg,
				(UNIX_TIMESTAMP() - CAST(RE.regi_fecha_posicion AS SIGNED)) AS dif_time,
				VE.vehi_patente,
				IF( GE.geoc_tipo=9, GE.geoc_codigo, GE.geoc_nombre) AS geoc_nombre,
				GE.geoc_tipo
				FROM `registro` AS RE 
				INNER JOIN `vehiculo` AS VE ON RE.regi_vehi_id = VE.vehi_id
				AND RE.regi_vehi_id IN($patentes)
				AND ( RE.regi_id IN ($ids[id]) OR RE.regi_fecha_posicion > (UNIX_TIMESTAMP()-(60*23)) )
					LEFT JOIN `gc_id` AS GI ON RE.regi_id = GI.gc_regi_id
					LEFT JOIN `geocerca` AS GE ON GE.geoc_id = GI.gc_geoc_id
						AND GE.geoc_visible = 1
				ORDER BY RE.regi_vehi_id, RE.regi_fecha_posicion DESC, RE.regi_fecha_recibido ASC, GE.geoc_tipo DESC;";
	
	$points_pat = array();
	$mysqli->query($set);

	if ($resultado = $mysqli->query($sql)) {

		while ($fila = $resultado->fetch_assoc()) {
			if( @!$points_pat[$fila["vehi_id"]]["st_move"] )
			{
				if(empty($points_pat[$fila["vehi_id"]])){
					$points_pat[$fila["vehi_id"]] = $fila;
					$points_pat[$fila["vehi_id"]]["st_move"] = ($fila["velocidad"] > 0);
				}else{
					$points_pat[$fila["vehi_id"]]["dif_time"] = $fila["dif_time"];
					//$points_pat[$fila["vehi_id"]]["date"] = $fila["fecha"];
					$points_pat[$fila["vehi_id"]]["st_move"] = ($fila["velocidad"] > 0);
				}
			}
		}
		$resultado->free();
	}	
	$mysqli->close();

	$jsondata = array();
	$alert_geoc = "";
	if ( count($points_pat)>0) {
		foreach($points_pat as $valor) {
			$animation = ($pat_sel == $valor["vehi_id"]) ? 'BOUNCE' : 'DROP';
			$dif_time = $valor["dif_time"];
			$dif_reg = $valor["dif_reg"];
			$SymbolPath = false;
			$ln_color = "#33f";
							
			switch (true) {
				case $dif_reg > 86400:
					$color = $negro;
					$SymbolPath = true;
					$ln_color = "#ff8000";
					break;	
				case $dif_reg > 900:
					$color = $azul;
					break;
				case $dif_time < 300:
					$color = $verde;
					break;
				case $dif_time < 1200:
					$color = $amarilla;
					break;
				default:
					$color = $rojo;
					break;
			}			

			if(!empty($valor["geoc_nombre"]) and $valor["vehi_tive_id"] == 1  and ($valor["geoc_tipo"] == 1 || $valor["geoc_tipo"] == 2) )
			{
				$alert_geoc .= "<li>" . $valor["vehi_patente"] . ": " . htmlentities($valor["geoc_nombre"]) . "</li>";
				//$ln_color = "#03fbe8";
				$ln_color = "#f00";
			}			
						
			$pos[$valor["vehi_id"]] = array('lng'=>$valor["lng"], 
										'lat'=>$valor["lat"], 
										'animation'=>$animation, 
										'patente'=>$valor["vehi_patente"],
										'azimut'=>$valor["azimut"],
										'fecha'=>$valor["fecha"],
										'velocidad'=>$valor["velocidad"],
										'geoc_tipo'=>($valor["geoc_tipo"]==9)? 'Ruta' : 'Geocerca',
										'ln_color'=>$ln_color,
										'symbolpath' => $SymbolPath,
										'info'=>htmlentities($valor["geoc_nombre"]),
										'color'=>$color);
		}

		$jsondata["alert_geoc"] = $alert_geoc;
		$jsondata['markers'] = $pos;
	}else{
		$jsondata["err"] = "No se encontraron datos de unidad seleccionada";
		
	}
	$jsondata["post"] = serialize($_POST);
	//$jsondata["post"] = $sql;
}

	$jsondata["post"] = serialize($_POST);
	header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);

	
?>
