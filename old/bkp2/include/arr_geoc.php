<?php
//	include("../include/db.php");
	require("../include/in_geoc.php");
	$sql = "SELECT
			geoc_codigo,
			AsText(geoc_poligono) AS poligono
			FROM `geocerca`";
    $converted_points = array();
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_object()) {
			$polygons = explode("((", $fila->poligono);
			$number = count($polygons)-1;
			for ( $n = 1; $n < count($polygons); $n++ )
			{
				$polygon = str_replace("))","",$polygons[1]);
				$points_str = explode(",",$polygon);
				foreach ($points_str as &$valor) {	
					$points = explode(" ",$valor);
					$lon  = $points[0];
					$lat  = $points[1];   
					$converted_points[$fila->geoc_codigo][] = "$lat,$lon";				
				}
			}			
			$pointLocation->pointInPolygon($point, $converted_points[$fila->geoc_codigo]);	
		}
		$resultado->close();
	}
	$mysqli->close();
	print_r($converted_points);
?>