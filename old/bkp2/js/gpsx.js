	var map, mkcluster, markers = [], geoc = [], routes = [], infoWindow = [], recorrido = [], zoom, pat_rec;
	
	updatemyGps();
	function updatemyGps(time) {
		window.setTimeout(function(){updatemyGps();},20000);
		console.log("update GPS");
		//$("input#pat_sel[type=hidden]").attr("value",undefined);
		myGps();
	}
	
	function myMap() {
		var mapCanvas =  $("#map-canvas")[0];
		var myCenter = new google.maps.LatLng(-23.199948, -69.647242);		
		var mapOptions = {
			center: myCenter,
			zoom: 8,
		};
		map = new google.maps.Map(mapCanvas, mapOptions);
		mkcluster = new MarkerClusterer(map, null, {imagePath: './images/m'});		
	}

	function route(idrt) {
		var url = "gps/route.php";
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: "&idrt=" + idrt,//$("form#routes").serialize(),
			success: function(data)
			{
				var points = data.route;
				console.log(points);
				//alert(data.post);
				reset_max_min();
				routes[idrt] = [];
				points.forEach(function(elemento) {
					//console.log(elemento.points);
					poli = parsePolyStrings(elemento.points, false, true);
					var contentString = '<h5><b>DATOS RUTA</b></h5>' +
						'<b>RUTA: </b>	' + elemento.geoc_codigo + '<br>'+
						'<b>NOMBRE: </b>	' + elemento.nombre + '<br>'+
						'<b>VELOCIDAD MAX: </b>	' + elemento.max + '<br>';
					routes[idrt].push(lineString(poli.arr, elemento.color, contentString, 300, true) );
				});
				zoom = poli.zoom;
				map.setCenter(poli.pos); 
				map.setZoom(zoom);			
			}
		});
	}
	function dialog_msj(msj) {
		$("div#dialog p").html(msj);		
		$( "#dialog" ).dialog({
			autoOpen: true,
		   	buttons: {
				Aceptar: function() {$( this ).dialog( "close" );}
			}}
		);
	}
	function myRec(id_pat) {
		var url = "gps/traceupd2.php";
		dialog.dialog("open");
		$.ajax({
			type: "POST",	
			dataType: "JSON",
			url: url,
			data: $("form#recorrido").serialize(),
			error: function(jqXHR, textStatus, errorThrown) {
				closeDownload();
				$("div#dialog  p").html("<h3>Error</h3> Sin conexión con el servidor, contactar con Webmáster");
				$( "#dialog" ).dialog({
					autoOpen: true,
				   	buttons: {
						Aceptar: function() {$( this ).dialog( "close" );}
					}}
				);
    		},
			success: function(data)
			{
				//alert(data.msj_err);
				closeDownload();
				if(data.msj_err != null ){
					$("div#dialog  p").text(data.msj_err);
					$( "#dialog" ).dialog({
						autoOpen: true,
					   	buttons: {
							Aceptar: function() {$( this ).dialog( "close" );}
						}}
					);
					return;
				}
				pat_rec = data.pat;
				
				popup.html(data.popup);
				popup.dialog('open');
				
				recorrido.forEach(function(elemento,i) {
					elemento["line"].setMap(null);
				});
				recorrido.splice(0,recorrido.length);

				points = data.pos;
				var poli;
				reset_max_min();

				//console.log(points);
				points.forEach(function(elemento,i) {
					
					var array_points = elemento.points;
					var string_points = "((" + array_points.join(", ") + "))";
					poli = parsePolyStrings(string_points, false, true);
					
					var contentString = '<h5><b>DATOS RECORRIDO</b></h5>' +
					'<b>Fecha ingreso: </b>	' + elemento.date_in + '<br>'+
					'<b>Fecha salida: </b>	' + elemento.date_out + '<br>'+
					'<b>Tiempo Min: </b>	' + Math.round(elemento.time_rec/60)+ '<br>'+
					'<b>Distancia rec: </b>	' +parseFloat(elemento.dist_rec).toFixed(2) + '<br>';

					//recorrido.push(lineString(poli.arr, elemento.color, contentString, 900 )); //'#00FF00'
					var temp_rec = [];
					temp_rec["line"] = lineString(poli.arr, elemento.color, contentString, 900 );
					temp_rec["zoom"] = poli.t_zoom;
					temp_rec["pos"] = poli.t_pos;
					recorrido[i] = temp_rec;
				});
				zoom = poli.zoom;
				map.setCenter(poli.pos); 
				map.setZoom(zoom);
			}
		});
	}

	function lineString(poly, color, contentString, zIndex, symbol) {
		if(!symbol){
			var lineSymbol = {
				path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
			};	
		}else{
			var lineSymbol = "";
		}
		var lineOptions = {
		  	strokeOpacity: 0.9,   
			strokeWeight: 3.5,
			geodesic: true,
			zIndex: zIndex,
			strokeColor: color,  
			icons: [{
				icon: lineSymbol,
				offset: '0%',
				repeat: '120px'
			}]   
		}
	
		var rec = new google.maps.Polyline(lineOptions);
		rec.setMap(map); 
		rec.setPath(poly);
		//animateCircle(rec);
		createInfoWindow(rec, contentString);		
		return rec;
	}
	
	function myGeocerca() {
		var url = "gps/geo.php";
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: $("form#geocercas").serialize(),
			success: function(data)
			{
				//alert(data.post);
				$("form#geocercas ul#something input[type*='checkbox']").prop('checked', false);
				var color = data.color;
				var poli = parsePolyStrings(data.poligono, true, false);
				zoom = poli.zoom;				
				geoc[data.id] = [];
				geoc[data.id]["center"] = poli.pos;
				map.setCenter(poli.pos); 
				map.setZoom(zoom);
				
				geoc[data.id]["polygon"] = new google.maps.Polygon({
				  paths: poli.arr,
				  map: map,
				  strokeColor: '#00FF00',
				  strokeOpacity: 1,
				  strokeWeight: 3,
				  fillColor: color,
				  fillOpacity: 0.3
				});
				
				var contentString = '<h5><b>GEOCERCA DATOS</b></h5>' +
					'<b>AREA: </b>	' + data.nombre + '<br>'+
					'<b>GC_ID: </b>	' + data.codigo + '<br>'+
					'<b>LATITUD: </b>	' + poli.pos.lat() + '<br>'+
					'<b>LONGITUD: </b>	' + poli.pos.lng() + '<br>';					
				//infoWindowG = new google.maps.InfoWindow();
				createInfoWindow( geoc[data.id]["polygon"], contentString);
				/*
				(function(polygon,contentString){
					google.maps.event.addListener( polygon, 'click', function(event) {
						infoWindowG.setContent(contentString);
						infoWindowG.setPosition(event.latLng);
						infoWindowG.open(map, polygon);
					});			
				})(geoc[data.id]["polygon"], contentString );*/
			}
		});
	}

	function myGps(pat_sel) {
		
		var url = "gps/pos.php";
		//if(pat_sel == undefined) alert("data.post");
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: $("form#patent").serialize() + "&pat_sel=" +  pat_sel,
			success: function(data)
			{
				//alert(data.post);
				//mkcluster.clearMarkers();	
				
				if( data.alert_geoc != "" &&  data.alert_geoc != null &&  data.alert_geoc != undefined && data.alert_geoc != $("div ul#alert_geoc1").html() )
				{
					$("div ul#alert_geoc1").html(data.alert_geoc);
					$("div ul#alert_geoc2").html($("div ul#alert_geoc2").html() + $("div ul#alert_geoc1").html());
					$("div#dialog  p").text("Se han cargado alertas");
					$( "#dialog" ).dialog();
				}
				
				$.each(data.markers, function(i, marker) 
				{
					if( $("form#patent ul li div input#" + i + "[type*='checkbox']:checked").length ==  1 )
					{
						var myposition = new google.maps.LatLng(marker.lat, marker.lng);
						if(marker.animation == 'BOUNCE'){
							animation = google.maps.Animation.BOUNCE;
							map.panTo(myposition);
							console.log("center GPS");
						}else{
							animation = null;		
						}
						var gcString = (marker.info != null)? "<br/><strong>Geocerca: </strong> " + marker.info : '';
						contentString = "<div><strong>Patente " + marker.patente + "</strong><br/><b>" + marker.velocidad 
									+ " km/h</b>" 
									+ "<br/><strong>Fecha:</strong> " + marker.fecha 
									+ gcString
									+ "</div>";
					  	var azimut = marker.azimut * 1;
					  	var color = marker.color;
						var ln_color = marker.ln_color;	
						//var polygonsymbol = 'M -2,1 -1,2 1,2 2,1 2,-1 1,-2 -1,-2 -2,-1 z'; //M -2,0 0,-2 2,0 0,2 z	
					var SymbolPath = (marker.symbolpath == true)? google.maps.SymbolPath.FORWARD_OPEN_ARROW : google.maps.SymbolPath.FORWARD_CLOSED_ARROW;	
						var icon = {
									path: SymbolPath,
									fillColor: color,
									fillOpacity: 100,
									strokeColor: ln_color,
									strokeWeight: 1.7,
									scale: 4,
									rotation: azimut
						};
						if (markers[i] != null) {
							markers[i].setPosition(myposition);
							markers[i].setIcon(icon);
							markers[i].setAnimation(animation);							
							if( infoWindow[i] != undefined){
								infoWindow[i].setContent(contentString);					
							}
 						}else{
							markers[i] = new google.maps.Marker({
								position: myposition,
								icon:icon,
								title: 'Patente: ' + marker.patente,
								animation: animation
							});						
						 	infoWindow[i] = new google.maps.InfoWindow();
							createInfoWindow( markers[i], contentString, infoWindow[i]);
							
							if( $("div#opcion1 input#group[type=checkbox]").is(':checked') ){
								mkcluster.addMarker(markers[i]);
							}else{
								markers[i].setMap(map);
							}
						}

						//alert(markers[i].getMap());						
					}			
				});		
				
				//mkcluster.addMarkers(markers);
				//mkcluster.resetViewport();
				//var markerCluster = new MarkerClusterer(map, markers, {imagePath: './images/m'});
			}
		});
	}	
	
	function createInfoWindow(marker, content, infoWindow) {
		if(infoWindow == undefined){
			infoWindow = new google.maps.InfoWindow();		
		}
		infoWindow.setContent(content);
		google.maps.event.addListener(marker, 'click', function(event){
			infoWindow.setPosition(event.latLng);
			infoWindow.open(map, marker);
			//map.setCenter(event.latLng);
		});
	}
	
	function animateCirclex(line) {
    	var count = 0;
    	window.setInterval(function() {
			temp = ( zoom * ( zoom / 3) );
			//alert(zoom);
      		count = (count + 1) % ( 200 * temp );
			var icons = line.get('icons');

			icons[0].offset = (count / ( 2 * temp ) ) + '%';
			line.set('icons', icons);
		}, 20);
	}
	
	function animateCircle(line) {
		var count = 0;
		window.setInterval(function() {
		  count = (count + 1) % 200;
		  var icons = line.get('icons');  
		  icons[0].offset = (count / 2) + '%';
		  line.set('icons', icons);
	  }, 20);
	}
	
	function getRandomColor() {
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	  }
	  return color;
	}	