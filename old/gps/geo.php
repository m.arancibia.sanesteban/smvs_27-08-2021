<?php

if( count($_POST) > -1 ) {

	include("../include/db.php");
	
	foreach($_POST as $key => $valor){
		if($valor == "on"){
			$id = $key;
		}
	};
	
	$sql = "SELECT
			geoc_codigo,
			geoc_nombre,
			geoc_color,
			AsText(geoc_poligono) AS poligono
			FROM `geocerca`
			WHERE geoc_id = $id LIMIT 1";
			
	$jsondata = array();
	$jsondata["post"] = serialize($_POST);

	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_object()) {
			$jsondata['id'] = $id;
			$jsondata['color'] = $fila->geoc_color;
			$jsondata['poligono'] = $fila->poligono;
			$jsondata['codigo'] = utf8_encode($fila->geoc_codigo);
			$jsondata['nombre'] = utf8_encode($fila->geoc_nombre);
		}
		$resultado->close();
    }
	$mysqli->close();

	header('Content-type: application/json; charset=utf-8');
    echo json_encode($jsondata);
    exit();
}