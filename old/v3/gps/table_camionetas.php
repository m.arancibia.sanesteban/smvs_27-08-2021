﻿<html>
<head>
<style>

  thead {
    color:green;
  }
  tbody {
    color:black;
  }
  tfoot {
    color:red;
  }
  table{
    width: auto;
    margin: 0 auto; 
  }
  table, th, td {
      border: 1px solid black;
      font-family: Calibri,sans-serif; 
      font-size: .9em;
      border-spacing: 0;
  }
  th{
    background-color: #8D0404;
    color: white;
  }
 

</style>
<title>CAMIONETAS.DMH</title>
</head>

<body>

<table>
  <thead>
    <tr>
      <th>PPU</th>
      <th>CODGC</th>
      <th>IDGC</th>
      <th>NOMGC</th>
      <th>HORA INGRESO</th>
      <th>HORA SALIDA</th>
      <th>TIEMPO [MIN]</th>
      <th>DIST [KM]</th>
      <th>VEL PROM [KM/H]</th>
      <th>VEL MAX [KM/H]</th>
      <th>POINT</th>
      <th>HORA EXCESO</th>      
      <th>TIPO GC</th>      
    </tr>
  </thead>
  <tbody>
 <?php
 set_time_limit (3600);
 
 $patentes = array(
"JLWC19",
"JPZC88",
"HLJH46",
"HYWX21",
"JJGX43",
"FKPJ90",
"FYWZ21",
"GHYK99",
"JYRB95",
"DPWL50",
"FVYR28"
);  // modificar patentes


	include("divisiones.php");	
	
  $date_in = "2018-12-29"; // modificar fechas 
	$date_get = @$_GET["fecha"];
	$date = (!empty($date_get)) ? $date_get : $date_in; 

  //$record = record($patentes, $date, true); //divisional - REPORTE ESPECIAL
  $record = record($patentes, $date, false); //todas las GC - REPORTE NORMAL 
  if(is_array($record)){
  foreach($record  as $var){
    if($var["geoc_nombre"] != "SxOP"){

?>
    <tr>
      <td><?php echo $var["patente"]; ?></td>
      <td><?php echo $var["geoc_id"]; ?></td>
      <td><?php echo $var["geoc_codigo"]; ?></td>
      <td><?php echo utf8_encode($var["geoc_nombre"]); ?></td>
      <td><?php echo $var["date_in"]; ?></td>
      <td><?php echo $var["date_out"]; ?></td>
      <td><?php echo $var["date_dif"]; ?></td>
      <td><?php echo str_replace(".",",",$var["dist_rec"]); ?></td>
      <td><?php echo str_replace(".",",",$var["promediovel"]); ?></td>
      <td><?php echo $var["max"]; ?></td>
      <td><?php echo $var["point"]; ?></td>
      <td><?php echo $var["hora"]; ?></td> 
      <td><?php echo $var["geoc_tipo"]; ?></td>       
  </tr>
<?php
    }
  }
  }
  /*
    }
  }*/
?> 
  </tbody>
</table>
</body>
</html>