<?php 
	include("../include/db.php");
	include("../include/pointinline.php");
	include("distance.php");

	//$patentes = "'CXSC74','DPLL84','DPRS16','DRHZ69'";
		$patentes = '"BGJK71",
"FGPK13",
"GFKJ18",
"HCKL25",
"HXPX88",
"CVPC58",
"CWHF69",
"DGRF89",
"DPFL27",
"DWBZ35",
"FPVV41",
"HBDP72",
"HHDP92",
"HHDP95",
"JPZC69",
"JYTK90",
"KBXC54",
"FCCL30",
"HCRH92",
"HYHL28",
"JCPB75",
"DPLL85",
"HSDZ53",
"KBBF51"
';
	$patentes = "'HCKL25'";
	
	$date_str = "2018-10-12";
	$date_end = "2018-10-12";

	//round((GE.geoc_max_speed + 5 ) - RE.regi_velocidad) AS dif_vel,
	$sql ="SELECT
				RE.regi_id,
				VE.vehi_patente,
				from_unixtime(RE.regi_fecha_posicion) AS fecha,
				RE.regi_fecha_posicion,
				RE.regi_latitud,
				RE.regi_longitud,
				RE.regi_velocidad,
			GE.geoc_id,
			GE.geoc_nombre,
			GE.geoc_codigo,
			GE.geoc_tipo,
			GE.geoc_max_speed
				FROM `vehiculo` AS VE 
				INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id 
					AND VE.vehi_patente IN($patentes)
					AND RE.regi_fecha_posicion BETWEEN UNIX_TIMESTAMP('$date_str 00:00:00') AND UNIX_TIMESTAMP('$date_end 23:59:59')
				LEFT JOIN `geocerca` AS GE ON 
					GE.geoc_visible = 1
					AND
					(
						(ST_CONTAINS(
						geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
						) AND GE.geoc_tipo IN(1,2,3,4,5,6,7,9))
						OR
						(CONTAINS(
						geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
						) AND GE.geoc_tipo IN(8))
					)
					ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";

	$part1 = array();
	$range_max =  0.030;
	$pointLocation = new PointLocation();
	$temp_end_gc = NULL;
	$temp_prev = array();
	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc())
		{
			
			$regi_id = $fila['regi_id'];

			if( $fila['geoc_tipo'] == NULL and $temp_end_gc != NULL )
			{
				$fila['geoc_id'] = $part1[$temp_end_gc][0]["geoc_id"];
				$fila['geoc_nombre'] = $part1[$temp_end_gc][0]["geoc_nombre"];
				$fila['geoc_tipo'] = $part1[$temp_end_gc][0]["geoc_tipo"];
				$fila['geoc_codigo'] = $part1[$temp_end_gc][0]["geoc_codigo"];
				$fila['geoc_max_speed'] = $part1[$temp_end_gc][0]["geoc_max_speed"];			
			}

			if( $fila['geoc_tipo'] == 8 )
			{	
				$temp_dist = $pointLocation->Glength_all_point( $fila['geoc_id'], $fila['regi_longitud'] .' '. $fila['regi_latitud'] );
						
				if($temp_dist > $range_max || $temp_dist === NULL ){
					$fila['geoc_id'] = NULL;
					$fila['geoc_nombre'] = NULL;
					$fila['geoc_tipo'] = NULL;
					$fila['geoc_codigo'] = NULL;
					$fila['geoc_max_speed'] = NULL;
				}else{
					$fila["dist"] = $temp_dist;
				}
					//$fila["dist"] = $temp_dist;
					
				if($fila['geoc_codigo'] != NULL )
				{
					$temp_end_gc = $regi_id;
				}elseif ( $temp_dist > 3 )
				{
					$temp_end_gc = NULL;
				}	
			}
						
			if( isset($part1[$regi_id][0])){
				$temp_row = $part1[$regi_id][0];
					
				switch ($temp_row["geoc_tipo"]) {
					case 1:
						if( $fila["geoc_tipo"] == 2  ){
							$part1[$regi_id][] = $fila;
						}
						break;
					case 3:
						if($fila["geoc_tipo"] == 8 and $temp_dist <= $range_max){
							$part1[$regi_id][0] = $fila;
						}
						break;
					case 8:
						if($fila["geoc_tipo"] == 8 and $temp_row["dist"] > $temp_dist)
						{
							$part1[$regi_id][0] = $fila;
						}
						break;
					case NULL:
							$part1[$regi_id][0] = $fila;
						break;
				}				
			}else
			{
				$part1[$regi_id][0] = $fila;			
			}
		}
	}
	//print_r($part1);

	$part2 = array();
	$id_part = 0;	
	$id_part_i = 0;
	$temp_geoc_tipo = 0;
	$temp_time_stop = 0;
	$temp_i_speeding = 0;
	$temp_time_speeding = 0;
	$speeding_id = 0;
	$temp_stop = array();
	
	foreach($part1 as $var){
		$num_var = count($var) - 1;
		$temp_var = $var[0];
		
		// START dist_points & time_dif
		$dist_points =
		(!empty($lat) and !empty($lon) and ( $lat != $temp_var["regi_latitud"] || $lon != $temp_var["regi_longitud"] ) ) ?
		$pointLocation->Glength($lat, $lon, $temp_var["regi_latitud"], $temp_var["regi_longitud"]) : 0;

		$time_dif = (!empty($regi_fecha_posicion)) ? $temp_var["regi_fecha_posicion"] - $regi_fecha_posicion : 0; 
		$speed_dif = (!empty($temp_var["geoc_max_speed"])) ? $temp_var["regi_velocidad"] - ($temp_var["geoc_max_speed"] + 5) : 0; 
		
		$vehi_patente = $temp_var['vehi_patente'];
		$regi_fecha_posicion = $temp_var["regi_fecha_posicion"];
		$lat = $temp_var["regi_latitud"];
		$lon = $temp_var["regi_longitud"];
		$regi_velocidad = $temp_var["regi_velocidad"];
		// END dist_points & time_dif
		
		if($temp_var['geoc_tipo'] == 1)
		{
			$bool1 = (@$part2[$vehi_patente][$id_part]["geoc_tipo"] != 1);
			if( !isset($part2[$vehi_patente][$id_part]) || $bool1)
			{
				$id_part = $id_part_i++;
				$part2[$vehi_patente][$id_part] = PartArray($temp_var);
				$part2[$vehi_patente][$id_part]["points"] = array();
				$temp_stop = array();
				$temp_speeding = array();
			}
			
			$temp_var = $var[$num_var];
			$temp_var["dist_points"]= $dist_points;
			$temp_var["time_dif"]= $time_dif;
			$temp_var["move"] = true; 
			if($speed_dif>0)
			{
				$temp_var["speed_dif"] = $speed_dif; 
			}
			//$temp_var["kumpa"] = "hola kpa $speed_dif  $prueba " . $temp_var["regi_velocidad"] . " - " .$temp_var["geoc_max_speed"];
			unset($temp_var['vehi_patente']);
			$num_var_points = (count($part2[$vehi_patente][$id_part]["points"])-1);

			if( $num_var_points >= 0 ){
				
				if($num_var_points == 3){
					//print_r($part2);
					print_r($part2[$vehi_patente][$id_part]);
					die();	
				}
				
				if($temp_var["geoc_id"] != $part2[$vehi_patente][$id_part]["points"][$num_var_points]["geoc_id"] )
				{
					$part2[$vehi_patente][$id_part]["points"][] = PartArray($temp_var);			
				}
			}else{
				$part2[$vehi_patente][$id_part]["points"][] = PartArray($temp_var);				
			}
			
			$num_var_points = (count($part2[$vehi_patente][$id_part]["points"])-1);
			
			echo "$num_var_points\n";
			$part2[$vehi_patente][$id_part]["points"][$num_var_points]["points"][] = PartArray($temp_var, false);	

			// SPEEDING
			if($speed_dif <= 0 )
			{
				if( $temp_i_speeding > 2 & $temp_time_speeding>=  60 ){
					$speeding_id++;
					foreach($temp_speeding as $key => $var){
						foreach($var as $var2 ){
							$part2[$vehi_patente][$id_part]["points"][$key]["points"][$var2]["speeding"] = $speeding_id;
						}
					}					
				}
				$temp_i_speeding = 0;
				$temp_time_speeding = 0;
				$temp_speeding = array();
			}else{
				$temp_i_speeding++;
				$temp_time_speeding += $time_dif;
				$temp_speeding[$num_var_points][] =  count($part2[$vehi_patente][$id_part]["points"][$num_var_points]["points"])-1;
			}
			// END SPEEDING
			
			// MOVE & IN_STOP 
			if($regi_velocidad > 0 || $dist_points > 0.050)
			{
				if($temp_time_stop > 180){
					foreach($temp_stop as $key => $var){
						foreach($var as $var2 ){
							echo "$key $var2 \n";
							$part2[$vehi_patente][$id_part]["points"][$key]["points"][$var2]["move"] = false;
						}
					}
				}
				if($temp_time_stop > 0)
				{
					$temp_time_stop = 0;
					$temp_stop = array();
				}
			}else{
				$temp_time_stop += $time_dif;
				$temp_stop[$num_var_points][] = count($part2[$vehi_patente][$id_part]["points"][$num_var_points]["points"])-1;
			}
			// MOVE & IN_STOP

		}else
		{
			$temp_var["dist_points"]= $dist_points;
			$temp_var["time_dif"]= $time_dif;
			if($speed_dif>0)
			{
				$temp_var["speed_dif"] = $speed_dif;
			}

			$bool1 = (@$part2[$vehi_patente][$id_part]["geoc_tipo"] == 1);
			$bool2 = (@$part2[$vehi_patente][$id_part]["geoc_id"] != $temp_var["geoc_id"]);
					
			if( !isset($part2[$vehi_patente][$id_part]) || $bool1 || $bool2)
			{
				$id_part = $id_part_i++;
				$bool3 = in_array( $temp_var['geoc_tipo'], array(8,9,NULL));
				$part2[$vehi_patente][$id_part] = PartArray($temp_var);
				$part2[$vehi_patente][$id_part]["pos_type"] = $bool3 ? "route" : "geoc";
				$part2[$vehi_patente][$id_part]["points"] = array();
				$temp_stop = array();
				$temp_speeding = array();
			}
			$temp_var["move"] = true;
			$part2[$vehi_patente][$id_part]["points"][] = PartArray($temp_var, false);


			// SPEEDING
			if($speed_dif <= 0 )
			{
				if( $temp_i_speeding > 2 & $temp_time_speeding >=  60 ){
					$speeding_id++;
					foreach($temp_speeding as $key => $var){
						foreach($var as $var2 ){
							$part2[$vehi_patente][$key]["points"][$var2]["speeding"] = $speeding_id;
						}
					}					
				}
				$temp_i_speeding = 0;
				$temp_time_speeding = 0;
				$temp_speeding = array();
			}else{
				$temp_i_speeding++;
				$temp_time_speeding += $time_dif;
				$temp_speeding[$id_part][] = count($part2[$vehi_patente][$id_part]["points"])-1;
			}
			// END SPEEDING

			// MOVE & IN_STOP 
			if($regi_velocidad > 0 || $dist_points > 0.050)
			{
				if($temp_time_stop > 180){
					foreach($temp_stop as $key => $var){
						foreach($var as $var2 ){
							$part2[$vehi_patente][$key]["points"][$var2]["move"] = false;
						}
					}
				}
				if($temp_time_stop > 0)
				{
					$temp_time_stop = 0;
					$temp_stop = array();
				}
			}else{
				$temp_time_stop += $time_dif;
				$temp_stop[$id_part][] = count($part2[$vehi_patente][$id_part]["points"])-1;
			}
			// MOVE & IN_STOP 
		}
	}
	
	foreach($part2 as $patente => $var ){
		$table['patente'] = $patente;
		foreach($var as $var2 ){
		$head = PartArray($var2);
		$temp_row = array_merge($table, $head);
		$row[] = $temp_row;
		$i = count($row) -1;
		$insert = true;
			foreach($var2["points"] as $var3 ){
				if(isset($var3["points"])){
					//echo "entro\n";
					$head = PartArray($var3);
					$temp_row = array_merge($table, $head);
					$insert = true;	
									
					foreach($var3["points"] as $var4 ){
						if($var4["move"] != @$move || $insert){
							$move = $var4["move"];
							$row[] = $temp_row;
							$i = count($row)-1;
							$insert =  false;
							$row[$i]["dist_points"] = 0;
							$row[$i]["time_dif"] = 0;
						}else{
							$row[$i]["dist_points"] += $var4["dist_points"];
							$row[$i]["time_dif"] += $var4["time_dif"];	
						}
						$row[$i]["move"] = $var4["move"] ;
						if(!isset($row[$i]["hin"])){
							$row[$i]["hin"] = $var4["fecha"] ;
						}
						$row[$i]["hout"] = $var4["fecha"];
						//print_r($var4);
					}
				}else{
					
					if($var3["move"] != @$move || $insert  && FALSE ){
						$move = $var3["move"];
						$row[] = $temp_row;
						$i = count($row)-1;
						$insert =  false;
						$row[$i]["dist_points"] = 0;
						$row[$i]["time_dif"] = 0;
					}else if ( FALSE ){
						$row[$i]["dist_points"] += $var3["dist_points"];
						$row[$i]["time_dif"] += $var3["time_dif"];	
					}
					$row[$i]["move"] = $var3["move"] ;
					if(!isset($row[$i]["hin"]) && FALSE ){
						$row[$i]["hin"] = $var3["fecha"] ;
					}
					//$row[$i]["hout"] = $var3["fecha"];
													
					//print_r($var3);				
					//echo "entro2";
				}
				
			}
		}
	}
	//print_r($row);
	
	//echo count($part2);
	//print_r($part2);	
	
	
	function PartArray($array, $bool = true){		
		$head = array("geoc_id","geoc_codigo","geoc_nombre","geoc_tipo","geoc_max_speed","pos_type");
		$body = array("regi_id","fecha","regi_fecha_posicion","regi_latitud","regi_longitud","regi_velocidad","dist_points","time_dif","move","kumpa","speed_dif");
		$temp_array = array();
		$section = ($bool) ? $head : $body  ; 
		foreach($section as $name){
			if(isset($array[$name])){
				$temp_array[$name] = $array[$name];
			}
		}
		return $temp_array;
	}
	
	class PartArray {
		private $head;
		private $body;
			
		function __construct() {
			$this->head = array("geoc_id","geoc_codigo","geoc_nombre","geoc_tipo","geoc_max_speed");
			$this->body = array("regi_id","fecha","regi_fecha_posicion","regi_latitud","regi_longitud","regi_velocidad","dist_points","time_dif","move","speed_dif");
		}
		
		function write($array, $bool = true){
			$temp_array = array();
			$section = ($bool) ? $this->head : $this->body ; 
			foreach($section as $name){
				if(isset($array[$name])){
					$temp_array[$name] = $array[$name];
				}
			}
			return $temp_array;		
		}
	
		function read() {
		}
	}
	
?>

<!DOCTYPE html>
<html>
<head>
<style>
 thead {
    color:green;
  }
  tbody {
    color:black;
  }
  tfoot {
    color:red;
  }
  table{
    width: auto;
    margin: 0 auto; 
  }
  table, th, td {
      border: 1px solid black;
      font-family: Calibri,sans-serif; 
      font-size: .9em;
      border-spacing: 0;
  }
  th{
    background-color: #8D0404;
    color: white;
  }
</style>
</head>

<body>

<table>
  <thead>
    <tr>
      <th>patente</th>
      <th>id speeding</th>
      <th>geoc_id</th>
      <th>geoc_codigo</th>
      <th>geoc_nombre</th>
      <th>geoc_tipo</th>
      <th>gmax_speed</th>
      <th>fecha</th>
      <th>Lat</th>
      <th>Lon</th>
      <th>regi_velocidad</th>
      <th>speed_dif</th>
      <th>dist_points</th>
      <th>time_dif</th>      
      <th>move</th>      
    </tr>
  </thead>
  <tbody>
  
 <?php
 //print_r($part2);
 foreach($part2 as $patente => $var ){
		foreach($var as $var2 ){
			foreach($var2["points"] as $var3 ){
				if(isset($var3["points"])){

					foreach($var3["points"] as $var4 ){
						//if( @$var4["speeding"]>0)
						{
							echo "<tr>";
							echo "<td>".$patente."</td>";
							echo "<td>".@$var4["speeding"]."</td>";
							echo "<td>".@$var3["geoc_id"]."</td>";
							echo "<td>".@$var3["geoc_codigo"]."</td>";
							echo "<td>".@$var3["geoc_nombre"]."</td>";
							echo "<td>".@$var3["geoc_tipo"]."</td>";
							echo "<td>".@$var2["geoc_max_speed"]."</td>";
							echo "<td>".@$var4["fecha"]."</td>";
							echo "<td>".@$var4["regi_latitud"]."</td>";
							echo "<td>".@$var4["regi_longitud"]."</td>";
							echo "<td>".@$var4["regi_velocidad"]."</td>";
							echo "<td>".@$var4["speed_dif"]."</td>";
							echo "<td>".@$var4["dist_points"]."</td>";
							echo "<td>".@$var4["time_dif"]."</td>";
							echo "<td>".@$var4["move"]."</td>";			
							echo "</tr>";
							
						};
					}
				}else{

					//if( @$var3["speeding"]>0)
					{
						echo "<tr>";
						echo "<td>".$patente."</td>";
						echo "<td>".@$var3["speeding"]."</td>";
						echo "<td>".@$var2["geoc_id"]."</td>";
						echo "<td>".@$var2["geoc_codigo"]."</td>";
						echo "<td>".@$var2["geoc_nombre"]."</td>";
						echo "<td>".@$var2["geoc_tipo"]."</td>";
						echo "<td>".@$var2["geoc_max_speed"]."</td>";
						echo "<td>".@$var3["fecha"]."</td>";
						echo "<td>".@$var3["regi_latitud"]."</td>";
						echo "<td>".@$var3["regi_longitud"]."</td>";
						echo "<td>".@$var3["regi_velocidad"]."</td>";
						echo "<td>".@$var3["speed_dif"]."</td>";
						echo "<td>".@$var3["dist_points"]."</td>";
						echo "<td>".@$var3["time_dif"]."</td>";
						echo "<td>".@$var3["move"]."</td>";
						echo "</tr>";
					};


				}
				
			}
		}
	}
	
 	
 ?> 
  
 <?php

	foreach($part1 as $var){
		break;
		foreach($var as $var2){
?>
    <tr>
<?php	
			foreach($var2 as $var3){

?>
      <td><?php echo $var3 ?></td>

<?php
		}
?>
    </tr>
<?php	

		}
	}
	
	
			
		//################ MOVING 
		/*
		if( $regi_velocidad > 0 || @$current["dist_points"] > 0.050 )
		{
					
			$temp_i_move++;
			$temp_time_stop = 0;
			if( $temp_time_stop >= 10 ) {
				if($moving_id === NULL){
					$moving_id = $it->update_row($temp_i_move ,"move");	
				}
				$current["move"] = $moving_id;
			}else{
				$current["stop"] = $temp_st;				
			}
		}else if( !isset($moving_id) || !empty($moving_id) ) {
			if(@$moving_id > @	$temp_st || !isset($temp_st)){
				$temp_st = !isset($moving_id) ? 0: $moving_id;
			}
			$current["stop"] = $temp_st;
			$temp_time_stop += $current["time_dif"];
			
			$temp_i_move = 0;
			$moving_id = NULL;		
		}
	*/		
	
		//################ MOVING END	
		
		
?> 
  </tbody>
</table>
</body>
</html>
