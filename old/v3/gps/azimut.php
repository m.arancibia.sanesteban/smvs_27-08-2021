<?php
	include("../include/pointinline.php");
	
	function azumit($points){
		list($xa,$ya,$xb,$yb)= explode(" ",$points);
		$y = rad2deg(atan(($xb-$xa)/($yb-$ya)));
		
		switch (true) {
			case (($xb-$xa)>0 && ($yb-$ya)<0) || (($xb-$xa)<0 && ($yb-$ya)<0):
				return $y+180;
				break;
			case ($xb-$xa) < 0 && ($yb-$ya) > 0:
				return $y+360;
				break;
			default:
				return $y;
			break;
		}
	}
	/*
	$punto1 = "0,0";
	$punto2 = "2,1";

	$punto1 = "-68.89765,-22.44966";
	$punto2 = "-68.89946,-22.45901";
	*/
	
	$punto1 = "-68.9176 -22.48867";
	$azimut = 227;
	
	
	$sql ="SELECT
				RE.regi_longitud,
				RE.regi_latitud,
				RE.regi_azimut,
			GE.geoc_id
				FROM `vehiculo` AS VE 
				INNER JOIN `registro` AS RE ON VE.vehi_id = RE.regi_vehi_id 
					AND VE.vehi_patente IN('bgjk71')
					AND RE.regi_fecha_posicion BETWEEN UNIX_TIMESTAMP('2018-11-08 13:51:26') AND UNIX_TIMESTAMP('2018-11-08 14:01:26')
				LEFT JOIN `geocerca` AS GE ON 
					GE.geoc_visible = 1
					AND
					(
						(CONTAINS(
						geoc_poligono, PointFromText(CONCAT( 'Point(', RE.regi_longitud, ' ', RE.regi_latitud, ')'))
						) AND GE.geoc_tipo IN(8))
					)
					ORDER BY vehi_patente ASC, regi_fecha_posicion ASC, regi_id ASC, geoc_tipo ASC";

	$pointLocation = new PointLocation();

	if ($resultado = $mysqli->query($sql)) {
		while ($fila = $resultado->fetch_assoc())
		{
			if($fila["geoc_id"]>0)
			{
				$punto1 = $fila["regi_longitud"] . " " . $fila["regi_latitud"];	
				$punto2 = $pointLocation->Glength_all_point(  $fila["geoc_id"], $punto1, false );
				print_r($punto2);
				echo $punto1 . " < ";
				echo $fila["regi_azimut"] . " => ";
				echo azumit($punto2[0]." ".$punto2[2] ) . ", b ";
				echo azumit($punto2[1]." ".$punto2[2] ). "\n";
			}
		}
	}
	

	//$punto2 = $pointLocation->Glength_all_point( 392, $punto1, false );	
	
	//echo azumit("$punto1 $punto2") - $azimut;
	//echo "<br>$punto1 $punto2"

?>