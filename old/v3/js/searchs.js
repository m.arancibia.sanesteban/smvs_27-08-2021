	function searchg() {
		var busqueda = $('#search_g'),
		titulo = $('form#geocercas ul#something li div label');
		$(titulo).each(function(){
			var li = $(this);
			$(busqueda).keyup(function(){
				this.value = this.value.toLowerCase();
				$(li).parent().parent().hide();
				var txt = $(this).val();
				if($(li).text().toLowerCase().indexOf(txt) > -1 || $(li).attr("title").toLowerCase().indexOf(txt) > -1 || $(li).attr("id") == "title" ){					
					$(li).parent().parent().show();
				}
			});
		});
	}

	function searchp() {
		var busqueda = $('#search_p'),
		titulo = $('form#patent ul#something li div label');
		$(titulo).each(function(){
			var li = $(this);
			$(busqueda).keyup(function(){
				this.value = this.value.toLowerCase();
				$(li).parent().parent().hide();
				var txt = $(this).val();

				if($(li).text().toLowerCase().indexOf(txt) > -1){
					$(li).parent().parent().show();
				}
			});
		});
	}
	
	
	function searchp2() {
		var busqueda = $('#search_p'),
		titulo = $('form#patent div#something ul li label');
		$(titulo).each(function(){
			var li = $(this);
			//alert(li);
			$(busqueda).keyup(function(){
				this.value = this.value.toLowerCase();
				$(li).parent().hide();
				var txt = $(this).val();
				//alert($(li).text());
				if($(li).text().toLowerCase().indexOf(txt) > -1){
					$(li).parent().show();
				}
			});
		});
	}
	

	function searchtp() {
		var busqueda = $('#search_tp'),
		titulo = $('form#recorrido div select#sel_reg option');
		$(titulo).each(function(){
			var li = $(this);
			$(busqueda).keyup(function(){
				this.value = this.value.toLowerCase();
				var txt = $(this).val();
				$(li).hide();
				if($(li).text().toLowerCase().indexOf(txt) > -1){
					$(li).show();
				}
			});
		});
	}