<!DOCTYPE html>
<html lang="en">
<head>
<title>.:: SMVS | SERCOING ::.</title>
<link rel="shortcut icon" type="image/x-icon" href="https://sercoing.cl/firma/serco.ico" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
 
	<script src="//code.jquery.com/jquery-1.12.4.js"></script>
	
	<!--FUENTES | FONT-->
	<!--TEXTOS--><link href="https://fonts.googleapis.com/css?family=Comfortaa|Montserrat|Roboto:300,400" rel="stylesheet">
	<!--ICONOS--><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
	<!--FUENTES | FONT-->

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
 
 	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   
	<script src="js/menu-desp.js"></script>
 	<script src="js/searchs.js"></script>
   	<script src="js/parsePoly.js"></script>
   	<script src="js/gps.js"></script>
   	<script src="js/markerclusterer.js"></script>
 	<link href="css/style.css" rel="stylesheet" media="screen">
    <link href="css/overflow-table.css" rel="stylesheet" media="screen">
        
	<script src="./js/jquery-ui-timepicker-addon.js"></script>
	<link href="./css/jquery-ui-timepicker-addon.css" rel="stylesheet" media="screen">
    
    <link rel="stylesheet" href="./css/accordion.css">
    <style type="text/css">
		.ui-widget-overlay{
			z-index:1000;
		}
	</style> 
    <script>
		//$(document).ready(function() {
		var progressTimer, progressbar, dialog, closeDownload, popup;
	  
		$(function() {
			google.maps.event.addDomListener(map, 'zoom_changed', function() {
				zoom = map.getZoom();
				//alert(zoom);
			});

			dialog = $( "#dialog_loading" ).dialog({
				autoOpen: false,
				closeOnEscape: false,
				resizable: false,
				modal: true,
				open: function() {
					progressTimer = setTimeout( progress, 500 );
					$(".ui-widget-overlay").css({
						opacity: 0.7,
						filter: "Alpha(Opacity=70)",
						backgroundColor: "white"
					});
				}
			});
			
			popup = $('<div id="popup"></div>').appendTo('body');
			popup.dialog({
					position:{  my: "left top", at: "right bottom", of: window },
					autoOpen: false,
					resizable: true,
					fluid: true,
					modal: false,
					title: 'Datos Recorrido',
					width: $(window).width() < 400 ? 300 : 'auto',
					height:350
			});
		 
			progressbar = $( "#progressbar" ),
	    	progressbar.progressbar({
				value: false,
				complete: function() {
					closeDownload();
				}
		});

		function progress() {
			var val = progressbar.progressbar( "value" ) || 0;
			if ( val > 95 ) {
				 val = 0;
			}
			progressbar.progressbar( "value", val + Math.floor( Math.random() * 3 ) );
			progressTimer = setTimeout( progress, 60 );
		}

		closeDownload = function closeDownload() {
			clearTimeout( progressTimer );
			dialog.dialog( "close" );
			progressbar.progressbar( "value", false );
		}

		$( "#dialog" ).dialog({
			autoOpen: false,
			show: {
				effect: "blind",
				duration: 200
		  	},
		  	hide: {
				effect: "explode",
				duration: 200
		  	},
			modal: true,
			open: function() {
				$(".ui-widget-overlay").css({
					opacity: 0.7,
					filter: "Alpha(Opacity=70)",
					backgroundColor: "white"
				});
			}
		});

	   	$( "#datepicker" ).datetimepicker({
			dateFormat: "dd-mm-yy",
			timeFormat: "HH:mm",
			currentText: "Nueva",
			closeText: "Buscar",
			timeText: "Horario",
			hourText: "Hora",
			minuteText: "Minutos"			
		});

		searchp2();
		searchg();
		searchtp();

		//$("ul li div input[type*='checkbox']").prop('checked', false);

		$("form#recorrido").on( "click", "input#maps_rec_clean", function(){
			if(recorrido.length > 0){
				recorrido.forEach(function(elemento,i) {
					elemento["line"].setMap(null);
				});
				recorrido.splice(0,recorrido.length);
				popup.dialog('close');
			}
		});
		
		$("div#opcion1 input#group[type=checkbox]" ).prop("checked",true);		
		$("div#opcion1" ).on( "click", "input#group[type=checkbox]", function(){
			
			//mkcluster.getMarkers()
			mkcluster.clearMarkers();
			if(this.checked){
				$.each(Object.keys(markers), function(key, value){
					if(markers[value].getMap()!= null){
						markers[value].setMap(null);
						mkcluster.addMarker(markers[value]);
					}
				})
			}else{
				$.each(Object.keys(markers), function(key, value){
					markers[value].setMap(map);
				})
			}
		});
		
		$("form#patent div#not-these" ).on( "click", "label", function(){
			id_patente = $(this).attr("id");
			myposition = markers[id_patente].getPosition();
			map.panTo(myposition);	
		});

		$("form#geocercas ul#not-these" ).on( "click", "label", function(){
			id_geoc = $(this).attr("id");
			myposition = geoc[id_geoc]["center"];
			map.panTo(myposition);
		});

		
		$("form#recorrido" ).on( "change", "select#sel_reg", function(){
			var id_pat = $(this).val();
			myRec(id_pat);
		});

		$("form#recorrido" ).on( "dblclick", "select#sel_reg", function(){
			myRec();	
		});
		
		$("form#recorrido" ).on( "click", "select#sel_reg", function(){
			var pat_temp = $(this).val();
			console.log(pat_rec +"=="+pat_temp);
			if(recorrido.length > 0 && pat_rec == pat_temp ){
				popup.dialog('open');
			}	
		});
				
		$("div#popup").on( "click", "#constrainer table tbody tr", function(){
			id_rec = $(this).attr('id')
			zoom = recorrido[id_rec]["zoom"];
			pos = recorrido[id_rec]["pos"];
			rec = recorrido[id_rec]["line"];
			map.panTo(pos);
			map.setZoom(zoom);
			/*animateCircle(rec);	
			var icons = [{
				icon: {
				path: 'M 0,-1 0,1', strokeOpacity: 1, scale: 4},
				offset: '0%',
				repeat: '20px'
			}]  
			rec.set('icons', icons);*/
		});
	
		form_select("routes");

		$("form#geocercas" ).on( "click", "input[type=checkbox]", function(){
			var id_ul = $(this).parents("ul").attr("id");	
			if ( id_ul == 'something' )
			{
				$(this).parent().attr("class","hidden");
				var id = "form#geocercas input[id="+ $(this).attr("name") +"_gch]";
				$(id).prop('checked',true);
				$(id).parent().attr("class","show");
				myGeocerca();
			}else if(id_ul == 'not-these'){
				$(this).parent().attr("class","hidden");
				var id_gc = $(this).attr("id").replace("_gch", "");
				var id = "form#geocercas input[id="+ id_gc +"_gc]"
				$(id).parent().attr("class","show");
				$(id).prop('checked',false);
				geoc[id_gc]['polygon'].setMap(null);
			}
		});
		
		form_select("patent");
		
		$("form#patentxxxx" ).on( "click", "input[type=checkbox]", function(){				
			var id_ul = $(this).parents("ul").attr("id");	
			if ( id_ul == 'something' )
			{
				$(this).parent().attr("class","hidden");
				var id = "form#patent input[id="+ $(this).attr("name") +"_id]";
				$(id).prop('checked',true);
				$(id).parent().attr("class","show");

			}else if(id_ul == 'not-these'){
				$(this).parent().attr("class","hidden");
				var patente = $(this).attr("id").replace("_id", "");
				var id = "form#patent input[id="+ patente +"]";
				$(id).parent().attr("class","show");
				$(id).prop('checked',false);
				markers[patente].setMap(null);
				markers[patente] = null;
			}	
		});
		});
	
	function form_select(pref){
		var form = "form#" + pref;
		$(form).on( "click", "input[type=checkbox]", function(){
			var id_ul = $(this).parents("div").attr("id"); //	console.log(id_ul) modificado UL
			if ( id_ul == 'something' )
			{
				$(this).parent().attr("class","hidden");
				var id = form + " input[id=" + $(this).attr("name") + "]";
				$(id).prop('checked',true);
				$(id).parent().attr("class","show");
				switch(pref) {
					case "routes":
						route( $(this).attr("name") );
						break;
					case "patent":
						id = $(this).parent().parent().attr("id");
					    $( "form#patent div#not-these" ).accordion({
							active: (id*1)
  						});
						myGps( $(this).attr("name") );
						break;				
				} 
			}else if(id_ul == 'not-these'){
				$(this).parent().attr("class","hidden");
				var id_input = $(this).attr("id"); //.replace("", "");
				var id = form + " input[id="+ id_input + "_" + pref + "]"
				$(id).parent().attr("class","show");
				$(id).prop('checked',false);
				
				switch(pref) {
					case "routes":
						routes[id_input].forEach(function(elemento,i) {
							elemento.setMap(null);
						});
						//routes.splice(0,routes.length);
						delete routes[id_input];
						break;
					case "patent":
						if( $("div#opcion1 input#group[type=checkbox]").is(':checked') ){
							//alert("entro");
							mkcluster.removeMarker(markers[id_input]);
							//markers[id_input] = null;
						}else{
							markers[id_input].setMap(null);
						}
						delete markers[id_input];	
						break;				
				}
			}
		});
	}
	</script>

</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid navbar">
    <!--<div class="col-md-9 col-sm-7"> <a class="navbar-brand" href="#">SMVS - SERCOING LTDA.</a> </div>--> <!--RESPALDO-->
    <div class="col-md-9 col-sm-7"><img src="http://sercoing.cl/firma/SercoingLogo_100x150.png" height="65px" width="auto" alt="SMVS"> <label for="SMVS - SERCOING LTDA." class="title-site">&nbsp;SMVS - SERCOING LTDA.</label></div>
    <!--<div class="col-md-5 col-sm-4"> <a class="navbar-brand" href="#">SMVS - SERCOING LTDA.</a> </div>-->
    <div class="col-md-3 col-sm-5" id="myNavbar"> 
      <!-- navbar-header  // collapse navbar-collapse
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Projects</a></li>
        <li><a href="#">Contact</a></li>
      </ul>-->
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span><i class="fas fa-user-shield"></i></span>&nbsp;CERRAR SESION</a></li>
        <li><a href="#"><span><i class="far fa-question-circle"></i></i></span>&nbsp;INFO</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid content">
  <div class="row">
	<div class="container-fluid contenedor--div">
        <div class="icono--div">
            <div class="icono">
            	<a href="#">            		
            		<span class="raya"></span>
            		<span class="raya"></span>
            		<span class="raya"></span>
            	</a>
            </div>
        </div>

        <div class="contenedor--menu" style="visibility:hidden">
        <div id="accordion" class="sidenav">


		<!--====================== ACCORDION - PATENTES | INICIO ======================-->	
          <h3>&nbsp;<span><i class="fas fa-car"></i></span>&nbsp;&nbsp;MOVILES</h3>  
          <div id="opcion1" class="opciones text-left">
          	<div class="#"><a href="#" class="btnagrupar"><input type="checkbox" name="group" id="group" checked /><label id="agrupar" for="group">&nbsp;AGRUPAR</label></a></div>  
            <div><input name="d" class="fontAwesome" placeholder="Buscar patente&nbsp;&#xF002; " id="search_p" type="text" style="font-family:Arial, FontAwesome; font-size: 1em; font-style: italic;" ></div>
            <form id="patent" class="list" action="#" method="post">
     <?php
        include("./include/db.php");
        $sql = 'SELECT
                max(vehi_id) as id,
                vehi_patente as patente,
				vehi_tive_id
                FROM vehiculo
				WHERE LEFT(vehi_patente, 1) != "@"
                GROUP BY patente, vehi_tive_id
                ORDER BY 3 ASC, 2 ASC';

        $li_nes = null; 
        $li_tas = null; 		
        $li_nes2 = null;
		$li_tas2 = null;
		$op_sel = null;
		
        if ($resultado = $mysqli->query($sql)) {
            while ($fila = $resultado->fetch_object()) {
				$op_sel .= '<option value="' . $fila->id . '">' . $fila->patente . "</option>\n";
				if( $fila->vehi_tive_id == 1 ) {		
					$li_nes .= '    <li>
					<input type="checkbox" name="' . $fila->id . '" id="' . $fila->id . '_patent" />
					<label for="' . $fila->id . '_patent">' . $fila->patente . '</label>
					</li>';
					$li_nes2 .= '		<li>
					<input type="checkbox" id="' . $fila->id . '" />
					<label id="' . $fila->id . '">' . $fila->patente . '</label>
					</li>';
				}else{
					$li_tas .= '    <li>
					<input type="checkbox" name="' . $fila->id . '" id="' . $fila->id . '_patent" />
					<label for="' . $fila->id . '_patent">' . $fila->patente . '</label>
					</li>';		
					$li_tas2 .= '		<li>
					<input type="checkbox" id="' . $fila->id . '" />
					<label id="' . $fila->id . '">' . $fila->patente . '</label>
					</li>';
				}
            }
            $resultado->close();
        }
        $mysqli->close();
    ?>
    <div id="something" class="accordion">
        <h3>CAMIONETAS</h3>
        <ul id="0">
            <?php echo $li_tas; ?>
        </ul>
        <h3>CAMIONES</h3>
        <ul id="1">
			<?php echo $li_nes; ?>
   		</ul>
    </div> 
    <div id="panel">
        <div class="acction"><p>seleccionado</p></div>
        <div id="not-these" class="accordion">
            <h3>CAMIONETAS</h3>
            <ul>
                <?php echo $li_tas2; ?>
            </ul>
            <h3>CAMIONES</h3>
            <ul>
                <?php echo $li_nes2; ?>
            </ul>
        </div> 
    </div>
</form>
          </div>
          <!--====================== ACCORDION - PATENTES | FIN ======================-->

          <!--====================== ACCORDION - GEOCERCAS | INICIO ======================-->
          <h3>&nbsp;<span><i class="fas fa-map-marker-alt"></i></span>&nbsp;&nbsp;GEOCERCAS</h3>   
          <div id="opcion2" class="opciones text-left">
            <div><input name="g" id="search_g" class="fontAwesome" placeholder="Buscar geocerca&nbsp;&#xF002;" type="text" style="font-family:Arial, FontAwesome; font-size: 1em; font-style: italic;" ></div>
            <form id="geocercas" class="list" action="#" method="post">
    <?php
        include("./include/db.php");
            $sql = "SELECT
                GE.geoc_id AS id,
                GE.geoc_nombre,
                GE.geoc_codigo,
                GE.geoc_empr_id,
                EM.empr_nombre
                FROM `geocerca` AS GE
	                LEFT JOIN empresa AS EM ON GE.geoc_empr_id = EM.empr_id 
					WHERE geoc_tipo IN(1,2,3,4,5,6,7)
					ORDER BY 4 DESC, 3 ASC";
        $li1 = null; 
        $li2 = null;
        $temp = null;
        if ($resultado = $mysqli->query($sql)) {
            while ($fila = $resultado->fetch_object()) {
                if($temp == null || $temp != $fila->geoc_empr_id ){
                $li1 .= '    <li id="title"><h5><strong>'.$fila->empr_nombre.'</strong></h5></li>';
                $temp = $fila->geoc_empr_id;
                }
                $li1 .= '    <li>
            <div class="show">
                <input type="checkbox" name="'.$fila->id.'" id="'.$fila->id.'_gc" />
                <label for="'.$fila->id.'_gc" title="'.utf8_encode($fila->geoc_nombre).'">'.utf8_encode($fila->geoc_codigo).'</label>
            </div>
        </li>';
                $li2 .= '    <li>
            <div class="hidden">
                <input type="checkbox" id="'.$fila->id.'_gch" />
                <label id="'.$fila->id.'" title="'.utf8_encode($fila->geoc_nombre).'">'.utf8_encode($fila->geoc_codigo).'</label>
            </div>
        </li>';
            }
            $resultado->close();
        }
        $mysqli->close();
    ?>
              <ul id="something">
                <?php echo $li1; ?>
              </ul>
              <ul id="not-these">
                <?php echo $li2; ?>
              </ul>
            </form>
          </div>
          <!--====================== ACCORDION - GEOCERCAS | FIN ======================-->

          <!--====================== ACCORDION - RUTAS | INICIO ======================-->
          <h3>&nbsp;<i class="fas fa-road"></i></span>&nbsp;&nbsp;RUTAS</h3>
          <div id="opcion3" class="text-left">
            <div><input name="g" id="search_g" class="fontAwesome" placeholder="Buscar ruta&nbsp;&#xF002;" type="text" style="font-family:Arial, FontAwesome; font-size: 1em; font-style: italic;" ></div>
            <form id="routes" class="list" action="#" method="post">
    <?php
        include("./include/db.php");	
						
			$sql = 'SELECT
					GE.geoc_codigo
					FROM `geocerca` AS GE
						WHERE geoc_tipo IN(8,9)
						GROUP BY geoc_codigo
						ORDER BY 1 ASC';
        $li1 = null; 
        $li2 = null;
        if ($resultado = $mysqli->query($sql)) {
            while ($fila = $resultado->fetch_object()) {
                $li1 .= '    <li>
            <div class="show">
                <input type="checkbox" name="' . $fila->geoc_codigo . '" id="' . $fila->geoc_codigo . '_routes" />
                <label for="' . $fila->geoc_codigo . '_routes">' . $fila->geoc_codigo . '</label>
            </div>
        </li>';
                $li2 .= '    <li>
            <div class="hidden">
                <input type="checkbox" id="' . $fila->geoc_codigo . '" />
                <label id="' . $fila->geoc_codigo . '" >' . $fila->geoc_codigo . '</label>
            </div>
        </li>';
            }
            $resultado->close();
        }
        $mysqli->close();
    ?>
              <ul id="something">
                <?php echo $li1; ?>
              </ul>
              <ul id="not-these">
                <?php echo $li2; ?>
              </ul>
            </form>
          </div>
          <!--====================== ACCORDION - RUTAS | FIN ======================-->

          <!--====================== ACCORDION - RECORRIDO | INICIO ======================-->
          <h3>&nbsp;<span><i class="fas fa-route"></i></span>&nbsp;&nbsp;RECORRIDOS</h3>
          <div id="opcion4" class="text-left">
 	          <form id="recorrido" class="list" action="#" method="post">
 	            <label for="rec_hours"><b>Tiempo de seguimiento</b></label>
                <div>      
                    <select name="rec_hours" id="rec_hours">
                        <option value="1">1 hora</option>
                        <option value="2">2 horas</option>
                        <option value="3">3 horas</option>
                        <option value="4">4 horas</option>
                        <option value="6">6 horas</option>
                        <option value="8">8 horas</option>
                        <option value="12">12 horas</option>
                        <option value="24">24 horas</option>
                        <option value="48">48 horas</option>
                    </select>
                </div>
                <label for="datepicker"><b>Fecha termino</b></label>
                <div><input type="text" id="datepicker" class="fontAwesome" name="date_end" placeholder="&#xF073;&nbsp;Seleccione fecha" style="font-family:Arial, FontAwesome; font-size: 1em; " ></div>
                <label for="sel_reg"><b>Veh&iacute;culo</b></label>
                <div>
					<div><input name="tp" id="search_tp" class="fontAwesome" placeholder="&#xf1b9;&nbsp;Seleccione vehiculo" type="text" style="font-family:Arial, FontAwesome; font-size: 1em; "></div>
                    <select name="sel_reg" id="sel_reg" size="12" style="min-height:100px">
                        <?php echo $op_sel; ?>
                    </select>
                </div>
                <div><input type="button" id="maps_rec_clean" value="Limpiar"></div>
			</form>
          </div>
          <!--====================== ACCORDION - RECORRIDO | FIN ======================-->

          <!--====================== ACCORDION - ALERTAS | INICIO ======================-->         
          <h3>&nbsp;<span><i class="fas fa-exclamation-triangle"></i></span>&nbsp;&nbsp;ALERTAS</h3>
          <div id="opcion5" class="text-left">
       		<ul id="alert_geoc1" title="NUEVA"></ul>
       		<ul id="alert_geoc2" title="ANTERIOR"></ul>
          </div>
          <!--====================== ACCORDION - ALERTAS | FIN ======================-->

        </div>
        </div>
    </div>
<!--====================== LOAD MAP | INICIO ======================-->  
    <div id="container-map" class="col-sm-12">
      <div id="map-canvas">&nbsp;</div>
    </div>
<!--====================== LOAD MAP | FIN ======================-->  
  </div>
</div>
<!--====================== POP-UP CARGA ALERTAS | INICIO ======================-->
<div id="dialog" title="Mensaje">
  <p>SE HAN CARGADO ALETAS</p>
</div>
<!--====================== POP-UP CARGA ALERTAS | FIN ======================-->

<!--====================== POP-UP "CARGANDO RECORRIDO" | INICIO ======================-->
<div id="dialog_loading" title="Cargando Recorrido">
  <div id="progressbar"></div>
</div>
<!--====================== POP-UP "CARGANDO RECORRIDO" | FIN ======================-->

<!--====================== LOAD ACCORDION | INICIO ======================-->
<script>
	$( "#accordion" ).accordion({
		heightStyle: "fill",
		collapsible: true
	});
    $( "form#patent div#something" ).accordion({
   	//	collapsible: true,
		heightStyle: "content"	
    });
    $( "form#patent div#not-these" ).accordion({
		active: false,
   		collapsible: true,
		heightStyle: "content"		
    });	
</script>
<!--====================== LOAD ACCORDION | FIN ======================-->

<!--====================== CARGA MAPA GOOGLE | INICIO ======================-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPSWiYwgsb-AHamk_0G6oAcTANacBVQzc&callback=myMap"></script>
<!--====================== CARGA MAPA GOOGLE | FIN ======================-->


<!--FOOTER - INICIO-->
<div id="footerDIv">
	<label for="">APLICACION DESARROLLADA POR DEPTO TI SERCOING LTDA. - SMVS&copy; 2019</label>
</div>

<!--FOOTER - FIN-->
</body>
</html>
